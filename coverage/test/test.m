function test()

    clear
    clear -global
    loadPositions();
    
    execute(1, 2.51327412e+03);   
    execute(2, 9.28246813e+03);   
    execute(3, 9.41740651e+03);    
    execute(6, 1.147034037e+04);    
    execute(7, 2.506623380e+04);    
    execute(8, 3.354231854e+04);    
    execute(9, 3.649378792e+04);    
    execute(10, 5.474864871e+04);  
    
    disp('>> Todos os testes passaram!');

end

function execute(i, evaluatedArea)

    global positions
    
    R = 20;
    
    position = positions.get(i);
    
    robots = [position repmat(R, length(position), 1)];    
    area = calcArea(robots);
    
    assert(isEqualTo(area, evaluatedArea), sprintf('Esperada �rea de %f mas foi encontrado %f', evaluatedArea, area));
    
end