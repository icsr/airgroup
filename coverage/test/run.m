function run(qtyOfRobots, isProfiling)
       
    clear -global subplotDim exit freeze
    
    clc
    close all
    clear -global
    
    global exit freeze
    
    averageSpeed = 10; %m/s
    fieldLength = 1.5 * qtyOfRobots;
    qtyOfScenes = 100;       
    radius = 20;
    
    if(nargin == 1)
        isProfiling = false;
    end
    
    exit = false;
    freeze = false;    
    
%     load(strcat('database/',num2str(scenario),'/position.mat'))

    robots = [fieldLength * rand(qtyOfRobots, 2) repmat(radius, qtyOfRobots, 1)];
    
    field = getField(robots);
    field([1,3]) = field([1,3]) - fieldLength;
    field([2,4]) = field([2,4]) + fieldLength;
    
    meanTime = animation(robots, averageSpeed, field, qtyOfScenes, isProfiling);
    
    disp(sprintf('Tempo m�dio: %0.2f ms', 1000*meanTime));
end


function meanTime = animation(robotsXYR, averageSpeed, field, qtyOfScenes, isProfiling)

    clear -global exit freeze scenes
    
    global freeze scenes keep exit

    [m, ~] = size(robotsXYR);
    
    meanTime = 0;
    
    h = figure('units','normalized','outerposition',[0 0 1 1]);
    set(h,'KeyPressFcn',@callbackFcn);
    
    scenes = java.util.HashMap;
    scenes.put(0, robotsXYR);
    
    keep = true;
    
    if(isProfiling)
        profile on
    end
    for i = 1 : qtyOfScenes
                
%         disp(['>> Passo ' num2str(i) ':'])
        tic;
        area = calcArea(robotsXYR);
        time = toc;
        
        meanTime = meanTime + time;
        
        clf
        plotAnimation(i, area, time, m, field);
        drawnow
            
        
        while(freeze)
            pause(1);
        end
        
        if(exit)
            break;
        end
        
        angles = 2 * pi * rand(m,1);
        
        robotsXYR(:,1) = robotsXYR(:,1) + averageSpeed * cos(angles);
        robotsXYR(:,2) = robotsXYR(:,2) + averageSpeed * sin(angles);
        
        scenes.put(i, robotsXYR);        
    end
    meanTime = meanTime / i;
    keep = false;
    
    if(isProfiling)
        profile viewer
    end
    
end

function callbackFcn(h,evt)
    global freeze exit
    
    if(strcmp(evt.Key, 's'))
        disp('Dados salvos em animation.mat')
        save('./saves/animation.mat', 'scenes');

        return;
    end
    
    if(strcmp(evt.Key, 'escape'))        
        freeze = false;
        close all;
        clc;        
        exit = true;
        return;
    end   
        
    if(freeze)
        freeze = false;
        else
            freeze = true;
    end
end