function main()

    clc
    close all
    clear -global
    clear
    global R keep

    init();
    loadPositions();

    R = 20;
    keep = false;
       
    execute(1, 2.513274122871835e+03, 2.51327412e+03);   
    execute(2, 9.276825492255421e+03, 9.28246813e+03);   
    execute(3, 9.418524259784359e+03, 9.41740651e+03);    
    execute(6, 1.146333309709628e+04, 1.147034037e+04);    
    execute(7, 2.506414078366689e+04, 2.506623380e+04);    
    execute(8, 3.353054578380456e+04, 3.354231854e+04);    
    execute(9, 3.648686788230640e+04, 3.649378792e+04);    
    execute(10, 5.473333035e+04, 5.474864871e+04);    

end

function execute(i, actualArea, evaluatedArea)

    global R positions
    
    position = positions.get(i);
    
    myTitle = sprintf('>>> Scenario %d', i);

    disp(myTitle)
    
    robots = [position repmat(R, length(position), 1)];
    tic;
    area = calcArea(robots);
    timeInSeconds = toc;
    plotScenario([area, timeInSeconds]);
    
    if(nargin == 1)
        actualArea = calcActualAreaSlowlyByPoints(sprintf('Scenario%d', i), area);        
    end
    
    if(nargin == 3)       
        assert(isEqualTo(area, evaluatedArea), sprintf('�rea encontrada foi %.2f mas deveria ser %.2f.', area, evaluatedArea));       
    end
    
    error = (area - actualArea);
    [m, ~] = size(robots);
    
    disp(sprintf('%d robots in %.5f ms\nEval. Area: %.5f\nActual Area: %.5f\nError: %.5f\n', m, 1000*timeInSeconds, area, actualArea, error));
    
end

function animation()
    load('animation04.mat');

    for(i = scenes.size() - 2 : scenes.size() - 2)

        robots = scenes.get(i);

        tic
        area = calcArea(robots)
        timeInSeconds = toc
        plotScenario([area, timeInSeconds])

    end

end