package br.ita.airgroup.icsr.multirobotnetworks;

import static org.junit.Assert.assertEquals;

import java.awt.Toolkit;

import org.junit.Before;
import org.junit.Test;

import br.ita.airgroup.icsr.multirobotnetworks.environment.Coordinate;
import br.ita.airgroup.icsr.multirobotnetworks.environment.Robot;
import br.ita.airgroup.icsr.multirobotnetworks.environment.Scenario;
import br.ita.airgroup.icsr.multirobotnetworks.gui.ScenarioWindow;

public class CoverageTest {
	
	private static final double PRECISION = Math.pow(10, -5);
	private ScenarioWindow window;
	private Scenario disjointScenario;

	@Before
	public void prepare(){	
		disjointScenario = new Scenario(new Coordinate(0,0), Toolkit.getDefaultToolkit().getScreenSize(), "Teste");
		disjointScenario.addRobot(new Robot(120, 320, 100), new Robot(175, 120, 100), new Robot(450,320,100));
	}

	@Test
	public void byDotsAndDisjointTest() {
		
		Coverage coverage = new DotsCoverage(disjointScenario);
		assertEquals(calculateDisjointCoverageArea(disjointScenario), coverage.calculate(), PRECISION);
		
	}
	
	
	@Test
	public void byGeometryAndDisjointTest(){
		
		Coverage dotsCoverage = new DotsCoverage(disjointScenario);
		Coverage geomCoverage = new DotsCoverage(disjointScenario);
		
		assertEquals(dotsCoverage.calculate(), geomCoverage.calculate(), PRECISION);
		
	}

	private double calculateDisjointCoverageArea(Scenario scenario) {
	
		double ret = 0;
		for(Robot r : scenario.getNetwork()){
			ret += Math.PI * Math.pow(r.getRange(), 2);
		}
		
		return ret / (scenario.getDimension().getWidth() * scenario.getDimension().getHeight());
		
		
	}

}
