cd('1,88m');
load('data.mat');
dados1 = dados;

allData = dados;

cd('../2,30m');
load('data.mat');

allData = [allData; dados];

cd ..

mkdir('all');
cd('all');

dados2 = dados;
clear dados;
dados = allData;
clear allData;
save('data.mat');

cd ..
plotThis('all');

cd('all');
figure

y=dados1(:,15);
y=[y; dados1(:,16)];
y=[y; dados1(:,17)];
y=[y; dados1(:,18)];
y=[y; dados1(:,19)];
y=[y; dados1(:,20)];
y=[y; dados1(:,21)];
y=[y; dados1(:,22)];
y=[y; dados1(:,23)];
y=[y; dados1(:,24)];
y = 100*y;
x=repmat(dados1(:,1),10,1);
subplot(2,1,1);
histfit(y,10,'normal');
grid minor
xlabel('Erro Relativo (%)')
ylabel('Quantidade');
title('Distribuição Normal dos Erros Relativos para h=1,88 m');
xlim([-20,15]);
ylim([0,60]);

subplot(2,1,2);
y=dados2(:,15);
y=[y; dados2(:,16)];
y=[y; dados2(:,17)];
y=[y; dados2(:,18)];
y=[y; dados2(:,19)];
y=[y; dados2(:,20)];
y=[y; dados2(:,21)];
y=[y; dados2(:,22)];
y=[y; dados2(:,23)];
y=[y; dados2(:,24)];
y = 100*y;
x=repmat(dados2(:,1),10,1);
histfit(y,10,'normal');
grid minor
xlabel('Erro Relativo (%)')
ylabel('Quantidade');
title('Distribuição Normal dos Erros Relativos para h=2,30 m');
xlim([-20,15]);
ylim([0,60]);
saveas(gcf, 'comparative_graph_normal.png', 'png');

clear dados1 dados2;

cd ..