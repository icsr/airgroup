function plotThis(folder)

prevFolder = pwd;
cd(folder);

load('data.mat');

close all
figure

[lines,cols] = size(dados);
samples = (cols-4)/4;

m = [1 : lines * 2 * samples]';

y=dados(:,15);
y=[y; dados(:,16)];
y=[y; dados(:,17)];
y=[y; dados(:,18)];
y=[y; dados(:,19)];
y=[y; dados(:,20)];
y=[y; dados(:,21)];
y=[y; dados(:,22)];
y=[y; dados(:,23)];
y=[y; dados(:,24)];

y = 100*y;
x=repmat(dados(:,1),10,1);

qtyOfMeas=2*5*size(dados,1);

%plot(m,y, 'o');
%grid minor

%figure
boxplot(y, x);
xlabel('Cena');
ylabel('Erro Relativo (%)')
grid minor
title(sprintf('Erro relativo em %d medições', qtyOfMeas));
saveas(gcf, 'graph_boxplot.png', 'png');

figure
histfit(y,10,'normal');
grid minor
xlabel('Erro Relativo (%)')
ylabel('Quantidade');
title(sprintf('Distribuição Normal dos Erros em %d medições', qtyOfMeas));
saveas(gcf, 'graph_normal.png', 'png');

cd(prevFolder);

end