/*
 * File:   topology.c
 * Author: israelicsr
 *
 * Created on 3 de Maio de 2018, 08:43
 */
#include "utility/utils.h"
#include "e_led.h"
#include <stdio.h>

void runTopology(){
    
    sendCommand(GET_POSTURE, NULL);
    getRobotPosture();
    
}