/*
 * File:   topology.c
 * Author: israelicsr
 *
 * Created on 3 de Maio de 2018, 08:43
 */
#include <stdio.h>
#include "../utility/command.h"
#include "../utility/posture_sensor.h"

void runTopology(){
    
    retrievePostureFromExternalSensor();
    getRobotPosture();
    
}