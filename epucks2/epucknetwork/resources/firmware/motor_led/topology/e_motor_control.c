#include "../../motor_led/advance_one_timer/e_led.h"
#include "../../motor_led/e_motors.h"
#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "./e_motor_control.h"
#include "../../utility/utility.h"
#include "../../utility/posture_sensor.h"
#include "../../bluetooth/e_bluetooth_handler.h"

int request = 0;
int interrupted = 0;
int busy = 0;


void turnToAngle(int angle) {

    targetAngle = angle;

    int gama = targetAngle - currentAngle;

    int signal = 1;
    if (abs(gama) > 180) {
        if (gama > 0) {
            signal = -1;
        }
        gama = signal * (360 - abs(gama));
    }

    rotationDegree(gama);

    sleep(waitingTimeInMils);

}

void moveCm(int distanceInCm) // distance in cm
{
    float distanceInSteps = distanceInCm * N;
    int steps = (int) (distanceInSteps);

    int a = (int) (steps / MAX_STEPS);
    int b = steps % MAX_STEPS;
    int i = 0;
    for (i = 0; i < a; i++) {
        drive(NORMAL_SPEED, NORMAL_SPEED, MAX_STEPS);
    }

    if (b > 0) {
        drive(NORMAL_SPEED, NORMAL_SPEED, b);
    }

    sleep(waitingTimeInMils);

}

void makeRequest() {
    request = 1;
}

void leaveRequest() {
    request = 0;
}

int isInterrupted() {
    return interrupted;
}

void resetInterrupt() {
    interrupted = 0;
}

int isBusy() {
    return busy;
}

void resetBusy() {
    busy = 0;
}

void rotationDegree(int clockwiseAngle) { // angle in degrees:positive angle - clockwise; negative angle anti-clockwise

    if (abs(clockwiseAngle) < minAxialDislocation) {
        return;
    }

    if (clockwiseAngle > 180) { // get the complement angle for minimal rotation robot

        int complement = clockwiseAngle - 180;
        clockwiseAngle = -(180 - complement);
    }
    int steps, i;
    float tmp = 0;
    int neg = 0;
    if (clockwiseAngle < 0) {
        neg++;
        clockwiseAngle = -clockwiseAngle;
    }
    for (i = 0; i < clockwiseAngle; i++) {
        tmp += D;
    }
    steps = (int) tmp;
    if (neg == 1) {
        drive(-600, 600, steps);
    } else {
        drive(600, -600, steps);
    }

}

int leftSpeed, rightSpeed;
int totalSteps;
int currentStep;

void drive(int lspeed, int rspeed, int stepcount) {

    totalSteps = stepcount;

    int mode, l1, r1, l2 = 0, r2 = 0, lastcount = 0;
    float f, g;
    int lm = (lspeed < 0 ? -1 : 1);
    int rm = (rspeed < 0 ? -1 : 1);

    lspeed = abs(lspeed);
    rspeed = abs(rspeed);

    e_set_steps_left(0);
    e_set_steps_right(0);

    if (lspeed > rspeed) {
        mode = 0;
    } else {
        mode = 1;
    }

#ifdef ACCON
    l1 = 100;
    r1 = 100;
#else
    l1 = lspeed;
    r1 = rspeed;
#endif
    while (mode >= 0) {
        busy = 1;

        if (request) {
            interrupted = 1;
            break;
        }
        if (mode == 0) {
            currentStep = e_get_steps_left();
        } else {
            currentStep = e_get_steps_right();
        }
        currentStep = abs(currentStep);
        if (l1 != l2) {

            e_set_speed_left(l1 * lm);
            lastcount = currentStep;
            l2 = l1;

        }
        if (r1 != r2) {

            e_set_speed_right(r1 * rm);
            lastcount = currentStep;
            r2 = r1;

        }

        if (currentStep >= stepcount) {
            mode = -1;
        }
#ifdef ACCON
        if (currentStep - lastcount > 5) {
            f = 2 * (float) currentStep / stepcount;

            if (f > 1) f = 2 - f;

            g = 1 - f;
            f = 1 - (g * g * g * g * g * g * g * g);

            l1 = (int) (lspeed * f);
            r1 = (int) (rspeed * f);
        }
#endif
    }
    e_set_speed_left(0);
    e_set_speed_right(0);
    busy = 0;
}