/* 
 * File:   e_motor_control.h
 * Author: Edney
 *
 * Created on 31 de Julho de 2015, 19:02
 */

#define R 36.8 // e-puck�s radius
#define N 77.64 // motor steps to perform 1cm
#define D 3.67 // motor steps to perform 1� 

#define MAX_STEPS 10000

#define DIAMETER 41.68
#define CIRCUMFERENCE (DIAMETER * PI)
#define MOTOR_STEPS_PER_REVOLUTION 20
#define MOTOR_TO_WHEEL_GEAR_RATIO 50
#define STEPS_PER_REVOLUTION (MOTOR_STEPS_PER_REVOLUTION * MOTOR_TO_WHEEL_GEAR_RATIO)
#define STEP_PER_D (float)(STEPS_PER_REVOLUTION / CIRCUMFERENCE)
#define MM_TO_STEPS(mm) (int)(mm * STEP_PER_D)

#define WHEEL_SEPARATION 54.33
#define STEPS_FOR_FULL_CIRCLE (int)((WHEEL_SEPARATION / DIAMETER) * STEPS_PER_REVOLUTION)

#define NORMAL_SPEED 600
#define MAX_SPEED 800

#define ACCON

extern int totalSteps;
extern int currentStep;
extern float distanceToMove, angleToMove; 

void turnToAngle(int angle);

void rotationDegree(int clockwiseAngle);

/**
 * Move the robot in centimeter
 * @param distance: amount centimeters to move
 */
void moveCm(int distance); 
/**
 * Rotate the robot in degree
 * @param angle
 */
void rotationDegree(int angle); 

/**
 * 
 * @param lspeed
 * @param rspeed
 * @param stepcount
 */
void drive(int lspeed, int rspeed, int stepcount);

void makeRequest(void);
void leaveRequest(void);
int isInterrupted();
void resetInterrupt();
int isBusy();
void resetBusy();