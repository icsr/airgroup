#define MAX_NUMBER_OF_COMPLEXES 5

float* toComplex(float real, float image);

float normSquared(float *complex);

float norm(float *complex);

float* multiplyComplexes(float* complex1, float* complex2);

float* divideComplexes(float* complex1, float* complex2);