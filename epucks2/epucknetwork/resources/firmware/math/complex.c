#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "complex.h"

float complexes[MAX_NUMBER_OF_COMPLEXES][2];
int cIndex = 0;

float* toComplex(float real, float image){

    complexes[cIndex][0] = real;
    complexes[cIndex][1] = image;
    float *p = &(complexes[cIndex][0]);

    cIndex = (cIndex + 1) % MAX_NUMBER_OF_COMPLEXES;

    return p;
}

float normSquared(float *complex){
    return powl(complex[0], 2) + powl(complex[1], 2);
}

float norm(float *complex){
    return sqrtl(normSquared(complex));
}

float* multiplyComplexes(float* complex1, float* complex2){
   float real = complex1[0] * complex2[0] - complex1[1] * complex2[1];
   float imag = complex1[0] * complex2[1] + complex1[1] * complex2[0];
   return toComplex(real, imag);
}

float* divideComplexes(float* complex1, float* complex2){
    float real = (complex1[0] * complex2[0] + complex1[1] * complex2[1]) / normSquared(complex2);
    float imag = (complex1[1] * complex2[0] - complex1[0] * complex2[1]) / normSquared(complex2);
    return toComplex(real, imag);
}