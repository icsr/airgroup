#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=mkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/firmware.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/firmware.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=-mafrlcsj
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS
SUB_IMAGE_ADDRESS_COMMAND=--image-address $(SUB_IMAGE_ADDRESS)
else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=main.c uart/e_init_uart1.s uart/e_init_uart2.s uart/e_uart1_rx_char.s uart/e_uart1_tx_char.s uart/e_uart2_rx_char.s uart/e_uart2_tx_char.s utility/error.c topology/topology.c utility/collision.c motor_led/topology/e_motor_control.c bluetooth/e_bluetooth_handler.c utility/utility.c bluetooth/e_bluetooth.c motor_led/e_motors.c motor_led/e_init_port.c a_d/e_accelerometer.c a_d/e_ad_conv.c a_d/e_micro.c a_d/advance_ad_scan/e_prox.c motor_led/advance_one_timer/e_agenda.c motor_led/advance_one_timer/e_led.c utility/queue.c utility/stringutils.c a_d/advance_ad_scan/e_ad_conv.c utility/command.c utility/posture_sensor.c utility/arena.c algorithm/random_waypoints.c algorithm/algorithm.c math/eigen.c math/collections.c algorithm/connectivityMaintenance.c math/complex.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/main.o ${OBJECTDIR}/uart/e_init_uart1.o ${OBJECTDIR}/uart/e_init_uart2.o ${OBJECTDIR}/uart/e_uart1_rx_char.o ${OBJECTDIR}/uart/e_uart1_tx_char.o ${OBJECTDIR}/uart/e_uart2_rx_char.o ${OBJECTDIR}/uart/e_uart2_tx_char.o ${OBJECTDIR}/utility/error.o ${OBJECTDIR}/topology/topology.o ${OBJECTDIR}/utility/collision.o ${OBJECTDIR}/motor_led/topology/e_motor_control.o ${OBJECTDIR}/bluetooth/e_bluetooth_handler.o ${OBJECTDIR}/utility/utility.o ${OBJECTDIR}/bluetooth/e_bluetooth.o ${OBJECTDIR}/motor_led/e_motors.o ${OBJECTDIR}/motor_led/e_init_port.o ${OBJECTDIR}/a_d/e_accelerometer.o ${OBJECTDIR}/a_d/e_ad_conv.o ${OBJECTDIR}/a_d/e_micro.o ${OBJECTDIR}/a_d/advance_ad_scan/e_prox.o ${OBJECTDIR}/motor_led/advance_one_timer/e_agenda.o ${OBJECTDIR}/motor_led/advance_one_timer/e_led.o ${OBJECTDIR}/utility/queue.o ${OBJECTDIR}/utility/stringutils.o ${OBJECTDIR}/a_d/advance_ad_scan/e_ad_conv.o ${OBJECTDIR}/utility/command.o ${OBJECTDIR}/utility/posture_sensor.o ${OBJECTDIR}/utility/arena.o ${OBJECTDIR}/algorithm/random_waypoints.o ${OBJECTDIR}/algorithm/algorithm.o ${OBJECTDIR}/math/eigen.o ${OBJECTDIR}/math/collections.o ${OBJECTDIR}/algorithm/connectivityMaintenance.o ${OBJECTDIR}/math/complex.o
POSSIBLE_DEPFILES=${OBJECTDIR}/main.o.d ${OBJECTDIR}/uart/e_init_uart1.o.d ${OBJECTDIR}/uart/e_init_uart2.o.d ${OBJECTDIR}/uart/e_uart1_rx_char.o.d ${OBJECTDIR}/uart/e_uart1_tx_char.o.d ${OBJECTDIR}/uart/e_uart2_rx_char.o.d ${OBJECTDIR}/uart/e_uart2_tx_char.o.d ${OBJECTDIR}/utility/error.o.d ${OBJECTDIR}/topology/topology.o.d ${OBJECTDIR}/utility/collision.o.d ${OBJECTDIR}/motor_led/topology/e_motor_control.o.d ${OBJECTDIR}/bluetooth/e_bluetooth_handler.o.d ${OBJECTDIR}/utility/utility.o.d ${OBJECTDIR}/bluetooth/e_bluetooth.o.d ${OBJECTDIR}/motor_led/e_motors.o.d ${OBJECTDIR}/motor_led/e_init_port.o.d ${OBJECTDIR}/a_d/e_accelerometer.o.d ${OBJECTDIR}/a_d/e_ad_conv.o.d ${OBJECTDIR}/a_d/e_micro.o.d ${OBJECTDIR}/a_d/advance_ad_scan/e_prox.o.d ${OBJECTDIR}/motor_led/advance_one_timer/e_agenda.o.d ${OBJECTDIR}/motor_led/advance_one_timer/e_led.o.d ${OBJECTDIR}/utility/queue.o.d ${OBJECTDIR}/utility/stringutils.o.d ${OBJECTDIR}/a_d/advance_ad_scan/e_ad_conv.o.d ${OBJECTDIR}/utility/command.o.d ${OBJECTDIR}/utility/posture_sensor.o.d ${OBJECTDIR}/utility/arena.o.d ${OBJECTDIR}/algorithm/random_waypoints.o.d ${OBJECTDIR}/algorithm/algorithm.o.d ${OBJECTDIR}/math/eigen.o.d ${OBJECTDIR}/math/collections.o.d ${OBJECTDIR}/algorithm/connectivityMaintenance.o.d ${OBJECTDIR}/math/complex.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/main.o ${OBJECTDIR}/uart/e_init_uart1.o ${OBJECTDIR}/uart/e_init_uart2.o ${OBJECTDIR}/uart/e_uart1_rx_char.o ${OBJECTDIR}/uart/e_uart1_tx_char.o ${OBJECTDIR}/uart/e_uart2_rx_char.o ${OBJECTDIR}/uart/e_uart2_tx_char.o ${OBJECTDIR}/utility/error.o ${OBJECTDIR}/topology/topology.o ${OBJECTDIR}/utility/collision.o ${OBJECTDIR}/motor_led/topology/e_motor_control.o ${OBJECTDIR}/bluetooth/e_bluetooth_handler.o ${OBJECTDIR}/utility/utility.o ${OBJECTDIR}/bluetooth/e_bluetooth.o ${OBJECTDIR}/motor_led/e_motors.o ${OBJECTDIR}/motor_led/e_init_port.o ${OBJECTDIR}/a_d/e_accelerometer.o ${OBJECTDIR}/a_d/e_ad_conv.o ${OBJECTDIR}/a_d/e_micro.o ${OBJECTDIR}/a_d/advance_ad_scan/e_prox.o ${OBJECTDIR}/motor_led/advance_one_timer/e_agenda.o ${OBJECTDIR}/motor_led/advance_one_timer/e_led.o ${OBJECTDIR}/utility/queue.o ${OBJECTDIR}/utility/stringutils.o ${OBJECTDIR}/a_d/advance_ad_scan/e_ad_conv.o ${OBJECTDIR}/utility/command.o ${OBJECTDIR}/utility/posture_sensor.o ${OBJECTDIR}/utility/arena.o ${OBJECTDIR}/algorithm/random_waypoints.o ${OBJECTDIR}/algorithm/algorithm.o ${OBJECTDIR}/math/eigen.o ${OBJECTDIR}/math/collections.o ${OBJECTDIR}/algorithm/connectivityMaintenance.o ${OBJECTDIR}/math/complex.o

# Source Files
SOURCEFILES=main.c uart/e_init_uart1.s uart/e_init_uart2.s uart/e_uart1_rx_char.s uart/e_uart1_tx_char.s uart/e_uart2_rx_char.s uart/e_uart2_tx_char.s utility/error.c topology/topology.c utility/collision.c motor_led/topology/e_motor_control.c bluetooth/e_bluetooth_handler.c utility/utility.c bluetooth/e_bluetooth.c motor_led/e_motors.c motor_led/e_init_port.c a_d/e_accelerometer.c a_d/e_ad_conv.c a_d/e_micro.c a_d/advance_ad_scan/e_prox.c motor_led/advance_one_timer/e_agenda.c motor_led/advance_one_timer/e_led.c utility/queue.c utility/stringutils.c a_d/advance_ad_scan/e_ad_conv.c utility/command.c utility/posture_sensor.c utility/arena.c algorithm/random_waypoints.c algorithm/algorithm.c math/eigen.c math/collections.c algorithm/connectivityMaintenance.c math/complex.c


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/firmware.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=30F6014A
MP_LINKER_FILE_OPTION=,--script=p30F6014A.gld
# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/main.o: main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/main.o.d 
	@${RM} ${OBJECTDIR}/main.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  main.c  -o ${OBJECTDIR}/main.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/main.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/main.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/utility/error.o: utility/error.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/utility" 
	@${RM} ${OBJECTDIR}/utility/error.o.d 
	@${RM} ${OBJECTDIR}/utility/error.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  utility/error.c  -o ${OBJECTDIR}/utility/error.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/utility/error.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/utility/error.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/topology/topology.o: topology/topology.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/topology" 
	@${RM} ${OBJECTDIR}/topology/topology.o.d 
	@${RM} ${OBJECTDIR}/topology/topology.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  topology/topology.c  -o ${OBJECTDIR}/topology/topology.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/topology/topology.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/topology/topology.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/utility/collision.o: utility/collision.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/utility" 
	@${RM} ${OBJECTDIR}/utility/collision.o.d 
	@${RM} ${OBJECTDIR}/utility/collision.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  utility/collision.c  -o ${OBJECTDIR}/utility/collision.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/utility/collision.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/utility/collision.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/motor_led/topology/e_motor_control.o: motor_led/topology/e_motor_control.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/motor_led/topology" 
	@${RM} ${OBJECTDIR}/motor_led/topology/e_motor_control.o.d 
	@${RM} ${OBJECTDIR}/motor_led/topology/e_motor_control.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  motor_led/topology/e_motor_control.c  -o ${OBJECTDIR}/motor_led/topology/e_motor_control.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/motor_led/topology/e_motor_control.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/motor_led/topology/e_motor_control.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/bluetooth/e_bluetooth_handler.o: bluetooth/e_bluetooth_handler.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/bluetooth" 
	@${RM} ${OBJECTDIR}/bluetooth/e_bluetooth_handler.o.d 
	@${RM} ${OBJECTDIR}/bluetooth/e_bluetooth_handler.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  bluetooth/e_bluetooth_handler.c  -o ${OBJECTDIR}/bluetooth/e_bluetooth_handler.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/bluetooth/e_bluetooth_handler.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/bluetooth/e_bluetooth_handler.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/utility/utility.o: utility/utility.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/utility" 
	@${RM} ${OBJECTDIR}/utility/utility.o.d 
	@${RM} ${OBJECTDIR}/utility/utility.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  utility/utility.c  -o ${OBJECTDIR}/utility/utility.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/utility/utility.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/utility/utility.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/bluetooth/e_bluetooth.o: bluetooth/e_bluetooth.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/bluetooth" 
	@${RM} ${OBJECTDIR}/bluetooth/e_bluetooth.o.d 
	@${RM} ${OBJECTDIR}/bluetooth/e_bluetooth.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  bluetooth/e_bluetooth.c  -o ${OBJECTDIR}/bluetooth/e_bluetooth.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/bluetooth/e_bluetooth.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/bluetooth/e_bluetooth.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/motor_led/e_motors.o: motor_led/e_motors.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/motor_led" 
	@${RM} ${OBJECTDIR}/motor_led/e_motors.o.d 
	@${RM} ${OBJECTDIR}/motor_led/e_motors.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  motor_led/e_motors.c  -o ${OBJECTDIR}/motor_led/e_motors.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/motor_led/e_motors.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/motor_led/e_motors.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/motor_led/e_init_port.o: motor_led/e_init_port.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/motor_led" 
	@${RM} ${OBJECTDIR}/motor_led/e_init_port.o.d 
	@${RM} ${OBJECTDIR}/motor_led/e_init_port.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  motor_led/e_init_port.c  -o ${OBJECTDIR}/motor_led/e_init_port.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/motor_led/e_init_port.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/motor_led/e_init_port.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/a_d/e_accelerometer.o: a_d/e_accelerometer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/a_d" 
	@${RM} ${OBJECTDIR}/a_d/e_accelerometer.o.d 
	@${RM} ${OBJECTDIR}/a_d/e_accelerometer.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  a_d/e_accelerometer.c  -o ${OBJECTDIR}/a_d/e_accelerometer.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/a_d/e_accelerometer.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/a_d/e_accelerometer.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/a_d/e_ad_conv.o: a_d/e_ad_conv.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/a_d" 
	@${RM} ${OBJECTDIR}/a_d/e_ad_conv.o.d 
	@${RM} ${OBJECTDIR}/a_d/e_ad_conv.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  a_d/e_ad_conv.c  -o ${OBJECTDIR}/a_d/e_ad_conv.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/a_d/e_ad_conv.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/a_d/e_ad_conv.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/a_d/e_micro.o: a_d/e_micro.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/a_d" 
	@${RM} ${OBJECTDIR}/a_d/e_micro.o.d 
	@${RM} ${OBJECTDIR}/a_d/e_micro.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  a_d/e_micro.c  -o ${OBJECTDIR}/a_d/e_micro.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/a_d/e_micro.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/a_d/e_micro.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/a_d/advance_ad_scan/e_prox.o: a_d/advance_ad_scan/e_prox.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/a_d/advance_ad_scan" 
	@${RM} ${OBJECTDIR}/a_d/advance_ad_scan/e_prox.o.d 
	@${RM} ${OBJECTDIR}/a_d/advance_ad_scan/e_prox.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  a_d/advance_ad_scan/e_prox.c  -o ${OBJECTDIR}/a_d/advance_ad_scan/e_prox.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/a_d/advance_ad_scan/e_prox.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/a_d/advance_ad_scan/e_prox.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/motor_led/advance_one_timer/e_agenda.o: motor_led/advance_one_timer/e_agenda.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/motor_led/advance_one_timer" 
	@${RM} ${OBJECTDIR}/motor_led/advance_one_timer/e_agenda.o.d 
	@${RM} ${OBJECTDIR}/motor_led/advance_one_timer/e_agenda.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  motor_led/advance_one_timer/e_agenda.c  -o ${OBJECTDIR}/motor_led/advance_one_timer/e_agenda.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/motor_led/advance_one_timer/e_agenda.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/motor_led/advance_one_timer/e_agenda.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/motor_led/advance_one_timer/e_led.o: motor_led/advance_one_timer/e_led.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/motor_led/advance_one_timer" 
	@${RM} ${OBJECTDIR}/motor_led/advance_one_timer/e_led.o.d 
	@${RM} ${OBJECTDIR}/motor_led/advance_one_timer/e_led.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  motor_led/advance_one_timer/e_led.c  -o ${OBJECTDIR}/motor_led/advance_one_timer/e_led.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/motor_led/advance_one_timer/e_led.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/motor_led/advance_one_timer/e_led.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/utility/queue.o: utility/queue.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/utility" 
	@${RM} ${OBJECTDIR}/utility/queue.o.d 
	@${RM} ${OBJECTDIR}/utility/queue.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  utility/queue.c  -o ${OBJECTDIR}/utility/queue.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/utility/queue.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/utility/queue.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/utility/stringutils.o: utility/stringutils.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/utility" 
	@${RM} ${OBJECTDIR}/utility/stringutils.o.d 
	@${RM} ${OBJECTDIR}/utility/stringutils.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  utility/stringutils.c  -o ${OBJECTDIR}/utility/stringutils.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/utility/stringutils.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/utility/stringutils.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/a_d/advance_ad_scan/e_ad_conv.o: a_d/advance_ad_scan/e_ad_conv.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/a_d/advance_ad_scan" 
	@${RM} ${OBJECTDIR}/a_d/advance_ad_scan/e_ad_conv.o.d 
	@${RM} ${OBJECTDIR}/a_d/advance_ad_scan/e_ad_conv.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  a_d/advance_ad_scan/e_ad_conv.c  -o ${OBJECTDIR}/a_d/advance_ad_scan/e_ad_conv.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/a_d/advance_ad_scan/e_ad_conv.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/a_d/advance_ad_scan/e_ad_conv.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/utility/command.o: utility/command.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/utility" 
	@${RM} ${OBJECTDIR}/utility/command.o.d 
	@${RM} ${OBJECTDIR}/utility/command.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  utility/command.c  -o ${OBJECTDIR}/utility/command.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/utility/command.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/utility/command.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/utility/posture_sensor.o: utility/posture_sensor.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/utility" 
	@${RM} ${OBJECTDIR}/utility/posture_sensor.o.d 
	@${RM} ${OBJECTDIR}/utility/posture_sensor.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  utility/posture_sensor.c  -o ${OBJECTDIR}/utility/posture_sensor.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/utility/posture_sensor.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/utility/posture_sensor.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/utility/arena.o: utility/arena.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/utility" 
	@${RM} ${OBJECTDIR}/utility/arena.o.d 
	@${RM} ${OBJECTDIR}/utility/arena.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  utility/arena.c  -o ${OBJECTDIR}/utility/arena.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/utility/arena.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/utility/arena.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/algorithm/random_waypoints.o: algorithm/random_waypoints.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/algorithm" 
	@${RM} ${OBJECTDIR}/algorithm/random_waypoints.o.d 
	@${RM} ${OBJECTDIR}/algorithm/random_waypoints.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  algorithm/random_waypoints.c  -o ${OBJECTDIR}/algorithm/random_waypoints.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/algorithm/random_waypoints.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/algorithm/random_waypoints.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/algorithm/algorithm.o: algorithm/algorithm.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/algorithm" 
	@${RM} ${OBJECTDIR}/algorithm/algorithm.o.d 
	@${RM} ${OBJECTDIR}/algorithm/algorithm.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  algorithm/algorithm.c  -o ${OBJECTDIR}/algorithm/algorithm.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/algorithm/algorithm.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/algorithm/algorithm.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/math/eigen.o: math/eigen.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/math" 
	@${RM} ${OBJECTDIR}/math/eigen.o.d 
	@${RM} ${OBJECTDIR}/math/eigen.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  math/eigen.c  -o ${OBJECTDIR}/math/eigen.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/math/eigen.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/math/eigen.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/math/collections.o: math/collections.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/math" 
	@${RM} ${OBJECTDIR}/math/collections.o.d 
	@${RM} ${OBJECTDIR}/math/collections.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  math/collections.c  -o ${OBJECTDIR}/math/collections.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/math/collections.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/math/collections.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/algorithm/connectivityMaintenance.o: algorithm/connectivityMaintenance.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/algorithm" 
	@${RM} ${OBJECTDIR}/algorithm/connectivityMaintenance.o.d 
	@${RM} ${OBJECTDIR}/algorithm/connectivityMaintenance.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  algorithm/connectivityMaintenance.c  -o ${OBJECTDIR}/algorithm/connectivityMaintenance.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/algorithm/connectivityMaintenance.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/algorithm/connectivityMaintenance.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/math/complex.o: math/complex.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/math" 
	@${RM} ${OBJECTDIR}/math/complex.o.d 
	@${RM} ${OBJECTDIR}/math/complex.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  math/complex.c  -o ${OBJECTDIR}/math/complex.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/math/complex.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/math/complex.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
else
${OBJECTDIR}/main.o: main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/main.o.d 
	@${RM} ${OBJECTDIR}/main.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  main.c  -o ${OBJECTDIR}/main.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/main.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/main.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/utility/error.o: utility/error.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/utility" 
	@${RM} ${OBJECTDIR}/utility/error.o.d 
	@${RM} ${OBJECTDIR}/utility/error.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  utility/error.c  -o ${OBJECTDIR}/utility/error.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/utility/error.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/utility/error.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/topology/topology.o: topology/topology.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/topology" 
	@${RM} ${OBJECTDIR}/topology/topology.o.d 
	@${RM} ${OBJECTDIR}/topology/topology.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  topology/topology.c  -o ${OBJECTDIR}/topology/topology.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/topology/topology.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/topology/topology.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/utility/collision.o: utility/collision.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/utility" 
	@${RM} ${OBJECTDIR}/utility/collision.o.d 
	@${RM} ${OBJECTDIR}/utility/collision.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  utility/collision.c  -o ${OBJECTDIR}/utility/collision.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/utility/collision.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/utility/collision.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/motor_led/topology/e_motor_control.o: motor_led/topology/e_motor_control.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/motor_led/topology" 
	@${RM} ${OBJECTDIR}/motor_led/topology/e_motor_control.o.d 
	@${RM} ${OBJECTDIR}/motor_led/topology/e_motor_control.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  motor_led/topology/e_motor_control.c  -o ${OBJECTDIR}/motor_led/topology/e_motor_control.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/motor_led/topology/e_motor_control.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/motor_led/topology/e_motor_control.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/bluetooth/e_bluetooth_handler.o: bluetooth/e_bluetooth_handler.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/bluetooth" 
	@${RM} ${OBJECTDIR}/bluetooth/e_bluetooth_handler.o.d 
	@${RM} ${OBJECTDIR}/bluetooth/e_bluetooth_handler.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  bluetooth/e_bluetooth_handler.c  -o ${OBJECTDIR}/bluetooth/e_bluetooth_handler.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/bluetooth/e_bluetooth_handler.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/bluetooth/e_bluetooth_handler.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/utility/utility.o: utility/utility.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/utility" 
	@${RM} ${OBJECTDIR}/utility/utility.o.d 
	@${RM} ${OBJECTDIR}/utility/utility.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  utility/utility.c  -o ${OBJECTDIR}/utility/utility.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/utility/utility.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/utility/utility.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/bluetooth/e_bluetooth.o: bluetooth/e_bluetooth.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/bluetooth" 
	@${RM} ${OBJECTDIR}/bluetooth/e_bluetooth.o.d 
	@${RM} ${OBJECTDIR}/bluetooth/e_bluetooth.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  bluetooth/e_bluetooth.c  -o ${OBJECTDIR}/bluetooth/e_bluetooth.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/bluetooth/e_bluetooth.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/bluetooth/e_bluetooth.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/motor_led/e_motors.o: motor_led/e_motors.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/motor_led" 
	@${RM} ${OBJECTDIR}/motor_led/e_motors.o.d 
	@${RM} ${OBJECTDIR}/motor_led/e_motors.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  motor_led/e_motors.c  -o ${OBJECTDIR}/motor_led/e_motors.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/motor_led/e_motors.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/motor_led/e_motors.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/motor_led/e_init_port.o: motor_led/e_init_port.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/motor_led" 
	@${RM} ${OBJECTDIR}/motor_led/e_init_port.o.d 
	@${RM} ${OBJECTDIR}/motor_led/e_init_port.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  motor_led/e_init_port.c  -o ${OBJECTDIR}/motor_led/e_init_port.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/motor_led/e_init_port.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/motor_led/e_init_port.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/a_d/e_accelerometer.o: a_d/e_accelerometer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/a_d" 
	@${RM} ${OBJECTDIR}/a_d/e_accelerometer.o.d 
	@${RM} ${OBJECTDIR}/a_d/e_accelerometer.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  a_d/e_accelerometer.c  -o ${OBJECTDIR}/a_d/e_accelerometer.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/a_d/e_accelerometer.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/a_d/e_accelerometer.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/a_d/e_ad_conv.o: a_d/e_ad_conv.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/a_d" 
	@${RM} ${OBJECTDIR}/a_d/e_ad_conv.o.d 
	@${RM} ${OBJECTDIR}/a_d/e_ad_conv.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  a_d/e_ad_conv.c  -o ${OBJECTDIR}/a_d/e_ad_conv.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/a_d/e_ad_conv.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/a_d/e_ad_conv.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/a_d/e_micro.o: a_d/e_micro.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/a_d" 
	@${RM} ${OBJECTDIR}/a_d/e_micro.o.d 
	@${RM} ${OBJECTDIR}/a_d/e_micro.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  a_d/e_micro.c  -o ${OBJECTDIR}/a_d/e_micro.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/a_d/e_micro.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/a_d/e_micro.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/a_d/advance_ad_scan/e_prox.o: a_d/advance_ad_scan/e_prox.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/a_d/advance_ad_scan" 
	@${RM} ${OBJECTDIR}/a_d/advance_ad_scan/e_prox.o.d 
	@${RM} ${OBJECTDIR}/a_d/advance_ad_scan/e_prox.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  a_d/advance_ad_scan/e_prox.c  -o ${OBJECTDIR}/a_d/advance_ad_scan/e_prox.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/a_d/advance_ad_scan/e_prox.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/a_d/advance_ad_scan/e_prox.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/motor_led/advance_one_timer/e_agenda.o: motor_led/advance_one_timer/e_agenda.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/motor_led/advance_one_timer" 
	@${RM} ${OBJECTDIR}/motor_led/advance_one_timer/e_agenda.o.d 
	@${RM} ${OBJECTDIR}/motor_led/advance_one_timer/e_agenda.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  motor_led/advance_one_timer/e_agenda.c  -o ${OBJECTDIR}/motor_led/advance_one_timer/e_agenda.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/motor_led/advance_one_timer/e_agenda.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/motor_led/advance_one_timer/e_agenda.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/motor_led/advance_one_timer/e_led.o: motor_led/advance_one_timer/e_led.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/motor_led/advance_one_timer" 
	@${RM} ${OBJECTDIR}/motor_led/advance_one_timer/e_led.o.d 
	@${RM} ${OBJECTDIR}/motor_led/advance_one_timer/e_led.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  motor_led/advance_one_timer/e_led.c  -o ${OBJECTDIR}/motor_led/advance_one_timer/e_led.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/motor_led/advance_one_timer/e_led.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/motor_led/advance_one_timer/e_led.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/utility/queue.o: utility/queue.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/utility" 
	@${RM} ${OBJECTDIR}/utility/queue.o.d 
	@${RM} ${OBJECTDIR}/utility/queue.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  utility/queue.c  -o ${OBJECTDIR}/utility/queue.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/utility/queue.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/utility/queue.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/utility/stringutils.o: utility/stringutils.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/utility" 
	@${RM} ${OBJECTDIR}/utility/stringutils.o.d 
	@${RM} ${OBJECTDIR}/utility/stringutils.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  utility/stringutils.c  -o ${OBJECTDIR}/utility/stringutils.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/utility/stringutils.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/utility/stringutils.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/a_d/advance_ad_scan/e_ad_conv.o: a_d/advance_ad_scan/e_ad_conv.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/a_d/advance_ad_scan" 
	@${RM} ${OBJECTDIR}/a_d/advance_ad_scan/e_ad_conv.o.d 
	@${RM} ${OBJECTDIR}/a_d/advance_ad_scan/e_ad_conv.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  a_d/advance_ad_scan/e_ad_conv.c  -o ${OBJECTDIR}/a_d/advance_ad_scan/e_ad_conv.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/a_d/advance_ad_scan/e_ad_conv.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/a_d/advance_ad_scan/e_ad_conv.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/utility/command.o: utility/command.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/utility" 
	@${RM} ${OBJECTDIR}/utility/command.o.d 
	@${RM} ${OBJECTDIR}/utility/command.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  utility/command.c  -o ${OBJECTDIR}/utility/command.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/utility/command.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/utility/command.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/utility/posture_sensor.o: utility/posture_sensor.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/utility" 
	@${RM} ${OBJECTDIR}/utility/posture_sensor.o.d 
	@${RM} ${OBJECTDIR}/utility/posture_sensor.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  utility/posture_sensor.c  -o ${OBJECTDIR}/utility/posture_sensor.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/utility/posture_sensor.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/utility/posture_sensor.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/utility/arena.o: utility/arena.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/utility" 
	@${RM} ${OBJECTDIR}/utility/arena.o.d 
	@${RM} ${OBJECTDIR}/utility/arena.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  utility/arena.c  -o ${OBJECTDIR}/utility/arena.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/utility/arena.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/utility/arena.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/algorithm/random_waypoints.o: algorithm/random_waypoints.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/algorithm" 
	@${RM} ${OBJECTDIR}/algorithm/random_waypoints.o.d 
	@${RM} ${OBJECTDIR}/algorithm/random_waypoints.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  algorithm/random_waypoints.c  -o ${OBJECTDIR}/algorithm/random_waypoints.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/algorithm/random_waypoints.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/algorithm/random_waypoints.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/algorithm/algorithm.o: algorithm/algorithm.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/algorithm" 
	@${RM} ${OBJECTDIR}/algorithm/algorithm.o.d 
	@${RM} ${OBJECTDIR}/algorithm/algorithm.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  algorithm/algorithm.c  -o ${OBJECTDIR}/algorithm/algorithm.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/algorithm/algorithm.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/algorithm/algorithm.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/math/eigen.o: math/eigen.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/math" 
	@${RM} ${OBJECTDIR}/math/eigen.o.d 
	@${RM} ${OBJECTDIR}/math/eigen.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  math/eigen.c  -o ${OBJECTDIR}/math/eigen.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/math/eigen.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/math/eigen.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/math/collections.o: math/collections.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/math" 
	@${RM} ${OBJECTDIR}/math/collections.o.d 
	@${RM} ${OBJECTDIR}/math/collections.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  math/collections.c  -o ${OBJECTDIR}/math/collections.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/math/collections.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/math/collections.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/algorithm/connectivityMaintenance.o: algorithm/connectivityMaintenance.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/algorithm" 
	@${RM} ${OBJECTDIR}/algorithm/connectivityMaintenance.o.d 
	@${RM} ${OBJECTDIR}/algorithm/connectivityMaintenance.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  algorithm/connectivityMaintenance.c  -o ${OBJECTDIR}/algorithm/connectivityMaintenance.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/algorithm/connectivityMaintenance.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/algorithm/connectivityMaintenance.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/math/complex.o: math/complex.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/math" 
	@${RM} ${OBJECTDIR}/math/complex.o.d 
	@${RM} ${OBJECTDIR}/math/complex.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  math/complex.c  -o ${OBJECTDIR}/math/complex.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/math/complex.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/math/complex.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/uart/e_init_uart1.o: uart/e_init_uart1.s  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/uart" 
	@${RM} ${OBJECTDIR}/uart/e_init_uart1.o.d 
	@${RM} ${OBJECTDIR}/uart/e_init_uart1.o 
	${MP_CC} $(MP_EXTRA_AS_PRE)  uart/e_init_uart1.s  -o ${OBJECTDIR}/uart/e_init_uart1.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  -Wa,-MD,"${OBJECTDIR}/uart/e_init_uart1.o.d",--defsym=__MPLAB_BUILD=1,--defsym=__ICD2RAM=1,--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_ICD3=1,-g,--no-relax$(MP_EXTRA_AS_POST)
	@${FIXDEPS} "${OBJECTDIR}/uart/e_init_uart1.o.d"  $(SILENT)  -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/uart/e_init_uart2.o: uart/e_init_uart2.s  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/uart" 
	@${RM} ${OBJECTDIR}/uart/e_init_uart2.o.d 
	@${RM} ${OBJECTDIR}/uart/e_init_uart2.o 
	${MP_CC} $(MP_EXTRA_AS_PRE)  uart/e_init_uart2.s  -o ${OBJECTDIR}/uart/e_init_uart2.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  -Wa,-MD,"${OBJECTDIR}/uart/e_init_uart2.o.d",--defsym=__MPLAB_BUILD=1,--defsym=__ICD2RAM=1,--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_ICD3=1,-g,--no-relax$(MP_EXTRA_AS_POST)
	@${FIXDEPS} "${OBJECTDIR}/uart/e_init_uart2.o.d"  $(SILENT)  -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/uart/e_uart1_rx_char.o: uart/e_uart1_rx_char.s  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/uart" 
	@${RM} ${OBJECTDIR}/uart/e_uart1_rx_char.o.d 
	@${RM} ${OBJECTDIR}/uart/e_uart1_rx_char.o 
	${MP_CC} $(MP_EXTRA_AS_PRE)  uart/e_uart1_rx_char.s  -o ${OBJECTDIR}/uart/e_uart1_rx_char.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  -Wa,-MD,"${OBJECTDIR}/uart/e_uart1_rx_char.o.d",--defsym=__MPLAB_BUILD=1,--defsym=__ICD2RAM=1,--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_ICD3=1,-g,--no-relax$(MP_EXTRA_AS_POST)
	@${FIXDEPS} "${OBJECTDIR}/uart/e_uart1_rx_char.o.d"  $(SILENT)  -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/uart/e_uart1_tx_char.o: uart/e_uart1_tx_char.s  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/uart" 
	@${RM} ${OBJECTDIR}/uart/e_uart1_tx_char.o.d 
	@${RM} ${OBJECTDIR}/uart/e_uart1_tx_char.o 
	${MP_CC} $(MP_EXTRA_AS_PRE)  uart/e_uart1_tx_char.s  -o ${OBJECTDIR}/uart/e_uart1_tx_char.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  -Wa,-MD,"${OBJECTDIR}/uart/e_uart1_tx_char.o.d",--defsym=__MPLAB_BUILD=1,--defsym=__ICD2RAM=1,--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_ICD3=1,-g,--no-relax$(MP_EXTRA_AS_POST)
	@${FIXDEPS} "${OBJECTDIR}/uart/e_uart1_tx_char.o.d"  $(SILENT)  -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/uart/e_uart2_rx_char.o: uart/e_uart2_rx_char.s  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/uart" 
	@${RM} ${OBJECTDIR}/uart/e_uart2_rx_char.o.d 
	@${RM} ${OBJECTDIR}/uart/e_uart2_rx_char.o 
	${MP_CC} $(MP_EXTRA_AS_PRE)  uart/e_uart2_rx_char.s  -o ${OBJECTDIR}/uart/e_uart2_rx_char.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  -Wa,-MD,"${OBJECTDIR}/uart/e_uart2_rx_char.o.d",--defsym=__MPLAB_BUILD=1,--defsym=__ICD2RAM=1,--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_ICD3=1,-g,--no-relax$(MP_EXTRA_AS_POST)
	@${FIXDEPS} "${OBJECTDIR}/uart/e_uart2_rx_char.o.d"  $(SILENT)  -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/uart/e_uart2_tx_char.o: uart/e_uart2_tx_char.s  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/uart" 
	@${RM} ${OBJECTDIR}/uart/e_uart2_tx_char.o.d 
	@${RM} ${OBJECTDIR}/uart/e_uart2_tx_char.o 
	${MP_CC} $(MP_EXTRA_AS_PRE)  uart/e_uart2_tx_char.s  -o ${OBJECTDIR}/uart/e_uart2_tx_char.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  -Wa,-MD,"${OBJECTDIR}/uart/e_uart2_tx_char.o.d",--defsym=__MPLAB_BUILD=1,--defsym=__ICD2RAM=1,--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_ICD3=1,-g,--no-relax$(MP_EXTRA_AS_POST)
	@${FIXDEPS} "${OBJECTDIR}/uart/e_uart2_tx_char.o.d"  $(SILENT)  -rsi ${MP_CC_DIR}../  
	
else
${OBJECTDIR}/uart/e_init_uart1.o: uart/e_init_uart1.s  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/uart" 
	@${RM} ${OBJECTDIR}/uart/e_init_uart1.o.d 
	@${RM} ${OBJECTDIR}/uart/e_init_uart1.o 
	${MP_CC} $(MP_EXTRA_AS_PRE)  uart/e_init_uart1.s  -o ${OBJECTDIR}/uart/e_init_uart1.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  -Wa,-MD,"${OBJECTDIR}/uart/e_init_uart1.o.d",--defsym=__MPLAB_BUILD=1,-g,--no-relax$(MP_EXTRA_AS_POST)
	@${FIXDEPS} "${OBJECTDIR}/uart/e_init_uart1.o.d"  $(SILENT)  -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/uart/e_init_uart2.o: uart/e_init_uart2.s  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/uart" 
	@${RM} ${OBJECTDIR}/uart/e_init_uart2.o.d 
	@${RM} ${OBJECTDIR}/uart/e_init_uart2.o 
	${MP_CC} $(MP_EXTRA_AS_PRE)  uart/e_init_uart2.s  -o ${OBJECTDIR}/uart/e_init_uart2.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  -Wa,-MD,"${OBJECTDIR}/uart/e_init_uart2.o.d",--defsym=__MPLAB_BUILD=1,-g,--no-relax$(MP_EXTRA_AS_POST)
	@${FIXDEPS} "${OBJECTDIR}/uart/e_init_uart2.o.d"  $(SILENT)  -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/uart/e_uart1_rx_char.o: uart/e_uart1_rx_char.s  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/uart" 
	@${RM} ${OBJECTDIR}/uart/e_uart1_rx_char.o.d 
	@${RM} ${OBJECTDIR}/uart/e_uart1_rx_char.o 
	${MP_CC} $(MP_EXTRA_AS_PRE)  uart/e_uart1_rx_char.s  -o ${OBJECTDIR}/uart/e_uart1_rx_char.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  -Wa,-MD,"${OBJECTDIR}/uart/e_uart1_rx_char.o.d",--defsym=__MPLAB_BUILD=1,-g,--no-relax$(MP_EXTRA_AS_POST)
	@${FIXDEPS} "${OBJECTDIR}/uart/e_uart1_rx_char.o.d"  $(SILENT)  -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/uart/e_uart1_tx_char.o: uart/e_uart1_tx_char.s  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/uart" 
	@${RM} ${OBJECTDIR}/uart/e_uart1_tx_char.o.d 
	@${RM} ${OBJECTDIR}/uart/e_uart1_tx_char.o 
	${MP_CC} $(MP_EXTRA_AS_PRE)  uart/e_uart1_tx_char.s  -o ${OBJECTDIR}/uart/e_uart1_tx_char.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  -Wa,-MD,"${OBJECTDIR}/uart/e_uart1_tx_char.o.d",--defsym=__MPLAB_BUILD=1,-g,--no-relax$(MP_EXTRA_AS_POST)
	@${FIXDEPS} "${OBJECTDIR}/uart/e_uart1_tx_char.o.d"  $(SILENT)  -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/uart/e_uart2_rx_char.o: uart/e_uart2_rx_char.s  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/uart" 
	@${RM} ${OBJECTDIR}/uart/e_uart2_rx_char.o.d 
	@${RM} ${OBJECTDIR}/uart/e_uart2_rx_char.o 
	${MP_CC} $(MP_EXTRA_AS_PRE)  uart/e_uart2_rx_char.s  -o ${OBJECTDIR}/uart/e_uart2_rx_char.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  -Wa,-MD,"${OBJECTDIR}/uart/e_uart2_rx_char.o.d",--defsym=__MPLAB_BUILD=1,-g,--no-relax$(MP_EXTRA_AS_POST)
	@${FIXDEPS} "${OBJECTDIR}/uart/e_uart2_rx_char.o.d"  $(SILENT)  -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/uart/e_uart2_tx_char.o: uart/e_uart2_tx_char.s  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/uart" 
	@${RM} ${OBJECTDIR}/uart/e_uart2_tx_char.o.d 
	@${RM} ${OBJECTDIR}/uart/e_uart2_tx_char.o 
	${MP_CC} $(MP_EXTRA_AS_PRE)  uart/e_uart2_tx_char.s  -o ${OBJECTDIR}/uart/e_uart2_tx_char.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  -Wa,-MD,"${OBJECTDIR}/uart/e_uart2_tx_char.o.d",--defsym=__MPLAB_BUILD=1,-g,--no-relax$(MP_EXTRA_AS_POST)
	@${FIXDEPS} "${OBJECTDIR}/uart/e_uart2_tx_char.o.d"  $(SILENT)  -rsi ${MP_CC_DIR}../  
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assemblePreproc
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/firmware.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -o dist/${CND_CONF}/${IMAGE_TYPE}/firmware.${IMAGE_TYPE}.${OUTPUT_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}      -mcpu=$(MP_PROCESSOR_OPTION)        -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)   -mreserve=data@0x800:0x81F -mreserve=data@0x820:0x821 -mreserve=data@0x822:0x823 -mreserve=data@0x824:0x84F   -Wl,,,--defsym=__MPLAB_BUILD=1,--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_ICD3=1,$(MP_LINKER_FILE_OPTION),--stack=16,--check-sections,--data-init,--pack-data,--handles,--isr,--no-gc-sections,--fill-upper=0,--stackguard=16,--no-force-link,--smart-io,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--report-mem,--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml$(MP_EXTRA_LD_POST) 
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/firmware.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -o dist/${CND_CONF}/${IMAGE_TYPE}/firmware.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}      -mcpu=$(MP_PROCESSOR_OPTION)        -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -Wl,,,--defsym=__MPLAB_BUILD=1,$(MP_LINKER_FILE_OPTION),--stack=16,--check-sections,--data-init,--pack-data,--handles,--isr,--no-gc-sections,--fill-upper=0,--stackguard=16,--no-force-link,--smart-io,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--report-mem,--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml$(MP_EXTRA_LD_POST) 
	${MP_CC_DIR}/xc16-bin2hex dist/${CND_CONF}/${IMAGE_TYPE}/firmware.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} -a  -omf=elf  
	
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell "${PATH_TO_IDE_BIN}"mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
