#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "uart/e_uart_char.h"
#include "motor_led/e_epuck_ports.h"
#include "motor_led/e_init_port.h"
#include "a_d/e_ad_conv.h"

#include "utility/command.h"
#include "utility/collision.h"
#include "utility/utility.h"
#include "utility/posture_sensor.h"
#include "topology/topology.h"
#include "algorithm/algorithm.h"

#include "motor_led/e_motors.h"
#include "utility/collision.h"
#include "utility/queue.h"
#include "motor_led/advance_one_timer/e_led.h"
#include "motor_led/topology/e_motor_control.h"
#include "p30F6014A.h"
#include "utility/error.h"

// timer 1 interrupt
// this code is executed every UPDATE_TIMING ms

void __attribute__((interrupt, auto_psv, shadow)) _T1Interrupt(void) {
    IFS0bits.T1IF = 0; // clear interrupt flag

    avoidCollision();
}

/* disable the Timer 1 */
void disableTMR1() {
    IFS0bits.T1IF = 0; // clear interrupt flag
    IEC0bits.T1IE = 0; // set interrupt disable bit
}

void enableTMR1() {
    IFS0bits.T1IF = 0; // clear interrupt flag
    IEC0bits.T1IE = 1; // set interrupt enable bit
}

/* init the Timer 1 */
void InitTMR1(void) {
    T1CON = 0;
    T1CONbits.TCKPS = 3; // prescsaler = 256
    TMR1 = 0; // clear timer 1
    PR1 = (6400*MILLISEC)/256.0; // interrupt after UPDATE_TIMING ms
    IFS0bits.T1IF = 0; // clear interrupt flag
    IEC0bits.T1IE = 1; // set interrupt enable bit
    T1CONbits.TON = 1; // start Timer1
}

int main(void) {

    state = START;

    e_init_port();
    e_init_uart1();
    e_init_ad();

    InitTMR1();
    
    if (RCONbits.POR) {
        resetEpuck();
    }
    
    clearQueue();
    
    state = STOPPED;
    
    retrievePostureFromExternalSensor();

    if (getselector() == 0) {
        while (1) {
            if (e_ischar_uart1()) {
                e_set_body_led(1);
            }
        }
    }

    if (getselector() == 1) {

        while (1) {
            receiveAndExecuteCommand();
        }

        return 1;
    }

    return 1;
}