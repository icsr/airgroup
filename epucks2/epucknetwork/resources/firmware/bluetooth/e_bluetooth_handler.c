#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "e_bluetooth.h"
#include "../uart/e_uart_char.h"
#include "../motor_led/e_epuck_ports.h"
#include "../motor_led/e_init_port.h"
#include "../motor_led/e_motors.h"
#include "../utility/utility.h"
#include "e_bluetooth_handler.h"
#include "../utility/command.h"

#define START_MESSAGE '['
#define END_MESSAGE ']'

void sendBluetoothString(char *message, int messageLength) {

    char m[BUFFER_SIZE_OUTPUT];
    clearCharArray(m, BUFFER_SIZE_OUTPUT);
    m[0] = 0x16;
    m[1] = 0x02;

    m[2] = START_MESSAGE;

    strcpy(&m[3], message);

    m[3 + messageLength] = END_MESSAGE;
    m[4 + messageLength] = 0x03;
    m[5 + messageLength] = 0x17;

    e_send_uart1_char(m, strlen(m));
    while (e_uart1_sending()) {
        NOP();
    }
}

void sendBluetoothMessage(char *message) {
    sendBluetoothString(message, strlen(message));
}

char bt_buffer_in[3];

void receiveBluetoothString(char *buffer, int bufferSize) {
    
    clearCharArray(buffer, bufferSize);

    while (1) {
        int count = 0;
        if (!e_ischar_uart1()) {
            break;
        }

        e_getchar_uart1(bt_buffer_in);

        if (bt_buffer_in[0] != 0x16) {
            continue;
        }

        while (!e_ischar_uart1()) {
            NOP();
        }

        e_getchar_uart1(bt_buffer_in);

        if (bt_buffer_in[0] != 0x02) {
            continue;
        }

        while (1) {
            while (!e_ischar_uart1()) {
                NOP();
            }

            e_getchar_uart1(bt_buffer_in);

            if (bt_buffer_in[0] == 0x03) {
                while (e_ischar_uart1()) {
                    e_getchar_uart1(bt_buffer_in); //???
                }
                return;
            }

            buffer[count++] = bt_buffer_in[0];
        }

    }
    sleep(100);

}