/* 
 * File:   e_bluetooth_handler.h
 * Author: Edney
 *
 * Created on September 5, 2015, 7:38 PM
 */

#ifndef E_BLUETOOTH_HANDLER_H
#define	E_BLUETOOTH_HANDLER_H

void sendBluetoothString(char *data, int datalength);
void sendBluetoothMessage(char *data);
void receiveBluetoothString(char *buffer, int bufferSize);

#endif	/* E_BLUETOOTH_HANDLER_H */

