#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "utility.h"
#include "command.h"

char* withZeros(int number) {

    char numberStr[20] = {'\0'};
    char aux[5] = {'\0'};

    int negative = number < 0;

    sprintf(aux, "%d", abs(number));
    int n = strlen(aux);
    int r = 4 - n;

    char* p;
    if (r <= 0) {
        p = aux;
        return p;
    }


    memset(numberStr, '0', r);

    strcat(numberStr, aux);

    p = numberStr;

    if (negative) {
        *p = '-';
    }

    return strdup(p);
}


int indexOf(char* text, char* search){

	return (strstr(text, search) - text);

}

char substringText[50];
char* substring(char* text, int beginIndex, int endIndex) {

    int i, j;

    clearCharArray(substringText, 50);

    for (i = beginIndex; i < endIndex; i++) {
        substringText[j++] = text[i];
    }

    char *p = substringText;

    return strdup(p);
}

void numberToString(float number, char* str){
    char format[5];
    sprintf(format, "%%0.%df", FLOAT_SCALE);
    
    sprintf(str, format,(double) number);
}