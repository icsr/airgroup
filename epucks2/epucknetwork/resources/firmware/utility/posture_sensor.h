#define MAX_POSITION_CHECKING_DEFAULT_VALUE 2
#define MAX_POSITION_CHECKING_OVERALL 3
#define MAX_GETTING_POSITION_DEFAULT_VALUE 3

extern long waitingTimeInMils;
extern int maxPostureError;
extern int maxFixingPositionCheck;
extern int maxGettingPosition;
extern int range;

void retrievePostureFromExternalSensor();

void checkIfMovedToRightPosition();