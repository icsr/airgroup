void enqueue(char* content);

void enqueueIfNotPresent(char selector, char* content);

void enqueueFirstIfNotPresent(char* content);

void enqueueFirst(char* content);

char* dequeue();

int isQueueEmpty();

int isQueueFull();

void clearQueue();

int contains(char* content);

int containsSelector(char selector);

void enqueueCommand(char selector, int* params, int paramsLength);

void enqueueCommandWithoutParams(char selector);

int queueSize();