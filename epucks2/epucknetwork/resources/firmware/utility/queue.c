/*
 * File:   queue.c
 * Author: israelicsr
 *
 * Created on 10 de Maio de 2018, 20:27
 */


#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include "utility.h"
#include "queue.h"


#define m 11
#define n 30

char Q[m][n];
int h = 0;
int t = 0;

int queueSize() {
    return (m - h + t) % m;
}

int isQueueEmpty() {
    return h == t;
}

int isQueueFull() {
    return queueSize() == m - 1;
}

void enqueueFirst(char* content) {

    if (isQueueFull()) {
        clearQueue();
    }


    h = (h - 1);
    if (h < 0)
        h = m - 1;
    strcpy(&Q[h][0], content);

}

void enqueueFirstIfNotPresent(char* content) {
    if (contains(content)) {
        return;
    }

    enqueueFirst(content);
}

void enqueue(char* content) {

    if (isQueueFull()) {
        clearQueue();
    }

    strcpy(&Q[t][0], content);

    t = (t + 1) % m;
}

void enqueueIfNotPresent(char selector, char* content) {
    
    if(containsSelector(selector)){
        return;
    }
    
    if (contains(content)) {
        return;
    }

    enqueue(content);
}

char* dequeue() {
    if (isQueueEmpty()) {
        return "NULL";
    }

    char* ret = &Q[h][0];

    h = (h + 1) % m;

    return ret;
}

void clearQueue() {
    h = 0;
    t = 0;
}


//??? is it work?
int containsSelector(char selector){
    char s[2];
    s[0] = selector;
    s[1] = '\0';
    
    return contains(s);
}

int contains(char* content) {

    int i;

    if (t >= h) {
        for (i = h; i < t; i++) {
            char *p = &Q[i][0];
            char *sub = strstr(p, content);
            if (sub != NULL) {
                return 1;
            }
        }
    } else {
        for (i = 0; i < t; t++) {
            char *p = &Q[i][0];
            char *sub = strstr(p, content);
            if (sub != NULL) {
                return 1;
            }
        }

        for (i = h; i < m; i++) {
            char *p = &Q[i][0];
            char *sub = strstr(p, content);
            if (sub != NULL) {
                return 1;
            }
        }
    }


    return 0;
}

char command[20];

void enqueueCommand(char selector, int* params, int paramsLength) {

    clearCharArray(command, 20);

    sprintf(command, "%c", selector);

    if (params != NULL && paramsLength > 0) {
        int i = 0;
        for (i = 0; i < paramsLength; i++) {
            sprintf(command, "%s,%d", command, params[i]);
        }
    }

    enqueueIfNotPresent(selector, command);
}

void enqueueCommandWithoutParams(char selector) {
    enqueueCommand(selector, NULL, 0);
}