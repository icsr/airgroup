/* Microchip Technology Inc. and its subsidiaries.  You may use this software 
 * and any derivatives exclusively with Microchip products. 
 * 
 * THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS".  NO WARRANTIES, WHETHER 
 * EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED 
 * WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A 
 * PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION 
 * WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION. 
 *
 * IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
 * INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
 * WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS 
 * BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.  TO THE 
 * FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS 
 * IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF 
 * ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
 *
 * MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE 
 * TERMS. 
 */

/* 
 * File:   
 * Author: 
 * Comments:
 * Revision history: 
 */

#define NAN -9999

#define NONE_SELECTOR '-'

#ifndef MOVE_SELECTOR 
    #define MOVE_SELECTOR 'M' 
#endif
#ifndef SET_SELECTOR
    #define SET_SELECTOR 'S' 
#endif
#ifndef GET_POSTURE_SELECTOR 
    #define GET_POSTURE_SELECTOR 'G' 
#endif
#ifndef ROTATE_SELECTOR
    #define ROTATE_SELECTOR 'R' 
#endif

#ifndef PUT_SELECTOR
    #define PUT_SELECTOR 'P' 
#endif

#ifndef PING_SELECTOR
    #define PING_SELECTOR 'p' 
#endif

#ifndef BOUNDARY_SELECTOR
    #define BOUNDARY_SELECTOR 'B' 
#endif

#ifndef ANSWER_SELECTOR
    #define ANSWER_SELECTOR 'W'
#endif

#ifndef TOPOLOGY_SELECTOR
    #define TOPOLOGY_SELECTOR 'T'
#endif

#ifndef TRACKING_REACHED
    #define TRACKING_REACHED 't'
#endif

#ifndef DISTANCE_SELECTOR
    #define DISTANCE_SELECTOR 'D'
#endif

#ifndef READ_CURRENT_POSTURE_SELECTOR
    #define READ_CURRENT_POSTURE_SELECTOR 'C'
#endif

#ifndef TO_ANGLE_SELECTOR
    #define TO_ANGLE_SELECTOR 'A'
#endif

#ifndef RESET_EPUCK_SELECTOR
    #define RESET_EPUCK_SELECTOR '0'
#endif

#ifndef FAIL_SELECTOR
    #define FAIL_SELECTOR '1'
#endif

#ifndef LED_SELECTOR
    #define LED_SELECTOR '2'
#endif

#ifndef GAIN
    #define GAIN 'E'
#endif

#define SET_NEIGHBORHOOD_SELECTOR 'N'

#define SECOND_EIGENVECTOR_SELECTOR 'V'

#define SECOND_EIGENVALUE_SELECTOR 'L'

#define RETRIEVE_NEIGHBORHOOD_SELECTOR 'I'

#define NB_SENSORS          8		// number of sensors
#define BIAS_SPEED      	200		// robot bias speed
#define SENSOR_THRESHOLD	300		// discount sensor noise below threshold
#define MAXSPEED 			800		// maximum robot speed


#define SEPARATOR ','

#define PUT_PROXIMITY_THRESHOLD 1
#define PUT_COLLISION_DEVIANT_ANGLE_AND_DISTANCE 2
#define WAITING_COUNTER 3
#define MAX_POSTURE_ERROR 4
#define MAX_FIXING_POSITION 5
#define MAX_GETTING_POSITION 6
#define ALGORITHM_CONFIGS 7
#define PUT_RANGE 8

#define SIGNAL_ZERO_AND_COMMA_SIZE 3
#define THREE_PARAMS_GROUP 3
#define FLOAT_SCALE 4
#define SCALE_SAFETY 5
#define MAX_PARAM_LENGTH (FLOAT_SCALE + SIGNAL_ZERO_AND_COMMA_SIZE + SCALE_SAFETY)

#define NUMBER_OF_SELECTOR_NETWORKSIZE_CURRENTPOSTURE (1 + 1 + 3)
#define NUMBER_OF_MATLAB_CONTROL 2
#define MAX_NUMBER_OF_PARAMS ((MAX_EPUCKS - 1) * THREE_PARAMS_GROUP + NUMBER_OF_SELECTOR_NETWORKSIZE_CURRENTPOSTURE + NUMBER_OF_MATLAB_CONTROL)



extern int robotId;
extern int ids[];

extern int arena[4][2];

extern char *paramString;

extern int distanceThreshold;
extern int collisionIRThreshold[4];
extern int collisionDeviantAngle;
extern int collisionDeviantDistance;

extern int initialized;

extern int distanceThreshold;

extern int trackingReached;

void sendPong();

void findNeighborsByBluetooth(); 

void receiveCommand();
void receiveAndExecuteCommand();
void moveToCoord(int x, int y);
void setRobotPosture();
void getRobotPosture();
void turnToAngle(int angle);
void putParams();
void setParams();
void extractParams();
void calibrateIR();
void moveDistance(int distance);

int countParamsInParamString();
void getRobotNeighbors();

void setRobotIndirectNeighbors();
void getRobotNeighbors();

void sendCommandWithoutParams(char selector);
void sendCommand(char selector, char* paramString);
void executeEnqueuedCommands();
void resetEpuck();

void resetTarget();
void fail();
void lightLED();

void disableTMR1();

void enableTMR1();
void sendMessage(char* message);
void turnRobotPosture(int toAngle);

void rotateAngle(int angle);
void clearCharArray(char* array, int arrayLength);

void setNeighborhoodAndMatlabControl();

void sendCommandWithFloatArrayParams(char selector, float* array, int arraySize);

void sendCommandWithIntArrayParams(char selector, int* array, int arraySize);

void sendCommandWithFloatParam(char selector, float param);

void sendInfo(char* message);

void sendTrackingReached();
