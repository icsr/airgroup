/*
 * File:   utils.c
 * Author: israelicsr
 *
 * Created on 26 de Mar�o de 2018, 20:18
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <math.h>

#include "../bluetooth/e_bluetooth_handler.h"
#include "../bluetooth/e_bluetooth.h"
#include "../motor_led/advance_one_timer/e_led.h"
#include "../motor_led/e_motors.h"
#include "utility.h"
#include "command.h"
#include "error.h"
#include "../uart/e_uart_char.h"
#include "../p30F6014A.h"
#include "queue.h"
#include "collision.h"
#include "posture_sensor.h"
#include "../motor_led/topology/e_motor_control.h"
#include "arena.h"
#include "../algorithm/algorithm.h"
#include "../utility/stringtutils.h"
#include "../math/collections.h"
#include "../algorithm/algorithm.h"

float readFloatParam();
int readIntParam();
int readFirstIntParam();
float readFirstFloatParam();

char input[BUFFER_SIZE_INPUT] = {'\0'};
char output[BUFFER_SIZE_OUTPUT] = {'\0'};

int collisionIRThreshold[] = {250, 250, 250, 250}; //thresholds values for collision avoidance and related to IR 0, 1, 6 and 7 
int collisionDeviantAngle = 45;
int collisionDeviantDistance = 10;

char SEPARATOR_STRING[2] = {'\0'};

float params[MAX_NUMBER_OF_PARAMS];
int paramsSize;

int robotId = 0; //keeps the current robot's id

int distanceThreshold = 100;
int initialized = 0;

void init();

void startTopology();

/*
 * Convert char defined separator to char[]
 */
void init() {

    if (initialized) {
        return;
    }

    SEPARATOR_STRING[0] = SEPARATOR;
    initialized = 1;
}

void resetTarget() {
    targetX = *currentX;
    targetY = *currentY;
    targetAngle = currentAngle;
    globalTargetSet = 0;
}

void resetTargetIfNeeded() {

    if (paramsSize < 2 || params[1]) {
        resetTarget();
    }

}

void executeCommand(char* command) {

    if (state == ON_FAIL && command[0] != RESET_EPUCK_SELECTOR) {
        return;
    }

    extractParams(command);

    switch (command[0]) {
        case SET_SELECTOR:
            setRobotPosture();
            break;
        case MOVE_SELECTOR:
            moveToCoord((int) params[0], (int) params[1]);
            break;
        case DISTANCE_SELECTOR:
            moveDistance((int) params[0]);
            break;
        case ROTATE_SELECTOR:
            rotateAngle((int) params[0]);
            break;
        case TO_ANGLE_SELECTOR:
            turnRobotPosture((int) params[0]);
            break;
        case GET_POSTURE_SELECTOR:
            getRobotPosture();
            break;
        case SET_NEIGHBORHOOD_SELECTOR:
            setNeighborhoodAndMatlabControl();
            break;
        case RETRIEVE_NEIGHBORHOOD_SELECTOR:
            sendCommandWithoutParams(SET_NEIGHBORHOOD_SELECTOR);
            break;
        case PUT_SELECTOR:
            putParams();
            break;
        case RESET_EPUCK_SELECTOR:
            resetEpuck();
            break;
        case FAIL_SELECTOR:
            fail();
            break;
        case LED_SELECTOR:
            lightLED();
            break;
        case TOPOLOGY_SELECTOR:
            startTopology();
            break;
        case BOUNDARY_SELECTOR:
            setArenaBoundary((int) params[0], (int) params[1], (int) params[2], (int) params[3]);
            break;
        case READ_CURRENT_POSTURE_SELECTOR:
            if(globalRunningAlgorithm){
                return;
            }
            retrievePostureFromExternalSensor();
            break;
        case GAIN:
            setTopologyGain(params[0], params[1], params[2], params[3]);
            break;
        case TRACKING_REACHED:
            sendTrackingReached();
            break;
         case PING_SELECTOR:
            sendPong();
            break;
        default:
            break;
    }
}

/*
 * Receive command and execute it.
 * pilha n�o pode encher ok
 * se encher limpa ok
 * empilha s� comandos diferentes ao receber
 * tratar string partida!
 */
void receiveAndExecuteCommand() {

    if (globalAvoidingCollision) {
        executeEnqueuedCommands();
    }

    if (e_ischar_uart1()) {
        if (!initialized) {
            init();
        }

        receiveBluetoothString(&input[0], BUFFER_SIZE_INPUT);

        if (input[0] == SET_SELECTOR) {
            executeCommand(&input[0]);
        } else {
            enqueue(&input[0]);
        }
    }

    executeEnqueuedCommands();

    if (globalRunningAlgorithm) {
        performAlgorithm();
    }
}

void executeEnqueuedCommands() {
    while (!isQueueEmpty()) {
        executeCommand(dequeue());
    }
}

void sendCommandWithoutParams(char selector) {
    sendCommand(selector, NULL);
}

void sendCommand(char selector, char* paramString) {

    clearCharArray(output, BUFFER_SIZE_OUTPUT);

    if (!initialized) {
        init();
    }

    output[0] = selector;

    if (paramString != NULL && strlen(paramString) > 0) {
        output[1] = SEPARATOR;

        char* c = &output[2];
        strcpy(c, paramString);
    }

    sendMessage(output);

    if (e_ischar_uart1()) {
        receiveAndExecuteCommand();
    }
}

void sendInfo(char* message){
    
    clearCharArray(output, BUFFER_SIZE_OUTPUT);

    if (!initialized) {
        init();
    }

    output[0] = NONE_SELECTOR;

    if (message != NULL && strlen(message) > 0) {
        output[1] = SEPARATOR;

        char* c = &output[2];
        strcpy(c, message);
    }

    sendMessage(output);
}

void sendMessage(char* message) {
    if (state == ON_FAIL) {
        return;
    }

    sendBluetoothMessage(message);
}

/*
 * Process PUT Commands
 * params[0] keeps option about which variable will be set
 * params[1] keeps the value to be set
 */
void putParams() {

    switch ((int) params[0]) {
        case PUT_PROXIMITY_THRESHOLD:
            collisionIRThreshold[0] = (int) params[1];
            collisionIRThreshold[1] = (int) params[2];
            collisionIRThreshold[2] = (int) params[3];
            collisionIRThreshold[3] = (int) params[4];
            break;
        case PUT_COLLISION_DEVIANT_ANGLE_AND_DISTANCE:
            collisionDeviantAngle = (int) params[1];
            collisionDeviantDistance = (int) params[2];
            break;
        case WAITING_COUNTER:
            waitingTimeInMils = (long) params[1];
            break;
        case MAX_POSTURE_ERROR:
            if (params[1] >= MIN_AXIAL_DISLOCATION) {
                maxPostureError = (int) params[1];
            }
            break;
        case MAX_FIXING_POSITION:
            maxFixingPositionCheck = (int) params[1];
        case MAX_GETTING_POSITION:
            maxGettingPosition = (int) params[1];
            break;
        case ALGORITHM_CONFIGS:
            setAlgorithmConfigs(params[1], (int) params[2], params[3], (int) params[4]);
            break;
        case PUT_RANGE:
            range = (int) params[1];
            break;        
        default:
            break;
    }
}

void startTopology() {
    setAlgorithm((int) params[0], (int) params[1]);
}

void moveToCoord(int x, int y) {

    if (!isPointInsideArena(x, y)) {
        informError(ERROR_CODE_COORD_OUT_ARENA);
        return;
    }

    state = MOVING;
    globalTargetSet = 1;

    targetX = x;
    targetY = y;
    targetAngle = currentAngle;

    int deltaX = targetX - *currentX;
    int deltaY = targetY - *currentY;

    state = ROTATING;
    turnToAngle((int) (atan2f(deltaY, deltaX) * 180 / PI));

    state = MOVING;
    moveCm((int) (sqrtf(powf(deltaX, 2) + powf(deltaY, 2))));

    retrievePostureFromExternalSensor();

}

void moveDistance(int distance) {

    state = MOVING;
    moveCm(distance);
    state = STOPPED;
    resetTargetIfNeeded();

    retrievePostureFromExternalSensor();
}

/*
 * Process MOVE Commands
 * params[0] keeps target position x
 * params[1] keeps target position y 
 * x keeps current x
 * y keeps current y
 * angle keeps current angle
 * these method set boolean running while moving for collision avoidance purposes
 */
void turnRobotPosture(int toAngle) {

    targetAngle = toAngle;

    state = ROTATING;
    turnToAngle(toAngle);
    state = STOPPED;

    retrievePostureFromExternalSensor();
}

void rotateAngle(int angle) {

    state = ROTATING;
    turnToAngle(currentAngle + angle);
    state = STOPPED;
    resetTargetIfNeeded();

    if (!globalAvoidingCollision) {
        retrievePostureFromExternalSensor();
    }
}

/*
 * Process SET commands
 * params[0] keeps robotId
 * params[1] keeps actual x
 * params[2] keeps actual y
 * params[3] keeps actual angle
 */

int retrievingPositionCounter = 0;
int trackingReached = 0;

void setRobotPosture() {

    e_set_front_led(1);

    int previousCurrent[3];
    int currentArray[3];

    previousCurrent[0] = *currentX;
    previousCurrent[1] = *currentY;
    previousCurrent[2] = currentAngle;

    robotId = (int) params[0];
    *currentX = (int) params[1];
    *currentY = (int) params[2];
    currentAngle = (int) params[3];

    currentArray[0] = *currentX;
    currentArray[1] = *currentY;
    currentArray[2] = currentAngle;

    if (retrievingPositionCounter > maxGettingPosition || isArraysEquals(&previousCurrent[0], &currentArray[0], 3, maxPostureError)) {
               
        if(retrievingPositionCounter > maxGettingPosition){
            blinkLED(3);
            trackingReached = 0;
        }else{
            trackingReached = 1;
        }
        
         retrievingPositionCounter = 0;

        e_set_front_led(0);

        if (state == RECEIVING_POSTURE) {
            state = STOPPED;            
        }
    } else {
        retrievingPositionCounter++;
        retrievePostureFromExternalSensor();
    }
}

void sendTrackingReached(){
    sendCommandWithFloatParam(TRACKING_REACHED, trackingReached);
}

void sendPong(){ 
    sendCommand(PING_SELECTOR, FW_VERSION);
}

/*
 * Process GET commands
 * Send message throughout bluetooth like this: "[robotId],x,y,angle" 
 */
void getRobotPosture() {

    clearCharArray(&output[0], BUFFER_SIZE_OUTPUT);
    sprintf(output, "%c,%d,%d,%d,%d", ANSWER_SELECTOR, robotId, *currentX, *currentY, currentAngle);
    sendMessage(output);
}

void extractParams(char* command) {

    char* p = command;
    char* q;

    int i = 0;
    char aux[MAX_PARAM_LENGTH + 5];

    while (1) {

        clearCharArray(aux, MAX_PARAM_LENGTH + 5);

        q = strstr(p, ",");
        p = q + 1;
        q = strstr(p, ",");
        if (q == NULL) {
            strcpy(aux, p);
        } else {
            memcpy(aux, p, q - p);
        }

        params[i++] = atof(aux);

        if (q == NULL) {
            paramsSize = i;
            break;
        }
    }
}

void resetEpuck() {
    clearQueue();
    retrievePostureFromExternalSensor();
    state = STOPPED;
   
    globalAvoidingCollision = 0;
    globalTargetSet = 0;
    globalArenaSet = 0;
    globalRunningAlgorithm = 0;

    leaveRequest();

    resetTarget();
    RCONbits.POR = 0;

    *currentX = -1; //keeps current x position
    *currentY = -1; //keeps current y position
    currentAngle = -1; //keeps current angle
    targetX = -1;
    targetY = -1;
    targetAngle = -1;
    e_set_led(8, 0);
    e_stop_led_blinking();

    __asm__ volatile ("reset");
}

void fail() {
    state = ON_FAIL;
    e_set_led(8, 1);
}

void lightLED() {
    e_set_body_led((int) params[0]);
}

int currentParam = 0;

float readFloatParam() {
    if (currentParam >= paramsSize) {
        return NAN;
    }
    return params[currentParam++];
}

int readIntParam() {
    return (int) readFloatParam();
}

int readFirstIntParam() {
    return (int) readFirstFloatParam();
}

float readFirstFloatParam(){
    currentParam = 0;
    return readFloatParam();
}



void setNeighborhoodAndMatlabControl() {
    
    qtyOfKnownEpucks = readFirstIntParam();
    qtyOfNeighbors = qtyOfKnownEpucks - 1;
        
    if (paramsSize != 3 * qtyOfNeighbors + 4 && paramsSize != 3 * qtyOfNeighbors + 6) {
        sendCommand(NONE_SELECTOR, "Wrong neighbors's params size.");
        return;
    }
    
    *currentX = readIntParam();
    *currentY = readIntParam();
    currentAngle = readIntParam();
    
    int i;
    for (i = 1; i < qtyOfKnownEpucks; i++) {
        allPositions[i][0] = readIntParam();
        allPositions[i][1] = readIntParam();
        readFloatParam(); //this is the lambda evaluated by the robot and it will be ignored
    }
    
    if(paramsSize == 3 * qtyOfNeighbors + 6){
        u_m[0] = readFloatParam();
        u_m[1] = readFloatParam();
    }

    receivedNeighbors = 1;
    receivedNeighborsAtLeastOnce = 1;
}

void sendCommandWithFloatArrayParams(char selector, float* array, int arraySize) {

    clearCharArray(output, BUFFER_SIZE_OUTPUT);

    output[0] = selector;
    output[1] = '\0';
    int i;

    char aux[MAX_PARAM_LENGTH + 5]; //??? FIX
    char aux2[MAX_PARAM_LENGTH + 5]; //??? FIX

    for (i = 0; i < arraySize; i++) {
        clearCharArray(aux, MAX_PARAM_LENGTH + 5);
                
        numberToString(array[i], aux2);

        sprintf(aux, ",%s", aux2);
        strcat(output, aux);
    }

    sendMessage(output);
}

void sendCommandWithIntArrayParams(char selector, int* array, int arraySize) {

    clearCharArray(output, BUFFER_SIZE_OUTPUT);

    output[0] = selector;
    output[1] = '\0';
    int i;

    char aux[MAX_PARAM_LENGTH + 5]; //??? FIX

    for (i = 0; i < arraySize; i++) {
        clearCharArray(aux, MAX_PARAM_LENGTH + 5);

        sprintf(aux, ",%d", array[i]);
        strcat(output, aux);
    }

    sendMessage(output);
}

void sendCommandWithFloatParam(char selector, float param) {

    clearCharArray(input, BUFFER_SIZE_INPUT);
           
    char* aux = input;    
    numberToString(param, aux);
    sendCommand(selector, aux);
}