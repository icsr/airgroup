

#define FW_VERSION "v2018-11-6-21:50"

#define MAX_BATT_VALUE 2560 // corresponds to 4.2 volts
#define MIN_BATT_VALUE 2070 // corresponds to 3.4 volts
#define BATT_VALUES_RANGE (MAX_BATT_VALUE-MIN_BATT_VALUE)

#define MIN_AXIAL_DISLOCATION 1

#define MAX_AXIAL_DISLOCATION 4

#define BUFFER_SIZE_INPUT ((MAX_EPUCKS - 1) * 18 + 35 + 100)

#define BUFFER_SIZE_OUTPUT 75

#define BODY_RADIUS 3.5

#define BLINK_TIME_MILS 400

#define NaN -2000000

#define PI 3.14159265 //Pi

typedef enum {ON_FAIL, START, STOPPED, RECEIVING_POSTURE, RECEIVING_NEIGHBORS, MOVING, ROTATING, CHECKING_POSTURE} STATES;

extern int* currentX;     //keeps current x position
extern int* currentY;      //keeps current y position
extern int currentAngle;  //keeps current angle
extern int targetX;
extern int targetY;
extern int targetAngle;

extern int minAxialDislocation;

extern int maxAxialDislocation;

extern STATES state;

extern int globalAvoidingCollision;

extern int globalTargetSet;

extern int globalArenaSet;

extern unsigned int e_last_acc_scan_id;
extern unsigned int tickAdcIsr;

int minValue(int* array, int size);

void ftoa(float n, char *res, int afterpoint);
int countArray(int* array);
void removeItem(int *array, int item);

void sleep(long timeInMils);

int roundFloatToInt(float number);

float roundFloat(float number);

int getselector();

int correctAngle(int angle);

void clearCharArray(char* array, int arrayLength) ;

void clearFloatArray(float* array, int arrayLength) ;

void clearIntArray(int* array, int arrayLength);

void clearIntMatrix(int* matrix, int rows, int cols);

void clearFloatMatrix(float* matrix, int rows, int cols) ;

int isTargetPostureReached(int maxDifference);

void blinkLED(int numberOfTimes);

int randomize(int minValue, int maxValue);

float coth(float x);

float distance (int x1, int y1, int x2, int y2);

float dotProduct(float* v1, float* v2, int arraySize);


void subtract(float* result, float* v1, float* v2, int arraySize);

void sum(float* result, float* v1, float* v2, int arraySize);

void multiplyScalar(float* result, float* v, float scalar, int arraySize);

void divideScalar(float* result, float* origin, float scalar, int arraySize);

int isNaN(int number);