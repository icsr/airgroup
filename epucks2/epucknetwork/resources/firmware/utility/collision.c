#include "../motor_led/topology/e_motor_control.h"
#include "../motor_led/e_motors.h"
#include "../motor_led/advance_one_timer/e_led.h"
#include "utility.h"
#include "command.h"
#include "error.h"
#include "../uart/e_uart_char.h"
#include "../a_d/advance_ad_scan/e_prox.h"
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "queue.h"
#include "stringtutils.h"
#include "collision.h"
#include "posture_sensor.h"

int calibrationDone = 0;
int infraredSensorID[] = {0, 1, 6, 7};
int hasSensorDetectedCollision[4];
int COLLISION_AVOIDANCE_SPEED = 300;
int performParam[2];

void avoidCollision() {

    prepareIR();

    int collisionDetected = isCollisionDetected();

    if (state == ROTATING) {
        return;
    }

    if (state != MOVING && !globalAvoidingCollision) {
        return;
    }

    performParam[1] = 0; // flag enqueued command not to reset target
    if (!collisionDetected && globalAvoidingCollision) {
        clearQueue();
        globalAvoidingCollision = 0;
        performParam[0] = collisionDeviantDistance;
        enqueueCommand(DISTANCE_SELECTOR, performParam, 2);
        enqueueCommand(READ_CURRENT_POSTURE_SELECTOR, NULL, 0);
    }

    if (collisionDetected) {

        globalAvoidingCollision = 1;
        makeRequest();
        e_set_steps_left(totalSteps);
        e_set_steps_right(totalSteps);

        performParam[0] = (-hasSensorDetectedCollision[0] - hasSensorDetectedCollision[1] + hasSensorDetectedCollision[2] + hasSensorDetectedCollision[3]) * collisionDeviantAngle;

        if (performParam[0] == 0) {
            performParam[0] = 180;
        }

        enqueueCommand(ROTATE_SELECTOR, performParam, 2);

        leaveRequest();
    }
}

void prepareIR() {
    scanIR();

    if (!calibrationDone) {
        e_calibrate_ir();

        calibrationDone = 1;
    }
}

int isCollisionDetected() {

    int collisionDetected = 0;
    int i;
    for (i = 0; i < 4; i++) {
        hasSensorDetectedCollision[i] = abs(e_get_prox(infraredSensorID[i])) >= collisionIRThreshold[i];
        if (hasSensorDetectedCollision[i]) {
            e_set_led(infraredSensorID[i], 1);
            collisionDetected = 1;
        } else {
            e_set_led(infraredSensorID[i], 0);
        }
    }

    return collisionDetected;
}