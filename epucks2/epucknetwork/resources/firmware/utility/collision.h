#define MAX_ACTING_COUNTER 10

extern int collisionAvoidSpeedLeft, collisionAvoidSpeedRight, hasSensorDetectedCollision[4];

void calibrateIR();
void followGetSensorValues(int *sensorTable);

void followsetSpeed(int LeftSpeed, int RightSpeed);

void avoidCollision();

int isCollisionDetected();

void prepareIR();