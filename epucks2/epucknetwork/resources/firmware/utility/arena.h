
extern int arena[4][2];

int isPointInsideArena(int x, int y);

void setArenaBoundary(int origX, int origY, int diagX, int diagY);

void resetArenaBoundary();