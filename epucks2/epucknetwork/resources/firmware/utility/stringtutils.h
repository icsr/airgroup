char* withZeros(int number);

void numberToString(float number, char* str);

int indexOf(char* text, char* search);

char* substring(char* text, int beginIndex, int endIndex);