#include <stdlib.h>
#include "utility.h"

int arena[4][2];

int isPointInsideArena(int x, int y){    
	return !globalArenaSet || (y < arena[1][1] && y > arena[0][1] && x < arena[1][0] && x > arena[0][0]);
}

void setArenaBoundary(int origX, int origY, int diagX, int diagY) {
    globalArenaSet = 1;
    arena[0][0] = min(origX, diagX) + 2 * BODY_RADIUS;
    arena[0][1] = min(origY, diagY) + 2 * BODY_RADIUS;
    arena[1][0] = max(origX, diagX) - 2 * BODY_RADIUS;
    arena[1][1] = max(origY, diagY) - 2 * BODY_RADIUS;
}

void resetArenaBoundary(){
    globalArenaSet = 0;
}
