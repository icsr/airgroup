/*
 * File:   error.c
 * Author: israelicsr
 *
 * Created on 14 de Abril de 2018, 11:08
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "utility.h"
#include "../motor_led/advance_one_timer/e_led.h"
#include "../uart/e_uart_char.h"
#include "command.h"
#include "error.h"


void showMessageError(int errorCode);

/*
 * errorCode 1 means function khopca was not called so pointers ids and weight are both NULL
 * blink led once if errorCode 1
 */
void informError(int errorCode){    

    showMessageError(errorCode);
    
    int i;
    for(i = 0; i < 3; i++){
        blinkLED(errorCode);
        sleep(1000);
    }

   
    resetTarget();
    sleep(500);
}

void showMessageError(int errorCode){
    
    switch(errorCode){
        case ERROR_CODE_ARENA_NOT_SET:
            sendCommand(NONE_SELECTOR, "Error: Arena was not set!");
            break;
        case ERROR_CODE_COORD_OUT_ARENA:
            sendCommand(NONE_SELECTOR, "Error: coordinate is off arena's boundary.");
            break;       
    }   
    
}