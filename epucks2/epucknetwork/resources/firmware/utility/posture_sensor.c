#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "command.h"
#include "../motor_led/e_motors.h"
#include "posture_sensor.h"
#include "collision.h"
#include "queue.h"
#include "utility.h"
#include "../algorithm/algorithm.h"
#include "../math/collections.h"


int fixingPositionCounter = 0;
long waitingTimeInMils = 500;
int maxPostureError = MIN_AXIAL_DISLOCATION;
int maxFixingPositionCheck = MAX_POSITION_CHECKING_DEFAULT_VALUE;
int maxGettingPosition = MAX_GETTING_POSITION_DEFAULT_VALUE;

int range = 20;

int sensorParams[2];
void checkIfMovedToRightPosition() {    
    
    if(!globalTargetSet || globalAvoidingCollision || globalRunningAlgorithm || state != STOPPED){
        e_set_body_led(0);
        return;
    }

    //???
    e_set_body_led(1);
    state = CHECKING_POSTURE;
    
    if (isTargetPostureReached(maxPostureError) || fixingPositionCounter > maxFixingPositionCheck) {
        fixingPositionCounter = 0;
        resetTarget();
        e_set_body_led(0);
        state = STOPPED;
        return;
    }

    fixingPositionCounter++;
   
    moveToCoord(targetX, targetY);
    
    state = STOPPED;

}

void retrievePostureFromExternalSensor() {
    
    if(globalRunningAlgorithm || globalAvoidingCollision){
        return;
    }
    
    e_set_front_led(0);
    
   state = RECEIVING_POSTURE;
   sleep(waitingTimeInMils);
   sendCommand(GET_POSTURE_SELECTOR, NULL);
}