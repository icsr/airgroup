#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "utility.h"
#include <assert.h>
#include <math.h>
#include "../motor_led/e_epuck_ports.h"
#include "../motor_led/e_init_port.h"
#include "../bluetooth/e_bluetooth_handler.h"
#include "../bluetooth/e_vulnerability.h"
#include "../motor_led/e_epuck_ports.h"
#include "../motor_led/e_init_port.h"
#include "../a_d/advance_ad_scan/e_acc.h"
#include "../a_d/advance_ad_scan/e_ad_conv.h"
#include "../motor_led/advance_one_timer/e_led.h"
#include "posture_sensor.h"
#include "command.h"
#include "../algorithm/algorithm.h"

STATES state = START;

int globalAvoidingCollision = 0;

int globalTargetSet = 0;

int globalArenaSet = 0;

int* currentX = &allPositions[0][0]; //keeps current x position
int* currentY = &allPositions[0][1]; //keeps current y position
int currentAngle = -1; //keeps current angle
int targetX = -1;
int targetY = -1;
int targetAngle = -1;

int minAxialDislocation = MIN_AXIAL_DISLOCATION;

int maxAxialDislocation = MAX_AXIAL_DISLOCATION;

int isTargetPostureReached(int maxDifference) {
    return abs(targetX - *currentX) <= maxDifference && abs(targetY - *currentY) <= maxDifference;
}

int minValue(int* array, int size) {
    int val = array[0];
    int i;
    for (i = 1; i < size; ++i) {
        if (array[i] < val) {
            val = array[i];
        }
    }
    return val;
}

void sleep(long timeInMils) {
    long startTime = clock();
    while (clock() - startTime < timeInMils * MILLISEC);
}

// reverses a string 'str' of length 'len'

void reverse(char *str, int len) {
    int i = 0, j = len - 1, temp;
    while (i < j) {
        temp = str[i];
        str[i] = str[j];
        str[j] = temp;
        i++;
        j--;
    }
}

// Converts a given integer x to string str[].  d is the number
// of digits required in output. If d is more than the number
// of digits in x, then 0s are added at the beginning.

int intToStr(int x, char str[], int d) {
    int i = 0;
    while (x) {
        str[i++] = (x % 10) + '0';
        x = x / 10;
    }
    // If number of digits required is more, then
    // add 0s at the beginning
    while (i < d)
        str[i++] = '0';

    reverse(str, i);
    str[i] = '\0';
    return i;
}

// Converts a floating point number to string.

void ftoa(float n, char *res, int afterpoint) {
    // Extract integer part
    int ipart = (int) n;
    // Extract floating part
    float fpart = n - (float) ipart;
    // convert integer part to string
    int i = intToStr(ipart, res, 0);

    // check for display option after point
    if (afterpoint != 0) {
        res[i] = '.'; // add dot

        // Get the value of fraction part upto given no.
        // of points after dot. The third parameter is needed
        // to handle cases like 233.007
        fpart = fpart * pow(10, afterpoint);

        intToStr((int) fpart, res + i + 1, afterpoint);
    }
}

int getselector() {
    return SELECTOR0 + 2 * SELECTOR1 + 4 * SELECTOR2 + 8 * SELECTOR3;
}

float roundFloat(float number) {
    return ceilf(roundFloatToInt(pow(10, FLOAT_SCALE) * number)) / pow(10, FLOAT_SCALE);
}

int roundFloatToInt(float number) {
    int aux1 = 10 * abs((int) number);
    int aux2 = abs((int) (10 * number));

    int aux3 = abs((int) number);
    if (aux2 - aux1 >= 5) {
        aux3++;
    }

    if (number < 0) {
        aux3 = -aux3;
    }

    return aux3;
}

int correctAngle(int angle) {
    if (abs(angle) > 360) {
        if (angle > 0) {
            while (abs(angle) > 360) {
                angle -= 360;
            }
        } else {
            while (abs(angle) > 360) {
                angle += 360;
            }
        }
    }

    int signal = 1;
    if (abs(angle) > 180) {
        if (angle > 0) {
            signal = -1;
        }
        angle = signal * (360 - abs(angle));
    }

    return angle;
}

void clearCharArray(char* array, int arrayLength) {
    int j;
    for (j = 0; j < arrayLength; j++) {
        array[j] = '\0';
    }
}

void clearFloatArray(float* array, int arrayLength) {
    int j;
    for (j = 0; j < arrayLength; j++) {
        array[j] = 0;
    }
}

void clearFloatMatrix(float* matrix, int rows, int cols) {
    int i;
    for (i = 0; i < rows; i++) {
        float* p = matrix + cols * i;
        clearFloatArray(p, cols);
    }
}

void clearIntMatrix(int* matrix, int rows, int cols) {
    int i;
    for (i = 0; i < rows; i++) {
        int* p = matrix + cols * i;
        clearIntArray(p, cols);
    }
}

void clearIntArray(int* array, int arrayLength) {
    int j;
    for (j = 0; j < arrayLength; j++) {
        array[j] = 0;
    }
}

void blinkLED(int numberOfTimes) {
    int i = 0;
    for (i = 0; i < numberOfTimes; i++) {
        e_set_front_led(1);
        sleep(BLINK_TIME_MILS);
        e_set_front_led(0);
        sleep(BLINK_TIME_MILS);
    }
}

int randomize(int minValue, int maxValue) {

    minValue = min(minValue, maxValue);
    maxValue = max(minValue, maxValue);

    srand(clock() + robotId + 1000);
    int randValue = (rand() % (maxValue - minValue + 1)) + minValue;
    return randValue;
}

float coth(float x) {
    return (expl(x) + expl(-x)) / (expl(x) - expl(-x));
}

float distance(int x1, int y1, int x2, int y2) {
    return sqrtl(powl(x1 - x2, 2) + powl(y1 - y2, 2));
}

void copyArray(float* src, float* dest, int fromPosition, int size) {
    int i;
    for (i = 0; i < size; i++) {
        dest[i] = src[fromPosition * size + i];
    }
}

float dotProduct(float* v1, float* v2, int arraySize) {
    float result = 0;

    int i;
    for (i = 0; i < arraySize; i++) {
        result += v1[i] * v2[i];
    }

    return result;
}

void subtract(float* result, float* v1, float* v2, int arraySize) {

    int i;
    for (i = 0; i < arraySize; i++) {
        result[i] = v1[i] - v2[i];
    }
}

void sum(float* result, float* v1, float* v2, int arraySize) {

    int i;
    for (i = 0; i < arraySize; i++) {
        result[i] = v1[i] + v2[i];
    }
}

void divideScalar(float* result, float* origin, float scalar, int arraySize) {
    multiplyScalar(result, origin, 1/scalar, arraySize);
}

void multiplyScalar(float* result, float* v, float scalar, int arraySize) {
    int i;
    for (i = 0; i < arraySize; i++) {
        result[i] = v[i] * scalar;
    }
}

int isNaN(int number){
	return number == NaN || number == NAN;
}