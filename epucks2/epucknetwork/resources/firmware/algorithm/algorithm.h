#define ID_RANDOM_WAYPOINTS 1
#define ID_CONNECTIVITY_MAINTENANCE 2
#define ID_CONNECTIVITY_MAINTENANCE_AND_MATLAB_CONTROLS_COMBINED 3
#define ID_MATLAB_CONTROL_ONLY 4
#define MAX_EPUCKS 15
#define MAX_WAITING_TIME_IN_CLOCKS 10000000
#define MIN_TURNING_TO_AVOID_COLLISION 5

extern int globalRunningAlgorithm;
extern int allPositions[MAX_EPUCKS][2];
extern float allV2i[MAX_EPUCKS];

extern float u_m[2];
extern float paramsConnectivity[2];

extern int qtyOfKnownEpucks;
extern int qtyOfNeighbors;

extern int receivedNeighbors;

extern int receivedNeighborsAtLeastOnce;

extern float laplacian[MAX_EPUCKS][MAX_EPUCKS];
extern float weights[MAX_EPUCKS][MAX_EPUCKS];
extern float secondEigenValue;
extern float* secondEigenVector;

extern float algWorkspace[2*MAX_EPUCKS];
extern float eigen_vectors_real[MAX_EPUCKS*MAX_EPUCKS];
extern float eigen_values_real[MAX_EPUCKS];
extern float eigen_values_imag[MAX_EPUCKS];

extern float connectivityThreshold;
extern float algebraicConnectivityLowerBound;

extern float sigmaSquared;

void setAlgorithm(int algorithmID, int start);

void performAlgorithm();

void setAlgorithmConfigs(float thresholdValue, int minDislocation, float algebraicConnectivityLowerBoundValue, int maxDislocation);

void resetEigens();

void combineControls();
void moveToNextPosition();

void updatePositionParams(float angle);

void performCollisionAvoidance();

void setTopologyGain(float connectivity, float robustness, float coverageArea, float collisionAvoidance);