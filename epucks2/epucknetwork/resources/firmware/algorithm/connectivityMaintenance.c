#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "../math/eigen.h"

#include "algorithm.h"
#include "../utility/utility.h"
#include "../math/collections.h"
#include "../utility/command.h"
#include "../utility/queue.h"
#include "../utility/posture_sensor.h"
#include "../utility/error.h"
#include "../math/complex.h"
#include "../utility/collision.h"



void populateAdjancencyMatrix();
void calculateDegreesDiagAndLaplacian();
float calculateSigmaSquared();
void calculateSecondEigens();
void sendSecondEigenVector();
void sendSecondEigenValue();
void evaluateControl();
void evaluateSecondEigenValue();

void performConnectivityMaintenance(int moveUnitAfterEvalControl) {

    if (globalAvoidingCollision || state != STOPPED) {
        return;
    }

    clearFloatMatrix(laplacian[0], MAX_EPUCKS, MAX_EPUCKS);
    clearFloatArray(algWorkspace, MAX_EPUCKS);

    evaluateSecondEigenValue();

    sendSecondEigenValue();

    sendSecondEigenVector();

    evaluateControl();

    if (moveUnitAfterEvalControl) {
        moveToNextPosition();
    }

}

void evaluateSecondEigenValue() {
    clearFloatMatrix(laplacian[0], MAX_EPUCKS, MAX_EPUCKS);
    clearFloatArray(algWorkspace, MAX_EPUCKS);

    calculateDegreesDiagAndLaplacian();
    calculateSecondEigens();
}

void sendSecondEigenVector() {
    sendCommandWithFloatParam(SECOND_EIGENVECTOR_SELECTOR, *secondEigenVector);
}

void sendSecondEigenValue() {
    sendCommandWithFloatParam(SECOND_EIGENVALUE_SELECTOR, secondEigenValue);
}

float calculateSigmaSquared() {
    return -powl(2 * range, 2) / 2 / logl(connectivityThreshold);
}

float calculateWeight(int x1, int y1, int x2, int y2) {

    if (isNaN(x1) || isNaN(x2) || isNaN(y1) || isNaN(y2)) {
        return 0;
    }

    float d = distance(x1, y1, x2, y2);
    if (d > 2 * range) {
        return 0;
    }

    return expl(-powl(d, 2) / 2 / sigmaSquared);

}

void populateAdjancencyMatrix() {
    int i, j;

    for (i = 0; i < qtyOfKnownEpucks; i++) {
        for (j = i + 1; j < qtyOfKnownEpucks; j++) {
            if (i > qtyOfNeighbors || j > qtyOfNeighbors) {
                weights[i][j] = 0;
            } else {
                weights[i][j] = calculateWeight(allPositions[i][0],
                        allPositions[i][1], allPositions[j][0],
                        allPositions[j][1]);
            }
            weights[j][i] = weights[i][j];
            laplacian[i][j] = weights[i][j];
            laplacian[j][i] = laplacian[i][j];
        }
    }
}

void calculateDegreesDiagAndLaplacian() {

    sigmaSquared = calculateSigmaSquared();
    populateAdjancencyMatrix();

    int i, j;
    for (i = 0; i < qtyOfKnownEpucks; i++) {
        for (j = 0; j < qtyOfKnownEpucks; j++) {
            algWorkspace[i] += laplacian[i][j];
        }
    }

    for (i = 0; i < qtyOfKnownEpucks; i++) {
        for (j = 0; j < qtyOfKnownEpucks; j++) {
            if (i == j) {
                laplacian[i][j] = algWorkspace[i] - laplacian[i][j];
            } else {
                laplacian[i][j] = -laplacian[i][j];
            }
        }
    }
}

void calculateSecondEigens() {

    float a[qtyOfKnownEpucks][qtyOfKnownEpucks];

    int i, j, index;
    for (i = 0; i < qtyOfKnownEpucks; i++) {
        for (j = 0; j < qtyOfKnownEpucks; j++) {
            a[i][j] = laplacian[i][j];
        }
    }

    eigen(a[0], qtyOfKnownEpucks);

    secondEigenValue = eigen_values_real[qtyOfNeighbors - 1]; // vector is sorted in descending order

    /*
     *    positions are such that the first line refers to current epuck, so the first line of 
     *    the eigenvector corresponding to the second smaller eigenvalue is the eigenvector's value 
     *    needed by the connectivity maintenance's algorithm.
     */
    for(i = 0; i < qtyOfKnownEpucks; i++){
    	index = (qtyOfNeighbors + 1) * (qtyOfNeighbors - 1) + i;
    	allV2i[i] =  eigen_vectors_real[index];
    }
}

void evaluateControl() {

    float gain,d,cschsquared;
    float ui[2];
    clearFloatArray(ui, 2);

    int j, k;
    for (j = 1; j < qtyOfKnownEpucks; j++) {
        
        cschsquared =  2 / powl(sinhf(secondEigenValue - algebraicConnectivityLowerBound),2);
    	d = distance(allPositions[0][0], allPositions[0][1], allPositions[j][0], allPositions[j][1]);
    	gain = -cschsquared / sigmaSquared / d * weights[0][j] * powl(allV2i[0] - allV2i[j],2);

        for (k = 0; k < 2; k++) {
            algWorkspace[k] = allPositions[0][k] - allPositions[j][k];
        }

        multiplyScalar(algWorkspace, algWorkspace, gain, 2);

        for (k = 0; k < 2; k++) {
            ui[k] += algWorkspace[k];
        }

    }

    float scalar;    
    if(minAxialDislocation >= MIN_AXIAL_DISLOCATION){
        scalar = norm(ui);
    }else{
        scalar = 1;
    }
    
    divideScalar(algWorkspace, ui, scalar, 2);
}