#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "../utility/utility.h"
#include "algorithm.h"
#include "../utility/arena.h"
#include "../utility/command.h"
#include "../utility/queue.h"

int paramsWaypoint[2];

void performRandomWaypoint() {

    paramsWaypoint[0] = randomize(arena[0][0], arena[1][0]);
    paramsWaypoint[1] = randomize(arena[0][1], arena[1][1]);

    
    //??? don't stop ever
    enqueueCommand(MOVE_SELECTOR, paramsWaypoint, 2);

}