#include <math.h>
#include <time.h>
#include <limits.h>
#include <stdlib.h>
#include "random_waypoints.h"
#include "connectivityMaintenance.h"
#include "algorithm.h"
#include "../utility/collision.h"
#include "../utility/arena.h"
#include "../utility/utility.h"
#include "../utility/error.h"
#include "../utility/queue.h"
#include "../utility/command.h"
#include "../motor_led/topology/e_motor_control.h"
#include "../math/complex.h"
#include "../utility/posture_sensor.h"

void resetNeighborsReceivingFlags();

int allPositions[MAX_EPUCKS][2];
float allV2i[MAX_EPUCKS];
int neighborsSize = 0;

int qtyOfKnownEpucks = 0;
int qtyOfNeighbors = 0;

float paramsConnectivity[2];

float u_m[2];

float gains[4] = {1,1,1,1};


float laplacian[MAX_EPUCKS][MAX_EPUCKS];
float weights[MAX_EPUCKS][MAX_EPUCKS];
float secondEigenValue = -1;
float* secondEigenVector = &allV2i[0];

float algWorkspace[2 * MAX_EPUCKS];
float eigen_vectors_real[MAX_EPUCKS*MAX_EPUCKS];
float eigen_values_real[MAX_EPUCKS];
float eigen_values_imag[MAX_EPUCKS];

int retrievingNeighbors = 0;
int receivedNeighbors = 0;

int receivedNeighborsAtLeastOnce = 0;

int globalRunningAlgorithm = 0;
int runningAlgorithmID = -1;

float connectivityThreshold = 0.08;

float algebraicConnectivityLowerBound = 0.4;

float sigmaSquared;

void setAlgorithmConfigs(float thresholdValue, int minDislocation, float algebraicConnectivityLowerBoundValue, int maxDislocation) {
    connectivityThreshold = thresholdValue;
    minAxialDislocation = minDislocation;
    algebraicConnectivityLowerBound = algebraicConnectivityLowerBoundValue;
    maxAxialDislocation = maxDislocation;
}

void setAlgorithm(int algorithmID, int start) {

    if (!start) {
        globalRunningAlgorithm = 0;
        runningAlgorithmID = -1;

        resetTarget();
        resetEigens();
        
        resetNeighborsReceivingFlags();
        
        return;
    }

    globalRunningAlgorithm = 1;
    runningAlgorithmID = algorithmID;
    
}

void performAlgorithm() {
    if (!globalRunningAlgorithm || globalAvoidingCollision || state != STOPPED) {
        return;
    }

    if (!receivedNeighbors) {
        if (!retrievingNeighbors) {            
            enqueueCommandWithoutParams(RETRIEVE_NEIGHBORHOOD_SELECTOR);
            retrievingNeighbors = 1;
        }
        return;
    }
           
    resetNeighborsReceivingFlags();
    
    switch (runningAlgorithmID) {
        case ID_RANDOM_WAYPOINTS:
            performRandomWaypoint();
            break;

        case ID_CONNECTIVITY_MAINTENANCE:
            performConnectivityMaintenance(1);
            break;

        case ID_CONNECTIVITY_MAINTENANCE_AND_MATLAB_CONTROLS_COMBINED:
            performConnectivityMaintenance(0);
            combineControls();
            moveToNextPosition();
            break;

        case ID_MATLAB_CONTROL_ONLY:
            algWorkspace[0] = u_m[0];
            algWorkspace[1] = u_m[1];
            u_m[0] = 0;
            u_m[0] = 0;
            combineControls();
            moveToNextPosition();
            break;
    }

}

void resetEigens() {
    secondEigenValue = -1;
    clearFloatArray(allV2i, MAX_EPUCKS);
    receivedNeighbors = 0;
}

void combineControls() {
    float u_c[] = {algWorkspace[0], algWorkspace[1]};
    float u[2];
    sum(u, u_c, u_m, 2);
    
    performCollisionAvoidance();   
    
    sum(u, u, algWorkspace, 2);
    
    float scalar = norm(u);
    
    if(scalar > powf(10, -1 * FLOAT_SCALE)){
        divideScalar(algWorkspace, u, scalar, 2);
    } else{
        algWorkspace[0] = u[0];
        algWorkspace[1] = u[1]; 
    }
}

void moveToNextPosition() {
    
    if(algWorkspace[1] == 0 && algWorkspace[0] == 0){
        return;
    }
    
    if (receivedNeighborsAtLeastOnce) {
        float angle = atan2f(algWorkspace[1], algWorkspace[0]) * 180 / PI;
        
        float delta = 0;
        
        do{
            turnToAngle(angle + delta);
            delta += MIN_TURNING_TO_AVOID_COLLISION;
            if(delta >= 360){
                break;
            }
        } while(gains[3] > 0 && isCollisionDetected());
        
        if(delta >= 360){
            return;
        }

        int previousCollisionDeviantDistance = collisionDeviantDistance;

        if (gains[3] > 0) {
            collisionDeviantDistance = MIN_AXIAL_DISLOCATION;
            state = MOVING;
        }

        moveCm(max(MIN_AXIAL_DISLOCATION, max(maxAxialDislocation, minAxialDislocation)));

        if (gains[3] > 0) {
            state = STOPPED;
            collisionDeviantDistance = previousCollisionDeviantDistance;
        }        
    }
}


void updatePositionParams(float angle) {
    paramsConnectivity[0] =
            (int) (*currentX + minAxialDislocation * cosf(angle * PI / 180));
    paramsConnectivity[1] =
            (int) (*currentY + minAxialDislocation * sinf(angle * PI / 180));
}

void resetNeighborsReceivingFlags(){
    receivedNeighbors = 0;
    retrievingNeighbors = 0;
}

void performCollisionAvoidance(){

    if(!isCollisionDetected()){
        algWorkspace[0] = 0;
        algWorkspace[1] = 0;
        return;
    }
    
    int angle = (- hasSensorDetectedCollision[0] - hasSensorDetectedCollision[1] + hasSensorDetectedCollision[2] + hasSensorDetectedCollision[3]) * collisionDeviantAngle;
    
    if(angle == 0){
        angle = 180;
    }
    
    angle += currentAngle;
    
    algWorkspace[0] = gains[3] * collisionDeviantDistance * cosf(angle * PI / 180);
    algWorkspace[1] = gains[3] * collisionDeviantDistance * sinf(angle * PI / 180);
    
}

void setTopologyGain(float connectivity, float robustness, float coverageArea, float collisionAvoidance){

    gains[0] = connectivity;
    gains[1] = robustness;
    gains[2] = coverageArea;
    gains[3] = collisionAvoidance;
    
}
