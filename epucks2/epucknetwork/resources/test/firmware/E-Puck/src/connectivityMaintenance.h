void populateAdjancencyMatrix();
void calculateDegreesDiagAndLaplacian();
float calculateSigmaSquared();
void calculateSecondEigens();
void evaluateControl();
void evaluateSecondEigenValue();
