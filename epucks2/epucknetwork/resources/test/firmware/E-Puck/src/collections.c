#include <stdlib.h>

void bubbleSort(float *array, int arrayLength) {

    int i, j;
    float temp;

    //Bubble sorting algorthm
    for (i = arrayLength - 2; i >= 0; i--) {
        for (j = 0; j <= i; j++) {

            if (array[j] > array[j + 1]) {
                temp = array[j];
                array[j] = array[j + 1];
                array[j + 1] = temp;
            }
        }
    }
}

int findIndex(float* array, int arrayLength, float searchedTarget, float maxDiff){
    int i;
    for(i = 0; i < arrayLength; i++){
        if(array[i] - searchedTarget <= maxDiff){
            return i;
        }
    }
    
    return -1;    
}

int isEquals(int dimension1, int dimension2, int maxDifference){
    return abs(dimension1 - dimension2) <= maxDifference;
}

int isArraysEquals(int* dimension1, int* dimension2, int arraysLength, int maxDifference){
    
    int eq = 1;
    int i;
    for(i = 0; i < arraysLength; i++){
        eq &= isEquals(dimension1[i], dimension2[i], maxDifference);
    }
    
    return eq;
}