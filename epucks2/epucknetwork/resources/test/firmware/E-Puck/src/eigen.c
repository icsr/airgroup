#include <math.h>
#include <float.h> 
#include <stdio.h>
#include <stdlib.h>

#ifndef _COMPLEX
#define _COMPLEX
#include "complex.h"
#endif

#ifndef _UTIL
#define _UTIL
#include "utility.h"
#endif

/***********************************************************
 *  This eigen() routine works for eigenvalue/vector analysis
 *         for real general square matrix A
 *         A will be destroyed
 *         rr,ri are vectors containing eigenvalues
 *         vr,vi are matrices containing (right) eigenvectors
 *
 *              A*[vr+vi*i] = [vr+vi*i] * diag{rr+ri*i}
 *
 *  Algorithm: Handbook for Automatic Computation, vol 2
 *             by Wilkinson and Reinsch, 1971
 *             most of source codes were taken from a public domain
 *             solftware called MATCALC.
 *  Credits:   to the authors of MATCALC
 *
 *  return     -1 not converged
 *              0 no complex eigenvalues/vectors
 *              1 complex eigenvalues/vectors
 *  Tianlin Wang at University of Illinois
 *  Thu May  6 15:22:31 CDT 1993
 ***************************************************************/

/* grabbed by N. Galtier november 2000 */
/* A starts from zero, coded as a vector */
/* As a substitution matrix A requires that lines sum to 1unity */
/* i.e. sum_over_i {A[pos(i,j,n) = 1 for any j. */
/* A[pos(i,j,n) is the i to j substitution rate */

/* eigen returns right eigen vectors v's, so that: */
/* A = v.diag(r).v-1 , where diag(r) is the diagonal */
/* matrix of eigenvalues, r is the matrix of right */
/* eigen vectors, and r-1 is the inverse of r */

#define square(a) ((a)*(a))
#define FOR(i,n) for(i=0; i<n; i++)
#define FPN(file) fputc('\n', file)

#define BASE        2    /* base of floating point arithmetic */
#define DIGITS     40    /* no. of digits to the base BASE in the fraction */

#define MAXITER    30    /* max. no. of iterations to converge */

#define pos(i,j,n)      ((i)*(n)+(j))

int eigen_perform(int job, float A[], int n, float rr[], float ri[],
		float vr[], float w[]);
void balance(float mat[], int n, int *low, int *hi, float scale[]);
void unbalance(int n, float vr[], int low, int hi, float scale[]);
int realeig(int job, float mat[], int n, int low, int hi, float valr[],
		float vali[], float vr[]);
void elemhess(int job, float mat[], int n, int low, int hi, float vr[],
		 int work[]);

int eigen_perform(int job, float A[], int n, float rr[], float ri[],
		float vr[], float work[]) {
	/* job=0: eigen values only
	 1: both eigen values and eigen vectors
	 float w[n*2]: work space
	 */
	int low, hi, i, j, k, it, istate = 0;
	float tiny = sqrt(pow((float) BASE, (float) (1 - DIGITS))), t;

	balance(A, n, &low, &hi, work);
	elemhess(job, A, n, low, hi, vr, (int*) (work + n));
	if (-1 == realeig(job, A, n, low, hi, rr, ri, vr))
		return (-1);
	if (job)
		unbalance(n, vr, low, hi, work);

	float tmp;
	/* sort, added by Z. Yang */
	for (i = 0; i < n; i++) {
		for (j = i + 1, it = i, t = rr[i]; j < n; j++)
			if (t < rr[j]) {
				t = rr[j];
				it = j;
			}
		rr[it] = rr[i];
		rr[i] = t;
		t = ri[it];
		ri[it] = ri[i];
		ri[i] = t;
		for (k = 0; k < n; k++) {
			t = vr[k * n + it];
			vr[k * n + it] = vr[k * n + i];
			vr[k * n + i] = t;
		}
		if (fabs(ri[i]) > tiny)
			istate = 1;
	}

	for(i = 0; i < n; i++){
		for(j = i; j < n; j++){
			tmp = vr[pos(i,j,n)];
					vr[pos(i,j,n)] = vr[pos(j,i,n)];
					vr[pos(j,i,n)] = tmp;
		}
	}

	return (istate);
}

void balance(float mat[], int n, int *low, int *hi, float scale[]) {
	/* Balance a matrix for calculation of eigenvalues and eigenvectors
	 */
	float c, f, g, r, s;
	int i, j, k, l, done;
	/* search for rows isolating an eigenvalue and push them down */
	for (k = n - 1; k >= 0; k--) {
		for (j = k; j >= 0; j--) {
			for (i = 0; i <= k; i++) {
				if (i != j && fabs(mat[pos(j, i, n)]) > DBL_MIN)
					break;
			}

			if (i > k) {
				scale[k] = j;

				if (j != k) {
					for (i = 0; i <= k; i++) {
						c = mat[pos(i, j, n)];
						mat[pos(i, j, n)] = mat[pos(i, k, n)];
						mat[pos(i, k, n)] = c;
					}

					for (i = 0; i < n; i++) {
						c = mat[pos(j, i, n)];
						mat[pos(j, i, n)] = mat[pos(k, i, n)];
						mat[pos(k, i, n)] = c;
					}
				}
				break;
			}
		}
		if (j < 0)
			break;
	}

	/* search for columns isolating an eigenvalue and push them left */

	for (l = 0; l <= k; l++) {
		for (j = l; j <= k; j++) {
			for (i = l; i <= k; i++) {
				if (i != j && fabs(mat[pos(i, j, n)]) > DBL_MIN)
					break;
			}
			if (i > k) {
				scale[l] = j;
				if (j != l) {
					for (i = 0; i <= k; i++) {
						c = mat[pos(i, j, n)];
						mat[pos(i, j, n)] = mat[pos(i, l, n)];
						mat[pos(i, l, n)] = c;
					}

					for (i = l; i < n; i++) {
						c = mat[pos(j, i, n)];
						mat[pos(j, i, n)] = mat[pos(l, i, n)];
						mat[pos(l, i, n)] = c;
					}
				}

				break;
			}
		}

		if (j > k)
			break;
	}

	*hi = k;
	*low = l;

	/* balance the submatrix in rows l through k */

	for (i = l; i <= k; i++) {
		scale[i] = 1;
	}

	do {
		for (done = 1, i = l; i <= k; i++) {
			for (c = 0, r = 0, j = l; j <= k; j++) {
				if (j != i) {
					c += fabs(mat[pos(j, i, n)]);
					r += fabs(mat[pos(i, j, n)]);
				}
			}

			if (fabs(c) > DBL_MIN && fabs(r) > DBL_MIN) {
				g = r / BASE;
				f = 1;
				s = c + r;

				while (c < g) {
					f *= BASE;
					c *= BASE * BASE;
				}

				g = r * BASE;

				while (c >= g) {
					f /= BASE;
					c /= BASE * BASE;
				}

				if ((c + r) / f < 0.95 * s) {
					done = 0;
					g = 1 / f;
					scale[i] *= f;

					for (j = l; j < n; j++) {
						mat[pos(i, j, n)] *= g;
					}

					for (j = 0; j <= k; j++) {
						mat[pos(j, i, n)] *= f;
					}
				}
			}
		}
	} while (!done);
}

/*
 * Transform back eigenvectors of a balanced matrix
 * into the eigenvectors of the original matrix
 */
void unbalance(int n, float vr[], int low, int hi, float scale[]) {
	int i, j, k;
	float tmp;

	for (i = low; i <= hi; i++) {
		for (j = 0; j < n; j++) {
			vr[pos(i, j, n)] *= scale[i];

		}
	}

	for (i = low - 1; i >= 0; i--) {
		if ((k = (int) scale[i])) {
			for (j = 0; j < n; j++) {
				tmp = vr[pos(i, j, n)];
				vr[pos(i, j, n)] = vr[pos(k, j, n)];
				vr[pos(k, j, n)] = tmp;
			}
		}
	}

	for (i = hi + 1; i < n; i++) {
		if ((k = (int) scale[i]) != i) {
			for (j = 0; j < n; j++) {
				tmp = vr[pos(i, j, n)];
				vr[pos(i, j, n)] = vr[pos(k, j, n)];
				vr[pos(k, j, n)] = tmp;
			}
		}
	}
}

/*
 * Reduce the submatrix in rows and columns low through hi of real matrix mat to
 * Hessenberg form by elementary similarity transformations
 */
void elemhess(int job, float mat[], int n, int low, int hi, float vr[],
		 int work[]) {
	/* work[n] */
	int i, j, m;
	float x, y;

	for (m = low + 1; m < hi; m++) {
		for (x = 0, i = m, j = m; j <= hi; j++) {
			if (fabs(mat[pos(j, m - 1, n)]) > fabs(x)) {
				x = mat[pos(j, m - 1, n)];
				i = j;
			}
		}

		if ((work[m] = i) != m) {
			for (j = m - 1; j < n; j++) {
				y = mat[pos(i, j, n)];
				mat[pos(i, j, n)] = mat[pos(m, j, n)];
				mat[pos(m, j, n)] = y;
			}

			for (j = 0; j <= hi; j++) {
				y = mat[pos(j, i, n)];
				mat[pos(j, i, n)] = mat[pos(j, m, n)];
				mat[pos(j, m, n)] = y;
			}
		}

		if (fabs(x) > DBL_MIN) {
			for (i = m + 1; i <= hi; i++) {
				if (fabs(y = mat[pos(i, m - 1, n)]) > DBL_MIN) {
					y = mat[pos(i, m - 1, n)] = y / x;

					for (j = m; j < n; j++) {
						mat[pos(i, j, n)] -= y * mat[pos(m, j, n)];
					}

					for (j = 0; j <= hi; j++) {
						mat[pos(j, m, n)] += y * mat[pos(j, i, n)];
					}
				}
			}
		}
	}
	if (job) {
		for (i = 0; i < n; i++) {
			for (j = 0; j < n; j++) {
				vr[pos(i, j, n)] = 0.0;
			}
			vr[pos(i, i, n)] = 1.0;
		}

		for (m = hi - 1; m > low; m--) {
			for (i = m + 1; i <= hi; i++) {
				vr[pos(i, m, n)] = mat[pos(i, m - 1, n)];
			}

			if ((i = work[m]) != m) {
				for (j = m; j <= hi; j++) {
					vr[pos(m, j, n)] = vr[pos(i, j, n)];
					vr[pos(i, j, n)] = 0.0;
				}
				vr[pos(i, m, n)] = 1.0;
			}
		}
	}
}

/*
 * Calculate eigenvalues and eigenvectors of a real upper Hessenberg matrix
 * Return 1 if converges successfully and 0 otherwise
 */

int realeig(int job, float mat[], int n, int low, int hi, float valr[],
		float vali[], float vr[]) {

	float p = .0, q = .0, r = .0, s = .0, t, w, x, y, z = 0, ra, sa, norm, eps;
	int niter, en, i, j, k, l, m;
	float precision = pow((float) BASE, (float) (1 - (int) DIGITS));

	eps = precision;
	for (i = 0; i < n; i++) {
		valr[i] = 0.0;
		vali[i] = 0.0;
	}
	/* store isolated roots and calculate norm */
	for (norm = 0, i = 0; i < n; i++) {
		for (j = max(0, i - 1); j < n; j++) {
			norm += fabs(mat[pos(i, j, n)]);
		}
		if (i < low || i > hi)
			valr[i] = mat[pos(i, i, n)];
	}
	t = 0;
	en = hi;

	while (en >= low) {
		niter = 0;
		for (;;) {

			/* look for single small subdiagonal element */

			for (l = en; l > low; l--) {
				s = fabs(mat[pos(l - 1, l - 1, n)]) + fabs(mat[pos(l, l, n)]);
				if (fabs(s) < DBL_MIN)
					s = norm;
				if (fabs(mat[pos(l, l - 1, n)]) <= eps * s)
					break;
			}

			/* form shift */

			x = mat[pos(en, en, n)];

			if (l == en) { /* one root found */
				valr[en] = x + t;
				if (job)
					mat[pos(en, en, n)] = x + t;
				en--;
				break;
			}

			y = mat[pos(en - 1, en - 1, n)];
			w = mat[pos(en, en - 1, n)] * mat[pos(en - 1, en, n)];

			if (l == en - 1) { /* two roots found */
				p = (y - x) / 2;
				q = p * p + w;
				z = sqrt(fabs(q));
				x += t;
				if (job) {
					mat[pos(en, en, n)] = x;
					mat[pos(en - 1, en - 1, n)] = y + t;
				}
				if (q < 0) { /* complex pair */
					valr[en - 1] = x + p;
					vali[en - 1] = z;
					valr[en] = x + p;
					vali[en] = -z;
				} else { /* real pair */
					z = (p < 0) ? p - z : p + z;
					valr[en - 1] = x + z;
					valr[en] = (fabs(z) < DBL_MIN) ? x + z : x - w / z;
					if (job) {
						x = mat[pos(en, en - 1, n)];
						s = fabs(x) + fabs(z);
						p = x / s;
						q = z / s;
						r = sqrt(p * p + q * q);
						p /= r;
						q /= r;
						for (j = en - 1; j < n; j++) {
							z = mat[pos(en - 1, j, n)];
							mat[pos(en - 1, j, n)] = q * z
									+ p * mat[pos(en, j, n)];
							mat[pos(en, j, n)] = q * mat[pos(en, j, n)] - p * z;
						}
						for (i = 0; i <= en; i++) {
							z = mat[pos(i, en - 1, n)];
							mat[pos(i, en - 1, n)] = q * z
									+ p * mat[pos(i, en, n)];
							mat[pos(i, en, n)] = q * mat[pos(i, en, n)] - p * z;
						}
						for (i = low; i <= hi; i++) {
							z = vr[pos(i, en - 1, n)];
							vr[pos(i, en - 1, n)] = q * z
									+ p * vr[pos(i, en, n)];
							vr[pos(i, en, n)] = q * vr[pos(i, en, n)] - p * z;
						}
					}
				}
				en -= 2;
				break;
			}
			if (niter == MAXITER)
				return (-1);
			if (niter != 0 && niter % 10 == 0) {
				t += x;
				for (i = low; i <= en; i++)
					mat[pos(i, i, n)] -= x;
				s = fabs(mat[pos(en, en - 1, n)])
						+ fabs(mat[pos(en - 1, en - 2, n)]);
				x = y = 0.75 * s;
				w = -0.4375 * s * s;
			}
			niter++;
			/* look for two consecutive small subdiagonal elements */
			for (m = en - 2; m >= l; m--) {
				z = mat[pos(m, m, n)];
				r = x - z;
				s = y - z;
				p = (r * s - w) / mat[pos(m + 1, m, n)] + mat[pos(m, m + 1, n)];
				q = mat[pos(m + 1, m + 1, n)] - z - r - s;
				r = mat[pos(m + 2, m + 1, n)];
				s = fabs(p) + fabs(q) + fabs(r);
				p /= s;
				q /= s;
				r /= s;
				if (m == l
						|| fabs(mat[pos(m, m - 1, n)]) * (fabs(q) + fabs(r))
								<= eps
										* (fabs(mat[pos(m - 1, m - 1, n)])
												+ fabs(z)
												+ fabs(
														mat[pos(m + 1, m + 1, n)]))
										* fabs(p))
					break;
			}
			for (i = m + 2; i <= en; i++)
				mat[pos(i, i - 2, n)] = 0;
			for (i = m + 3; i <= en; i++)
				mat[pos(i, i - 3, n)] = 0;
			/* float QR step involving rows l to en and columns m to en */
			for (k = m; k < en; k++) {
				if (k != m) {
					p = mat[pos(k, k - 1, n)];
					q = mat[pos(k + 1, k - 1, n)];
					r = (k == en - 1) ? 0 : mat[pos(k + 2, k - 1, n)];
					if (fabs(x = fabs(p) + fabs(q) + fabs(r)) < DBL_MIN)
						continue;
					p /= x;
					q /= x;
					r /= x;
				}
				s = sqrt(p * p + q * q + r * r);
				if (p < 0)
					s = -s;
				if (k != m) {
					mat[pos(k, k - 1, n)] = -s * x;
				} else if (l != m) {
					mat[pos(k, k - 1, n)] = -mat[pos(k, k - 1, n)];
				}
				p += s;
				x = p / s;
				y = q / s;
				z = r / s;
				q /= p;
				r /= p;
				/* row modification */
				for (j = k; j <= (!job ? en : n - 1); j++) {
					p = mat[pos(k, j, n)] + q * mat[pos(k + 1, j, n)];
					if (k != en - 1) {
						p += r * mat[pos(k + 2, j, n)];
						mat[pos(k + 2, j, n)] -= p * z;
					}
					mat[pos(k + 1, j, n)] -= p * y;
					mat[pos(k, j, n)] -= p * x;
				}
				j = min(en, k + 3);
				/* column modification */
				for (i = (!job ? l : 0); i <= j; i++) {
					p = x * mat[pos(i, k, n)] + y * mat[pos(i, k + 1, n)];
					if (k != en - 1) {
						p += z * mat[pos(i, k + 2, n)];
						mat[pos(i, k + 2, n)] -= p * r;
					}
					mat[pos(i, k + 1, n)] -= p * q;
					mat[pos(i, k, n)] -= p;
				}
				if (job) { /* accumulate transformations */
					for (i = low; i <= hi; i++) {
						p = x * vr[pos(i, k, n)] + y * vr[pos(i, k + 1, n)];
						if (k != en - 1) {
							p += z * vr[pos(i, k + 2, n)];
							vr[pos(i, k + 2, n)] -= p * r;
						}
						vr[pos(i, k + 1, n)] -= p * q;
						vr[pos(i, k, n)] -= p;
					}
				}
			}
		}
	}

	if (!job)
		return (0);
	if (fabs(norm) > DBL_MIN) {
		/* back substitute to find vectors of upper triangular form */
		for (en = n - 1; en >= 0; en--) {
			p = valr[en];
			if ((q = vali[en]) < 0) { /* complex vector */
				m = en - 1;
				if (fabs(mat[pos(en, en - 1, n)])
						> fabs(mat[pos(en - 1, en, n)])) {
					mat[pos(en - 1, en - 1, n)] = q / mat[pos(en, en - 1, n)];
					mat[pos(en - 1, en, n)] = (p - mat[pos(en, en, n)])
							/ mat[pos(en, en - 1, n)];
				} else {
					float* v = divideComplexes(toComplex(0, -mat[pos(en - 1, en, n)]), toComplex(mat[pos(en - 1, en - 1, n)] - p, q));
					mat[pos(en - 1, en - 1, n)] = v[0];
					mat[pos(en - 1, en, n)] = v[1];
				}
				mat[pos(en, en - 1, n)] = 0;
				mat[pos(en, en, n)] = 1;
				for (i = en - 2; i >= 0; i--) {
					w = mat[pos(i, i, n)] - p;
					ra = 0;
					sa = mat[pos(i, en, n)];
					for (j = m; j < en; j++) {
						ra += mat[pos(i, j, n)] * mat[pos(j, en - 1, n)];
						sa += mat[pos(i, j, n)] * mat[pos(j, en, n)];
					}
					if (vali[i] < 0) {
						z = w;
						r = ra;
						s = sa;
					} else {
						m = i;
						if (fabs(vali[i]) < DBL_MIN) {
							float* v = divideComplexes(toComplex(-ra, -sa), toComplex(w, q));
							mat[pos(i, en - 1, n)] = v[0];
							mat[pos(i, en, n)] = v[1];
						} else { /* solve complex equations */
							x = mat[pos(i, i + 1, n)];
							y = mat[pos(i + 1, i, n)];
							float* v = toComplex((valr[i] - p) * (valr[i] - p)
									+ vali[i] * vali[i] - q * q, (valr[i] - p) * 2 * q);
							if (fabs(v[0]) + fabs(v[1]) < DBL_MIN) {
								v[0] = eps * norm
										* (fabs(w) + fabs(q) + fabs(x) + fabs(y)
												+ fabs(z));
							}
							v = divideComplexes(toComplex(x * r - z * ra + q * sa,
									x * s - z * sa - q * ra), v);

							mat[pos(i, en - 1, n)] = v[0];
							mat[pos(i, en, n)] = v[1];
							if (fabs(x) > fabs(z) + fabs(q)) {
								mat[pos(i + 1, en - 1, n)] = (-ra
										- w * mat[pos(i, en - 1, n)]
										+ q * mat[pos(i, en, n)]) / x;
								mat[pos(i + 1, en, n)] = (-sa
										- w * mat[pos(i, en, n)]
										- q * mat[pos(i, en - 1, n)]) / x;
							} else {
								v = divideComplexes(toComplex(-r - y * mat[pos(i, en - 1, n)],
										-s - y * mat[pos(i, en, n)]), toComplex(z, q));
								mat[pos(i + 1, en - 1, n)] = v[0];
								mat[pos(i + 1, en, n)] = v[1];
							}
						}
					}
				}
			} else if (fabs(q) < DBL_MIN) { /* real vector */
				m = en;
				mat[pos(en, en, n)] = 1;
				for (i = en - 1; i >= 0; i--) {
					w = mat[pos(i, i, n)] - p;
					r = mat[pos(i, en, n)];
					for (j = m; j < en; j++) {
						r += mat[pos(i, j, n)] * mat[pos(j, en, n)];
					}
					if (vali[i] < 0) {
						z = w;
						s = r;
					} else {
						m = i;
						if (fabs(vali[i]) < DBL_MIN) {
							if (fabs(t = w) < DBL_MIN)
								t = eps * norm;
							mat[pos(i, en, n)] = -r / t;
						} else { /* solve real equations */
							x = mat[pos(i, i + 1, n)];
							y = mat[pos(i + 1, i, n)];
							q = (valr[i] - p) * (valr[i] - p)
									+ vali[i] * vali[i];
							t = (x * s - z * r) / q;
							mat[pos(i, en, n)] = t;
							if (fabs(x) <= fabs(z)) {
								mat[pos(i + 1, en, n)] = (-s - y * t) / z;
							} else {
								mat[pos(i + 1, en, n)] = (-r - w * t) / x;
							}
						}
					}
				}
			}
		}
		/* vectors of isolated roots */
		for (i = 0; i < n; i++) {
			if (i < low || i > hi) {
				for (j = i; j < n; j++) {
					vr[pos(i, j, n)] = mat[pos(i, j, n)];
				}
			}
		}
		/* multiply by transformation matrix */

		for (j = n - 1; j >= low; j--) {
			m = min(j, hi);
			for (i = low; i <= hi; i++) {
				for (z = 0, k = low; k <= m; k++) {
					z += vr[pos(i, k, n)] * mat[pos(k, j, n)];
				}
				vr[pos(i, j, n)] = z;
			}
		}
	}
	/* rearrange complex eigenvectors */
	for (j = 0; j < n; j++) {
		if (fabs(vali[j]) > DBL_MIN) {
			for (i = 0; i < n; i++) {
				vr[pos(i, j + 1, n)] = vr[pos(i, j, n)];
			}
			j++;
		}
	}
	return (0);
}

void eigen(float *matrix, int matrixDimension) {

	if (matrixDimension > MAX_EPUCKS) {
		return;
	}
    
    clearFloatArray(eigen_vectors_real, MAX_EPUCKS*MAX_EPUCKS);
    clearFloatArray(eigen_values_real, MAX_EPUCKS);
    clearFloatArray(eigen_values_imag, MAX_EPUCKS);

	eigen_perform(1, matrix, matrixDimension, eigen_values_real,
			eigen_values_imag, eigen_vectors_real,
			algWorkspace);

}
