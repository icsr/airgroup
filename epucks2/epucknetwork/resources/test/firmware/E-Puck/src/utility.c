#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <assert.h>
#include <math.h>

#ifndef _UTIL
#define _UTIL
#include "utility.h"

// Utility

/*
 * algorithm.c
 *
 *  Created on: 10 de set de 2019
 *      Author: israelicsr
 */


int isTargetPostureReached(int maxDifference) {
    return abs(targetX - *currentX) <= maxDifference && abs(targetY - *currentY) <= maxDifference;
}

int minValue(int* array, int size) {
    int val = array[0];
    int i;
    for (i = 1; i < size; ++i) {
        if (array[i] < val) {
            val = array[i];
        }
    }
    return val;
}

void reverse(char *str, int len) {
    int i = 0, j = len - 1, temp;
    while (i < j) {
        temp = str[i];
        str[i] = str[j];
        str[j] = temp;
        i++;
        j--;
    }
}

// Converts a given integer x to string str[].  d is the number
// of digits required in output. If d is more than the number
// of digits in x, then 0s are added at the beginning.

int intToStr(int x, char str[], int d) {
    int i = 0;
    while (x) {
        str[i++] = (x % 10) + '0';
        x = x / 10;
    }
    // If number of digits required is more, then
    // add 0s at the beginning
    while (i < d)
        str[i++] = '0';

    reverse(str, i);
    str[i] = '\0';
    return i;
}

// Converts a floating point number to string.

void ftoa(float n, char *res, int afterpoint) {
    // Extract integer part
    int ipart = (int) n;
    // Extract floating part
    float fpart = n - (float) ipart;
    // convert integer part to string
    int i = intToStr(ipart, res, 0);

    // check for display option after point
    if (afterpoint != 0) {
        res[i] = '.'; // add dot

        // Get the value of fraction part upto given no.
        // of points after dot. The third parameter is needed
        // to handle cases like 233.007
        fpart = fpart * pow(10, afterpoint);

        intToStr((int) fpart, res + i + 1, afterpoint);
    }
}

int roundFloatToInt(float number) {
    int aux1 = 10 * abs((int) number);
    int aux2 = abs((int) (10 * number));

    int aux3 = abs((int) number);
    if (aux2 - aux1 >= 5) {
        aux3++;
    }

    if (number < 0) {
        aux3 = -aux3;
    }

    return aux3;
}

int correctAngle(int angle) {
    if (abs(angle) > 360) {
        if (angle > 0) {
            while (abs(angle) > 360) {
                angle -= 360;
            }
        } else {
            while (abs(angle) > 360) {
                angle += 360;
            }
        }
    }

    int signal = 1;
    if (abs(angle) > 180) {
        if (angle > 0) {
            signal = -1;
        }
        angle = signal * (360 - abs(angle));
    }

    return angle;
}

void clearCharArray(char* array, int arrayLength) {
    int j;
    for (j = 0; j < arrayLength; j++) {
        array[j] = '\0';
    }
}

void clearFloatArray(float* array, int arrayLength) {
    int j;
    for (j = 0; j < arrayLength; j++) {
        array[j] = 0;
    }
}

void clearFloatMatrix(float* matrix, int rows, int cols) {
    int i;
    for (i = 0; i < rows; i++) {
        float* p = matrix + cols * i;
        clearFloatArray(p, cols);
    }
}

void clearIntMatrix(int* matrix, int rows, int cols) {
    int i;
    for (i = 0; i < rows; i++) {
        int* p = matrix + cols * i;
        clearIntArray(p, cols);
    }
}

void clearIntArray(int* array, int arrayLength) {
    int j;
    for (j = 0; j < arrayLength; j++) {
        array[j] = 0;
    }
}

float coth(float x) {
    return (expl(x) + expl(-x)) / (expl(x) - expl(-x));
}

float distance(int x1, int y1, int x2, int y2) {
    return sqrtl(powl(x1 - x2, 2) + powl(y1 - y2, 2));
}

void copyArray(float* src, float* dest, int fromPosition, int size) {
    int i;
    for (i = 0; i < size; i++) {
        dest[i] = src[fromPosition * size + i];
    }
}

float dotProduct(float* v1, float* v2, int arraySize) {
    float result = 0;

    int i;
    for (i = 0; i < arraySize; i++) {
        result += v1[i] * v2[i];
    }

    return result;
}

void subtract(float* result, float* v1, float* v2, int arraySize) {

    int i;
    for (i = 0; i < arraySize; i++) {
        result[i] = v1[i] - v2[i];
    }
}

void sum(float* result, float* v1, float* v2, int arraySize) {

    int i;
    for (i = 0; i < arraySize; i++) {
        result[i] = v1[i] + v2[i];
    }
}

void divideScalar(float* result, float* origin, float scalar, int arraySize) {
    multiplyScalar(result, origin, 1/scalar, arraySize);
}

void multiplyScalar(float* result, float* v, float scalar, int arraySize) {
    int i;
    for (i = 0; i < arraySize; i++) {
        result[i] = v[i] * scalar;
    }
}

int isNaN(int number){
	return number == NaN || number == NAN;
}


#endif

