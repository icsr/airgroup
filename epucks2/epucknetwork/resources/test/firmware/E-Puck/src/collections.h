void bubbleSort(float *array, int arrayLength);

int findIndex(float* array, int arrayLength, float searchedTarget, float maxDiff);

int isEquals(int dimension1, int dimension2, int maxDifference);

int isArraysEquals(int* dimension1, int* dimension2, int arraysLength, int maxDifference);