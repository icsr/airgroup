/*
 ============================================================================
 Name        : E-Puck.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

#ifndef _EIGEN
#define _EIGEN
#include "eigen.h"
#endif

#ifndef _UTIL
#define _UTIL
#include "utility.h"
#endif

#ifndef _CONNECTIVITY
#define _CONNECTIVITY
#include "connectivityMaintenance.h"
#endif

int range = 10;

int neighborsSize = 0;

int qtyOfKnownEpucks = 0;
int qtyOfNeighbors = 0;
float secondEigenValue = -1;
float gains[4] = {1,1,1,1};
float* secondEigenVector = &allV2i[0];

void imprimirFloatMatrix(float* matrix, int a, int b, int c);
void imprimirIntMatrix(int* matrix, int lines, int cols, int maxCols);

int main(void) {
	qtyOfKnownEpucks = 6;
	qtyOfNeighbors = qtyOfKnownEpucks - 1;

	allPositions[0][0] = -10;
	allPositions[0][1] = 0;

	allPositions[1][0] = 10;
	allPositions[1][1] = 0;

	allPositions[2][0] = -15;
	allPositions[2][1] = 15;

	allPositions[3][0] = 15;
	allPositions[3][1] = 15;

	allPositions[4][0] = -15;
	allPositions[4][1] = -15;

	allPositions[5][0] =  15;
	allPositions[5][1] = -15;

	calculateDegreesDiagAndLaplacian();

//	imprimirIntMatrix(allPositions[0], qtyOfKnownEpucks, 2, 2);
	imprimirFloatMatrix(weights[0], qtyOfKnownEpucks, qtyOfKnownEpucks, MAX_EPUCKS);
	imprimirFloatMatrix(laplacian[0], qtyOfKnownEpucks, qtyOfKnownEpucks, MAX_EPUCKS);

	calculateSecondEigens();

	imprimirFloatMatrix(eigen_values_real, qtyOfKnownEpucks, 1, 1);
	printf("sev: %f\n\n", secondEigenValue);

	imprimirFloatMatrix(allV2i, qtyOfKnownEpucks, 1, 1);

	evaluateControl();

	imprimirFloatMatrix(algWorkspace, 1, 2, 2);

	return EXIT_SUCCESS;
}

void imprimirIntMatrix(int* matrix, int lines, int cols, int maxCols){
	printf("---------------\n");
	for(int i = 0; i < lines; i++){
		for(int j = 0; j < cols; j++){
			printf("%d", *(matrix + i*maxCols + j));
			if(j < qtyOfKnownEpucks - 1){
				printf("\t");
			}
		}
		printf("\n");
	}
	printf("---------------\n");
}

void imprimirFloatMatrix(float* matrix, int lines, int cols, int maxCols){
	printf("---------------\n");
	for(int i = 0; i < lines; i++){
		for(int j = 0; j < cols; j++){
			printf("%f", *(matrix + i*maxCols + j));
			if(j < qtyOfKnownEpucks - 1){
				printf("\t");
			}
		}
		printf("\n");
	}
	printf("---------------\n");
}


