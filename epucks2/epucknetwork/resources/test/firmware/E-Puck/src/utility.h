//algorithm.h

#ifndef max
#define	max(a,b)	(((a) > (b)) ? (a) : (b))
#endif

#ifndef min
#define	min(a,b)	(((a) < (b)) ? (a) : (b))
#endif


int isNaN(int number);

#define NaN -2000000

#ifndef NAN
#define NAN -2000000
#endif

#define ID_RANDOM_WAYPOINTS 1
#define ID_CONNECTIVITY_MAINTENANCE 2
#define ID_CONNECTIVITY_MAINTENANCE_AND_MATLAB_CONTROLS_COMBINED 3
#define ID_MATLAB_CONTROL_ONLY 4
#define MAX_EPUCKS 10
#define MAX_WAITING_TIME_IN_CLOCKS 10000000
#define MIN_TURNING_TO_AVOID_COLLISION 5

extern int globalRunningAlgorithm;
extern int allPositions[MAX_EPUCKS][2];
extern float allV2i[MAX_EPUCKS];

extern float u_m[2];
extern float paramsConnectivity[2];

extern int qtyOfKnownEpucks;
extern int qtyOfNeighbors;

extern int receivedNeighbors;

extern int receivedNeighborsAtLeastOnce;

extern float laplacian[MAX_EPUCKS][MAX_EPUCKS];
extern float weights[MAX_EPUCKS][MAX_EPUCKS];
extern float secondEigenValue;
extern float* secondEigenVector;

extern float algWorkspace[2*MAX_EPUCKS];
extern float eigen_vectors_real[MAX_EPUCKS*MAX_EPUCKS];
extern float eigen_values_real[MAX_EPUCKS];
extern float eigen_values_imag[MAX_EPUCKS];

extern float connectivityThreshold;
extern float algebraicConnectivityLowerBound;

extern float sigmaSquared;

//

#define FW_VERSION "v2018-11-6-21:50"

#define MAX_BATT_VALUE 2560 // corresponds to 4.2 volts
#define MIN_BATT_VALUE 2070 // corresponds to 3.4 volts
#define BATT_VALUES_RANGE (MAX_BATT_VALUE-MIN_BATT_VALUE)

#define MIN_AXIAL_DISLOCATION 1

#define MAX_AXIAL_DISLOCATION 4

#define BUFFER_SIZE_INPUT ((MAX_EPUCKS - 1) * 18 + 35 + 100)

#define BUFFER_SIZE_OUTPUT 75

#define BODY_RADIUS 3.5

#define BLINK_TIME_MILS 400

#define NaN -2000000

#define PI 3.14159265 //Pi

extern int* currentX;     //keeps current x position
extern int* currentY;      //keeps current y position
extern int currentAngle;  //keeps current angle
extern int targetX;
extern int targetY;
extern int targetAngle;
extern int range;

extern float algebraicConnectivityLowerBound;
extern int minAxialDislocation;

extern int maxAxialDislocation;

extern int globalAvoidingCollision;

extern int globalTargetSet;

extern int globalArenaSet;

extern unsigned int e_last_acc_scan_id;
extern unsigned int tickAdcIsr;

int minValue(int* array, int size);

void ftoa(float n, char *res, int afterpoint);
int countArray(int* array);
void removeItem(int *array, int item);

void sleep(long timeInMils);

int roundFloatToInt(float number);

float roundFloat(float number);

int getselector();

int correctAngle(int angle);

void clearCharArray(char* array, int arrayLength) ;

void clearFloatArray(float* array, int arrayLength) ;

void clearIntArray(int* array, int arrayLength);

void clearIntMatrix(int* matrix, int rows, int cols);

void clearFloatMatrix(float* matrix, int rows, int cols) ;

int isTargetPostureReached(int maxDifference);

void blinkLED(int numberOfTimes);

int randomize(int minValue, int maxValue);

float coth(float x);

float distance (int x1, int y1, int x2, int y2);

float dotProduct(float* v1, float* v2, int arraySize);


void subtract(float* result, float* v1, float* v2, int arraySize);

void sum(float* result, float* v1, float* v2, int arraySize);

void multiplyScalar(float* result, float* v, float scalar, int arraySize);

void divideScalar(float* result, float* origin, float scalar, int arraySize);

int isNaN(int number);


#define ID_RANDOM_WAYPOINTS 1
#define ID_CONNECTIVITY_MAINTENANCE 2
#define ID_CONNECTIVITY_MAINTENANCE_AND_MATLAB_CONTROLS_COMBINED 3
#define ID_MATLAB_CONTROL_ONLY 4
#define MAX_EPUCKS 10
#define MAX_WAITING_TIME_IN_CLOCKS 10000000
#define MIN_TURNING_TO_AVOID_COLLISION 5

int allPositions[MAX_EPUCKS][2];
float allV2i[MAX_EPUCKS];


float paramsConnectivity[2];

float u_m[2];

float laplacian[MAX_EPUCKS][MAX_EPUCKS];
float weights[MAX_EPUCKS][MAX_EPUCKS];

float algWorkspace[2 * MAX_EPUCKS];
float eigen_vectors_real[MAX_EPUCKS*MAX_EPUCKS];
float eigen_values_real[MAX_EPUCKS];
float eigen_values_imag[MAX_EPUCKS];

