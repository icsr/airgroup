% filename: combinedControlLaw.m
% Purpose: executes the differencial equation for the combined control law
% during the time set
% - position - x and y coordinates for each network agent
% - goal_position - x and y goal position coordinates for each network agent regarding robustness improvement
% - centroid - x and y goal position coordinates for each network agent
% regarding coverage
% - param - parametrization struct
% - options - graph options (set the parametrization for graph properties
% computation
% Output:
% factorU - final x and y coordinates for each network agent after
% simulation time interval

function [dotxy] = performControl(position, gains, threshold, epsilon, range, bodyRadius, maxMovingDistance, performSafeControl)

precision = 5;

[param, options, tolerance] = config(gains, threshold, epsilon, range);

[nr, nc] = size(position);

[A] = initialize_matrixA(position,param,options); % matrix adjacency initialization

ac  =  algebraic_connectivity_New(A,param.normalized); % algebraic connectivity computation

% CONNECTIVITY MAINTENANCE

% if the algebraic connectivity is lower than epsion its gain is set to
% zero in order to avoid convergence problems. The algebraic connectivity mechanism ensures convergence for ac>epsilon.
% as networks face perturbation the ac can abruptally decrease lower than
% epsilon. Otherwise, param.adpGain=param.gainConnectivityController

dotxyC=zeros(nr,nc);
if param.gainConnectivityControl ~= 0 % for evaluation performance improvement
    try
        [dotxyC] = connectivity_controller(position,param,options);   % connectivity factor
    catch e
        disp('Error on Connectivity')
        disp(e)
    end
end



% % COLLISION AVOIDANCE
% dotxyO = zeros(nr,2);
% if param.gainCollisionAvoidance > 0  % if collision avoidance is active and its gain > 0
%     dotxyO = collisionAvoidance_controller(position,param); % collision avoidance
% end


% ROBUSTNESS IMPROVEMENT CONTROL LAW
dotxyR=zeros(nr,nc);

if param.gainRobustnessControl ~= 0 % for evaluation performance improvement
    try
        [goalPosition, ~] = define_goalPosition(position,param); % vN is the list of vulnerable nodes
        [dotxyR] = robustness_controller(position, goalPosition, param);  % robustness factor
     catch e       
        disp('Error on Robustness')
        disp(e)
    end
end


%  COVERAGE AREA IMPROVEMENT CONTROL LAW
dotxyA=zeros(nr,nc);

if  param.gainCoverageControl>0  % for evaluation performance improvement
    try
        [centroid] = define_centroid_local(position,options,param); % defines the controid of each node voronoi diagram cell
        [dotxyA] = coverage_controller(position,centroid,param);   % coverage factor
     catch e
        disp('Error on Coverage')
        disp(e)
    end
end

dotxyC = normalize(dotxyC);
dotxyR = normalize(dotxyR);
dotxyA = normalize(dotxyA);

dotxy = dotxyC*param.gainConnectivityControl + dotxyR*param.gainRobustnessControl + dotxyA*param.gainCoverageControl;


if(performSafeControl)
    param.maxMovingDistance = maxMovingDistance;
    param.epsilon = epsilon;
    param.bodyRadius = bodyRadius;
    param.rangeRadius = range;
    dotxy = safeControl(dotxy, position, param, options);
end

dotxy = normalize(dotxy);
dotxy = roundIt(dotxy, precision);

end