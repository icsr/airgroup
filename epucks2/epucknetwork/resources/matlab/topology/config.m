function [param, options, tolerance] = config(gains, threshold, epsilon, range)

% gain setting
param.gainConnectivityControl = gains(1);
param.gainRobustnessControl = gains(2);
param.gainCoverageControl = gains(3);

param.threshold = threshold; %algegraic connectivity control law parameter
param.epsilon = epsilon; % epsilon
param.range = 2*range;
param.sigma = sqrt((-param.range^2)/(2*log(param.threshold))); % algebraic connectivity control law parameter
param.normalized = 0;
param.k = 10;  % linear velocity
param.tol=1e-5; % used for rounding float and avoid problems in comparison methods
param.nrPaths = 2; % number of redundant paths a node should have to its 2-hop neighbors in order to be not vulnerable
param.theta=1; % if 1, a random number is generated to define the vulnerability threshold, if < 1 its value defines the vulnerability threshold

options.unweighted = 0;
tolerance = 10E-3; % tolerance for comparing float values

param.fractionIteration=0.9; % 1 if is one node, < 1 if it is a fraction of the network node at each iteration
param.failureOp = 'BC';  % type of failure

end