function obj = plotTopologyRecordingsGroup(folder, threshold, epsilon, range, arenaArea, loadDataPlot, ignoreNx, mytitle)

    if(~exist('ignoreNx', 'var'))
        ignoreNx = [];
    end

    plotTopologyRecordingsGroup2(folder, threshold, epsilon, range, arenaArea, loadDataPlot, ignoreNx, mytitle);
    load(sprintf('%s/dataPlot.mat', folder));
    
    obj.numberOfExecutions = numberOfExecutions;
    obj.numberOfGoodExecutions = numberOfExecutions - max(size(ignoreNx));
    obj.ranking = ranking;
    obj.ignoreNx = ignoreNx;
    obj.folder = folder;
    
    obj.threshold = threshold;
    obj.epsilon = epsilon;
    obj.range = range; 
    obj.arenaArea = arenaArea;
        
end