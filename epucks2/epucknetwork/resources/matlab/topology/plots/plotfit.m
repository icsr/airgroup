function plotfit(x,y, line)
    
    plot(x,y,line);
    
    k = find(~isnan(y));
    xk = x(k);
    yk = y(k);
    
    p = polyfit(xk,yk, 5);
    
    xfit = min(xk) : mean(abs(xk))/10 : max(xk);
    yfit = polyval(p, xfit);

    plot(xfit, yfit, '-');    

end