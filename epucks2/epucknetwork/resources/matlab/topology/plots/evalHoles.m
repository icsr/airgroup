function scoreH = evalHoles(pos_cell)

global outerPolygons innerPolygons;

evalPolygons();

n = max(size(pos_cell));
R = ones(size(cell2mat(pos_cell(1)),1))*20;

inHolePoints = 0;
totalPoints = 0;

r = zeros(n,1);

for a = 1 : n
    pos=[cell2mat(pos_cell(a)), R];
    
    calcArea(pos);
    
    if(isempty(outerPolygons))
        continue;
    end
    
    rangeX = min(pos(:,1)) : max(pos(:,1));
    rangeY = min(pos(:,2)) : max(pos(:,2));
    
    for i = rangeX        
        for j = rangeY       
           insideOuterPolygon = inpolygon(i, j, outerPolygons(:,1), outerPolygons(:,2)); 
           
           if(~insideOuterPolygon)
               continue;
           end
           
           inHole = 1;
           for k = 1 : size(pos,1)
                totalPoints = totalPoints + 1;
                if(pdist2([i,j], [pos(k,1), pos(k,2)]) <= R)
                    inHole = 0;
                    break;           
                end
           end
           
           if(inHole)
               inHolePoints = inHolePoints + 1;
           end
            
        end
    end
    
    if(totalPoints == 0)
        r(a) = 0;
    else
        r(a) = inHolePoints / totalPoints;
    end  
    
end

mmax = max(max(abs(r)));

scoreH = r;

end