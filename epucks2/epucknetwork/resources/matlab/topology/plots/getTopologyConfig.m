function topologyConfig = getTopologyConfig(recpath)

    pattern = 'topology\s*#[0-9]+';
    start = regexpi(recpath, pattern);
    
    idxs = start + regexpi(recpath(start:end), '/') - 1;
       
    if(max(size(idxs)) == 1)
        idxs(2) = size(recpath,2) + 1;
    end
    
    topologyConfig.label = recpath(start : idxs(1) - 1);
    topologyConfig.gain = recpath(idxs(1) + 1 : idxs(2) - 1);

end