function plotCircle(center, radius, color)

    rectangle('Position',[(center - radius) 2*radius 2*radius],'Curvature',[1 1], 'FaceColor', color)

end