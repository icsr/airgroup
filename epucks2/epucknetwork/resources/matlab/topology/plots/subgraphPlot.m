function subgraph = subgraphPlot(g)

options = set_matlab_bgl_default();
options.istrans = 0;
options.nocheck=0;
options.unweighted=1;
options.full2sparse=1;

subgraph = components(matrix(g),options);


end