function addLegend(X, Y, label, color, index,maxIndex)

    t = xticks;
    
    xText = cast(X(end) + round((index/maxIndex) * (t(2)-t(1)) / 10), 'double');
    yText = cast(Y(end), 'double');
    text(xText, yText, label, 'FontSize', 20, 'Color', color);
        
end