function [aboveValues, belowValues, meanValues] = quantitiesCounter(Y)

meanValues = mean(Y,2, 'omitnan');

for j = 1 : size(Y,1)
	aboveValues(j) = max(size(find(Y(j,:) >= meanValues(j))));
end

for j = 1 : size(Y,1)
	belowValues(j) = max(size(find(Y(j,:) < meanValues(j))));
end

aboveValues = aboveValues';
belowValues = belowValues';

end