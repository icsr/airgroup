function savePlot(h, file, fontSize, titleSize)

set(gca,'FontSize',fontSize)

set(gcf,'Color','w')

hline = findobj(gca, 'type', 'line');
set(hline,'LineWidth', 3)

htitle = get(gca, 'title');
set(htitle,'FontSize', titleSize)

set(h, 'Units', 'Normalized', 'OuterPosition', [0,0,1,1]);

set(gcf,'PaperPositionMode', 'auto');

set(gcf, 'ResizeFcn', 'resize_fcn');

resize_fcn

pause(2)

saveas(h, sprintf('%s.jpg', file));
savefig(h, sprintf('%s.fig', file));

end