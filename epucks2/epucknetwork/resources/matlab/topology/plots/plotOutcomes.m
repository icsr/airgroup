addpath('./plots/');
addpath('matlab_bgl');
addpath('matgraph');
addpath('matgraph_MIT');
addpath('general');
addpath('metrics');
addpath('voronoi');
addpath('metrics/lib/core');
addpath('metrics/lib/support');

dirk1t1 = '/media/sf_outcomes/t1/k1';
dirk1t2 = '/media/sf_outcomes/t2/k1';
dirk1t3 = '/media/sf_outcomes/t3/k1';

dirk2t1 = '/media/sf_outcomes/t1/k2';
dirk2t2 = '/media/sf_outcomes/t2/k2';
dirk2t3 = '/media/sf_outcomes/t3/k2';

dirk3t1 = '/media/sf_outcomes/t1/k3';
dirk3t3 = '/media/sf_outcomes/t3/k3';

scoreK1 = evalrec({dirk1t1, dirk1t2, dirk1t3});
scoreK2 = evalrec({dirk2t1, dirk2t2, dirk2t3});
scoreK3 = evalrec({dirk3t1, dirk3t3});

scoreFolder='/media/sf_outcomes/charts';


plotScores({scoreK1, scoreK2, scoreK3}, scoreFolder)


close all

