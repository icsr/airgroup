function s = polyfun2str(fun, dec)

    s = 'y=';
    n = max(size(fun));
    first = 1;
    for i = 1:n
        p = fun(i);
        
        p = round(10^dec*p)/10^dec;
        
        if(abs(p) < 10^-10)
            first = first + 1;
            continue
        end
        
        if(p > 0 && i > first)
            s = strcat(s, '+');
        end
      
        
        if(n-i>=2)
            x = strcat('x^',num2str(n-i));
        elseif(n-i==1)
            x = 'x';
        else
            x = '';
        end
            
        s = strcat(s,num2str(p),x);
        
        end

end