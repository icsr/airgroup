function ranking = plotOutcome(X, Y, labelX, labelY, plotTitle, recordingsGroupFolder, fitcurve, removeSpikes, lowerbound)

    COLOR_MAX_INTENSITY = 0.8;
    LINE_WIDTH = 2;
    MARKER_SIZE = 8;
    POLY_DEGREE = 15;
    
    if(~exist('fitcurve', 'var'))
       fitcurve = 1;
    end

    if(~exist('removeSpikes', 'var'))
       removeSpikes = 0;
    end

    removeSpikes = 0;
    
    close all
    numberOfExecutions = size(Y, 2);
    numberOfTakes = size(Y,1);
    
    warning('off')
    
    markers = {'+', 'o', '*', '.', 'x', 'square', 'diamond', '^', 'v', '>', '<', 'pentagram', 'hexagram', 'none'};

    hf = figure;
    hold on
    grid minor
        
    h = nan(numberOfExecutions, 1);
    ranking = zeros(numberOfExecutions,1);
    
    meanY = mean(Y,2, 'omitnan');
       
    indexLegend = 1;
    for nx = 1 : numberOfExecutions
        
        if(isnan(mean(Y(:,nx), 'includenan')))
            continue;
        end        
        
        while(1)
           r = nx / numberOfExecutions;
           g = rand;
           b = rand;
           if(g < COLOR_MAX_INTENSITY && b < COLOR_MAX_INTENSITY)
               break;
           end
        end
        
        color = [r,g,b];
        
        marker = cell2mat(markers(mod(nx, size(markers, 2))));
        
        if(removeSpikes)            
            
            V = unique(round(Y(:,nx)));
            N = histc(round(Y(:,nx)), V);
            threshold = max(N) / 5;
            k = find(N < threshold);
            
            for a = 1 : size(k,1)
               Y(round(Y(:, nx)) == V(k(a)), nx) = NaN; 
            end
            
            S = Y(:,nx);
            for i = 2 : size(S,1) - 1
                
                previous = find(~isnan(S));
                previous = max(previous(previous < i));
                
                next = find(~isnan(S));
                next = min(next(next > i));
                
                if(isempty(next) || isempty(previous))
                    continue;
                end
                
                if(S(next) == S(previous) && S(i) ~= S(next))
                    Y(i,nx) = S(next);
                end                
            end            
        end
            
        if(~fitcurve)
            lineStl = '-';
            textLegend(indexLegend) = {sprintf('# %d', indexLegend)};
            indexLegend = indexLegend + 1;
        else
            lineStl = '.';
        end
        
        
        plot(X, Y(:, nx), lineStl, 'LineWidth', LINE_WIDTH*1.2, 'Color', color, 'Marker', marker, 'MarkerSize', MARKER_SIZE/2);
        k = Y(:, nx) >= mean(Y(:,nx), 'omitnan');
        ranking(nx) = size(k(k == true),1) * 100 / numberOfTakes;
        
    end
    
    if(~fitcurve)
        legend(textLegend, 'Location', 'North', 'Orientation', 'Horizontal');
    else        
        P = polyfit(cast(X, 'double'), max(Y,[],2), POLY_DEGREE);
        eY = polyval(P, cast(X, 'double'));        
        plot(X, eY, '-', 'LineWidth', LINE_WIDTH, 'Color', 'r');
        
        addLegend(X, eY, 'Max', 'r', 1, 1);
        
        P = polyfit(cast(X, 'double'), mean(Y,2, 'omitnan'), POLY_DEGREE);
        eY = polyval(P, cast(X, 'double'));        
        plot(X, eY, '-', 'LineWidth', LINE_WIDTH, 'Color', 'b');
        addLegend(X, eY, 'Mean', 'b', 1, 1);
        
        P = polyfit(cast(X, 'double'), min(Y,[],2), POLY_DEGREE);
        eY = polyval(P, cast(X, 'double'));        
        plot(X, eY, '-', 'LineWidth', LINE_WIDTH, 'Color', [0,1,0]);
        addLegend(X, eY, 'Min', [0, 1, 0], 1, 1);
               
    end
    
    xlabel(labelX);
    ylabel(labelY);
    title(plotTitle);
    yt = yticks;
    ymean = mean(yticks);
    if abs(min(yt) - ymean) < 10^-4        
        yticks([ymean - 1, ymean, ymean + 1]);
    end
    
    labelY = regexprep(labelY, '\s|\(|\)|\%', '');
    
    savePlot(hf, sprintf('%s%s', recordingsGroupFolder, labelY));
        
    warning('on')    
        
end