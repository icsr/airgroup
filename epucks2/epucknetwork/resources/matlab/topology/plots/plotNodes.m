function plotNodes(position)
    
    close all
  
    plot(position(:,1), position(:,2), 'o')
    hold on
    grid on

    [m, ~] = size(position);
    viscircles([position(:,1), position(:,2)], 20*ones(m,1))

    axis equal
    
    hold off

end