function legappend(newLegend)

    previousLegend = legend;
    strleg = previousLegend.String;
    n = size(strleg,2);
    legs = cell(1,n);
    
    for i = 1 : n - 1
        legs(i) = {cell2mat(strleg(i))};
    end
    
    legs(n) = {newLegend};
    legend(legs);

end