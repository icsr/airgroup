function plotTopologyRecordingsGroup2(recordingsGroupFolder, threshold, epsilon, range, arenaArea, loadDataPlot, ignoreNx, mytitle)

if(~exist('ignoreNx', 'var'))
    ignoreNx = [];
end


topologyConfigLabel = strsplit(recordingsGroupFolder, '/');
topologyConfigLabel = cell2mat(topologyConfigLabel(end));

options = set_matlab_bgl_default();
options.istrans = 0;
options.nocheck=0;
options.unweighted=1;
options.full2sparse=1;

dataPlot = sprintf('%s/dataPlot.mat', recordingsGroupFolder);

%Definition

POSITION_SIZE = 2;

EPUCK_ID_COLUM = 1;
TIME_IN_MILS_COLUMN = 2;
X_COLUMN = 3;
Y_COLUMN = 4;
ON_FAIL_COLUMN = 8;
TAKE = 13;

[param, options, tolerance] = config([0,0,0], threshold, epsilon, range);

recordingsGroupFolder = strcat(recordingsGroupFolder, '/');

csvGroup = dir(strcat(recordingsGroupFolder,'20*.mat'));

ignoredFilesReport = strcat(recordingsGroupFolder,'ignoredFiles.txt');
if(~isempty(ignoreNx))
    ignoredFiles = csvGroup(ignoreNx,:);
    f = fopen(ignoredFilesReport,'a');
    for nId = 1 : size(ignoredFiles, 1)   
       fprintf(f, "%s\n", ignoredFiles(nId,:).name);  
    end
    fclose(f);
else
    if(exist(ignoredFilesReport, 'file'))
        delete(ignoredFilesReport);
    end
end

if(isempty(csvGroup))
    error('No files found');
end

[numberOfExecutions, ~] = size(csvGroup);

% read CSV
maxTakes = nan(numberOfExecutions,1);
for nx = 1:numberOfExecutions
    load(strcat(recordingsGroupFolder,csvGroup(nx).name));
    maxTakes(nx) = max(samples(:, TIME_IN_MILS_COLUMN));
end

minSample = csvGroup(maxTakes == min(maxTakes));

load(strcat(recordingsGroupFolder, minSample(1).name));

% epucks ids
ids = unique(samples(:,EPUCK_ID_COLUM));
[numberOfEpucks, ~] = size(ids);

%takes of each execution
takes = unique(samples(:,TIME_IN_MILS_COLUMN));
[numberOfTakes, ~] = size(takes);

ids = unique(samples(:, EPUCK_ID_COLUM));
[numberOfEpucks, ~] = size(ids);

redo = true;
if(exist(dataPlot, 'file'))
    if(~exist('loadDataPlot', 'var'))
        reply = input('Load saved plot Data? (Y/n)','s');
    else
        reply = loadDataPlot;
    end
    
    if(~exist('reply', 'var') || isempty(reply))
        reply = 'Y';
    end
    
    if(reply == 'Y' || reply == 'y')
        signoreNx = ignoreNx;
        load(dataPlot);
        ignoreNx = signoreNx;
        redo = false;
    end
end

minTakes = intmax;    
    
if(redo)    
    for nx = 1 : numberOfExecutions
        load(strcat(recordingsGroupFolder,csvGroup(nx).name));
        
        positions = nan(numberOfEpucks, 2);
        
        ids = unique(samples(:,EPUCK_ID_COLUM));
        
        for nId = 1 : size(ids, 1)            
            k = find(samples(:,1) == ids(nId));

            for j = 1 : size(k,1)
                samples(k(j), TAKE) = size(find(samples(1 : k(j)) == ids(nId)), 2);
            end
        end
        
        takes = unique(samples(:, TAKE));

         if(max(takes) < minTakes)
            minTakes = max(takes);
            baseSamples = nx;
         end
         
         samples = cast(samples, 'double');
         save(strcat(recordingsGroupFolder,csvGroup(nx).name), 'samples');
    end
    
    for nx = 1 : numberOfExecutions
        load(strcat(recordingsGroupFolder,csvGroup(nx).name));
        
        takes = unique(samples(:, TAKE));        
        samples(samples(:, TAKE) > minTakes, :) = [];  
       
        for nt = 1 : minTakes            

            positions = samples(samples(:, TAKE) == takes(nt) & samples(:, ON_FAIL_COLUMN) == 0, [X_COLUMN, Y_COLUMN]);
            pos(nt) = {positions};
            
            
            [A] = initialize_matrixA(positions,param,options); % matrix adjacency initialization
            lambdas(nt, nx) = algebraic_connectivity_New(A,param.normalized); %  algebraic_connectivity_New(A,param.normalized); % algebraic connectivity computation

            [data] = robustnessPlot(positions, param, options);
            robustnessLevel(nt, nx) = data(1);
            giantComponent(nt, nx) = data(2);

            %             [~, vulnerables(nt, nx)] = local_vulnerability(positions,param);
            [coverageArea(nt, nx), ~] = calcArea([positions, ones(numberOfEpucks,1)*range]);
        end
    end
    
    load(strcat(recordingsGroupFolder,csvGroup(baseSamples).name));
    snapshotPeriodInSec = (samples(end, TIME_IN_MILS_COLUMN) - samples(1, TIME_IN_MILS_COLUMN))/ 1000 / minTakes;
    
end


  
LABELX = sprintf('Snapshot # (T=%0.2f s)', snapshotPeriodInSec);

close all


slambdas = lambdas;

scoverageArea = coverageArea;
sgiantComponent = giantComponent;
srobustnessLevel = robustnessLevel;
% obj12CRaT1g = plotTopologyRecordingsGroup(dir12CraT1, 0.2, 0.02, 20, 23838, reply, [4, 10], titles(1));
% obj12RaT1g = plotTopologyRecordingsGroup(dir12RaT1, 0.2, 0.02, 20, 23838, reply, [], titles(2));

lambdas(:, ignoreNx) = NaN;
robustnessLevel(:, ignoreNx) = NaN;
giantComponent(:, ignoreNx) = NaN;
coverageArea(:, ignoreNx) = NaN;

clear ranking;

takes = [1 : size(lambdas,1)]';

ranking(:,1) = plotOutcome(takes, lambdas, LABELX, 'Algebraic Connectivity', mytitle, recordingsGroupFolder);
Y = epsilon * ones(size(takes,1), 1);
plot(takes, Y, 'r--');
addLegend(takes, Y, 'Lower Bound', 'r', 1,1);

savePlot(gcf, sprintf('%s%s', recordingsGroupFolder, 'AlgebraicConnectivity'));

ranking(:,2) = plotOutcome(takes, robustnessLevel * 100, LABELX, 'Robustness (%)', mytitle, recordingsGroupFolder, 0, 1);

ranking(:,2) = plotOutcome(takes, robustnessLevel * 100, LABELX, 'Robustness Curve (%)', mytitle, recordingsGroupFolder, 1, 0);

plotOutcome(takes, giantComponent * 100, LABELX, 'Giant Component (%)', mytitle, recordingsGroupFolder, 0, 1);

ranking(:,3) = plotOutcome(takes, coverageArea / arenaArea * 100, LABELX, 'Coverage Area (%)', mytitle, recordingsGroupFolder);

lambdas = slambdas;
coverageArea = scoverageArea;
giantComponent = sgiantComponent;
robustnessLevel = srobustnessLevel;

numberOfGoodExecutions = numberOfExecutions - size(ignoreNx,2);

if(~exist('pos', 'var'))
    pos = [];
end

save(dataPlot, 'pos', 'takes', 'snapshotPeriodInSec', 'lambdas', 'robustnessLevel', 'giantComponent', 'coverageArea', 'ranking', 'numberOfGoodExecutions', 'numberOfExecutions', 'ignoreNx');

close all
h = figure;
hold on
grid on

x = 1 : numberOfExecutions;
plot(ranking(:,1));
plot(ranking(:,2));
plot(ranking(:,3));

xlabel('Executions');
ylabel('Ranking');
legend('Connectivity Maintenance', 'Robustness', 'Coverage Area');
savePlot(gcf, sprintf('%s%s', recordingsGroupFolder, 'Ranking of Executions'));

close all

end