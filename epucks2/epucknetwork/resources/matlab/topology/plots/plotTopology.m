function plotTopology(positions, rangeRadius, bodyRadius, ids)

grid on
hold on
axis equal

positions = positions(:, [1,2]);

n = size(positions,1);

if ~exist('ids', 'var')
   ids = 1 : n; 
end

m = max(size(ids));

for i = 1 : n
   
    center = positions(i, :);
    plotCircle(center, rangeRadius, [.6 .6 .6 .3]);
    plotCircle(center, bodyRadius, [0 0 0 0.7]);
    h = text(center(1),center(2), num2str(ids(i)), 'Color', 'k'); 
    ext = h.Extent;
    set(h, 'Position', [center, 0] + [-(ext(3))/2,0,0]);
    
    h = text(center(1),center(2), num2str(ids(i)), 'Color', 'w'); 
    ext = h.Extent;
    set(h, 'Position', [center, 0] + [-(ext(3))/2.05,0,0]);


end


for i = 1 : n
    for j = i + 1 : n
       if(pdist2(positions(i, :), positions(j, :)) <= 2 * rangeRadius)
           plot([positions(i, 1); positions(j, 1)], [positions(i, 2); positions(j, 2)], 'color', [1,1,1,0.3], 'LineWidth', 5); 
       end
    end
end


hold off

end