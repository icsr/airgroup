function plotScores(allScores, scoreOutput)

    m = max(size(allScores));
    
    k1Scores = cell2mat(allScores(1));
    k2Scores = cell2mat(allScores(2));
    k3Scores = cell2mat(allScores(3));
    
    k1ScoresL = k1Scores.scoreL;
    k1ScoresR = k1Scores.scoreR;
    k1ScoresA = k1Scores.scoreA;
    
    k2ScoresL = k2Scores.scoreL;
    k2ScoresR = k2Scores.scoreR;
    k2ScoresA = k2Scores.scoreA;
    
    k3ScoresL = k3Scores.scoreL;
    k3ScoresR = k3Scores.scoreR;
    k3ScoresA = k3Scores.scoreA;
    
    k1t1ScoresL = cell2mat(k1ScoresL(1));
    k1t1ScoresR = cell2mat(k1ScoresR(1));
    k1t1ScoresA = cell2mat(k1ScoresA(1));
    
    k1t2ScoresL = cell2mat(k1ScoresL(2));
    k1t2ScoresR = cell2mat(k1ScoresR(2));
    k1t2ScoresA = cell2mat(k1ScoresA(2));
    
    k1t3ScoresL = cell2mat(k1ScoresL(3));
    k1t3ScoresR = cell2mat(k1ScoresR(3));
    k1t3ScoresA = cell2mat(k1ScoresA(3));
    
    k2t1ScoresL = cell2mat(k2ScoresL(1));
    k2t1ScoresR = cell2mat(k2ScoresR(1));
    k2t1ScoresA = cell2mat(k2ScoresA(1));
    
    k2t2ScoresL = cell2mat(k2ScoresL(2));
    k2t2ScoresR = cell2mat(k2ScoresR(2));
    k2t2ScoresA = cell2mat(k2ScoresA(2));
    
    k2t3ScoresL = cell2mat(k2ScoresL(3));
    k2t3ScoresR = cell2mat(k2ScoresR(3));
    k2t3ScoresA = cell2mat(k2ScoresA(3));
    
    k3t1ScoresL = cell2mat(k3ScoresL(1));
    k3t1ScoresR = cell2mat(k3ScoresR(1));
    k3t1ScoresA = cell2mat(k3ScoresA(1));

    k3t3ScoresL = cell2mat(k3ScoresL(2));
    k3t3ScoresR = cell2mat(k3ScoresR(2));
    k3t3ScoresA = cell2mat(k3ScoresA(2));
           
    maxL = max([k1t1ScoresL; k1t2ScoresL; k1t3ScoresL; k2t1ScoresL; k2t2ScoresL; k2t3ScoresL; k3t1ScoresL; k3t3ScoresL]);
    maxR = max([k1t1ScoresR; k1t2ScoresR; k1t3ScoresR; k2t1ScoresR; k2t2ScoresR; k2t3ScoresR; k3t1ScoresR; k3t3ScoresR]);
    maxA = max([k1t1ScoresA; k1t2ScoresA; k1t3ScoresA; k2t1ScoresA; k2t2ScoresA; k2t3ScoresA; k3t1ScoresA; k3t3ScoresA]);
    
    minL = min([k1t1ScoresL; k1t2ScoresL; k1t3ScoresL; k2t1ScoresL; k2t2ScoresL; k2t3ScoresL; k3t1ScoresL; k3t3ScoresL]);
    minR = min([k1t1ScoresR; k1t2ScoresR; k1t3ScoresR; k2t1ScoresR; k2t2ScoresR; k2t3ScoresR; k3t1ScoresR; k3t3ScoresR]);
    minA = min([k1t1ScoresA; k1t2ScoresA; k1t3ScoresA; k2t1ScoresA; k2t2ScoresA; k2t3ScoresA; k3t1ScoresA; k3t3ScoresA]);
    
    minL0 = min([k1t1ScoresL(1); k1t2ScoresL(1); k1t3ScoresL(1); k2t1ScoresL(1); k2t2ScoresL(1); k2t3ScoresL(1); k3t1ScoresL(1); k3t3ScoresL(1)]);
    minR0 = min([k1t1ScoresR(1); k1t2ScoresR(1); k1t3ScoresR(1); k2t1ScoresR(1); k2t2ScoresR(1); k2t3ScoresR(1); k3t1ScoresR(1); k3t3ScoresR(1)]);
    minA0 = min([k1t1ScoresA(1); k1t2ScoresA(1); k1t3ScoresA(1); k2t1ScoresA(1); k2t2ScoresA(1); k2t3ScoresA(1); k3t1ScoresA(1); k3t3ScoresA(1)]);
    
    kScores = {k1Scores, k2Scores, k3Scores};
    m = max(size(kScores));
    
    allTitles = {{'K_1=[1.0, 1.0, 1.0] applied to T_1', 'K_1=[1.0, 1.0, 1.0] applied to T_2', 'K_1=[1.0, 1.0, 1.0] applied to T_3'}, ...
        {'K_2=[1.0, 1.0, 0.5] applied to T_1', 'K_2=[1.0, 1.0, 0.5] applied to T_2', 'K_2=[1.0, 1.0, 0.5] applied to T_3'},    ...
        {'K_3=[0.0, 1.0, 0.5] applied to T_1', 'K_3=[0.0, 1.0, 0.5] applied to T_3'}};
    
    save(strcat(scoreOutput,'/experiment.mat'));
    
    for i = 1 : m
        
        titles = allTitles(i);       
        kSc = cell2mat(kScores(i));
        
        n = max(size(kSc.snapshotPeriodInSec));
        
        for j = 1 : n
            
            score.scoreL = cell2mat(kSc.scoreL(j));
            score.scoreR = cell2mat(kSc.scoreR(j));
            score.scoreA = cell2mat(kSc.scoreA(j));
            score.overall = cell2mat(kSc.scoreO(j));
    %         score.scoreH = cell2mat(scoreH(i));

            f = kSc.snapshotPeriodInSec(j);
            
            path = strcat(scoreOutput,'/scoreNotNorm_E',num2str(i),num2str(j));
            
            
            [h1, h2, h3] = plotScore2(score, path, f, strcat(titles{1}{j},' ( ', num2str(round(f)),' sec/snapshot)'));
                        
            continue;

            h = figure;

           POLY_DEGREE = 7;
           MARKER_SIZE = 10;

           hold on
           grid on
           xlabel(sprintf('Experiment Runtime (min)'));
           ylabel('Ratio Of Holes (%)')
           takes = 1 : max(size(score.overall));
           takes = (takes - 1) * double(f.snapshotPeriodInSec) / 60;

           score.scoreH = score.scoreH * 100;

           plot(takes, score.scoreH, '.', 'Color', 'k', 'MarkerSize', MARKER_SIZE);

           title(titles(i))

           PH = polyfit(takes', score.scoreH, POLY_DEGREE);

           X = 1 : max(takes);

           eH = polyval(PH, X);

           plot(X, eH, 'Color', 'k');

           savePlot(h, strcat(scoreOutput,'/holes_',num2str(i)), 30, 30);

           %legend('Holes Ratio', 'Location', 'southoutside', 'orientation', 'horizontal'); 

        end
    end

    for i = 1 : m
        
        titles = allTitles(i);       
        kSc = cell2mat(kScores(i));
        
        n = max(size(kSc.snapshotPeriodInSec));
        
        for j = 1 : n
            
            score.scoreL = cell2mat(kSc.scoreL(j)) / maxL;
            score.scoreR = cell2mat(kSc.scoreR(j)) / maxR;
            score.scoreA = cell2mat(kSc.scoreA(j)) / maxA;
            score.overall = cell2mat(kSc.scoreO(j));
    %         score.scoreH = cell2mat(scoreH(i));

            f = kSc.snapshotPeriodInSec(j);
            
            h = plotScore(score, f, strcat(titles{1}{j},' ( ', num2str(round(f)),' sec/snapshot)'));
                        
            if(j == 2 & n == 2)
                j = 3;
            end
            
            savePlot(h, strcat(scoreOutput,'/score_E',num2str(i),num2str(j)));
            

            continue;

            h = figure;

           POLY_DEGREE = 7;
           MARKER_SIZE = 10;

           hold on
           grid on
           xlabel(sprintf('Experiment Runtime (min)'));
           ylabel('Ratio Of Holes (%)')
           takes = 1 : max(size(score.overall));
           takes = (takes - 1) * double(f.snapshotPeriodInSec) / 60;

           score.scoreH = score.scoreH * 100;

           plot(takes, score.scoreH, '.', 'Color', 'k', 'MarkerSize', MARKER_SIZE);

           title(titles(i))

           PH = polyfit(takes', score.scoreH, POLY_DEGREE);

           X = 1 : max(takes);

           eH = polyval(PH, X);

           plot(X, eH, 'Color', 'k');

           savePlot(h, strcat(scoreOutput,'/holes_',num2str(i)));

           %legend('Holes Ratio', 'Location', 'southoutside', 'orientation', 'horizontal'); 

        end
    end
    
    
end
    