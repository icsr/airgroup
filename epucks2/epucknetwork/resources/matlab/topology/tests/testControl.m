function [lambdas, coverageArea] = testControl(allPositions, numberOfIterations, gains, threshold, epsilon, range, bodyRadius, distance, performSafeControl)

if ~exist('performSafeControl', 'var')
    performSafeControl = 1;
end


if iscell(allPositions)
    allPositions = cell2mat(allPositions);
end

[param, options, ~] = config([0,0,0], threshold, epsilon, range);

lambdas = nan(numberOfIterations,1);
coverageArea = nan(numberOfIterations,1);
robustnessLevel = nan(numberOfIterations,1);

close all
figure('units', 'normalized', 'Position', [0,0,1,1])
shg

n = size(allPositions,1);

samples = nan(numberOfIterations * n, 13);

% lambdas(i) = algebraic_connectivity_New(A,param.normalized); %  algebraic_connectivity_New(A,param.normalized); % algebraic connectivity computation
%     [areas(i), ~] = calcArea([allPositions(:,1:2), ones(size(allPositions,1),1)*range]);
%     
%      [data] = robustnessPlot(allPositions(:,1:2), param, options);
%      robustnessLevel(i) = data(1);
%      
%      
%          meanLambdas(:,i) = mean(f.lambdas(1:minTakes,:),2);    
%         
%         meanAreas(:,i) = mean(f.coverageArea(1:minTakes,:),2);    
%         
%         meanRobustness(:,i) = mean(f.robustnessLevel(1:minTakes,:),2);

for i = 1 : numberOfIterations

    [A] = initialize_matrixA(allPositions(:,1:2),param,options); % matrix adjacency initialization
    lambdas(i) = algebraic_connectivity_New(A,param.normalized); %  algebraic_connectivity_New(A,param.normalized); % algebraic connectivity computation
    [coverageArea(i), ~] = calcArea([allPositions(:,1:2), ones(size(allPositions,1),1)*range]);
    
     [data] = robustnessPlot(allPositions(:,1:2), param, options);
     robustnessLevel(i) = data(1);
    
%     if lambdas(i) == 0
%         disp('Null lambda detected on testControl')
%         break;
%     end

    subplot(1,2,1)

    cla
    plotTopology(allPositions(:,[1,2]), range, bodyRadius);
    drawnow

    subplot(1,2,2)
    score = getScore(lambdas, robustnessLevel, coverageArea);
    
    plot(1:numberOfIterations, score.overall, 1:numberOfIterations, score.scoreL, 1:numberOfIterations, score.scoreR, 1:numberOfIterations, score.scoreA);
    legend('Overall Score', 'Connectivity Score', 'Robustness Score', 'Coverage Score');
    grid on
    
    samples((i-1) * n + 1 : n * i, :) = [(1 : n)', zeros(n,1), allPositions(:,[1,2]), zeros(n, 8), i * ones(n,1)];
    
    u = performControl(allPositions(:, [1,2]), gains, threshold, epsilon, range , bodyRadius, distance, performSafeControl);

    allPositions(:,[1,2]) = allPositions(:,[1,2]) + u; 
    
%     if(i > 20 && max(diff(areas(i - 20 : i))) == 0)
%         break;
%     end
    
end

save(strcat('/tmp/dataPlot_n=', num2str(n), '_g=', mat2str(gains), '.mat'), 'samples', 'lambdas', 'coverageArea', 'robustnessLevel', 'gains', 'param', 'options');

end
