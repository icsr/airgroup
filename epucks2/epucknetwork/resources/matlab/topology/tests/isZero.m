function zero = isZero(value)
    zero = abs(value) < 10^-8;
end

    