load('post3k3.mat')

R = 20;
delta = 0.02;
param.range = 2*R;
options.unweighted = 0;

param.sigma = R / sqrt(-(2*log(delta))^-1);
[A] = initialize_matrixA(pos, param, options);

D = diag(sum(A,2));

L = D - A;

[eVec, eVal] = eig(L);

eVal = sort(diag(eVal))

v2 = eVal(2)