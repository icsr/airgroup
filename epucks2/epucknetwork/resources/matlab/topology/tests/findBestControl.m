function [threshold, epsilon] = findBestControl(numberOfIterationsPerTest, increment, gains, range, bodyRadius, normalizeEachControl, distance)

epsilon = 0;
i = 1;

clear performance

while(true)
    threshold = increment;
    while(true)
        [lambdas, areas] = testControl(numberOfIterationsPerTest, gains, threshold, epsilon, range, bodyRadius, normalizeEachControl, distance);
        
        if min(lambdas) == 0
            lambdas(:) = 0;
        end
        performance(i,1) = mean(lambdas);
        performance(i,2) = mean(areas);
        performance(i,3) = threshold;
        performance(i,4) = epsilon;
        i = i + 1;
        
        threshold =  threshold + increment;
        if threshold > 1
            break;
        end
    end
    epsilon = epsilon + increment;
    
    if epsilon > 1
        break;
    end
end

    rank = performance(:,2) / max(performance(:,2)) + performance(:,1) / max(performance(:,1));
    
    save('/tmp/performance.mat', 'performance', 'rank');

    maxPerformance = performance(rank ==  max(rank), [3,4]);
    
    threshold = maxPerformance(1, 1);
    epsilon = maxPerformance(1, 2);

end