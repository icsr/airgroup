function pos = createSquare(robotsPerEdge)

    pos = [];
    for i = 1 : robotsPerEdge
        
        pos = [pos; 30*(i-1), 0;  30 * (i-1), 30 * (robotsPerEdge - 1)];
        
        if(i > 1 && i < robotsPerEdge)
            pos = [pos; 0, 30 * (i-1); 30 * (robotsPerEdge - 1), 30 * (i-1)];
        end
    end
   
end