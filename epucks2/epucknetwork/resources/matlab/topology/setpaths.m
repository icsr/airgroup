function setpaths

addpath('matgraph');
addpath('matgraph_MIT');
addpath('general');
addpath('metrics');
addpath('voronoi');
addpath('metrics/lib/core');
addpath('metrics/lib/support');
addpath('plots');
addpath('rectools');
addpath('matlab_bgl');
addpath('tests');
addpath('./');

end