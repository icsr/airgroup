function logControl(logfile, robotId,time,x,y,lambda,dotxy,failComm, gains)

    if exist(logfile, 'file')
        load(logfile);
    else
        headers = {'robotId', 'time in mils', 'x', 'y', 'lambda', 'dotxy-x', 'dotxy-y', 'failComm', 'connectivityGain', 'robustnessGain', 'coverageAreaGain'};
    end
    
    if ~exist('samples', 'var')        
        samples = [];                
    end
    
    samples = [samples; robotId, time, x, y, lambda, dotxy, failComm, gains];
   
    
    save(logfile, 'samples', 'headers');

end