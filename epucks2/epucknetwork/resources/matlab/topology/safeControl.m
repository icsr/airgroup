function dotxy = safeControl(dotxy, currentPositions, param, options, avoidDisconnection)

if ~exist('avoidDisconnection', 'var')
    avoidDisconnection = 0;
end

dotxy = normalize(dotxy);

normalizedDotxy = dotxy;

for d = param.maxMovingDistance : -1 : 0

    dotxy = d * normalizedDotxy;
    
    safeness = getControlSafeness(dotxy, currentPositions, param, options, avoidDisconnection);
    
    if(safeness.isSafe)
        return;
    end

    lastsafeness = [];
    while(true)
        
        dotxy(safeness.onCollisionDanger,:) = dotxy(safeness.onCollisionDanger,:) / 10;
        
        modulus = sqrt(dotxy(:,1).^2 + dotxy(:,2).^2);
        
        if(min(isZero(modulus(safeness.onCollisionDanger))) == 1)
           break; 
        end
                
        dotxy(isZero(modulus), :) = 0;
        
        if(max(max(dotxy(safeness.onCollisionDanger))) == 0)
            break;
        end
        
        newPositions = currentPositions + dotxy;
        safeness = getControlSafeness(dotxy, newPositions, param, options, avoidDisconnection);
        
        if(safeness.isSafe)
            return;
        end
                      
        if(~isempty(lastsafeness) && min(min(lastsafeness.onCollisionDanger==safeness.onCollisionDanger)) == 0)           
            break;
        end

        lastsafeness = safeness;    
        
    end
    
end
end