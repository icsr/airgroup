function r = roundIt(number, precision)

    r = round(number*10^precision) / 10^precision;

end