function score = getScore(lambdas, robustness, areas)     

    score.scoreL = (lambdas);
    score.scoreR = (robustness);
    score.scoreA = (areas);
    
    normalizedscoreL = score.scoreL / max(max(abs(score.scoreL)));
    normalizedscoreR = score.scoreR / max(max(abs(score.scoreR)));
    normalizedscoreA = score.scoreA / max(max(abs(score.scoreA)));
        
    score.overall = normalizedscoreL + normalizedscoreR + normalizedscoreA;
    score.overall = score.overall / max(abs(score.overall));
   
end