function safeness = getControlSafeness(dotxy, currentPositions, param, options, avoidDisconnection)

    newpositions = currentPositions + dotxy;
    [A] = initialize_matrixA(newpositions(:,1:2),param,options); % matrix adjacency initialization
    lambda = algebraic_connectivity_New(A,param.normalized); %  algebraic_connectivity_New(A,param.normalized); % algebraic connectivity computation
    
    distances = ceil(pdist2(newpositions, newpositions));
    onCollisionDanger = sum((distances <= 1.1 * 2 * param.bodyRadius) & distances > 0) ~= 0;
   
    safeness.isSafe = max(onCollisionDanger) == 0;
    
    if avoidDisconnection
        safeness.isSafe = safeness.isSafe & ~isZero(lambda);
    end
    
    safeness.lambda = lambda;
    safeness.onCollisionDanger = onCollisionDanger;
    
end