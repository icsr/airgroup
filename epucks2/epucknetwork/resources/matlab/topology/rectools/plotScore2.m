function [h1, h2, h3] = plotScore2(score, path, snapshotPeriodInSec, scoreTitle)

    function h = prepareFig()       
       h = figure;   
       POLY_DEGREE = 3;
       MARKER_SIZE = 10;
       
       hold on
       grid on
       xlabel(sprintf('Experiment Runtime (min)'));
       
       
       takes = 1 : max(size(score.overall));
       takes = (takes - 1) * double(snapshotPeriodInSec) / 60;

       xlim([0,10]);
       
       grid minor     
       title(scoreTitle, 'FontSize', 20);
       resize_fcn

    end


   h1 = prepareFig();
%    plot(takes, score.overall, '.');
   plot(takes, score.scoreL, '+', 'Color', 'r', 'MarkerSize', MARKER_SIZE);
   PL = polyfit(takes', score.scoreL, POLY_DEGREE);
   eL = polyval(PL, takes);
   plot(takes, eL, 'Color', 'r');
   polystrC = polyfun2str(PL,POLY_DEGREE);
   text(1.02*takes(5), 0.98*eL(5), polystrC, 'FontSize', 20, 'BackgroundColor', 'w',  'Color', 'r', 'EdgeColor', 'r');
   ylabel('Mean \lambda')
   ylim([0, 1]);
   
   y = 0.02 * ones(1, max(size(takes)));
   plot(takes, y, 'k--');
   legend('Mean \lambda', 'Curve Fit', '\epsilon', 'Location', 'southeast', 'Orientation', 'vertical')
   
   savePlot(h1, strcat(path, '_C'), 40, 20);
   
   h2=prepareFig();
   plot(takes, score.scoreR, 'o', 'Color', 'b', 'MarkerSize', MARKER_SIZE);
   PR = polyfit(takes', score.scoreR, POLY_DEGREE);
   eR = polyval(PR, takes);
   plot(takes, eR, 'Color', 'b');
   polystrR = polyfun2str(PR,POLY_DEGREE);
   text(1.02*takes(5), 0.98*eR(5), polystrR, 'FontSize', 20, 'BackgroundColor', 'w',  'Color', 'b', 'EdgeColor', 'b');
   ylabel('Mean \theta')
   ylim([0, .25]);
   legend('Mean \theta', 'Curve Fit', '\epsilon', 'Location', 'southeast', 'Orientation', 'vertical')
   savePlot(h2, strcat(path, '_R'), 40, 20);
   
   
   h3=prepareFig();
   plot(takes, score.scoreA, 'x', 'Color', 'k', 'MarkerSize', MARKER_SIZE);
%    plot(takes, score.scoreH, '+', 'Color', 'g', 'MarkerSize', MARKER_SIZE);
   PA = polyfit(takes', score.scoreA, POLY_DEGREE);
   eA = polyval(PA, takes);
   plot(takes, eA, 'Color', 'k');
   polystrA = polyfun2str(PA,POLY_DEGREE);
   text(1.02*takes(5), 0.98*eA(5), polystrA, 'FontSize', 20, 'BackgroundColor', 'w',  'Color', 'k', 'EdgeColor', 'k');
   ylabel('Mean \Lambda')
   ylim([8000, 14000]); 
   legend('Mean \Lambda', 'Curve Fit', '\epsilon', 'Location', 'southeast', 'Orientation', 'vertical');
   savePlot(h3, strcat(path, '_A'), 40, 20);
   
   
%    legend('Connectivity Score (S_C)', 'Robustness Score (S_R)', 'Cov. Area Score (S_A)', 'Location', 'southeast', 'orientation', 'vertical'); 
       
end