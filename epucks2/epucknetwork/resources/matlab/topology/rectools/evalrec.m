function score = evalrec(recpath)
    n = max(size(recpath));
    minTakes = Inf;
    
    n = max(size(recpath));
    
    meanLambdas = cell(n,1);
    meanAreas = cell(n,1);
    meanRobustness = cell(n,1);
    
    
    for i = 1 : n
               
        file = cell2mat(recpath(i));
        if(isdir(file))  
            directory = file;
            file = strcat(directory, '/dataPlot.mat');
        else
            [directory,~,~] = fileparts(file);
        end
        f = load(file);
        
        if(~isfield(f,'takes') && size(f.samples,2) >= 13)
            f.takes = unique(f.samples(:,13));
        end
        
        minTakes = min(minTakes, size(f.takes,1));

        meanLambdas(i) = {mean(f.lambdas(1:minTakes,:),2)};    
        
        meanAreas(i) = {mean(f.coverageArea(1:minTakes,:),2)};    
        
        meanRobustness(i) = {mean(f.robustnessLevel(1:minTakes,:),2)}; 
        
        sc = getScore(cell2mat(meanLambdas(i)), cell2mat(meanRobustness(i)), cell2mat(meanAreas(i)));
        
        score.scoreL(i) = {sc.scoreL}';
        score.scoreR(i) = {sc.scoreR}';
        score.scoreA(i) = {sc.scoreA}';
        score.scoreO(i) = {sc.overall}';
        score.snapshotPeriodInSec(i) = f.snapshotPeriodInSec;
        
%         if(reply ~= 'Y')
%             poss = f.pos;
% %             scoreH(i) = {evalHoles(poss(1:minTakes))};
%         else
%             load(strcat(scoreOutput,'/score.mat'), 'scoreH');
% %             ss = cell2mat(scoreH(i));
% %             ss = ss(1:minTakes);
% %             scoreH(i) = {ss};
%         end
        
             
    end
    
%     if(~exist(scoreOutput, 'dir'))
%         mkdir(scoreOutput);
%     end
    
%     save(strcat(scoreOutput,'/score.mat'), 'scoreO', 'scoreA', 'scoreL', 'scoreR', 'scoreH');    
%     save(strcat(scoreOutput,'/score.mat'), 'score');  
    
    close all
       
%     close all
%     
%     h = figure;
%         
%     for i = 1 : n
%         subplot(n / 2,2,i)
%         
%         hold on
%         grid on
%         xlabel('Snapshots')
%         ylabel('Score')
%         
%         takes = 1 : max(size(cell2mat(scoreO(i))));
%         
%         plot(takes', cell2mat(scoreO(i)));
%         plot(takes', cell2mat(scoreL(i)));
%         plot(takes', cell2mat(scoreR(i)));
%         plot(takes', cell2mat(scoreA(i)));
%         title(strrep(mat2str(cell2mat(titles(i))), '_', ','));        
%     end
%     
%     legend('Overall Score', 'Connectivity Score', 'Robustness Score', 'Cov. Area Score', 'Location', 'south', 'orientation', 'horizontal');
% %     
%     set(h, 'Units', 'Normalized', 'Position', [0,0,0.5,0.5]);
%     
%     saveas(h, strcat(scoreOutput,'/score.png'));

end