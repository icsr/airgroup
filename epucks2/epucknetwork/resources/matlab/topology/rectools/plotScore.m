function h = plotScore(score, snapshotPeriodInSec, scoreTitle)

   h = figure;
   
   POLY_DEGREE = 3;
   MARKER_SIZE = 10;
       
   hold on
   grid on
   xlabel(sprintf('Experiment Runtime (min)'));
   ylabel('Score')
   takes = 1 : max(size(score.overall));
   takes = (takes - 1) * double(snapshotPeriodInSec) / 60;
   
   xlim([0,10]);
   ylim([0, 1]);
   grid minor
   
   
%    plot(takes, score.overall, '.');
   plot(takes, score.scoreL, '+', 'Color', 'r', 'MarkerSize', MARKER_SIZE);
   plot(takes, score.scoreR, 'o', 'Color', 'b', 'MarkerSize', MARKER_SIZE);
   plot(takes, score.scoreA, 'x', 'Color', 'k', 'MarkerSize', MARKER_SIZE);
%    plot(takes, score.scoreH, '+', 'Color', 'g', 'MarkerSize', MARKER_SIZE);

   title(scoreTitle)
   
   PL = polyfit(takes', score.scoreL, POLY_DEGREE);
   PR = polyfit(takes', score.scoreR, POLY_DEGREE);
   PA = polyfit(takes', score.scoreA, POLY_DEGREE);
%    PH = polyfit(takes', score.scoreH, POLY_DEGREE);
   
   
   eL = polyval(PL, takes);
   eR = polyval(PR, takes);
   eA = polyval(PA, takes);
%    eH = polyval(PH, X);
   
   plot(takes, eL, 'Color', 'r');
   plot(takes, eR, 'Color', 'b');
   plot(takes, eA, 'Color', 'k');
   % plot(X, eH, 'Color', 'g');
   
   polystrC = polyfun2str(PL,POLY_DEGREE);
   polystrR = polyfun2str(PR,POLY_DEGREE);
   polystrA = polyfun2str(PA,POLY_DEGREE);
   
   
   text(1.02*takes(5), 0.98*eL(5), polystrC, 'FontSize', 20, 'BackgroundColor', 'w',  'Color', 'r', 'EdgeColor', 'r');
   text(1.02*takes(5), 0.98*eR(5), polystrR, 'FontSize', 20, 'BackgroundColor', 'w',  'Color', 'b', 'EdgeColor', 'b');
   text(1.02*takes(5), 0.98*eA(5), polystrA, 'FontSize', 20, 'BackgroundColor', 'w',  'Color', 'k', 'EdgeColor', 'k');
   
   x = 1460;
   y = 600;
   deltay = 50;
   
   text(x, y + 2*deltay, '— S_C', 'Units', 'pixels','FontSize', 30, 'Color', 'r');
   text(x, y + 1*deltay, '— S_R', 'Units', 'pixels','FontSize', 30, 'Color', 'b');
   text(x, y + 0*deltay, '— S_A', 'Units', 'pixels','FontSize', 30, 'Color', 'k');
   
   ah = annotation('textbox', 'EdgeColor', 'k', 'Units', 'pixels', 'Position', [x+230 y+70 135 180]);
   uistack(ah,'bottom')
   
%    legend('Connectivity Score (S_C)', 'Robustness Score (S_R)', 'Cov. Area Score (S_A)', 'Location', 'southoutside', 'orientation', 'horizontal'); 
       
end