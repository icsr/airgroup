function collinear = isCollinear(threePoints)

    collinear = abs(diff(threePoints));
    collinear = collinear(collinear == 0);
    collinear = ~isempty(collinear);

end