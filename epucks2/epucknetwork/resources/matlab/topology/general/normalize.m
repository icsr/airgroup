function x = normalize(x)

    lines = size(x,1);
    
    for i = 1 : lines
       x(i, [1,2]) = x(i, [1,2]) / norm(x(i, [1,2])); 
    end
    
    x(isnan(x)) = 0;

end