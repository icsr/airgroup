package br.ita.airgroup.israel.epucknetwork.helper.exception;

public class ParametersException extends Exception {

	public ParametersException(int qtyOfParameters) {
		super(String.format("%d parameters are needed.", qtyOfParameters));
	}

	private static final long serialVersionUID = -4940273778702781458L;

}
