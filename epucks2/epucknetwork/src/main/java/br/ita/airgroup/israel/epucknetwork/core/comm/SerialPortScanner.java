package br.ita.airgroup.israel.epucknetwork.core.comm;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import br.ita.airgroup.israel.epucknetwork.core.comm.port.SerialPortCommunicationally;
import br.ita.airgroup.israel.epucknetwork.helper.BaseSystem;
import br.ita.airgroup.israel.epucknetwork.helper.exception.PortAccessException;
import br.ita.airgroup.israel.epucknetwork.helper.exception.SettingsException;

public class SerialPortScanner {

	static {
		BaseSystem.init();
	}

	public static final int WINDOWS_PORT_NUMBER = 1;
	public static final String WINDOWS_HEADER = "COM";
	public static final int LINUX_PORT_NUMBER = 0;
	public static final String LINUX_HEADER = "/dev/rfcomm";
	private String header;
	private int firstPortNumber;
	private int qtyOfPorts;
	private Optional<List<SerialPortCommunicationally<?>>> ports = Optional.empty();

	public SerialPortScanner(int qtyOfPorts) throws SettingsException {
		setQtyOfPorts(qtyOfPorts);
		setPortAccordinglyToSO();
	}

	private void setQtyOfPorts(int qtyOfPorts) throws SettingsException {
		if (qtyOfPorts <= 0) {
			throw new SettingsException("Invalid qty of ports,");
		}

		this.qtyOfPorts = qtyOfPorts;
	}

	private void setPortAccordinglyToSO() {
		if (!BaseSystem.isRunningOnLinux()) {
			header = WINDOWS_HEADER;
			firstPortNumber = WINDOWS_PORT_NUMBER;
			return;
		}

		header = LINUX_HEADER;
		firstPortNumber = LINUX_PORT_NUMBER;

	}

	public List<SerialPortCommunicationally<?>> scan() throws PortAccessException {

		ports = Optional.ofNullable(Stream.iterate(1L, i -> i + 1).limit(this.qtyOfPorts)
				.map(i -> new SerialPortBuilder(String.format("%s%s", this.header, this.firstPortNumber + i)).build())
				.collect(Collectors.toList()));

		return getPorts();

	}

	private List<SerialPortCommunicationally<?>> getPorts() {
		return ports.orElse(new ArrayList<>());
	}

	public String getHeader() {
		return header;
	}

	public int getFirstPortNumber() {
		return firstPortNumber;
	}

}
