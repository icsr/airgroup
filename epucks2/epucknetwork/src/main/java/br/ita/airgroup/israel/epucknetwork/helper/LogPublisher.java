package br.ita.airgroup.israel.epucknetwork.helper;

import java.util.HashMap;
import java.util.Map;

import br.ita.airgroup.israel.commons.news.Publisher;
import br.ita.airgroup.israel.commons.news.Subscriber;

public class LogPublisher implements Publisher<String> {

	private static final Map<Subscriber<String>, TYPE> subscribers = new HashMap<>();

	@Override
	public void subscribe(Subscriber<String> subscriber, TYPE type) {
		subscribers.put(subscriber, type);
	}

	@Override
	public void unsubscribe(Subscriber<String> subscriber) {
		subscribers.remove(subscriber);
	}

	@Override
	public void report(String newContent, TYPE type) {
		subscribers.entrySet().parallelStream().filter(e -> e.getValue() == type)
				.forEach(e -> e.getKey().receiveReport(newContent));
	}

}
