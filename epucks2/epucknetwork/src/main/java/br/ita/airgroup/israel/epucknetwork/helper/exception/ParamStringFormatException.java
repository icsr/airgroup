package br.ita.airgroup.israel.epucknetwork.helper.exception;

import br.ita.airgroup.israel.epucknetwork.helper.message.Message;

public class ParamStringFormatException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 83616720134309613L;
	
	public ParamStringFormatException() {
		super(String.format("paramString é da forma %s", Message.MESSAGE_REGEX));
	}

}
