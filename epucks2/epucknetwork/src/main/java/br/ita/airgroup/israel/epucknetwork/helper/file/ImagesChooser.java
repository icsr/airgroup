package br.ita.airgroup.israel.epucknetwork.helper.file;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Optional;
import java.util.stream.Stream;

import javax.swing.filechooser.FileFilter;

import br.ita.airgroup.israel.commons.utils.StringUtils;

public class ImagesChooser extends FileChooser {

	private String[] extensions;

	public ImagesChooser(String title, Optional<String> initFolder, String... acceptableExtensions) {
		super(title, initFolder);

		this.extensions = acceptableExtensions;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -5890670949512035904L;

	@Override
	protected void config() {
		setApproveButtonText("Select Images");
		this.setFileSelectionMode(FileChooser.FILES_ONLY);
		setMultiSelectionEnabled(true);
		
		setFileFilter(new FileFilter() {

			@Override
			public String getDescription() {
				return StringUtils.EMPTY;
			}

			@Override
			public boolean accept(File f) {
				
				if(f.isDirectory()){
					return true;
				}
				
				try {
					boolean check = Files.probeContentType(f.toPath()).toLowerCase().contains("image");

					if (extensions.length == 0) {
						return check;
					}

					return check && Stream.of(extensions)
							.filter(e -> f.getAbsolutePath().toLowerCase().endsWith(e.toLowerCase())).findAny()
							.isPresent();

				} catch (IOException e) {
					return false;
				}
			}
		});

	}

}
