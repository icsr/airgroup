package br.ita.airgroup.israel.epucknetwork.gui.components;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.List;

import br.ita.airgroup.israel.commons.utils.MathUtils;
import br.ita.airgroup.israel.epucknetwork.gui.aux.Exhibitable;

public class PointsCanvas extends Canvas {

	private static final long serialVersionUID = 7849101331491685862L;
	private List<Point> points;
	private int radius;

	public PointsCanvas(List<Point> points, Dimension dimension) {

		this.setPreferredSize(dimension);
		this.setSize(dimension);
		this.setMaximumSize(dimension);
		this.setVisible(true);
		
		this.points = points;

		radius = (int) (MathUtils.distance(points.get(0), MathUtils.closerPoint(points, points.get(0)).get()));

	}

	@Override
	public void paint(Graphics g) {
		
		g.setColor(Color.BLACK);
		Graphics2D.class.cast(g).setComposite(Exhibitable.transparent);

		points.forEach(p -> g.fillOval(p.x, p.y, radius, radius));
	}

}
