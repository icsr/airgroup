package br.ita.airgroup.israel.epucknetwork.helper.tracking;

import java.awt.Color;
import java.awt.Composite;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.swing.JPanel;

import org.apache.commons.math.util.MathUtils;

import boofcv.gui.feature.VisualizeShapes;
import br.ita.airgroup.israel.commons.utils.Font;
import br.ita.airgroup.israel.commons.utils.StringUtils;
import br.ita.airgroup.israel.epucknetwork.core.controller.Epuck;
import br.ita.airgroup.israel.epucknetwork.core.controller.Topology;
import br.ita.airgroup.israel.epucknetwork.core.imaging.Arena;
import br.ita.airgroup.israel.epucknetwork.core.imaging.Camera;
import br.ita.airgroup.israel.epucknetwork.core.imaging.EdgeLengthAdjustFactor;
import br.ita.airgroup.israel.epucknetwork.core.imaging.tracking.ArenaResizer.ArenaBoundaryPhase;
import br.ita.airgroup.israel.epucknetwork.core.imaging.tracking.DraggedEpuck;
import br.ita.airgroup.israel.epucknetwork.gui.aux.Exhibitable;
import br.ita.airgroup.israel.epucknetwork.helper.Logger;
import br.ita.airgroup.israel.epucknetwork.helper.Posture;
import br.ita.airgroup.israel.epucknetwork.helper.file.Properties;
import br.ita.airgroup.israel.epucknetwork.helper.file.Properties.Key;
import georegression.struct.point.Point2D_F64;
import georegression.struct.shapes.Polygon2D_F64;

public class TrackerDrawings {

	public enum Show {
		Range(true), Posture(true), ID(true), Arena(true), Contour(true), Log(false), Clock(true), Connectivity(true), Logo(true);
		public boolean selected = true;

		private Show(boolean selected) {
			this.selected = selected;
		}
	};

	static Rectangle2D cameraPanelRectangle = new Rectangle2D.Double();
	public static final int AXIS_LENGTH = 100;

	public static void drawDraggingEpuck(EpuckDragger dragger, Arena arena, double adjustFactorInCmPerPx,
			Graphics2D g2) {

		Optional<DraggedEpuck> selectedEpuck = dragger.getSelectedEpuck();

		if (!selectedEpuck.isPresent() || !selectedEpuck.get().isDragging()) {
			return;
		}

		selectedEpuck.get().getTargetPoint(false).ifPresent(t -> {
			g2.setFont(Font.small(true));
			g2.setColor(Exhibitable.GOLD_COLOR);
			g2.setComposite(Exhibitable.halfOpaque);

			g2.drawLine(selectedEpuck.get().getPosturePx().x + arena.getOffset(false).x,
					selectedEpuck.get().getPosturePx().y + arena.getOffset(false).y, t.x, t.y);

			Point adjustedPoint = TrackerUtils.cameraPositionInPxToAbsolutePositionInCm(arena, t,
					adjustFactorInCmPerPx);
			String targetCoord = !dragger.justAngle ? String.format("(%d,%d)", adjustedPoint.x, adjustedPoint.y)
					: String.format("(%d\u00B0)",
							(int) Posture.getAngle(selectedEpuck.get().getPostureCm().getPoint(), adjustedPoint));
			drawString(g2, targetCoord, t.x - g2.getFontMetrics().stringWidth(targetCoord) / 2,
					t.y + 2 * g2.getFontMetrics().getHeight(), Exhibitable.GOLD_COLOR, Color.BLACK,
					Exhibitable.halfOpaque, Font.small(true));
		});

	}

	public static void drawConnectivity(Graphics2D g2, Topology topology, Arena arena) {

		if (!Show.Connectivity.selected) {
			return;
		}

		g2.setColor(Color.BLACK);
		g2.setComposite(Exhibitable.veryTransparent);
		g2.setFont(Font.mean(true));
		g2.setStroke(Exhibitable.thinStroke);

		int offset = Camera.logoHeight + Exhibitable.PADDING_THICK;

		offset = drawEigenOnScreen(g2, arena, Optional.empty(), topology.evaluateAlgebraicConnectivity(), offset);

		for (int id : topology.getEpuckIds()) {
			Epuck epuck = topology.getEpuck(id);

			if (epuck.isOnFail()) {
				continue;
			}

			drawConnectionEdge(g2, arena, epuck);
			offset = drawEigenOnScreen(g2, arena, Optional.of(epuck), epuck.getSecondEigenValue(), offset);
		}
	}

	private static int drawEigenOnScreen(Graphics2D g2, Arena arena, Optional<Epuck> epuck,
			Optional<Float> secondEigenValue, int offsetY) {
		if (!secondEigenValue.isPresent()) {
			return offsetY;
		}

		String value = formatSecondEigenvalue(epuck, secondEigenValue.get());
		drawString(g2, value, arena.getRightPx() - g2.getFontMetrics().stringWidth(value), offsetY, Color.WHITE,
				Color.BLACK, Exhibitable.transparent, Font.small(true));
		return offsetY + g2.getFontMetrics().getHeight();

	}

	private static String formatSecondEigenvalue(Optional<Epuck> epuck, Float secondEigenValue) {
		if (epuck.isPresent()) {
			return String.format("%s (%d): %.2f", StringUtils.GREEK_LAMBDA, epuck.get().getId(),
					MathUtils.round(secondEigenValue, 2));
		} else {
			return String.format("%s: %.2f", StringUtils.GREEK_LAMBDA, MathUtils.round(secondEigenValue, 2));
		}
	}

	private static void drawConnectionEdge(Graphics2D g2, Arena arena, Epuck epuck) {
		List<Epuck> olderNeighbors = epuck.getDirectNeighbors().stream().filter(e -> e.getId() > epuck.getId())
				.filter(e -> !e.isOnFail()).collect(Collectors.toList());

		g2.setComposite(Exhibitable.veryTransparent);
		g2.setColor(Color.BLACK);

		Point2D_F64 source = TrackerUtils.absolutePositionInCmTocameraCoordinateInPx(arena,
				epuck.getPostureCm().getPoint(), arena.getAdjustFactorCmPerPx());
		olderNeighbors.forEach(o -> {
			Point2D_F64 destination = TrackerUtils.absolutePositionInCmTocameraCoordinateInPx(arena,
					o.getPostureCm().getPoint(), arena.getAdjustFactorCmPerPx());
			g2.drawLine((int) source.x, (int) source.y, (int) destination.x, (int) destination.y);
		});
	}

	public static void drawClock(Arena arena, Graphics2D g2) {
		if (!Show.Clock.selected) {
			return;
		}

		String clock = LocalDateTime.now().format(Logger.dateFormat);
		int stringWidth = g2.getFontMetrics().stringWidth(clock);
		float x = arena.getLeftPx() + 0.5f * (arena.getWidthPx() - stringWidth);
		float y = Exhibitable.PADDING + arena.getTopPx();

		g2.setComposite(Exhibitable.halfOpaque);
		drawString(g2, clock, x, y, Exhibitable.GOLD_COLOR, Color.BLACK, Exhibitable.halfOpaque,
				Font.mean(true).deriveFont(Font.BOLD));
		
		clock = null;
	}

	public static void drawString(Graphics2D g2, String text, float x, float y, Color fontColor, Color backColor,
			Composite opacity, Font font) {
		g2.setComposite(opacity);
		g2.setFont(font);

		g2.setColor(backColor);

		g2.drawString(text, x - 0.8f, y - 0.8f);
		g2.setColor(fontColor);
		g2.drawString(text, x, y);

	}

	public static void drawLog(Arena arena, Graphics2D g2) {
		if (!Show.Log.selected) {
			return;
		}

		List<String> lines = Logger.getLines(true, true);

		int counter = 0;
		for (String line : lines) {
			int y = arena.getBottomPx() - Exhibitable.PADDING_THICK - (counter++) * g2.getFontMetrics().getHeight();
			if (y < arena.getTopPx() + Exhibitable.PADDING_THICK) {
				break;
			}
			drawString(g2, line, arena.getLeftPx() + Exhibitable.PADDING, y, Color.LIGHT_GRAY, Color.BLACK,
					Exhibitable.transparent, Font.small(true));
		}
	}

	public static void drawCountour(Polygon2D_F64 polygon, Point polygonCenter, Graphics2D g2,
			EdgeLengthAdjustFactor adjustFactor) {
		if (Show.Contour.selected) {
			VisualizeShapes.drawPolygon(polygon, true, g2, true);

			g2.setComposite(Exhibitable.fullOpaque);

			String adjustedString = String.format("%d cm ~ %d px", (int) adjustFactor.getEdgeInCm(),
					(int) adjustFactor.getEdgeInPx());

			drawString(g2, adjustedString, polygonCenter.x - g2.getFontMetrics().stringWidth(adjustedString) / 2,
					polygonCenter.y + g2.getFontMetrics().getHeight()
							+ (int) (polygon.getSideLength(0) * Math.sqrt(2) / 2));
		}

	}

	public static void drawString(Graphics2D g2, String adjustedString, int x, int y) {
		drawString(g2, adjustedString, x, y, g2.getColor(), Color.BLACK, g2.getComposite(),
				Font.class.isInstance(g2.getFont()) ? (Font) g2.getFont() : Font.normal(true));
	}

	public static void drawArenaBoundary(Arena arena, JPanel cameraPanel, Graphics2D g2, Composite composite) {
		g2.setColor(Color.BLACK);
		g2.setStroke(Exhibitable.thinStroke);

		g2.setComposite(composite);
		cameraPanelRectangle.setFrame(0, 0, cameraPanel.getWidth(), cameraPanel.getHeight());
		g2.fill(arena.excludeFromTotalArea(cameraPanelRectangle));
	}

	public static void drawAxisAndBoundary(Arena arena, JPanel cameraPanel, ArenaBoundaryPhase phase, Graphics2D g2) {
		g2.setColor(Color.darkGray);
		g2.setComposite(Exhibitable.halfOpaque);
		g2.setStroke(Exhibitable.mediumStroke);
		g2.setFont(Font.mean(true));

		g2.drawLine(arena.getOriginPx().x, arena.getOriginPx().y, arena.getOriginPx().x + AXIS_LENGTH,
				arena.getOriginPx().y);
		g2.drawString(Exhibitable.X, arena.getOriginPx().x + AXIS_LENGTH + 10, arena.getOriginPx().y + 10);

		g2.drawLine(arena.getOriginPx().x, arena.getOriginPx().y, arena.getOriginPx().x,
				arena.getOriginPx().y + AXIS_LENGTH);
		g2.drawString(Exhibitable.Y, arena.getOriginPx().x, arena.getOriginPx().y + AXIS_LENGTH + 20);

		// TODO FIX THAT
		if (phase == ArenaBoundaryPhase.ORIGIN) {
			g2.setFont(Font.mean(true));
			TrackerDrawings.drawString(g2, Exhibitable.CLICK_ON_A_POINT_TO_SET_AXIS_OFFSET_AND_ARENA_ORIGIN,
					cameraPanel.getWidth() / 4, cameraPanel.getHeight() / 2);
			TrackerDrawings.drawArenaBoundary(arena, cameraPanel, g2, Exhibitable.transparent);
		} else if (phase == ArenaBoundaryPhase.DIAGONAL) {
			g2.setFont(Font.mean(true));
			TrackerDrawings.drawString(g2, Exhibitable.CLICK_ON_A_POINT_TO_SET_ARENA_DIAGONAL_BOUNDARY,
					cameraPanel.getWidth() / 4, cameraPanel.getHeight() / 2);

			TrackerDrawings.drawArenaBoundary(arena, cameraPanel, g2, Exhibitable.transparent);
		} else if (Show.Arena.selected) {
			TrackerDrawings.drawArenaBoundary(arena, cameraPanel, g2, Properties.getAsComposite(Key.ArenaOpacity));
		}
	}

	public static int drawPosture(Graphics2D g2, Posture drawingPosture, int currentOffsetY, Epuck epuck) {
		if (!Show.Posture.selected) {
			return 0;
		}
		g2.setFont(Font.small(true));
		g2.setColor(Color.WHITE);
		g2.setComposite(Exhibitable.fullOpaque);
		currentOffsetY += g2.getFontMetrics().getHeight();
		String postureString = epuck.getPostureCm().toString();

		drawString(g2, postureString,
				(int) (drawingPosture.x
						+ 0.5f * (epuck.getBodyRadiusInPx() - g2.getFontMetrics().stringWidth(postureString))),
				drawingPosture.y + currentOffsetY);

		return currentOffsetY + g2.getFontMetrics().getHeight();
	}

	public static int drawFiducial(Graphics2D g2, Posture drawingPosture, int currentOffset, BufferedImage image,
			Epuck epuck) {
		if (!Show.Posture.selected) {
			return 0;
		}
		g2.setFont(Font.small(true));
		g2.setColor(Color.WHITE);
		g2.setComposite(Exhibitable.transparent);
		int fiducialLength = epuck.getBodyRadiusInPx();
		g2.drawImage(image, drawingPosture.x, drawingPosture.y + currentOffset, fiducialLength, fiducialLength,
				Color.BLACK, null);
		return currentOffset + fiducialLength;
	}

	public static int drawID(Graphics2D g2, Posture drawingPosture, int currentOffsetY, boolean unique, Epuck epuck) {
		if (!Show.ID.selected) {
			return 0;
		}
		g2.setFont(Font.normal(true));
		if (unique) {
			g2.setColor(Color.WHITE);
		} else {
			g2.setColor(Color.RED);
		}
		g2.setComposite(Exhibitable.fullOpaque);
		String idString = String.format("%d", epuck.getId());

		currentOffsetY -= g2.getFontMetrics().getHeight() / 2;
		drawString(g2, idString,
				(int) (drawingPosture.x
						+ 0.5f * (epuck.getBodyRadiusInPx() - g2.getFontMetrics().stringWidth(idString))),
				drawingPosture.y + currentOffsetY);
		return currentOffsetY + g2.getFontMetrics().getHeight() / 2;
	}

	public static void drawRange(Graphics2D g2, Posture drawingPosture, Epuck epuck) {
		if (!Show.Range.selected) {
			return;
		}

		drawRange(g2, drawingPosture, epuck.getRangeInPx(), epuck.isSelected(), epuck.isOnFail(),
				Exhibitable.BLUE_COLOR);
	}

	public static void drawRange(Graphics2D g2, Posture drawingPosture, int range, boolean selected, boolean fail,
			Color color) {
		g2.setFont(Font.small(false));
		g2.setColor(color);
		g2.setComposite(Exhibitable.veryTransparent);
		g2.fillOval(drawingPosture.x - range, drawingPosture.y - range, 2 * range, 2 * range);

		if (selected) {
			g2.setColor(Color.RED);
			g2.setStroke(Exhibitable.thinStroke);
			g2.drawOval(drawingPosture.x - range, drawingPosture.y - range, 2 * range, 2 * range);
		}

		if (fail) {
			g2.setColor(Color.RED);
			g2.setFont(Font.mean(false));
			g2.setComposite(Exhibitable.halfOpaque);
			String message = "ON FAIL";
			g2.drawString(message, drawingPosture.x - g2.getFontMetrics().stringWidth(message) / 2,
					drawingPosture.y + g2.getFontMetrics().getHeight() / 2 + range);
		}
	}

	public static void drawPostureIndicator(Graphics2D g2, Posture center, double offsetFromCenter, double distance,
			int angle) {

		if (!Show.Posture.selected) {
			return;
		}

		g2.setColor(Color.GREEN);
		g2.setComposite(Exhibitable.transparent);
		g2.setStroke(Exhibitable.mediumStroke);
		double rad = angle * Math.PI / 180;

		g2.drawLine((int) (center.x + offsetFromCenter * Math.cos(rad)),
				(int) (center.y + offsetFromCenter * Math.sin(rad)), (int) (center.x + distance * Math.cos(rad)),
				(int) (center.y + distance * Math.sin(rad)));
	}

	public static void drawEpucksTracked(int totalPatternsFound, Graphics2D g2, Camera camera) {
		g2.setFont(Font.small(false));
		g2.setColor(Color.WHITE);
		String info = String.format("Epucks tracked: %2d", totalPatternsFound);
		drawString(g2, info, camera.getSouthwestCorner().x - g2.getFontMetrics().stringWidth(info),
				camera.getSouthwestCorner().y);
	}
}
