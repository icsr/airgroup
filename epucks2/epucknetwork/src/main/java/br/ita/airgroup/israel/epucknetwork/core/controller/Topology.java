package br.ita.airgroup.israel.epucknetwork.core.controller;

import java.awt.Point;
import java.io.IOException;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.tuple.Pair;

import br.ita.airgroup.israel.commons.clock.Clock;
import br.ita.airgroup.israel.commons.graph.Graph;
import br.ita.airgroup.israel.commons.thread.DaemonThread;
import br.ita.airgroup.israel.commons.utils.MathUtils;
import br.ita.airgroup.israel.commons.validation.NumberValidation;
import br.ita.airgroup.israel.commons.validation.exception.ValidationException;
import br.ita.airgroup.israel.epucknetwork.core.algorithm.Algorithm;
import br.ita.airgroup.israel.epucknetwork.core.command.PutSelector;
import br.ita.airgroup.israel.epucknetwork.core.command.PutSelectorParam;
import br.ita.airgroup.israel.epucknetwork.core.imaging.Arena;
import br.ita.airgroup.israel.epucknetwork.core.imaging.Fiducial;
import br.ita.airgroup.israel.epucknetwork.helper.BaseSystem;
import br.ita.airgroup.israel.epucknetwork.helper.Logger;
import br.ita.airgroup.israel.epucknetwork.helper.Posture;
import br.ita.airgroup.israel.epucknetwork.helper.PostureRandomizer;
import br.ita.airgroup.israel.epucknetwork.helper.exception.EmptyException;
import br.ita.airgroup.israel.epucknetwork.helper.exception.PortAccessException;
import br.ita.airgroup.israel.epucknetwork.helper.exception.SettingsException;
import br.ita.airgroup.israel.epucknetwork.helper.file.CSVChooser;
import br.ita.airgroup.israel.epucknetwork.helper.file.DirectoryChooser;
import br.ita.airgroup.israel.epucknetwork.helper.file.Properties;
import br.ita.airgroup.israel.epucknetwork.helper.file.Properties.Key;
import br.ita.airgroup.israel.epucknetwork.helper.file.ScriptGenerated;
import br.ita.airgroup.israel.epucknetwork.helper.message.Messenger;
import br.ita.airgroup.israel.epucknetwork.helper.observer.Observable;
import br.ita.airgroup.israel.epucknetwork.helper.observer.Observer;
import br.ita.airgroup.israel.epucknetwork.matlab.helper.TopologyGain;

public class Topology implements Observer<Set<Epuck>> {

	private static final int MIN_TIME_BETWEEN_WAKE_UPS = 10;
	private static final int MAX_TIME_OUT_OF_CONTACT = 10;
	private static final int SEEK_TRACKING_DISLOCATION = 10;
	public static final int ONE_SECOND_IN_MILS = 1000;
	public static final long FIVE_SECONDS_IN_MILS = 5000;
	public static final long TEN_SECONDS_IN_MILS = 10000;
	public static final long FIVE_MINUTES_IN_SEC = 60 * 5;
	Map<Integer, Epuck> epucks = new HashMap<>();
	Map<Integer, BluetoothHandler> bluetooths = new HashMap<>();
	Map<Integer, Fiducial> fiducials = new HashMap<>();
	protected boolean initialized = false;
	protected Path fiducialFolder;
	private Optional<Algorithm> algorithm = Optional.empty();
	private Optional<Arena> arena = Optional.empty();
	private Map<PutSelector, List<PutSelectorParam>> putSelectors = new HashMap<>();
	protected static Path savingFolder = BaseSystem.POSTURES_DATA_HOME;
	private TopologyGain gain = new TopologyGain(1, 0, 0, 1);
	private Path recordingFolder = BaseSystem.RECORDING_HOME;
	private TopologyLoader topologyLoader = new TopologyLoader();
	private Optional<Map<BluetoothHandler, Boolean>> wellPlacedEpuck = Optional.empty();
	private List<BluetoothHandler> notWellPlaced = new ArrayList<>();
	private LocalDateTime lastWakeup;

	static {
		Logger.info(Topology.class, true, "says: have selector on position 1");
	}

	public Topology(Path fiducialFolder) throws SettingsException, EmptyException, PortAccessException {
		this.fiducialFolder = fiducialFolder;
	}

	public void init() throws SettingsException, EmptyException, PortAccessException {

		ScriptGenerated.populateFiducialsEpucksAndBluetoothMap(fiducialFolder, bluetooths, epucks, fiducials);
		initialized = true;

		initPutSelectorsMap();

		connectThemAll();
	}

	public void setGain(float connectivity, float robustness, float coverageArea, float collisionAvoidance) {
		
		try {
			NumberValidation.checkNormalized(connectivity, robustness, coverageArea, collisionAvoidance);
		} catch (ValidationException e1) {
			e1.printStackTrace();
			return;
		}
		
		gain.setConnectivity(connectivity);
		gain.setRobustness(robustness);
		gain.setCoverageArea(coverageArea);
		gain.setCollisionAvoidance(collisionAvoidance);

		epucks.values().stream().forEach(e -> {
			getBluetooth(e.getId()).sendTopologyGain(gain);
			e.setTopologyGain(gain);
		});
	}

	public TopologyGain getGain() {
		return gain;
	}

	public void initPutSelectorsMap() {
		Arrays.stream(PutSelector.values()).forEach(v -> {
			List<PutSelectorParam> params = new ArrayList<>();
			v.getParams().forEach(p -> {
				params.add(PutSelectorParam.of(p.getLabel(), Properties.getAsFloat(v, p)));
			});
			putSelectors.put(v, params);
		});
	}

	private void connectThemAll() {
		reconnect(getBluetooths());
		sendConfigs();
	}

	public void shutdown() {
		stop();
		disconnectAll();

		initialized = false;
	}

	@Override
	protected void finalize() throws Throwable {
		shutdown();
	}

	public List<Epuck> getEpucks() {
		return this.epucks.values().stream().sorted().collect(Collectors.toList());
	}

	public Epuck getEpuck(int id) {
		return this.epucks.get(id);
	}

	public Set<Integer> getEpuckIds() {
		return this.epucks.keySet();
	}

	public BluetoothHandler getBluetooth(Integer id) {
		return bluetooths.get(id);
	}

	public void start() {

		if (isRunning()) {
			return;
		}

		Logger.info(Topology.class, "Starting...");
		if (!initialized) {
			Logger.error(this.getClass(), "Topology not initialized!");
			return;
		}

		if (!algorithm.isPresent()) {
			Logger.error(this.getClass(), "Algorithm not set");
			return;
		}

		algorithm.ifPresent(a -> {
			a.start();
		});

		this.lastWakeup = Clock.now();
	}

	private boolean isRunning() {
		return algorithm.isPresent() && algorithm.get().isRunning();
	}

	public void stop() {
		algorithm.filter(a -> a.isRunning()).ifPresent(a -> a.stop());

		Logger.info(Topology.class, "Has stop.");
	}

	public boolean hasInitted() {
		return initialized;
	}

	public List<BluetoothHandler> getBluetooths() {
		return bluetooths.values().stream().sorted((a, b) -> a.getEpuck().getId() - b.getEpuck().getId())
				.collect(Collectors.toList());
	}

	public Map<Integer, Fiducial> getFiducials() {
		return fiducials;
	}

	public Fiducial getFiducial(int id) {
		return fiducials.get(id);
	}

	public synchronized void disconnect(List<BluetoothHandler> handlers) {
		handlers.forEach(b -> {
			b.disconnect();
		});
	}

	public synchronized void reconnect(List<BluetoothHandler> handlers) {
		handlers.forEach(b -> {
			b.connect();
		});
	}

	public synchronized void resetEpucks(List<BluetoothHandler> handlers) {
		handlers.forEach(h -> h.resetEpuck());
	}

	public synchronized void failEpucks(List<BluetoothHandler> handlers) {
		handlers.forEach(b -> b.failEpuck());
	}

	public void disconnectAll() {
		bluetooths.values().forEach(b -> {
			b.disconnect();
		});
	}

	public void setArena(Arena arena) {
		this.arena = Optional.of(arena);
		this.getBluetooths().forEach(b -> b.setArena(arena));
	}

	public Optional<Arena> getArena() {
		return arena;
	}

	public void reconnectAll() {
		reconnect(this.getBluetooths());
	}

	public void resetAll() {
		resetEpucks(getBluetooths());
	}

	@Override
	public void observe(Observable<Set<Epuck>> observable) {
		bluetooths.values().iterator().next().observe(observable);
	}

	public void lightThemAll(boolean on) {
		bluetooths.values().forEach(b -> {
			b.lightEpuck(on);
		});
	}

	private void resetConnections(List<BluetoothHandler> handlers) {
		disconnect(handlers);

		try {
			Thread.sleep(5 * DaemonThread.WAITING_TIME * handlers.size());
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		reconnect(handlers);

		try {
			Thread.sleep(5 * DaemonThread.WAITING_TIME * handlers.size());
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		resetEpucks(handlers);

		try {
			Thread.sleep(5 * DaemonThread.WAITING_TIME * handlers.size());
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void listEpucks() {

		String format = "\n%-4s %-8s %-20s %s";
		StringBuilder str = new StringBuilder();
		str.append(String.format(format, "ID", "PORT", "FIDUCIAL", "HCI"));
		getEpucks().forEach(e -> {
			str.append(String.format(format, e.getId(), e.getPort().getPortName(),
					getFiducial(e.getId()).getFiducialFile().toString(), e.getBluetoothAdapter()));
		});

		Logger.info(Topology.class, true, str.toString());
	}

	public void setAlgorithm(Algorithm algorithm) {
		this.algorithm = Optional.of(algorithm);

		getEpucks().forEach(e -> e.setAlgorithm(algorithm));
	}

	public Optional<Algorithm> getAlgorithm() {
		return algorithm;
	}

	public int size() {
		return getEpucks().size();
	}

	public void putCommand(PutSelector putSelector, List<PutSelectorParam> params) {
		putSelectors.put(putSelector, params);
		getBluetooths().forEach(b -> b.putCommand(putSelector, PutSelectorParam.getValues(putSelector, params)));
	}

	private Runnable getResender(BluetoothHandler handler) {
		return () -> {
			putSelectors.forEach((k, v) -> {
				handler.putCommand(k, PutSelectorParam.getValues(k, v));
				try {
					Thread.sleep(DaemonThread.WAITING_TIME);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			});
			arena.ifPresent(a -> handler.sendArenaBoudary(a));

			try {
				Thread.sleep(DaemonThread.WAITING_TIME);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			handler.sendTopologyGain(gain);

			try {
				Thread.sleep(DaemonThread.WAITING_TIME);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			handler.sendReadPosture();
		};
	}

	public void sendConfigs() {
		sendConfigs(getBluetooths());
	}

	private void sendConfigs(List<BluetoothHandler> bluetooths) {
		bluetooths.forEach(handler -> {
			DaemonThread.buildAndStart(String.format("Config Sender for epuck %d", handler.getEpuck().getId()),
					getResender(handler));
		});
	}

	public void setEpuckRangeInCm(int rangeInCm) {
		getEpucks().forEach(e -> e.setRangeInCm(rangeInCm));
	}

	public Optional<Float> evaluateAlgebraicConnectivity() {

		Optional<Epuck> nonTrackedEpuck = getEpucks().stream().filter(e -> !e.isTrackedByCamera()).findAny();

		if (nonTrackedEpuck.isPresent()) {
			return Optional.empty();
		}

		return Optional.of((float) toGraphOnPostures().evaluateAlgebraicConnectivity());
	}

	public Graph<String> toGraphOnPostures() {
		return Posture.toGraph(getEpucks().stream().map(e -> e.getPostureCm()).collect(Collectors.toList()),
				Epuck.getRange(), currentConnectivityThreshold());
	}

	public static float currentConnectivityThreshold() {
		return Properties.getFirstAsFloat(PutSelector.ALGORITHM_CONFIGS);
	}

	public void saveToCSV() {
		try {
			DirectoryChooser chooser = new DirectoryChooser("Select Folder to save CSV",
					Optional.of(BaseSystem.POSTURES_DATA_HOME.toString()));
			Path csvPath = chooser.showAndSelectSingle().toPath();
			PostureRandomizer.save(csvPath, String.format("topology_%d_epucks", this.getEpucks().size()), this);

			persistTopology(csvPath);
		} catch (EmptyException | IOException e) {
			e.printStackTrace();
		}
	}

	public static List<Posture> readFromCSV(Path csvFilepath) throws IOException, SettingsException {
		return PostureRandomizer.readFromCSV(csvFilepath).getRight();
	}

	public Pair<Path, Boolean> loadTopology() throws IOException, SettingsException, EmptyException {
		Path csvPath = selectTopologyCSV();

		return Pair.of(csvPath, loadTopology(csvPath));
	}

	public void loadTopologyReset() {
		topologyLoader.reset();
	}

	public Path selectTopologyCSV() throws EmptyException {
		CSVChooser csvChooser = new CSVChooser("Select topology CSV file", BaseSystem.POSTURES_DATA_HOME);
		Path csvPath = csvChooser.showAndSelectSingle().toPath();
		persistTopology(csvPath);
		return csvPath;
	}

	private void persistTopology(Path csvPath) {
		BaseSystem.POSTURES_DATA_HOME = csvPath.toFile().isFile() ? csvPath.getParent() : csvPath;
		Properties.add(Key.PosturesHome, BaseSystem.POSTURES_DATA_HOME.toString());
	}

	public boolean loadTopology(Path csvFilepath) throws IOException, SettingsException {

		List<Posture> postures = readFromCSV(csvFilepath).stream()
				.filter(p -> this.getArena().get().isInsideCm(p.getPoint())).collect(Collectors.toList());

		if (postures.isEmpty()) {
			throw new SettingsException("This CSV is incompatible with the current arena's dimension.");
		}

		if (postures.size() != this.getEpucks().size()) {
			String message = String.format("This CSV refers to %d epucks, but there are %d in arena.", postures.size(),
					this.getEpucks().size());
			Messenger.info(message);
			throw new SettingsException(message);
		}

		Properties.add(Key.LastCSVFilePath, csvFilepath.toString());

		return moveClosestEpucksToPostures(postures);

	}

	public boolean moveClosestEpucksToPoints(List<Point> targetPoints) {

		Map<BluetoothHandler, Point> handlerToMove = topologyLoader.findBestClosestEpucks(getBluetooths(),
				targetPoints);

		wellPlacedEpuck = Optional.of(TopologyLoader.areEpucksWellPlaced(targetPoints, getBluetooths(),
				Properties.getAsInteger(Key.MAX_POSITIONING_ERROR_IN_CM)));

		this.notWellPlaced = wellPlacedEpuck.get().entrySet().stream().filter(e -> !e.getValue()).map(e -> e.getKey())
				.collect(Collectors.toList());

		if (notWellPlaced.isEmpty()) {
			return true;
		}

		notWellPlaced.forEach(h -> {
			if (h.getEpuck().isTrackedByCamera()) {
				h.sendMoveCommand(handlerToMove.get(h));
			} else {
				seekTracking(true);
			}
		});

		return notWellPlaced.isEmpty();
	}

	public List<Epuck> getNotWellPlacedEpucks() {
		return notWellPlaced.parallelStream().map(h -> h.getEpuck()).collect(Collectors.toList());
	}

	private void randomAngle(BluetoothHandler handler) {
		handler.sendRotateAngle((int) MathUtils.randomize(Pair.of(-60d, -15d), Pair.of(15d, 60d)));
	}

	public boolean moveClosestEpucksToPostures(List<Posture> targetPostures) {
		return moveClosestEpucksToPoints(targetPostures.stream().map(p -> p.getPoint()).collect(Collectors.toList()));
	}

	public void turnToZero() {
		getBluetooths().forEach(b -> b.sendTurnToAngle(0));
	}

	public void turnTo90() {
		getBluetooths().forEach(b -> b.sendTurnToAngle(90));
	}

	public Path setRecordingFolder(Path recordingFolder) {
		return this.recordingFolder = recordingFolder;
	}

	public Path getRecordingFolder() {
		return recordingFolder;
	}

	public List<Epuck> hasRecentlyMoved() {
		List<Epuck> list = getEpucks().stream().filter(e -> e.hasRecentlyDislocated()).collect(Collectors.toList());

		Logger.info(Topology.class, false, "Has recenlty moved: "
				+ list.stream().map(e -> e.toString()).reduce((a, b) -> String.format("%s,%s", a, b)).orElse("none"));
		return list;
	}

	public List<Epuck> hasMoved() {
		List<Epuck> list = getEpucks().stream().filter(e -> e.hasDislocated()).collect(Collectors.toList());
		Logger.info(Topology.class, false, "Has moved: "
				+ list.stream().map(e -> e.toString()).reduce((a, b) -> String.format("%s,%s", a, b)).orElse("none"));
		return list;
	}

	public List<Epuck> getTrackedEpucks() {
		return getEpucks().stream().filter(e -> e.isTrackedByCamera()).collect(Collectors.toList());
	}

	public List<BluetoothHandler> getUntrackedEpucks() {
		return getBluetooths().stream().filter(b -> !b.getEpuck().isTrackedByCamera()).collect(Collectors.toList());
	}

	public boolean hasUntrackedEpucks() {
		return !getUntrackedEpucks().isEmpty();
	}

	public boolean areAllEpucksTracked() {
		return getTrackedEpucks().size() == getEpucks().size();
	}

	public synchronized void seekTracking(boolean withDislocation) {

		List<DaemonThread> threads = new ArrayList<>();

		getUntrackedEpucks().forEach(b -> {

			if (b.getEpuck().isTrackedByCamera()) {
				return;
			}

			threads.add(DaemonThread.buildAndStart("TrackingSeeker", () -> {

				Logger.info(b.getEpuck(), "Seeking tracking...");
				randomAngle(b);

				try {
					Thread.sleep(ONE_SECOND_IN_MILS);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}

				if (b.getEpuck().isTrackedByCamera() || !withDislocation
						|| (wellPlacedEpuck.isPresent() && wellPlacedEpuck.get().get(b))) {
					return;
				}

				b.sendDislocation(SEEK_TRACKING_DISLOCATION);

				try {
					Thread.sleep(Topology.FIVE_SECONDS_IN_MILS);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				algorithm.filter(a -> a.isRunning()).ifPresent(a -> a.start(Arrays.asList(b)));
			}));

		});

		while (threads.stream().filter(t -> t.isAlive()).findAny().isPresent()) {
			try {
				Thread.sleep(FIVE_SECONDS_IN_MILS);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		threads.clear();
	}

	public List<Point> getPositions() {
		return getEpucks().stream().map(e -> e.getPostureCm().getPoint()).collect(Collectors.toList());
	}

	public boolean hasAllEpucksRecentlyStopped() {
		return hasRecentlyStopped().size() == getEpucks().size();
	}

	public boolean hasAllEpucksMoved() {
		return hasMoved().size() == getEpucks().size();
	}

	public boolean hasAllEpucksRecentlyMoved() {
		return hasRecentlyMoved().size() == getEpucks().size();
	}

	public boolean hasAtLeastOneEpuckRecentlyMoving() {
		return !hasRecentlyMoved().isEmpty();
	}

	public boolean hasAtLeastOneEpuckRecentlyNotMoving() {
		return hasRecentlyMoved().size() != getEpucks().size();
	}

	public void wakeSleepyEpucks(boolean resetIfNeverMoved) {

		if (!isRunning() || !Clock.hasExpired(lastWakeup, MIN_TIME_BETWEEN_WAKE_UPS)) {
			return;
		}

		hasRecentlyStopped().forEach(e -> {
			Logger.info(e, false, "Wake up!");
			getBluetooth(e.getId()).sendNeighbors();
		});

		if (!resetIfNeverMoved) {
			return;
		}

		List<BluetoothHandler> sleepyBluetooths = getEpucksWhoNeverMovedAndNotCommunicatedRecently();

		resetConnections(sleepyBluetooths);
		sendConfigs(sleepyBluetooths);

		this.algorithm.filter(a -> a.isRunning()).ifPresent(a -> a.start(
				hasNeverMoved().parallelStream().map(e -> getBluetooth(e.getId())).collect(Collectors.toList())));

		this.lastWakeup = Clock.now();
	}

	private List<BluetoothHandler> getEpucksWhoNeverMovedAndNotCommunicatedRecently() {
		return hasNeverMoved().stream().filter(e -> e.getLastCommand().isPresent()
				&& Clock.hasExpired(e.getLastCommand().get().getLeft(), MAX_TIME_OUT_OF_CONTACT)).map(e -> {
					BluetoothHandler bluetooth = getBluetooth(e.getId());
					Logger.warn(e, "Resetting to wake it up...");
					return bluetooth;
				}).collect(Collectors.toList());
	}

	public List<Epuck> hasRecentlyStopped() {
		return ListUtils.subtract(getEpucks(), hasRecentlyMoved());
	}

	private List<Epuck> hasNeverMoved() {
		return ListUtils.subtract(getEpucks(), hasMoved());
	}
}
