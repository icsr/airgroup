package br.ita.airgroup.israel.epucknetwork.core.algorithm;

import java.util.ArrayList;
import java.util.List;

import br.ita.airgroup.israel.epucknetwork.core.controller.BluetoothHandler;
import br.ita.airgroup.israel.epucknetwork.core.imaging.Arena;
import br.ita.airgroup.israel.epucknetwork.helper.Posture;
import br.ita.airgroup.israel.epucknetwork.helper.annotation.Algorithm;

@Algorithm
public class SquareGroup extends NonAutonomyAlgorithm {

	SquareGroup(int separationInCm, boolean waitForEachLine) {
		this.separationInCm = separationInCm;
		this.waitForEachLine = waitForEachLine;
	}

	public SquareGroup() {

	}

	protected void groupOnArena(List<BluetoothHandler> bluetooths, Arena arena) {

		int maxCols = qtyPerLine;
		int maxLines = 2 + (int) Math.ceil((bluetooths.size() - 2 * maxCols) / 2);
		
		List<Posture> postures = new ArrayList<>();

		int xZero = arena.getCenterCm(true).x - maxCols * separationInCm / 2;
		int yZero = arena.getCenterCm(true).y - maxLines * separationInCm / 2;
		int x = xZero;
		int y = yZero;

		int elem = 0;
		for (int i = 0; i < maxLines; i++) {
			for (int j = 0; j < maxCols; j++) {
				if (i == 0 || i == (maxLines - 1) || j == 0 || j == (maxCols - 1)) {
					if(elem++ < bluetooths.size()){
						postures.add(new Posture(x + j * separationInCm, y + i * separationInCm, 0));
					}
				}
			}
		}
		
		moveClosestEpuckToPoint(postures);
	}


}
