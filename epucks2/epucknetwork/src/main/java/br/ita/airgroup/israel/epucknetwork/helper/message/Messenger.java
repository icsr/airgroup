package br.ita.airgroup.israel.epucknetwork.helper.message;

import java.awt.BorderLayout;
import java.awt.ScrollPane;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import br.ita.airgroup.israel.commons.thread.DaemonThread;
import br.ita.airgroup.israel.commons.utils.StringUtils;
import br.ita.airgroup.israel.epucknetwork.core.controller.Topology;
import br.ita.airgroup.israel.epucknetwork.core.imaging.Camera;
import br.ita.airgroup.israel.epucknetwork.gui.ApplicationGUI;
import br.ita.airgroup.israel.epucknetwork.gui.aux.Exhibitable;
import br.ita.airgroup.israel.epucknetwork.helper.BaseSystem;
import br.ita.airgroup.israel.epucknetwork.helper.exception.SettingsException;

public class Messenger {

	private static final String BEEP_SOUND_FILE = "./resources/audio/beep.wav";
	private static final String ALERT_SOUND_FILE = "./resources/audio/alert.wav";
	private static final String START_SOUND_FILE = "./resources/audio/start.wav";
	private static final String HTML_TAG_REGEX = "<.*?>";
	public static final String BREAKLINE = "<br/>";
	private static boolean active = true;
	private static Clip alert;
	private static Clip beep;
	private static Optional<Camera> camera = Optional.empty();
	private static Clip start;

	static {
		try {
			alert = AudioSystem.getClip();
			alert.open(AudioSystem.getAudioInputStream(new File(ALERT_SOUND_FILE)));
			beep = AudioSystem.getClip();
			beep.open(AudioSystem.getAudioInputStream(new File(BEEP_SOUND_FILE)));
			start = AudioSystem.getClip();
			start.open(AudioSystem.getAudioInputStream(new File(START_SOUND_FILE)));
		} catch (LineUnavailableException | IOException | UnsupportedAudioFileException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void warn(String header, Optional<String> footer, List<String> commands,
			Optional<String> postAlertMessage) throws SettingsException {
		info(header, footer, commands, postAlertMessage);
		throw new SettingsException("Permissão insuficiente ao sistema operacional. Execute os comandos previstos");
	}

	public static void info(String header, Optional<String> footer, List<String> commands) {
		info(header, footer, commands, Optional.empty());
	}

	public static void info(String header, String footer, String content) {
		info(header, Optional.of(footer), Arrays.asList(content));
	}

	public static void info(String header, Optional<String> footer, List<String> commands,
			Optional<String> postAlertMessage) {
		Optional<List<String>> commandsOp = Optional.ofNullable(commands);

		JPanel mainPanel = new JPanel(new BorderLayout());
		JLabel label = new JLabel(header);

		ScrollPane textPane = new ScrollPane();
		final JLabel text = new JLabel();
		text.setText(String.format("<html>%s</html>", String.join(BREAKLINE, commandsOp.orElse(new ArrayList<>()))));

		textPane.add(text);

		JButton copy = new JButton("Copy to clipboard");

		copy.addActionListener(e -> {
			BaseSystem.copyToClipboard(
					text.getText().replaceAll(BREAKLINE, "\n").replaceAll(HTML_TAG_REGEX, StringUtils.EMPTY));
		});

		JDialog dialog = new JDialog();

		JButton close = new JButton("Close and clear clipboard");
		close.addActionListener(e -> {
			postAlertMessage.ifPresent(m -> JOptionPane.showMessageDialog(null, postAlertMessage.get(),
					"Attention Required", JOptionPane.WARNING_MESSAGE));
			dialog.dispose();
		});

		JPanel buttonPanel = new JPanel(new BorderLayout());
		buttonPanel.add(close, BorderLayout.EAST);
		buttonPanel.add(copy, BorderLayout.WEST);
		buttonPanel.setBorder(BorderFactory.createEmptyBorder(20, 0, 0, 0));

		mainPanel.add(label, BorderLayout.NORTH);
		if (commandsOp.isPresent()) {
			mainPanel.add(textPane, BorderLayout.CENTER);
		}

		mainPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

		if (footer.isPresent()) {
			JLabel footerLabel = new JLabel(footer.get());
			mainPanel.remove(buttonPanel);
			JPanel southPanel = new JPanel(new BorderLayout());
			southPanel.add(buttonPanel, BorderLayout.SOUTH);
			southPanel.add(footerLabel, BorderLayout.NORTH);
			mainPanel.add(southPanel, BorderLayout.SOUTH);
		} else {
			mainPanel.add(buttonPanel, BorderLayout.SOUTH);
		}

		dialog.setTitle(ApplicationGUI.TITLE);
		dialog.setModal(true);
		dialog.setContentPane(mainPanel);

		dialog.pack();
		dialog.setLocation(BaseSystem.centeredPoint(dialog));
		dialog.setVisible(true);
	}

	public static void warn(String message, List<String> listOfThings, Optional<String> postAlertMessage)
			throws SettingsException {
		warn(message, Optional.empty(), listOfThings, postAlertMessage);
	}

	public static void warn(String message, List<String> listOfThings) throws SettingsException {
		warn(message, listOfThings, Optional.empty());
	}

	public static void error(String message) throws SettingsException {
		info(message);
		throw new SettingsException(message);
	}

	public static void info(String message) {
		if (Messenger.active) {
			JOptionPane.showMessageDialog(null, message, ApplicationGUI.TITLE, JOptionPane.INFORMATION_MESSAGE);
		}
	}

	public static void warning(String message) {
		JOptionPane.showMessageDialog(null, message, ApplicationGUI.TITLE, JOptionPane.WARNING_MESSAGE);
	}

	public static void criticalError(String header, List<String> listOfCriticalIssues) {
		info(header, Optional.empty(), listOfCriticalIssues);
		BaseSystem.shutdown();
	}

	public static boolean answerIfIsTrue(String question) {
		return JOptionPane.showConfirmDialog(null, question, Exhibitable.TITLE, JOptionPane.YES_NO_OPTION,
				JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION;
	}

	public static Optional<String> askSomething(String message) {
		return Optional
				.ofNullable(JOptionPane.showInputDialog(null, message, Exhibitable.TITLE, JOptionPane.PLAIN_MESSAGE));
	}

	public static void setActive(boolean active) {
		Messenger.active = active;
	}

	public static Optional<Double> askForANumber(String message) {
		return askSomething(message).map(s -> Double.parseDouble(s));
	}

	public static void beep() {
		playSound(beep);
	}

	public static void alert() {
		playSound(alert);
	}
	
	public static void start() {
		playSound(start);
	}

	private static void playSound(Clip clip) {
		DaemonThread.buildAndStart("BeepSoundThread", () -> {
			try {
				clip.start();
				while (clip.isRunning()) {
					Thread.sleep(Topology.ONE_SECOND_IN_MILS);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}
	
	public static void shutdown() {
		try {
			alert.stop();
			alert.close();
			beep.stop();
			beep.close();
			start.stop();
			start.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
