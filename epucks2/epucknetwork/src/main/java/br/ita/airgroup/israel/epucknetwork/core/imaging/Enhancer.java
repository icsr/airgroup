package br.ita.airgroup.israel.epucknetwork.core.imaging;

import java.awt.image.BufferedImage;

import boofcv.alg.enhance.EnhanceImageOps;
import boofcv.alg.misc.ImageStatistics;
import boofcv.io.image.ConvertBufferedImage;
import boofcv.struct.image.GrayU8;
import br.ita.airgroup.israel.epucknetwork.helper.file.Properties;
import br.ita.airgroup.israel.epucknetwork.helper.file.Properties.Key;

//import org.opencv.core.Core;
//import org.opencv.core.Mat;
//import org.opencv.imgcodecs.Imgcodecs;


public class Enhancer {
	
	private static int radius = Properties.getAsInteger(Key.EnhancerRadius);

	public static BufferedImage enhanceImage(BufferedImage image) {
		GrayU8 gray = ConvertBufferedImage.convertFrom(image, (GrayU8) null);
		GrayU8 adjusted = gray.createSameShape();

		int histogram[] = new int[256];
		int transform[] = new int[256];

		ImageStatistics.histogram(gray, 0, histogram);
		EnhanceImageOps.equalize(histogram, transform);
		EnhanceImageOps.applyTransform(gray, transform, adjusted);
		
		EnhanceImageOps.sharpen8(gray, adjusted);

		image = ConvertBufferedImage.convertTo(adjusted, null);
		return image;
	}
	
	public static synchronized void setRadius(int radius) {
		Enhancer.radius = radius;
		Properties.add(Key.EnhancerRadius, radius);
	}
	
	public static synchronized int getRadius() {
		return radius;
	}

}
