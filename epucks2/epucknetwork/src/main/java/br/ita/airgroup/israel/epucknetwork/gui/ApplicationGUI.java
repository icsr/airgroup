package br.ita.airgroup.israel.epucknetwork.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import br.ita.airgroup.israel.commons.news.Publisher;
import br.ita.airgroup.israel.commons.utils.Font;
import br.ita.airgroup.israel.commons.utils.SwingUtils;
import br.ita.airgroup.israel.epucknetwork.core.algorithm.Algorithm;
import br.ita.airgroup.israel.epucknetwork.core.algorithm.MatlabAlgorithm;
import br.ita.airgroup.israel.epucknetwork.core.command.Selector;
import br.ita.airgroup.israel.epucknetwork.core.controller.Topology;
import br.ita.airgroup.israel.epucknetwork.core.imaging.Camera;
import br.ita.airgroup.israel.epucknetwork.core.imaging.Enhancer;
import br.ita.airgroup.israel.epucknetwork.core.imaging.Fiducial;
import br.ita.airgroup.israel.epucknetwork.core.imaging.tracking.Tracker;
import br.ita.airgroup.israel.epucknetwork.gui.aux.AboutGUI;
import br.ita.airgroup.israel.epucknetwork.gui.aux.ChooseEpucksToPerformAction;
import br.ita.airgroup.israel.epucknetwork.gui.aux.Exhibitable;
import br.ita.airgroup.israel.epucknetwork.gui.aux.LoadingScreen;
import br.ita.airgroup.israel.epucknetwork.gui.aux.panel.PutSelectorPanel;
import br.ita.airgroup.israel.epucknetwork.gui.components.AlgorithmRadioButtonMenuItem;
import br.ita.airgroup.israel.epucknetwork.gui.components.GuiCheckbox;
import br.ita.airgroup.israel.epucknetwork.gui.components.GuiFloat;
import br.ita.airgroup.israel.epucknetwork.gui.components.GuiGroup;
import br.ita.airgroup.israel.epucknetwork.gui.components.GuiInteger;
import br.ita.airgroup.israel.epucknetwork.gui.components.GuiSlider;
import br.ita.airgroup.israel.epucknetwork.gui.components.GuiText;
import br.ita.airgroup.israel.epucknetwork.gui.components.SelectableCheckbox;
import br.ita.airgroup.israel.epucknetwork.helper.BaseSystem;
import br.ita.airgroup.israel.epucknetwork.helper.Logger;
import br.ita.airgroup.israel.epucknetwork.helper.exception.EmptyException;
import br.ita.airgroup.israel.epucknetwork.helper.exception.PortAccessException;
import br.ita.airgroup.israel.epucknetwork.helper.exception.SettingsException;
import br.ita.airgroup.israel.epucknetwork.helper.file.DirectoryChooser;
import br.ita.airgroup.israel.epucknetwork.helper.file.Properties;
import br.ita.airgroup.israel.epucknetwork.helper.file.Properties.Key;
import br.ita.airgroup.israel.epucknetwork.helper.message.Messenger;
import br.ita.airgroup.israel.epucknetwork.helper.observer.Observable;
import br.ita.airgroup.israel.epucknetwork.helper.tracking.TrackerDrawings.Show;

public class ApplicationGUI extends JFrame implements Exhibitable<ApplicationGUI>, Observable<Void>, AutoCloseable {

	private static final long serialVersionUID = -2603436290031149549L;

	protected Topology topology;

	protected Camera camera;

	private boolean epucksConnected;

	protected List<Component> topologyComponentsActivedWhenEpucksAreConnectedAndTracked = new ArrayList<>();

	protected List<Component> topologyComponentsDeactivedWhenEpucksAreConnectedAndTracked = new ArrayList<>();

	protected List<Component> topologyComponentsActivedWhenEpucksAreConnected = new ArrayList<>();

	private final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

	private JPanel cameraTabbedPane = new JPanel();

	protected Optional<Tracker> tracker = Optional.empty();

	private JMenuBar menuBar;

	private JMenu trackerMenu;

	private JMenuItem selectedFiducialFolder;

	private JMenu chooseCameraMenu;

	private ApplicationSidePanel sidePanel;

	private PutSelectorPanel putSelectorPanel;

	public GuiFloat experimentTimeToRun;

	public GuiText experimentGroupName;

	private GuiCheckbox<Boolean> normalizedEachControl;

	private JScrollPane cameraPanel;

	private GuiCheckbox<Void> convertToRandomWhenNullControl;

	static {
		LoadingScreen.showIt();
	}

	ApplicationGUI() throws FileNotFoundException, IOException, SettingsException {
		super(TITLE);
		Properties.add(Key.LOCK, true);

		init().then().initLogger().then().initTopologyAndTracker().then().initTracker().then().initExperiments().then()
				.showWindow();

		sidePanel.directCommandPanel.grabFocus();

		LoadingScreen.hideIt();

	}

	private ApplicationGUI initExperiments() {

		menuBar.add(initExperimentsMenu());

		return this;
	}

	private ApplicationGUI initTracker() {

		if (!Optional.ofNullable(trackerMenu).isPresent()) {
			trackerMenu = new JMenu("Tracker");
			menuBar.add(trackerMenu);
			initTrackerMenu();
		}

		if (!tracker.isPresent() || !camera.isCalibrated()) {
			trackerMenu.setEnabled(false);
		} else {
			trackerMenu.setEnabled(true);
		}

		return this;

	}

	private void initTrackerMenu() {

		if (!this.tracker.isPresent()) {
			return;
		}

		JCheckBoxMenuItem enhancedTracking = new JCheckBoxMenuItem("Enhanced Tracking");
		enhancedTracking.setSelected(Tracker.isEnhanceTracking());
		enhancedTracking.addActionListener(a -> {
			Tracker.setEnhanceTracking(enhancedTracking.isSelected());
		});

		trackerMenu.add(enhancedTracking);

		JMenu arenaMenu = new JMenu("Arena Boundary");

		JMenuItem boundaryItem = new JMenuItem("Set Boundary");
		boundaryItem.addActionListener(a -> tracker.get().setArenaBoundary());

		JMenuItem resetBoundaryItem = new JMenuItem("Reset Boundary");
		resetBoundaryItem.addActionListener(a -> tracker.get().resetArenaBoundary());

		arenaMenu.add(boundaryItem);
		arenaMenu.add(resetBoundaryItem);

		JMenu trackerThresholdMenu = new JMenu("Tracker Threshold");

		ButtonGroup group = new ButtonGroup();
		tracker.get().getThresholdAvailableConfigs().forEach(t -> {
			JRadioButtonMenuItem menuItem = new JRadioButtonMenuItem(t.name());
			group.add(menuItem);
			menuItem.addActionListener(
					a -> tracker.get().setTrackerThreshold(JMenuItem.class.cast(a.getSource()).getText()));
			trackerThresholdMenu.add(menuItem);

			menuItem.setSelected(Properties.get(Key.TrackerThreshold).equals(t.name()));
		});

		JPanel thresholdPanel = new JPanel(new GridLayout(0, 2));

		GuiInteger thresholdWidth = new GuiInteger("Local", 4, Key.TrackerThresholdWidth);
		thresholdWidth.whenChange(() -> tracker.get().setConfigThresholdWidth(thresholdWidth.getValue()));

		GuiInteger detectorThreshold = new GuiInteger("Global", 4, Key.DetectorThreshold);
		detectorThreshold.whenChange(() -> tracker.get().setDetectorThreshold(detectorThreshold.getValue()));

		thresholdPanel.add(thresholdWidth);
		thresholdPanel.add(detectorThreshold);

		trackerMenu.setMnemonic('k');
		trackerMenu.add(arenaMenu);
		trackerMenu.addSeparator();
		trackerMenu.add(trackerThresholdMenu);
		trackerMenu.add(thresholdPanel);
		trackerMenu.addSeparator();

		tracker.get().setShowNoneOnTracker();

		JPanel chooseWhatShowOnCamera = new JPanel(new GridLayout(0, 2));
		chooseWhatShowOnCamera.setBorder(BorderFactory.createTitledBorder("Show on camera"));

		Stream.of(Show.values()).sorted((v1, v2) -> v1.name().compareToIgnoreCase(v2.name())).forEach(v -> {
			boolean showThis = Properties.get(v);
			JCheckBox check = new JCheckBox(v.name());
			check.setSelected(showThis);
			chooseWhatShowOnCamera.add(check);
			tracker.get().setShowOnTracker(v, showThis);
			check.addActionListener(a -> changeShowOnTracker(a));
		});

		trackerMenu.add(chooseWhatShowOnCamera);

		topologyComponentsActivedWhenEpucksAreConnectedAndTracked.add(trackerMenu);
	}

	private void changeShowOnTracker(ActionEvent a) {

		JCheckBox changedCheck = JCheckBox.class.cast(a.getSource());
		Show show = Tracker.getShow(changedCheck.getText()).get();
		tracker.get().setShowOnTracker(show, changedCheck.isSelected());
		Properties.add(show, changedCheck.isSelected());
	}

	private ApplicationGUI initEpucksToCommandAndTracker() {

		if (epucksConnected) {
			activeEpucksComponents();
		}

		if (!this.camera.isCalibrated()) {
			return this;
		}

		tracker = Optional.of(new Tracker(this.camera, this.topology));
		topology.setArena(tracker.get().getArena());
		sidePanel.directCommandPanel.setTracker(tracker.get());

		activateTrackingComponents();

		return this;
	}

	private void activeEpucksComponents() {
		topologyComponentsActivedWhenEpucksAreConnected.forEach(c -> c.setEnabled(true));
	}

	private ApplicationGUI then() {
		return this;
	}

	private ApplicationGUI initLogger() {
		Logger.info(ApplicationGUI.class, "has started.");
		Logger.subscribe(sidePanel.logPane, Publisher.TYPE.PARTIAL);

		return this;
	}

	private ApplicationGUI initTopologyAndTracker() {

		if (topology != null && topology.hasInitted()) {
			topology.shutdown();
		}

		if (!Properties.constains(Key.FiducialFolder)) {
			Logger.info(ApplicationGUI.class, "Fiducial folder still not selected.");
			epucksConnected = false;
		} else {

			Logger.info(ApplicationGUI.class, "is initializing Topology...");

			try {
				topology = new Topology(Paths.get(Properties.get(Key.FiducialFolder)));
				topology.init();

				putSelectorPanel.setTopology(topology);
				putSelectorPanel.init();

				sidePanel.directCommandPanel.setTopology(topology);

				epucksConnected = true;

				initEpucksToCommandAndTracker();

				initTopologyMenu();

				Logger.info(ApplicationGUI.class, "Topology is ready!");
			} catch (SettingsException | EmptyException | PortAccessException e) {
				epucksConnected = false;
			}

		}

		return this;

	}

	private void initTopologyMenu() {

		JMenu topologyMenu = new JMenu("Topology");
		topologyMenu.setMnemonic('T');

		JMenuItem save = new JMenuItem("Save topology");
		save.setMnemonic('S');
		save.addActionListener(evt -> topology.saveToCSV());

		JMenuItem load = new JMenuItem("Load topology");
		load.setMnemonic('L');
		load.addActionListener(evt -> {
			try {
				topology.loadTopology();
			} catch (IOException | SettingsException | EmptyException e) {
				e.printStackTrace();
			}
		});

		topologyMenu.add(save);
		topologyMenu.add(load);

		menuBar.add(topologyMenu);
	}

	protected void activateTrackingComponents() {
		topologyComponentsActivedWhenEpucksAreConnectedAndTracked.forEach(c -> {
			c.setEnabled(epucksConnected);
		});

		topologyComponentsDeactivedWhenEpucksAreConnectedAndTracked.forEach(c -> {
			c.setEnabled(!epucksConnected);
		});
	}

	protected void disableUntilEpucksTracked(Component component) {
		component.setEnabled(false);
		topologyComponentsActivedWhenEpucksAreConnectedAndTracked.add(component);
	}

	protected void disableUntilEpucksConnected(Component component) {
		component.setEnabled(false);
		topologyComponentsActivedWhenEpucksAreConnected.add(component);
	}

	protected void disableUntilEpuckTracked(JPanel component, boolean includeChildren) {
		disableUntilEpucksTracked(component);

		IntStream.range(0, component.getComponentCount()).mapToObj(i -> component.getComponent(i))
				.forEach(c -> disableUntilEpucksTracked(c));
	}

	public ApplicationGUI init() {
		this.setVisible(false);

		this.setIconImage(appIcon.getImage());
		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		try {
			initCameraPanel();
		} catch (SettingsException e1) {
			e1.printStackTrace();
		}

		menuBar = initMenuBar(screenSize);
		sidePanel = new ApplicationSidePanel(cameraTabbedPane.getPreferredSize(), this);

		this.setLayout(new BorderLayout());
		this.add(menuBar, BorderLayout.NORTH);
		this.add(cameraTabbedPane, BorderLayout.CENTER);
		this.add(sidePanel, BorderLayout.EAST);

		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				exitApp();
			}

		});

		return this;

	}

	private void exitApp() {

		topology.shutdown();

		Properties.saveIt();

		if (!Messenger.answerIfIsTrue("Do you really want to exit?")) {
			return;
		}

		Logger.info(this.getClass(), "Application shutting down...");

		Properties.add(Key.LOCK, false);

		this.camera.shutdown();
		tracker.ifPresent(t -> t.shutdow());

		this.dispose();

		BaseSystem.shutdown();
	}

	private void initCameraPanel() throws SettingsException {

		camera = new Camera(Properties.getAsOptional(Key.CameraName), Optional.of(this));

		resizeCameraPanel();

		cameraTabbedPane.removeAll();
		cameraTabbedPane.add(cameraPanel);
	}

	private void resizeCameraPanel() {
		cameraPanel = new JScrollPane(camera.getPanel(), JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		cameraPanel.setAutoscrolls(true);
		SwingUtilities.invokeLater(() -> {
			cameraPanel.getVerticalScrollBar().setUnitIncrement(20);
			cameraPanel.getHorizontalScrollBar().setUnitIncrement(20);
		});

		cameraPanel.setPreferredSize(
				SwingUtils.min(SwingUtils.scaleDimension(screenSize, 0.70f, 0.9f), camera.getPanel().getSize()));
	}

	public static Supplier<? extends EmptyException> emptySelectionExceptionSupplier = () -> {
		return new EmptyException("Empty choice.");
	};

	private JMenuBar initMenuBar(Dimension screenSize) {
		JMenu epuckMenu = initEpuckMenu();

		JMenuBar menuBar = new JMenuBar();
		menuBar.add(initApplicationMenu());
		menuBar.add(initFiducialMenu());
		menuBar.add(initCameraMenu());
		menuBar.add(initLogMenu());
		menuBar.add(epuckMenu);

		return menuBar;
	}

	private JMenu initExperimentsMenu() {

		JMenu experimentsMenu = new JMenu("Experiments");

		experimentGroupName = new GuiText("Group Name", 10, Key.ExperimentGroupName);
		experimentTimeToRun = new GuiFloat("Time To Run (min)", 3, Key.ExperimentTimeToRunInMinutes);

		experimentGroupName.whenChange(() -> Properties.add(Key.ExperimentGroupName, experimentGroupName.getValue()));
		experimentTimeToRun
				.whenChange(() -> Properties.add(Key.ExperimentTimeToRunInMinutes, experimentTimeToRun.getValue()));

		GuiInteger repetitions = new GuiInteger("Qty Of Experiments", 4, Key.ExperimentRepetitions);
		repetitions.whenChange(() -> Properties.add(Key.ExperimentRepetitions, repetitions.getValue()));

		GuiInteger maxPositionError = new GuiInteger("Max Positioning Error (cm)", 4, Key.MAX_POSITIONING_ERROR_IN_CM);

		GuiGroup config = new GuiGroup("Config", 0, 2);

		config.add(experimentGroupName);
		config.add(experimentTimeToRun);
		config.add(repetitions);
		config.add(maxPositionError);

		experimentsMenu.add(config);

		experimentsMenu.addSeparator();

		JMenuItem changeRecordingFolder = new JMenuItem("Change Recording Folder");
		JMenuItem currentRecordingFolder = new JMenuItem(getCurrentRecordingHome());

		changeRecordingFolder.setMnemonic('R');
		changeRecordingFolder.addActionListener(e -> {
			BaseSystem.setRecordingHome();
			currentRecordingFolder.setText(getCurrentRecordingHome());
		});
		currentRecordingFolder.setFont(Font.small(false));
		currentRecordingFolder.setEnabled(false);

		experimentsMenu.add(changeRecordingFolder);
		experimentsMenu.add(currentRecordingFolder);

		experimentsMenu.addSeparator();

		experimentsMenu.addSeparator();

		normalizedEachControl = new GuiCheckbox<>("Normalize Each Control", Optional.empty(), Key.NormalizeControl);
		normalizedEachControl.addActionListener(e -> {
			topology.getAlgorithm().filter(a -> MatlabAlgorithm.class.isInstance(a)).ifPresent(a -> {
				MatlabAlgorithm.class.cast(a).setNormalizedEachControl(normalizedEachControl.isSelected());
				Properties.add(Key.NormalizeControl, normalizedEachControl.isSelected());
			});
		});
		
		normalizedEachControl.setEnabled(false);

		convertToRandomWhenNullControl = new GuiCheckbox<>("Random Control When Exception", Optional.empty(),
				Key.ConvertNullControlToRandom);
		convertToRandomWhenNullControl.addActionListener(e -> {
			try {
				Properties.add(Key.ConvertNullControlToRandom, convertToRandomWhenNullControl.isSelected());
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		});

		GuiGroup matlabControl = new GuiGroup("Matlab Control", 0, 2);
		
		GuiSlider safetyRange = new GuiSlider("Safety Range (%)", 0, 5, Key.RangeSafety);
		
		matlabControl.add(normalizedEachControl);
		matlabControl.add(convertToRandomWhenNullControl);

		experimentsMenu.add(matlabControl);
		
		experimentsMenu.addSeparator();
		
		experimentsMenu.add(safetyRange);

		experimentsMenu.addSeparator();

		JMenu chooseAlgorithm = new JMenu("Algorithm");

		ActionListener actionListener = l -> {
			AlgorithmRadioButtonMenuItem algorithmMenuItem = AlgorithmRadioButtonMenuItem.class.cast(l.getSource());
			algorithmMenuItem.setAlgorithm(true);
		};

		ButtonGroup group = new ButtonGroup();
		Algorithm.getAlgorithms().forEach(a -> {
			AlgorithmRadioButtonMenuItem alg = new AlgorithmRadioButtonMenuItem(a, topology, camera);

			chooseAlgorithm.add(alg);
			alg.addActionListener(actionListener);

			if (a.getClass().getName().equals(Properties.get(Key.TopologyAlgorithmName))) {
				alg.setAlgorithm(true);
				topology.setAlgorithm(a);
				alg.setSelected(true);
			}

			group.add(alg);
		});

		experimentsMenu.add(chooseAlgorithm);

		GuiGroup algorithmGains = new GuiGroup("Gains", new GridLayout(0, 1));

		GuiSlider connectivity = new GuiSlider("Connectivity Maint.", 0, 100, Key.ConnectivityGain);
		GuiSlider robustness = new GuiSlider("Robustness", 0, 100, Key.RobustnessGain);
		GuiSlider coverageArea = new GuiSlider("Coverage Area", 0, 100, Key.CoverageArea);
		GuiSlider collisionAvoidance = new GuiSlider("Collision Avoidance", 0, 100, Key.CollisionAvoidance);
		
		this.topology.setGain(connectivity.getValue(true), robustness.getValue(true), coverageArea.getValue(true),
				collisionAvoidance.getValue(true));

		Runnable listener = () -> {
			this.topology.setGain(connectivity.getValue(true), robustness.getValue(true), coverageArea.getValue(true),
					collisionAvoidance.getValue(true));
		};

		connectivity.whenChange(listener);
		robustness.whenChange(listener);
		coverageArea.whenChange(listener);
		collisionAvoidance.whenChange(listener);

		algorithmGains.add(connectivity);
		algorithmGains.add(robustness);
		algorithmGains.add(coverageArea);
		algorithmGains.add(collisionAvoidance);

		experimentsMenu.add(algorithmGains);

		return experimentsMenu;
	}

	private JMenu initLogMenu() {

		JMenu logMenu = new JMenu("Log");
		logMenu.setMnemonic('g');

		JPanel showOnLogPanel = new JPanel();
		showOnLogPanel.setLayout(new GridLayout(0, 2));

		Arrays.stream(Selector.values()).sorted((a, b) -> a.getDescription().compareToIgnoreCase(b.getDescription()))
				.map(s -> new SelectableCheckbox(s)).forEach(c -> showOnLogPanel.add(c));

		logMenu.add(showOnLogPanel);

		JButton selectAll = new JButton("Select All");
		JButton unselectAll = new JButton("Unselect All");

		ActionListener actionListener = a -> {
			Arrays.stream(showOnLogPanel.getComponents()).filter(c -> SelectableCheckbox.class.isInstance(c))
					.forEach(c -> {
						SelectableCheckbox.class.cast(c).setSelected(a.getSource() == selectAll);
					});
		};
		selectAll.addActionListener(actionListener);
		unselectAll.addActionListener(actionListener);

		JPanel showOnLogButtonPanel = new JPanel(new FlowLayout());

		showOnLogButtonPanel.add(selectAll);
		showOnLogButtonPanel.add(unselectAll);

		logMenu.add(showOnLogButtonPanel);

		return logMenu;
	}

	private String getCurrentRecordingHome() {
		return String.format("Current: %s", BaseSystem.RECORDING_HOME.toString());
	}

	private JMenu initEpuckMenu() {
		JMenu epuckMenu = new JMenu("Epucks");
		epuckMenu.setMnemonic('u');

		JMenuItem listEpuckButton = new JMenuItem("List them");
		listEpuckButton.setMnemonic('L');
		listEpuckButton.addActionListener(a -> topology.listEpucks());

		JMenuItem massiveActions = new JMenuItem("Massive Actions");
		massiveActions.setMnemonic('M');
		massiveActions.addActionListener(a -> chooseEpucksToPerformActionGUI());

		JMenuItem uploadFirmware = new JMenuItem("Upload Firmware");
		uploadFirmware.setMnemonic('U');
		uploadFirmware.addActionListener(a -> prepareUpload());

		epuckMenu.add(massiveActions);
		epuckMenu.addSeparator();
		epuckMenu.add(listEpuckButton);
		epuckMenu.addSeparator();
		epuckMenu.add(uploadFirmware);
		epuckMenu.addSeparator();

		putSelectorPanel = new PutSelectorPanel();

		epuckMenu.add(putSelectorPanel);

		disableUntilEpucksConnected(epuckMenu);

		return epuckMenu;
	}

	private void prepareUpload() {
		topology.disconnectAll();

		Messenger.warning("Send Firmware using Topology Shell Script\nONLY THEN click OK...");

		topology.reconnectAll();
	}

	private void chooseEpucksToPerformActionGUI() {

		if (!Optional.ofNullable(topology).isPresent()) {
			initTopologyAndTracker();
		}
		new ChooseEpucksToPerformAction(topology);
	}

	private JMenu initCameraMenu() {

		JMenu cameraMenu = new JMenu("Camera");
		cameraMenu.setMnemonic('C');

		GuiSlider brightness = new GuiSlider("Brightness", -30, 200, Key.Brightness);
		brightness.whenChange(() -> {
			camera.setBrightness(1f + (float) brightness.getValue() / 100);
		});

		JCheckBox showFilter = new JCheckBox("Show Enhanced Image");
		showFilter.setSelected(this.camera.isShowEnhancedImage());
		showFilter.addActionListener(a -> {
			this.camera.setShowEnhancedImage(showFilter.isSelected());
		});

		GuiSlider radiusSlider = new GuiSlider("Radius", 0, 500, Key.EnhancerRadius);
		radiusSlider.whenChange(() -> Enhancer.setRadius(radiusSlider.getValue()));

		JPanel enhancingPanel = new JPanel(new GridLayout(0, 1));
		enhancingPanel.setBorder(BorderFactory.createTitledBorder("Enhancements"));
		enhancingPanel.add(showFilter);
		enhancingPanel.add(radiusSlider);

		JMenuItem restartCameraMenu = new JMenuItem("Restart camera");
		restartCameraMenu.setMnemonic('R');
		restartCameraMenu.addActionListener(a -> this.camera.init());

		JMenuItem calibrateCameraMenu = new JMenuItem("Calibrate camera");
		calibrateCameraMenu.setMnemonic('C');
		calibrateCameraMenu.addActionListener(a -> calibrateCamera());

		chooseCameraMenu = new JMenu("Choose camera");
		chooseCameraMenu.setMnemonic('h');

		this.camera.getCameras().forEach(c -> {
			JMenuItem item = new JMenuItem(c.getName());
			chooseCameraMenu.add(item);
			item.addActionListener(a -> {
				JMenuItem selectedMenuItem = JMenuItem.class.cast(a.getSource());
				this.camera.setCamera(selectedMenuItem.getText());

				disableSelectedCameraOnMenu(selectedMenuItem.getText());
			});

			if (c.getName().equals(this.camera.getName())) {
				item.setEnabled(false);
			}
		});

		cameraMenu.add(brightness);
		cameraMenu.addSeparator();
		cameraMenu.add(chooseCameraMenu);
		cameraMenu.addSeparator();
		cameraMenu.add(enhancingPanel);
		cameraMenu.addSeparator();
		cameraMenu.add(calibrateCameraMenu);
		cameraMenu.add(restartCameraMenu);

		return cameraMenu;
	}

	private void disableSelectedCameraOnMenu(String cameraName) {
		IntStream.range(0, chooseCameraMenu.getItemCount()).forEach(i -> {
			if (chooseCameraMenu.getItem(i).getText().equals(cameraName)) {
				chooseCameraMenu.getItem(i).setEnabled(false);
			} else {
				chooseCameraMenu.getItem(i).setEnabled(true);
			}

		});
	}

	private void calibrateCamera() {

		this.cameraTabbedPane.setEnabled(false);
		this.camera.calibrate();
		this.cameraTabbedPane.setEnabled(true);
	}

	private JMenu initFiducialMenu() {
		JMenu fiducialMenu = new JMenu("Fiducial");
		fiducialMenu.setMnemonic('F');

		JMenuItem generateFiducialButton = new JMenuItem("Generate Printable EPS Fiducial");
		generateFiducialButton.setMnemonic('G');
		generateFiducialButton.addActionListener(a -> generatePrintableFiducialAction());

		JMenuItem selectFiducialFolderButton = new JMenuItem("Set fiducials folder and link to Epucks");
		selectFiducialFolderButton.setMnemonic('S');
		selectFiducialFolderButton
				.addActionListener(a -> selectFiducialFolder(Properties.getAsOptional(Key.FiducialFolder)));

		selectedFiducialFolder = new JMenuItem();
		selectedFiducialFolder.setEnabled(false);
		selectedFiducialFolder.setFont(Font.small(false));
		setSelectFiducialText();

		JMenuItem linkEpucksToFiducials = new JMenuItem("Link Epucks to Fiducials");
		linkEpucksToFiducials.setMnemonic('L');
		linkEpucksToFiducials.addActionListener(a -> linkEpucksToFiducials());

		fiducialMenu.add(generateFiducialButton);
		fiducialMenu.addSeparator();
		fiducialMenu.add(selectFiducialFolderButton);
		fiducialMenu.add(selectedFiducialFolder);
		fiducialMenu.addSeparator();
		fiducialMenu.add(linkEpucksToFiducials);

		return fiducialMenu;
	}

	private void linkEpucksToFiducials() {

		try {
			Fiducial.showEpuckLinkingGUI(this.topology.getFiducials(), Properties.getAsPath(Key.FiducialFolder));
			initTopologyAndTracker();
		} catch (SettingsException | EmptyException e) {
			e.printStackTrace();
		}
	}

	private JMenu initApplicationMenu() {
		JMenu applicationMenu = new JMenu("Application");
		applicationMenu.setMnemonic('A');

		JMenuItem aboutButton = new JMenuItem("About");
		aboutButton.setMnemonic('A');
		aboutButton.addActionListener(a -> new AboutGUI().showWindow());

		JMenuItem exitButton = new JMenuItem("Exit Application");
		exitButton.addActionListener(a -> exitApp());
		exitButton.setMnemonic('X');

		GuiSlider fontBaseSize = new GuiSlider("Font Base Size", 5, 40, Key.FontBaseSize);
		fontBaseSize.whenChange(() -> Font.setBaseSize(fontBaseSize.getValue()));

		GuiSlider arenaOpacity = new GuiSlider("Arena Opacity", 0, 100, Key.ArenaOpacity);

		applicationMenu.add(arenaOpacity);
		applicationMenu.add(fontBaseSize);
		applicationMenu.addSeparator();
		applicationMenu.add(aboutButton);
		applicationMenu.addSeparator();
		applicationMenu.add(exitButton);
		return applicationMenu;
	}

	private void selectFiducialFolder(Optional<String> initFolder) {
		try {
			selectFiducialFolder(new DirectoryChooser(TITLE, initFolder).showAndSelectSingle().toPath());
		} catch (EmptyException e) {
			return;
		}
	}

	private void selectFiducialFolder(Path initFolder) {
		Properties.add(Key.FiducialFolder, initFolder.toString());
		setSelectFiducialText();
		initTopologyAndTracker();
	}

	private void setSelectFiducialText() {
		this.selectedFiducialFolder.setText(String.format("Current: %s", Properties.get(Key.FiducialFolder)));
	}

	private void generatePrintableFiducialAction() {
		try {
			selectFiducialFolder(Fiducial.generatePrintableFiducial(Optional.empty()));
		} catch (EmptyException e1) {
			e1.printStackTrace();
		}
	}

	@Override
	public ApplicationGUI showWindow() {

		this.setMinimumSize(new Dimension(800, 600));
		this.pack();
		this.setLocationRelativeTo(null);
		this.setVisible(true);

		return this;
	}

	// TODO still need this?
	@Override
	public void notify(Void content) {
		this.setVisible(false);
		this.repaint();
		showWindow();
	}

	public Topology getTopology() {
		return topology;
	}

	@Override
	public void close() throws Exception {
		exitApp();
	}

	public void lockMenu() {
		lockMenu(true);
	}

	public void unlockMenu() {
		lockMenu(false);
	}

	private void lockMenu(boolean locked) {
		// TODO ELIMINATE?
		// this.menuBar.setEnabled(!locked);
		// IntStream.range(0, this.menuBar.getMenuCount()).forEach(i ->
		// menuBar.getMenu(i).setEnabled(!locked));
	}

}