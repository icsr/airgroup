package br.ita.airgroup.israel.epucknetwork.helper;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;

import org.apache.commons.lang3.StringUtils;

import br.ita.airgroup.israel.commons.utils.FileUtils;
import br.ita.airgroup.israel.commons.utils.SortedProperties;
import br.ita.airgroup.israel.epucknetwork.core.command.PutSelector;
import br.ita.airgroup.israel.epucknetwork.core.command.PutSelectorParam;
import br.ita.airgroup.israel.epucknetwork.helper.file.Properties;

public class ExperimentInfo {

	public static final String comment = "Each line of MAT-file contains:\\nTake #, Time (mils), Epuck ID, x (cm), y (cm), angle (deg), On Fail";

	public static Path save(Experiment experiment) throws IOException {

		Path filepath = FileUtils.getPath(experiment.getSavingFolder(), FileUtils.INFO_SUFFIX);
		SortedProperties prop = new SortedProperties(filepath);

		prop.put("Group", experiment.getGroupName());
		prop.put("Subgroup", experiment.getSubgroupName());
		prop.put("TimeToRun(sec)", Long.toString(experiment.getTimeToRun()));

		Properties.toMap().entrySet().stream().forEach(e -> {
			prop.put(getAsKey(e.getKey()), e.getValue());
		});

		Arrays.asList(PutSelector.values()).stream().forEach(s -> {
			s.getParams().forEach(p -> {
				prop.put(getAsKey(s, p), Float.toString(p.getValue()));
			});
		});

		prop.save(comment);
		return filepath;
	}

	static String getAsKey(PutSelector s, PutSelectorParam p) {
		return getAsKey(String.format("%s->%s", s, p.getKey()));
	}

	static String getAsKey(String key) {

		StringBuilder str = new StringBuilder();

		Arrays.asList(StringUtils.split(key.replaceAll("[\n\t=]", "_"))).forEach(t -> {
			String firstLetter = t.substring(0, 1).toUpperCase();
			str.append(firstLetter).append(t.substring(1));
		});

		return str.toString();
	}

}
