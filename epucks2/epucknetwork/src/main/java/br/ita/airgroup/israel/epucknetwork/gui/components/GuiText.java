package br.ita.airgroup.israel.epucknetwork.gui.components;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JTextField;

import br.ita.airgroup.israel.commons.messenger.Growl;
import br.ita.airgroup.israel.epucknetwork.helper.file.Properties;
import br.ita.airgroup.israel.epucknetwork.helper.file.Properties.Key;

public class GuiText extends JPanel implements Keyable<String, String>{
	
	protected static final long serialVersionUID = -2545582953443782450L;
	protected JTextField text;
	private Key key;

	public GuiText(String label, int columns, Key associatedKey) {
		this.key = associatedKey;
		text = new JTextField(columns);
		text.setText(loadKey());
		this.setBorder(BorderFactory.createTitledBorder(label));
		this.add(text);
		
		whenChange(() -> {});
	}
	
	public void whenChange(Runnable listener){
		text.addActionListener(evt -> {
			saveKey(text.getText());
			listener.run();
			Growl.show(String.format("%s saved.", key.name()));
		});
	}
	
	public String getValue(){
		return text.getText();
	}

	@Override
	public String loadKey() {
		return Properties.get(key);
	}

	@Override
	public void saveKey(String value) {
		Properties.add(key, value);
	}

	@Override
	public void setValue(String value) {
		text.setText(value);
	}

}
