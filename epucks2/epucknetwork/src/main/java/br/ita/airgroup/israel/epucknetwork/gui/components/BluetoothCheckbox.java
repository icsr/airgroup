package br.ita.airgroup.israel.epucknetwork.gui.components;

import javax.swing.JCheckBox;

import br.ita.airgroup.israel.epucknetwork.core.controller.BluetoothHandler;
import br.ita.airgroup.israel.epucknetwork.core.controller.Epuck;

public class BluetoothCheckbox extends JCheckBox{

	private static final long serialVersionUID = -2210864010717101751L;
	private BluetoothHandler handler;

	public BluetoothCheckbox(BluetoothHandler handler) {
		setHandler(handler);
	}
	
	public void setHandler(BluetoothHandler handler) {
		this.handler = handler;
		setText(handler.getEpuck().toString());
		this.setSelected(false);
	}

	public Epuck getEpuck() {
		return handler.getEpuck();
	}
	
	public BluetoothHandler getHandler(){
		return handler;
	}

	@Override
	protected void fireStateChanged() {
		handler.getEpuck().setSelected(this.isSelected());
		super.fireStateChanged();
	}

}
