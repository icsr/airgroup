package br.ita.airgroup.israel.epucknetwork.core.command;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import br.ita.airgroup.israel.commons.clock.Clock;
import br.ita.airgroup.israel.commons.coord.Duo;
import br.ita.airgroup.israel.commons.utils.ArrayUtils;
import br.ita.airgroup.israel.commons.utils.MathUtils;
import br.ita.airgroup.israel.commons.utils.StringUtils;
import br.ita.airgroup.israel.epucknetwork.core.comm.port.SerialPortCommunicationally;
import br.ita.airgroup.israel.epucknetwork.core.controller.Epuck;
import br.ita.airgroup.israel.epucknetwork.core.imaging.Arena;
import br.ita.airgroup.israel.epucknetwork.helper.BaseSystem;
import br.ita.airgroup.israel.epucknetwork.helper.Logger;
import br.ita.airgroup.israel.epucknetwork.helper.Posture;
import br.ita.airgroup.israel.epucknetwork.helper.exception.MaxSendingCommandsAttemptives;
import br.ita.airgroup.israel.epucknetwork.helper.exception.ParametersException;
import br.ita.airgroup.israel.epucknetwork.helper.message.Message;

public class EpuckCommand {

	public static final String SECOND_EIGENVECTOR_SET_COMMAND_REGEX = String.format("%c%s%s",
			Selector.SECOND_EIGENVECTOR.selector, Message.SEPARATOR, MathUtils.FLOAT_REGEX);
	public static final String SECOND_EIGENVALUE_SET_COMMAND_REGEX = String.format("%c%s%s",
			Selector.SECOND_EIGENVALUE.selector, Message.SEPARATOR, MathUtils.FLOAT_REGEX);
	public static final String SINGLE_COMMAND_REGEX;
	private static final int MAX_SENDING_TRIES = 2;
	private static final String NONE_REGEX = String.format("^%c%s.*$", Selector.NONE.selector, Message.SEPARATOR);
	public static final String NEIGHBORS_AS_PARAMS_REGEX = String.format("[0-9]+%s[0-9]+(%s%s)+", Message.SEPARATOR,
			Message.SEPARATOR, MathUtils.FLOAT_REGEX);

	static {
		StringBuilder regex = new StringBuilder("[");
		Arrays.stream(Selector.values()).filter(s -> s.isCommand()).forEach(v -> regex.append(v.selector));
		regex.append("](,").append(StringUtils.NUMBER_REGEX).append(")*");
		SINGLE_COMMAND_REGEX = regex.toString();
	}

	protected SerialPortCommunicationally<?> serialPort;
	protected Epuck epuck;
	private int sendingCommandErrorCount = 0;

	Optional<Posture> receivedPosture = Optional.empty();
	private boolean autoPostureSender;
	public boolean receivingMessage;
	private Duo<Boolean, Long> pingPong = Duo.of(false, 0l);

	synchronized void setReceivedPosture(Optional<String> receivedPosture) {
		this.receivedPosture = Optional.of(Posture.parsePosture(receivedPosture.get(), 2));
	}

	public EpuckCommand(SerialPortCommunicationally<?> serialPort, Epuck epuck) {
		this.serialPort = serialPort;
		this.epuck = epuck;
	}

	private List<Float> extractParamsAsFloat(String plainCommand) {
		return Stream.of(plainCommand.split(Message.SEPARATOR)).skip(1).map(s -> Float.parseFloat(s))
				.collect(Collectors.toList());
	}

	private static List<String> extractParams(String plainCommand) {
		return Stream.of(plainCommand.split(Message.SEPARATOR)).skip(1).collect(Collectors.toList());
	}

	protected static String extractPlainCommand(Selectable selector, List<String> params) {
		StringBuilder str = new StringBuilder();
		str.append(selector.getSelectorChar());

		params.forEach(p -> {
			str.append(Message.SEPARATOR);
			str.append(minimize(p));
		});

		return str.toString();
	}

	private static String minimize(String number) {
		if (Pattern.matches("^\\-{0,1}[0-9]+\\.0$", number)) {
			return number.replaceAll("\\.0", "");
		}

		if (Pattern.matches("^\\-{0,1}0\\.[0-9]+$", number)) {
			return number.replaceAll("0\\.", ".");
		}

		return number;
	}

	public boolean processCommandToSend(Selectable selector, Integer... params)
			throws ParametersException, MaxSendingCommandsAttemptives {
		return processCommandToSend(selector, ArrayUtils.toString(params));
	}

	public boolean processCommandToSend(PutSelector putSelector, List<Float> params)
			throws ParametersException, MaxSendingCommandsAttemptives {
		List<String> list = new ArrayList<>();
		list.add(Integer.toString(putSelector.getId()));
		list.addAll(params.stream().map(p -> MathUtils.toString(p)).collect(Collectors.toList()));
		return processCommandToSend(Selector.PUT, list);
	}

	public boolean processCommandToSend(PutSelector putSelector, Integer... params)
			throws ParametersException, MaxSendingCommandsAttemptives {
		List<String> list = new ArrayList<>();
		list.add(Integer.toString(putSelector.getId()));
		list.addAll(ArrayUtils.toString(params));
		return processCommandToSend(putSelector, list);
	}

	public boolean processCommandsToSend(String command) throws ParametersException, MaxSendingCommandsAttemptives {
		return processCommandToSend(Selector.get(command), extractParams(command));
	}

	public boolean processCommandsToSend(Selector selector, Float... params)
			throws ParametersException, MaxSendingCommandsAttemptives {
		return processCommandToSend(selector, ArrayUtils.toString(params));
	}

	private boolean processCommandToSend(Selectable selector, List<String> params)
			throws ParametersException, MaxSendingCommandsAttemptives {

		String plainCommand = extractPlainCommand(selector, params);

		logThis(selector, plainCommand);

		if (selector == Selector.MOVE) {
			if (!isPointInsideArena(params)) {
				epuck.getArena().ifPresent(a -> {
					Logger.warn(this.getClass(),
							String.format("Coordinate (%s) is off arena's limit [(%d,%d),(%d,%d)].",
									StringUtils.join(params), a.getOrigin(true).x, a.getOrigin(true).y,
									a.getDiagonal(true).x, a.getDiagonal(true).y));
				});
				return false;
			}
		} else if (selector == Selector.PING) {
			Clock.tic();
			pingPong.left = false;
		}

		return process(plainCommand, params);
	}

	private void logThis(Selectable selector, String plainCommand) {
		Logger.info(this.getClass(), epuck, Selector.isSelected(selector), plainCommand,
				String.format("(%s)", selector.getDescription()));
	}

	private void logThis(Epuck epuck, Selectable selector, String plainCommand) {
		Logger.info(epuck, Selector.isSelected(selector), plainCommand,
				String.format("(%s)", selector.getDescription()));
	}

	public boolean processReceivedCommand(String plainCommand)
			throws ParametersException, MaxSendingCommandsAttemptives {
		
		epuck.setLastReceivedCommand(plainCommand);		

		if (this.epuck.isOnFail()) {
			Logger.warn(epuck, String.format("is on fail. Ignoring received command: %s.", plainCommand));
			return true;
		}

		synchronized (receivedPosture) {

			Selectable selector = Selector.get(plainCommand);
			if (!isReceivableCommand(plainCommand, selector)) {
				return false;
			}

			logThis(epuck, selector, plainCommand);

			if (selector == Selector.NONE || selector == Selector.MOVE) {
				return true;
			}

			if (selector == Selector.ANSWER) {
				setReceivedPosture(Optional.of(plainCommand));
				return true;
			}

			if (selector == Selector.SET_NEIGHBORHOOD) {
				sendNeighborHood();
				return true;
			}

			if (selector == Selector.SECOND_EIGENVECTOR) {
				saveSecondEigenVector(plainCommand);
				return true;
			}

			if (selector == Selector.SECOND_EIGENVALUE) {
				saveSecondEigenValue(plainCommand);
				return true;
			}

			if (selector == Selector.GET_POSTURE) {
				return sendPosture();
			}

			if (selector == Selector.TRACKING_REACHED) {
				epuck.setTrackingReached(extractParamsAsFloat(plainCommand).get(0).intValue() == 1);
				return true;
			}

			if (selector == Selector.PING) {
				pingPong.left = true;
				pingPong.right = Clock.toc();
				Logger.info(epuck, true, extractParams(plainCommand).get(0));
				return true;
			}

		}
		Logger.error(EpuckCommand.class, "Command", plainCommand, "not recognized.");
		return false;

	}

	protected void saveSecondEigenVector(String plainCommand) {
		epuck.setSecondEigenVector(Arrays.stream(plainCommand.split(Message.SEPARATOR)).skip(1)
				.map(s -> Float.parseFloat(s)).findFirst().get());
	}

	protected void saveSecondEigenValue(String plainCommand) {

		epuck.setSecondEigenValue(Arrays.stream(plainCommand.split(Message.SEPARATOR)).skip(1)
				.map(s -> Float.parseFloat(s)).findFirst().get());
	}

	public void sendNeighborHood() throws ParametersException, MaxSendingCommandsAttemptives {
		processCommandToSend(Selector.SET_NEIGHBORHOOD, epuck.getNeighborhoodAsList());
	}

	protected boolean isReceivableCommand(String plainCommand) {
		return isReceivableCommand(plainCommand, Selector.get(plainCommand));
	}

	protected boolean isReceivableCommand(String plainCommand, Selectable selector) {

		if (selector == Selector.NONE) {
			return Pattern.matches(NONE_REGEX, plainCommand);
		}

		if (selector == Selector.PING) {
			return true;
		}

		if (!Pattern.matches(SINGLE_COMMAND_REGEX, plainCommand)) {
			Logger.error(EpuckCommand.class, String.format("Rejected invalid command: '%s'", plainCommand));
			return false;
		}

		if (!selector.isReceivable()) {
			Logger.error(EpuckCommand.class, "Rejected not receivable command: ", plainCommand);
			return false;
		}

		return true;
	}

	private boolean process(String plainCommand, List<String> params)
			throws ParametersException, MaxSendingCommandsAttemptives {

		if (!Optional.ofNullable(plainCommand).isPresent()) {
			return true;
		}

		Selector selector = Selector.get(plainCommand);

		switch (selector) {
		case MOVE:
			checkParams(2, params);
			break;
		case NONE:
		case ANSWER:
			return true;
		case ROTATE:
			checkParams(1, params);
			break;
		case SET_POSTURE:
			checkParams(4, params);
			break;
		case DISTANCE:
			checkParams(1, params);
			break;
		case TO_ANGLE:
			checkParams(1, params);
			break;
		default:
			break;
		}

		return sendMessageThruBluetooth(plainCommand);

	}

	private boolean isPointInsideArena(List<String> params) {
		if (!epuck.getArena().isPresent() || params.size() < 2) {
			return true;
		}

		return epuck.getArena().get()
				.isInsideCm(new Point(Integer.parseInt(params.get(0)), Integer.parseInt(params.get(1))));
	}

	private void checkParams(int n, List<?> params) throws ParametersException {
		if (params.size() != n) {
			throw new ParametersException(n);
		}
	}

	private boolean sendMessageThruBluetooth(String plainCommand) throws MaxSendingCommandsAttemptives {

		if (this.epuck.isOnFail()) {
			Logger.warn(epuck, String.format("is on fail. Ignoring sending command: %s.", plainCommand));
			return true;
		}

		if (hasCommandExceedInSize(plainCommand)) {
			Logger.error(epuck, "Command exceed maximum length allowed.");
			return false;
		}

		if (!serialPort.write(Message.dinamicMessage(plainCommand))) {
			if (sendingCommandErrorCount++ < MAX_SENDING_TRIES) {
				throw new MaxSendingCommandsAttemptives();
			}
			return false;
		} else {
			sendingCommandErrorCount = 0;
		}

		return true;
	}

	private boolean hasCommandExceedInSize(String plainCommand) {
		return plainCommand.length() > (BaseSystem.MAX_EPUCKS - 1) * 18 + 35;
	}

	public boolean sendPosture() throws ParametersException, MaxSendingCommandsAttemptives {
		if (!epuck.isTrackedByCamera()) {
			Logger.warn(epuck, "Ignored posture request until epuck got tracked.");
			return false;
		}

		processCommandToSend(Selector.SET_POSTURE, epuck.getId(), epuck.getPostureCm().x, epuck.getPostureCm().y,
				epuck.getPostureCm().angle);

		return true;
	}

	public void sendArenaBoundary(Arena arena) throws ParametersException, MaxSendingCommandsAttemptives {
		if (arena.isAdjustFactorSet()) {
			processCommandToSend(Selector.BOUNDARY, 0, 0, arena.getWidth(true), arena.getHeight(true));
		}
	}

	public static boolean isValidCommand(String command) {
		return Pattern.matches(SINGLE_COMMAND_REGEX, command);
	}

	public boolean isAutoPostureSenderActivated() {
		return this.autoPostureSender;
	}

	public boolean isReceivingMessage() {
		return receivingMessage;
	}

	public void setReceivingMessage(boolean receivingMessage) {
		this.receivingMessage = receivingMessage;
	}

	public Duo<Boolean, Long> getPingPong() {
		return pingPong;
	}

}
