package br.ita.airgroup.israel.epucknetwork.helper.exception;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import br.ita.airgroup.israel.epucknetwork.core.comm.port.SerialPortCommunicationally;
import br.ita.airgroup.israel.epucknetwork.helper.message.Messenger;

public class PortAccessException extends Exception {

	private static final String MESSAGE_HEADER = "These required ports are not available:";
	private static final String INSTRUCTIONS = "Please check Manual on section 'Bluetooth Connection'.";
	private List<SerialPortCommunicationally<?>> portsWithError;
	public PortAccessException(List<SerialPortCommunicationally<?>> portsWithError) {
		super(toMessage(portsWithError));

		this.portsWithError = portsWithError;
		
		Messenger.info(MESSAGE_HEADER, Optional.of(INSTRUCTIONS), portsWithError.stream().map(p -> p.getPortName()).collect(Collectors.toList()));
	}

	protected static String toMessage(List<SerialPortCommunicationally<?>> portsWithError) {

		return String.format("%s\n%s", MESSAGE_HEADER,
				String.join("\n\t", portsWithError.stream().map(p -> p.getPortName()).collect(Collectors.toList())));

	}

	public List<SerialPortCommunicationally<?>> getPortsWithError() {
		return portsWithError;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -386172685711638032L;

}
