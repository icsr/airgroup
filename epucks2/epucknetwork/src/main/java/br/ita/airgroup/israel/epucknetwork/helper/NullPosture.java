package br.ita.airgroup.israel.epucknetwork.helper;

public class NullPosture extends Posture {
	
	private static final long serialVersionUID = 6055250779221037992L;

	public NullPosture() {
		super(0,0,0);
	}

}
