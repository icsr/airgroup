package br.ita.airgroup.israel.epucknetwork.matlab;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import org.apache.commons.lang.StringUtils;

import com.mathworks.toolbox.javabuilder.MWException;
import com.mathworks.toolbox.javabuilder.MWNumericArray;

import br.ita.airgroup.israel.commons.coord.Position;
import br.ita.airgroup.israel.commons.utils.NumberUtils;
import br.ita.airgroup.israel.epucknetwork.core.command.Selector;
import br.ita.airgroup.israel.epucknetwork.core.controller.Epuck;
import br.ita.airgroup.israel.epucknetwork.helper.AlgorithmConfigs;
import br.ita.airgroup.israel.epucknetwork.helper.Logger;
import br.ita.airgroup.israel.epucknetwork.helper.file.Properties;
import br.ita.airgroup.israel.epucknetwork.helper.file.Properties.Key;
import br.ita.airgroup.israel.epucknetwork.matlab.helper.TopologyGain;
import br.ita.airgroup.israel.epucknetwork.matlab.helper.UControl;
import performControl.MatlabRuntime;

public class MatlabControlFacade extends MatlabFacade{

	private boolean convertNullControlToRandom = Properties.getAsBoolean(Key.ConvertNullControlToRandom);

	public MatlabControlFacade() throws MWException {

	}

	private Object[] toMatlabInput(double[][] positions, Object[] gains, float threshold, float epsilon, float range, float bodyRadius, float maxMovingDistance, float performSafeControl) {
		return new Object[] {positions, gains, threshold, epsilon, range, bodyRadius, maxMovingDistance, performSafeControl};
	}

	private Object[] toMatlabOutput() {
		return new Object[1];
	}

	public List<UControl> performControl(List<Position> positions, TopologyGain gains) throws MWException {

		Logger.info(this.getClass(), Selector.SHOW_CONTROL_EVALUATION.isSelected(), "Evaluating control for positions ",
				StringUtils.join(positions, ","));

		synchronized (matlab) {
			
			Object[] in = toMatlabInput(Position.toArray(positions), gains.toArray(), AlgorithmConfigs.getThreshold(),
					AlgorithmConfigs.getEpsilon(), AlgorithmConfigs.getSafetyRange(),
					Epuck.getBodyRadiusInCm(),
					AlgorithmConfigs.getMaxMovingDistance(),
					NumberUtils.toFloat(AlgorithmConfigs.isSafeControl()));
			Object[] out = toMatlabOutput();

			matlab.ifPresent(m -> {
				try {
					m.performControl(out, in);
				} catch (MWException e) {
					e.printStackTrace();
				}
			});

			return extractDotXY(out, positions.size());
		}
	}

	private List<UControl> extractDotXY(Object[] out, int numberOfRobots) {
		MWNumericArray array = (MWNumericArray) out[0];

		float[] data = array.getFloatData();
		List<UControl> controls = new ArrayList<>();

		for (int i = 0; i < data.length / 2; i++) {
			controls.add(new UControl(data[i], data[i + data.length / 2]));
		}

		return controls;
	}

	public static void shutdown() {
		MatlabRuntime.disposeAllInstances();
	}

	public static float[] toArray(double... values) {

		float[] array = new float[values.length];
		IntStream.range(0, values.length).forEach(i -> array[i] = (float) values[i]);

		return array;

	}

	public static List<Position> toList(float[] X, float[] Y) {

		List<Position> list = new ArrayList<>();
		IntStream.range(0, X.length).forEach(i -> list.add(new Position(X[i], Y[i])));

		return list;

	}

	public boolean isConvertNullControlToRandom() {
		return convertNullControlToRandom;
	}

	public void setConvertNullControlToRandom(boolean convertNullControlToRandom) {
		this.convertNullControlToRandom = convertNullControlToRandom;
	}

}
