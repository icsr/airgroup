package br.ita.airgroup.israel.epucknetwork.helper.tracking;

import java.awt.FontMetrics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.ddogleg.struct.FastQueue;
import org.ejml.UtilEjml;
import org.ejml.data.DMatrixRMaj;

import br.ita.airgroup.israel.commons.utils.MathUtils;
import br.ita.airgroup.israel.epucknetwork.core.controller.Epuck;
import br.ita.airgroup.israel.epucknetwork.core.controller.Topology;
import br.ita.airgroup.israel.epucknetwork.core.imaging.Arena;
import br.ita.airgroup.israel.epucknetwork.core.imaging.tracking.DraggedEpuck;
import br.ita.airgroup.israel.epucknetwork.helper.Posture;
import georegression.struct.point.Point2D_F64;
import georegression.struct.se.Se3_F64;
import georegression.struct.shapes.Polygon2D_F64;

public class TrackerUtils {

	private static final int MAX_ERROR_IN_PX_WHEN_COMPARING_SIDES = 4;

	public static Point2D_F64 getCenter(FastQueue<Point2D_F64> vertexes) {
		return vertexes.toList().stream().reduce((p1, p2) -> mean(p1, p2)).get();
	}

	public static Point2D_F64 mean(Point2D_F64... points) {
		return Arrays.stream(points).reduce((p1, p2) -> new Point2D_F64(mean(p1.x, p2.x), mean(p1.y, p2.y))).get();
	}

	public static double mean(double... values) {
		return MathUtils.mean(values);
	}

	private static double[] matrixToEulerXYZ(DMatrixRMaj M) {

		double[] euler = new double[3];
		double m31 = M.get(2, 0);
		double rotX, rotY, rotZ;

		if (Math.abs(Math.abs(m31) - 1) < UtilEjml.EPS) {
			double m12 = M.get(0, 1);
			double m13 = M.get(0, 2);

			rotZ = 0;
			double gamma = Math.atan2(m12, m13);

			if (m31 < 0) {
				rotY = Math.PI / 2.0;
				rotX = rotZ + gamma;
			} else {
				rotY = -Math.PI / 2.0;
				rotX = -rotZ + gamma;
			}
		} else {
			double m32 = M.get(2, 1);
			double m33 = M.get(2, 2);

			double m21 = M.get(1, 0);
			double m11 = M.get(0, 0);

			rotY = -Math.asin(m31);
			double cosRotY = Math.cos(rotY);
			rotX = Math.atan2(m32 / cosRotY, m33 / cosRotY);
			rotZ = Math.atan2(m21 / cosRotY, m11 / cosRotY);
		}

		euler[0] = rotX;
		euler[1] = rotY;
		euler[2] = rotZ;

		return euler;
	}

	private static double eulerToDegreee(double eulerAngle, double signal) {
		double angle;
		angle = (eulerAngle * 180.0) / Math.PI - 90;

		if (angle < 0) {
			angle += 360.0;
		}

		if (angle > 360) {
			angle = -360;
		}

		return angle;
	}

	public static double getAngle(Se3_F64 targetToSensor) {
		return eulerToDegreee(matrixToEulerXYZ(targetToSensor.getRotation())[2], targetToSensor.getRotation().get(2));
	}

	public static int textWidth(String text, FontMetrics metrics) {
		return text.length() * metrics.charWidth('o');
	}

	public static Posture adjustPosture(Posture posture, double factor) {
		return new Posture(MathUtils.round(posture.x * factor), MathUtils.round(posture.y * factor), posture.angle);
	}

	public static Point adjustPoint(Point point, double factor) {
		return new Point(MathUtils.round(point.x * factor), MathUtils.round(point.y * factor));
	}

	public static Point2D_F64 adjustPoint(Point2D_F64 point, double factor) {
		return new Point2D_F64(point.x * factor, point.y * factor);
	}

	public static double evaluateDistance(Point2D_F64 p1, Point2D_F64 p2) {
		return Math.sqrt(Math.pow(p1.x - p2.x, 2) + Math.pow(p1.y - p2.y, 2));
	}

	public static boolean isSquare(Polygon2D_F64 polygon) {
		if (polygon.size() != 4) {
			return false;
		}

		for (int i = 1; i < polygon.size(); i++) {
			if (Math.abs(polygon.getSideLength(i) - polygon.getSideLength(0)) > MAX_ERROR_IN_PX_WHEN_COMPARING_SIDES) {
				return false;
			}
		}

		return true;
	}

	public static Point cameraPositionInPxToAbsolutePositionInCm(Arena arena, Point pointPx,
			double adjustFactorInCmPerPx) {
		return adjustPoint(new Point(pointPx.x - arena.getOffset(false).x, pointPx.y - arena.getOffset(false).y),
				adjustFactorInCmPerPx);
	}

	public static Point cameraPositionInPxToAbsolutePositionInCm(Arena arena, Point2D_F64 pointInPx,
			double adjustFactorInCmPerPx) {
		return cameraPositionInPxToAbsolutePositionInCm(arena,
				new Point(MathUtils.round(pointInPx.x), MathUtils.round(pointInPx.y)), adjustFactorInCmPerPx);
	}

	public static Point2D_F64 absolutePositionInCmTocameraCoordinateInPx(Arena arena, Point pointInCm,
			double adjustFactorInCmPerPx) {
		Point2D_F64 adjustedPoint = adjustPoint(new Point2D_F64(pointInCm.x, pointInCm.y), 1d / adjustFactorInCmPerPx);
		adjustedPoint.x += arena.getOffset(false).x;
		adjustedPoint.y += arena.getOffset(false).y;
		return adjustedPoint;
	}

	public static Point absolutePositionInCmToRelativePositionInCm(Arena arena, Point pointInCm) {
		pointInCm.x -= arena.getOffset(true).x;
		pointInCm.y -= arena.getOffset(true).y;
		return pointInCm;
	}

	public static Point relativePositionInCmToAbsolutePositionInCm(Arena arena, Point pointInCm) {
		pointInCm.x += arena.getOffset(true).x;
		pointInCm.y += arena.getOffset(true).y;
		return pointInCm;
	}

	public static Optional<Epuck> findSelectedEpuck(List<Epuck> epucks, Point clickedPointCm) {
		return epucks.stream()
				.filter(e -> MathUtils.distance(e.getPostureCm().getPoint(), clickedPointCm) <= e.getRangeInCm())
				.reduce((e1, e2) -> MathUtils.distance(e1.getPostureCm().getPoint(), clickedPointCm) < MathUtils
						.distance(e2.getPostureCm().getPoint(), clickedPointCm) ? e1 : e2);
	}

	public static Optional<DraggedEpuck> findEpuck(Arena arena, Topology topology, double adjustFactorInCmPerPx,
			MouseEvent me) {
		Point clickedPointCm = TrackerUtils.cameraPositionInPxToAbsolutePositionInCm(arena, me.getPoint(),
				adjustFactorInCmPerPx);
		return TrackerUtils.findSelectedEpuck(topology.getEpucks(), clickedPointCm).map(e -> new DraggedEpuck(e));
	}

	public static Point abolutePositionInCmToRelativeCameraPositionInPx(Arena arena, Point point) {
		Point adjustPoint = adjustPoint(point, MathUtils.divide(1, arena.getAdjustFactorCmPerPx()));
		adjustPoint.translate(arena.getLeftPx(), arena.getTopPx());
		return adjustPoint;
	}

}
