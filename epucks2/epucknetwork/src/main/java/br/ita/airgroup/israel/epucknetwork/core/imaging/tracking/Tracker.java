package br.ita.airgroup.israel.epucknetwork.core.imaging.tracking;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import boofcv.abst.fiducial.SquareImage_to_FiducialDetector;
import boofcv.alg.distort.LensDistortionNarrowFOV;
import boofcv.alg.distort.pinhole.LensDistortionPinhole;
import boofcv.alg.filter.binary.GThresholdImageOps;
import boofcv.alg.filter.binary.ThresholdImageOps;
import boofcv.alg.shapes.polygon.DetectPolygonBinaryGrayRefine;
import boofcv.factory.fiducial.ConfigFiducialImage;
import boofcv.factory.fiducial.FactoryFiducial;
import boofcv.factory.filter.binary.ConfigThreshold;
import boofcv.factory.filter.binary.ThresholdType;
import boofcv.factory.shape.ConfigPolygonDetector;
import boofcv.factory.shape.FactoryShapeDetector;
import boofcv.io.calibration.CalibrationIO;
import boofcv.io.image.ConvertBufferedImage;
import boofcv.io.image.UtilImageIO;
import boofcv.struct.calib.CameraPinholeRadial;
import boofcv.struct.image.GrayF32;
import boofcv.struct.image.GrayU8;
import boofcv.struct.image.ImageType;
import br.ita.airgroup.israel.commons.utils.Font;
import br.ita.airgroup.israel.epucknetwork.core.controller.Epuck;
import br.ita.airgroup.israel.epucknetwork.core.controller.Topology;
import br.ita.airgroup.israel.epucknetwork.core.imaging.Arena;
import br.ita.airgroup.israel.epucknetwork.core.imaging.Camera;
import br.ita.airgroup.israel.epucknetwork.core.imaging.EdgeLengthAdjustFactor;
import br.ita.airgroup.israel.epucknetwork.core.imaging.Enhancer;
import br.ita.airgroup.israel.epucknetwork.core.imaging.Fiducial;
import br.ita.airgroup.israel.epucknetwork.gui.aux.Exhibitable;
import br.ita.airgroup.israel.epucknetwork.helper.Logger;
import br.ita.airgroup.israel.epucknetwork.helper.Posture;
import br.ita.airgroup.israel.epucknetwork.helper.file.Properties;
import br.ita.airgroup.israel.epucknetwork.helper.file.Properties.Key;
import br.ita.airgroup.israel.epucknetwork.helper.observer.Observable;
import br.ita.airgroup.israel.epucknetwork.helper.observer.Observer;
import br.ita.airgroup.israel.epucknetwork.helper.tracking.EpuckDragger;
import br.ita.airgroup.israel.epucknetwork.helper.tracking.TrackerDrawings;
import br.ita.airgroup.israel.epucknetwork.helper.tracking.TrackerDrawings.Show;
import br.ita.airgroup.israel.epucknetwork.helper.tracking.TrackerUtils;
import georegression.struct.point.Point2D_F64;
import georegression.struct.se.Se3_F64;
import georegression.struct.shapes.Polygon2D_F64;

public class Tracker implements Trackable, Observer<Void> {

	private static boolean enhanceTracking = Properties.getAsBoolean(Key.EnhanceImage);
	List<Integer> detectedEpucks = new ArrayList<>();

	private ThresholdType configThreshold = Properties.getAsTrackerThreshold(Key.TrackerThreshold);
	private int configThresholdWidth = Properties.getAsInteger(Key.TrackerThresholdWidth);
	private double detectorThreshold = Properties.getAsInteger(Key.DetectorThreshold);

	private Camera camera;
	private Topology topology;
	private CameraPinholeRadial param;
	private SquareImage_to_FiducialDetector<GrayF32> patternDetector;
	private List<Integer> sortedEpucksId = new ArrayList<>();
	private List<BufferedImage> sortedFiducials = new ArrayList<>();
	private final Arena arena;

	private DetectPolygonBinaryGrayRefine<GrayU8> polygonDetector;
	EdgeLengthAdjustFactor adjustFactor = new EdgeLengthAdjustFactor(Fiducial.SHAPE_SIZE_IN_CENTIMETERS);
	List<Observable<?>> observables = new ArrayList<>();
	private EpuckDragger epuckDragger;
	private LensDistortionNarrowFOV lensDistortion;
	private ArenaResizer arenaResizer;

	/*
	 * https://boofcv.org/index.php?title=Example_Fiducial_Square_Image
	 */
	public Tracker(Camera targetCamera, Topology topology) {
		this.camera = targetCamera;
		this.topology = topology;

		sortedEpucksId = topology.getEpuckIds().stream().sorted().collect(Collectors.toList());
		Collections.sort(sortedEpucksId);

		configureDetectors();

		this.camera.setTracker(this);

		this.arena = new Arena(Properties.getAsInteger(Key.ARENA_ORIGIN_X), Properties.getAsInteger(Key.ARENA_ORIGIN_Y),
				Properties.getAsInteger(Key.ARENA_DIAGONAL_X), Properties.getAsInteger(Key.ARENA_DIAGONAL_Y));

		this.camera.setArena(arena);

		epuckDragger = new EpuckDragger(this.camera.getPanel(), arena, topology, adjustFactor, observables);

		setShowAllOnTracker();

		this.arenaResizer = new ArenaResizer(arena, camera, this.topology, this.adjustFactor);
	}

	public void setShowAllOnTracker() {
		Stream.of(Show.values()).forEach(v -> v.selected = true);
	}

	public void setShowNoneOnTracker() {
		Stream.of(Show.values()).forEach(v -> v.selected = false);
	}

	public void setShowOnTracker(Show show, boolean selected) {
		show.selected = selected;
	}

	public static Optional<Show> getShow(String name) {
		return Stream.of(Show.values()).filter(v -> v.name().equals(name)).findFirst();
	}

	@Override
	public void configureDetectors() {

		if (!this.camera.checkIfCalibrated()) {
			Logger.info(Camera.class, "Proceed CALIBRATION on", this.camera.getName());
			return;
		}

		param = CalibrationIO.load(this.camera.getCalibrationParams().toFile());
		LensDistortionNarrowFOV lensDistortion = new LensDistortionPinhole(param);

		this.lensDistortion = lensDistortion;
		configurePatternDetector();
		configurePolygonDetector();
	}

	private void configurePatternDetector() {

		if (patternDetector == null) {
			proceedConfigurePatternDetector();
		} else {
			synchronized (patternDetector) {
				proceedConfigurePatternDetector();
			}
		}
	}

	public void proceedConfigurePatternDetector() {
		patternDetector = FactoryFiducial.squareImage(new ConfigFiducialImage(), configThreshold(), GrayF32.class);
		patternDetector.setLensDistortion(lensDistortion);

		this.sortedEpucksId.forEach(id -> {
			BufferedImage image = UtilImageIO.loadImage(topology.getFiducial(id).getFiducialFile().toString());

			GrayF32 convertedImage = ConvertBufferedImage.convertFrom(image, true, ImageType.single(GrayF32.class));
			patternDetector.addPatternImage(convertedImage, detectorThreshold,
					Properties.getAsInteger(Key.FiducialSizeCm));
			sortedFiducials.add(image);
		});

		Logger.info(Tracker.class, "Tracker innited with threshold", configThreshold.name());
	}

	private void configurePolygonDetector() {
		ConfigPolygonDetector config = new ConfigPolygonDetector(Fiducial.SHAPE_EDGES, Fiducial.SHAPE_EDGES);
		config.detector.convex = true;
		config.detector.maximumSides = Fiducial.SHAPE_EDGES;
		polygonDetector = FactoryShapeDetector.polygon(config, GrayU8.class);
	}

	@Override
	public void detectAndDrawPatternsAndPolygons(BufferedImage image, Graphics2D g2) {
		detectPolygonsAndEvaluateEdgeLengthAdjustFactor(image, g2);
		detectPatterns(image, g2);

		TrackerDrawings.drawAxisAndBoundary(arena, camera.getPanel(), arenaResizer.phase, g2);
		evalPatternsAndDrawInfos(g2);
		evalAndDrawCountour(g2);
		TrackerDrawings.drawLog(arena, g2);
		TrackerDrawings.drawClock(arena, g2);
		TrackerDrawings.drawDraggingEpuck(epuckDragger, arena, adjustFactor.consolidateFactorInCmPerPx(), g2);
	}

	private void evalAndDrawCountour(Graphics2D g2) {

		List<Polygon2D_F64> polygons = polygonDetector.getPolygons(null, null);
		g2.setStroke(Exhibitable.thinStroke);

		polygons.stream().filter(p -> TrackerUtils.isSquare(p)).forEach(p -> {
			Point polygonCenter = Arena.calculateCenterOf(IntStream.range(0, p.size())
					.mapToObj(i -> new Point((int) p.get(i).x, (int) p.get(i).y)).collect(Collectors.toList()));

			if (!arena.isInsidePx(polygonCenter)) {
				return;
			}
			
			adjustFactor.evaluate(p);

			g2.setColor(Color.ORANGE);
			g2.setComposite(Exhibitable.halfOpaque);

			TrackerDrawings.drawCountour(p, polygonCenter, g2, adjustFactor);
		});

	}

	private int detectPatterns(BufferedImage image, Graphics2D g2) {
		synchronized (patternDetector) {

			patternDetector.detect(convertImage(image));

			if (patternDetector.totalFound() <= 0) {
				g2.setFont(Font.mean(true));
				g2.setColor(Color.WHITE);
				TrackerDrawings.drawString(g2, Exhibitable.NOTHING_TRACKED, image.getWidth() / 4,
						image.getHeight() / 2);
				return 0;
			}

			return patternDetector.totalFound();
		}
	}

	private static GrayF32 convertImage(BufferedImage image) {
		if (isEnhanceTracking()) {
			image = Enhancer.enhanceImage(image);
		}

		return ConvertBufferedImage.convertFrom(image, true, ImageType.single(GrayF32.class));
	}

	private void detectPolygonsAndEvaluateEdgeLengthAdjustFactor(BufferedImage image, Graphics2D g2) {
		GrayU8 input = ConvertBufferedImage.convertFromSingle(image, null, GrayU8.class);
		GrayU8 binary = new GrayU8(input.width, input.height);

		int threshold = (int) GThresholdImageOps.computeOtsu(input, 0, 255);
		ThresholdImageOps.threshold(input, binary, threshold, true);

		polygonDetector.process(input, binary);
	}

	protected Epuck getEpuck(int detectorId) {
		return this.topology.getEpuck(this.sortedEpucksId.get(detectorId));
	}

	public List<ThresholdType> getThresholdAvailableConfigs() {
		return Arrays.stream(ThresholdType.values()).sorted((a, b) -> a.compareTo(b)).collect(Collectors.toList());
	}

	public void setTrackerThreshold(ThresholdType threshold) {
		this.configThreshold = threshold;

		Properties.add(Key.TrackerThreshold, threshold.name());

		configurePatternDetector();

		Logger.info(Tracker.class, "Threshold set as", threshold.name());
	}

	public void setTrackerThreshold(String thresholdType) {
		setTrackerThreshold(
				Arrays.stream(ThresholdType.values()).filter(t -> t.name().equals(thresholdType)).findFirst().get());
	}

	private ConfigThreshold configThreshold() {

		if (!configThreshold.isAdaptive()) {
			return ConfigThreshold.fixed(configThresholdWidth);
		}

		if (configThreshold.isGlobal()) {
			return ConfigThreshold.global(configThreshold);
		} else {
			return ConfigThreshold.local(configThreshold, this.configThresholdWidth);
		}

	}

	public Arena getArena() {
		return this.arena;
	}

	private void evalPatternsAndDrawInfos(Graphics2D g2) {
		evalPatterns();
		drawInfos(g2);
	}

	public void drawInfos(Graphics2D g2) {
		Posture drawingPosture = new Posture();

		topology.getEpucks().forEach(epuck -> {

			Point2D_F64 locationPixel = TrackerUtils.absolutePositionInCmTocameraCoordinateInPx(arena,
					epuck.getPostureCm().getPoint(), epuck.getAdjustFactorInCmPerPx());
			int angle = epuck.getPosturePx().angle;

			drawingPosture.setPosture((int) locationPixel.x, (int) locationPixel.y, angle);

			TrackerDrawings.drawRange(g2, drawingPosture, epuck);
			TrackerDrawings.drawPostureIndicator(g2, drawingPosture, 0.6 * epuck.getBodyRadiusInPx(),
					epuck.getRangeInPx(), epuck.getPostureCm().angle);
			TrackerDrawings.drawConnectivity(g2, topology, arena);

			drawingPosture.translate(2 * epuck.getBodyRadiusInPx(), -2 * epuck.getBodyRadiusInPx());

			int offsetY = TrackerDrawings.drawID(g2, drawingPosture, 0, isUnique(epuck.getId()), epuck);
			offsetY = TrackerDrawings.drawFiducial(g2, drawingPosture, offsetY, epuck.getFiducial(), epuck);
			TrackerDrawings.drawPosture(g2, drawingPosture, offsetY, epuck);
		});

	}

	public void evalPatterns() {
		Se3_F64 targetToSensor = new Se3_F64();
		Point2D_F64 locationPixel = new Point2D_F64();

		detectedEpucks.clear();

		IntStream.range(0, patternDetector.totalFound()).forEach(i -> {

			int detectorId = (int) patternDetector.getId(i);

			try {
				patternDetector.getImageLocation(i, locationPixel);
				patternDetector.getFiducialToCamera(i, targetToSensor);
				int angle = (int) TrackerUtils.getAngle(targetToSensor);

				if (!arena.isInsidePx((int) locationPixel.x, (int) locationPixel.y)) {
					return;
				}

				Epuck epuck = getEpuck(detectorId);
				detectedEpucks.add(epuck.getId());
				boolean unique = isUnique(epuck.getId());

				epuck.setTrackedByCamera(unique);
				epuck.setFiducial(sortedFiducials.get(detectorId));

				double adjustFactorInCmPerPx = adjustFactor.consolidateFactorInCmPerPx();
				if (unique) {
					topology.getBluetooth(epuck.getId()).setPostureInCm(TrackerUtils
							.cameraPositionInPxToAbsolutePositionInCm(arena, locationPixel, adjustFactorInCmPerPx),
							angle, adjustFactorInCmPerPx);
				}

				arena.setAjustFactorCmPerPx(adjustFactorInCmPerPx);

				epuck = null;
			} catch (Exception e) {
				e.printStackTrace();
			}

		});
	}

	private boolean isUnique(int id) {
		return detectedEpucks.stream().filter(i -> i == id).count() == 1;
	}

	@Override
	public void observe(Observable<Void> observable) {
		observables.add(observable);
	}

	public static boolean isEnhanceTracking() {
		return Tracker.enhanceTracking;
	}

	public static void setEnhanceTracking(boolean enhanceTracking) {
		Tracker.enhanceTracking = enhanceTracking;
		Properties.add(Key.EnhanceImage, enhanceTracking);
	}

	public ThresholdType getConfigThreshold() {
		return configThreshold;
	}

	public void setConfigThreshold(ThresholdType configThreshold) {
		this.configThreshold = configThreshold;
		Properties.add(configThreshold);
		configurePatternDetector();
	}

	public int getConfigThresholdWidth() {
		return configThresholdWidth;
	}

	public void setConfigThresholdWidth(int configThresholdWidth) {
		this.configThresholdWidth = configThresholdWidth;
		Properties.add(Key.TrackerThresholdWidth, configThresholdWidth);
		if (!this.configThreshold.isGlobal()) {
			configurePatternDetector();
		}
	}

	public double getDetectorThreshold() {
		return detectorThreshold;
	}

	public void setDetectorThreshold(double detectorThreshold) {
		this.detectorThreshold = detectorThreshold;
		Properties.add(Key.DetectorThreshold, (int) detectorThreshold);
		configurePatternDetector();
	}

	public void shutdow() {
		Properties.add(Key.AdjustFactorCmPerPx, (float) adjustFactor.consolidateFactorInCmPerPx());
	}

	public void setArenaBoundary() {
		arenaResizer.setArenaBoundary();
	}

	public void resetArenaBoundary() {
		arenaResizer.resetArenaBoundary();
	}

}