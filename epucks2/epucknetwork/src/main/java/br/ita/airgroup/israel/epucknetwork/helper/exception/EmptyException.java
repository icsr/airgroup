package br.ita.airgroup.israel.epucknetwork.helper.exception;

public class EmptyException extends Exception {

	public EmptyException(String string) {
		super(string);
	}
	
	public EmptyException() {
		super();
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 362799022117005460L;

}
