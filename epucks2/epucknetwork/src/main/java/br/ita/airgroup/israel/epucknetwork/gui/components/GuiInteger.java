package br.ita.airgroup.israel.epucknetwork.gui.components;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JTextField;

import br.ita.airgroup.israel.commons.messenger.Growl;
import br.ita.airgroup.israel.commons.utils.MathUtils;
import br.ita.airgroup.israel.epucknetwork.helper.file.Properties;
import br.ita.airgroup.israel.epucknetwork.helper.file.Properties.Key;

public class GuiInteger extends JPanel implements Keyable<Integer, Integer> {

	private static final long serialVersionUID = -4107071792733901366L;
	protected JTextField text;
	private Key key;

	public GuiInteger(String label, int columns, Key associatedKey) {

		this.key = associatedKey;
		text = new JTextField(columns);
		text.setText(Properties.get(associatedKey));
		this.setBorder(BorderFactory.createTitledBorder(label));
		this.add(text);

		text.addKeyListener(new KeyAdapter() {

			@Override
			public void keyTyped(KeyEvent e) {

				if (!MathUtils.isNumber(text.getText())) {
					text.setText(MathUtils.toNumberAsString(text.getText()));
				}

				super.keyTyped(e);
			}

		});
		
		whenChange(() -> {});
	}

	@Override
	public Integer loadKey() {
		return Properties.getAsInteger(key);
	}

	@Override
	public void saveKey(Integer value) {
		Properties.add(key, value);
	}

	@Override
	public Integer getValue() {
		return Integer.parseInt(text.getText());
	}

	@Override
	public void setValue(Integer value) {
		text.setText(Integer.toString(value));
	}

	@Override
	public void whenChange(Runnable listener) {
		this.text.addActionListener(evt -> {
			saveKey(Integer.parseInt(text.getText()));
			listener.run();
			Growl.show(String.format("%s saved.", key.name()));
		});
	}

}
