package br.ita.airgroup.israel.epucknetwork.helper.observer;

import javax.swing.JToggleButton;

public class ObservableToggleButton extends JToggleButton implements Observable<Boolean> {

	private static final long serialVersionUID = -5451663447147465133L;

	public ObservableToggleButton(String text) {
		super(text);
	}

	@Override
	public void notify(Boolean error) {
		if(error){
			this.setSelected(false);
		}
	}

}
