package br.ita.airgroup.israel.epucknetwork.helper;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.swing.JDialog;
import javax.swing.JPanel;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.tuple.Pair;

import br.ita.airgroup.israel.commons.graph.Graph;
import br.ita.airgroup.israel.commons.utils.FileUtils;
import br.ita.airgroup.israel.commons.utils.MathUtils;
import br.ita.airgroup.israel.epucknetwork.core.command.PutSelector;
import br.ita.airgroup.israel.epucknetwork.core.controller.Topology;
import br.ita.airgroup.israel.epucknetwork.core.imaging.Arena;
import br.ita.airgroup.israel.epucknetwork.helper.exception.EmptyException;
import br.ita.airgroup.israel.epucknetwork.helper.exception.SettingsException;
import br.ita.airgroup.israel.epucknetwork.helper.file.Properties;
import br.ita.airgroup.israel.epucknetwork.helper.message.Message;

public class PostureRandomizer {

	private static final String LABELS = "x,y,angle";
	private static final String ALGEBRAIC_CONNECTIVITY_LABEL = "Algebraic Connectivity,";
	private static final String ALGEBRAIC_CONNECTIVITY_REGEX = ALGEBRAIC_CONNECTIVITY_LABEL + "[\\.0-9]+";
	private List<Posture> postures;
	private Graph<String> graph;
	private double algebraicConnectivity;
	private double algebraicConnectivityLowerBound;
	private Arena portionOfArena;
	private int epuckRangeCm;
	private int safeDistance;
	private int qtyOfEpucks;
	private ArenaPanel panel;
	private JDialog dialog;
	protected static Path savingFolder = BaseSystem.RANDOM_POSTURES_HOME;

	public PostureRandomizer(int qtyOfEpucks, int safeDistance, int epuckRangeCm, Arena portionOfArena,
			double algebraicConnectivityLowerBound) {

		this.qtyOfEpucks = qtyOfEpucks;
		this.safeDistance = safeDistance;
		this.epuckRangeCm = epuckRangeCm;
		this.portionOfArena = portionOfArena;
		this.algebraicConnectivityLowerBound = algebraicConnectivityLowerBound;
	}

	public JPanel toPanel() {
		return panel;
	}

	public static Pair<Float, List<Posture>> readFromCSV(Path csvFilePath) throws IOException, SettingsException {

		if (!isRightlyFormatted(csvFilePath)) {
			throw new SettingsException("Format of CSV is incorrect.");
		}

		List<String> lines = FileUtils.readLines(csvFilePath);
		float algebraicConnectivityValue = Float.parseFloat(lines.get(0).split(Message.SEPARATOR)[1]);

		List<Posture> posturesValues = lines.stream().skip(2).map(l -> Posture.parsePosture(l))
				.collect(Collectors.toList());
		return Pair.of(algebraicConnectivityValue, posturesValues);
	}

	public File saveToPNG(String filename) throws FileNotFoundException, IOException {
		return saveToPNG(FileUtils.getPath(savingFolder, filename, FileUtils.PNG_SUFFIX));
	}

	private File saveToPNG(Path filepath) throws FileNotFoundException, IOException {

		render();
		FileUtils.saveImage(filepath, panel.image);
		Logger.info(PostureRandomizer.class, filepath.toString(), "was created.");
		return filepath.toFile();

	}

	public void save(String filename) throws FileNotFoundException, EmptyException, IOException {
		saveToCSV(filename);
		saveToPNG(filename);
	}

	public static void save(Path folder, String filename, Topology topology) throws IOException {
		if(folder.toFile().isFile()) {
			folder = folder.getParent();
		}
		
		savingFolder = folder;
		save(filename, topology);
	}
	
	public static void save(String filename, Topology topology) throws IOException {
		Path pngPath = FileUtils.getPath(savingFolder, filename, FileUtils.PNG_SUFFIX);
		saveToCSV(filename, topology);

		List<Posture> postures = getPostures(topology);
		int minX = postures.stream().reduce((a, b) -> a.x <= b.x ? a : b).get().x;
		int minY = postures.stream().reduce((a, b) -> a.y <= b.y ? a : b).get().y;
		int maxX = postures.stream().reduce((a, b) -> a.x >= b.x ? a : b).get().x;
		int maxY = postures.stream().reduce((a, b) -> a.y >= b.y ? a : b).get().y;

		int range = topology.getEpucks().get(0).getRangeInCm();

		Arena arena = new Arena(minX - 2 * range, minY - 2 * range, maxX + 2 * range, maxY + 2 * range, 1);

		FileUtils.saveImage(pngPath, Posture.drawPosturesOnImage(800, postures, arena, range, true));
	}

	public File saveToCSV(String filename) throws FileNotFoundException, EmptyException, IOException {
		Path filepath = FileUtils.getPath(savingFolder, filename, FileUtils.CSV_SUFFIX);
		saveToCSV(filepath, this.postures, this.algebraicConnectivity);
		return filepath.toFile();
	}
	
	public void saveToCSV(Path filepath) throws FileNotFoundException, IOException {
		saveToCSV(filepath, postures, algebraicConnectivity);
	}

	private static void saveToCSV(Path filepath, List<Posture> postures, double algebraicConnectivity)
			throws FileNotFoundException, IOException {

		StringBuilder str = new StringBuilder();
		str.append(ALGEBRAIC_CONNECTIVITY_LABEL).append(algebraicConnectivity).append("\n");

		str.append(LABELS).append("\n");

		postures.forEach(p -> str.append(p.asCSV()).append("\n"));

		FileOutputStream out = new FileOutputStream(filepath.toFile());
		IOUtils.write(str.toString(), out, Charset.defaultCharset());
		out.flush();
		out.close();

		Logger.info(PostureRandomizer.class, filepath.toString(), "was created.");

	}

	public static void saveToCSV(String filename, Topology topology) throws IOException {

		saveToCSV(FileUtils.getPath(savingFolder, filename, FileUtils.CSV_SUFFIX), getPostures(topology),
				topology.evaluateAlgebraicConnectivity().orElse(0f));
	}

	private static List<Posture> getPostures(Topology topology) {
		return topology.getEpucks().stream().map(e -> e.getPostureCm()).collect(Collectors.toList());
	}

	public List<Posture> random() {

		postures = new ArrayList<Posture>();

		while (true) {

			postures.clear();

			while (postures.size() < qtyOfEpucks) {
				Posture newPosture = randomize(portionOfArena);

				if (!isNewPostureInsideSafeZone(safeDistance, postures, newPosture)) {
					postures.add(newPosture);
				}

			}

			graph = Posture.toGraph(postures, epuckRangeCm, Properties.getFirstAsFloat(PutSelector.ALGORITHM_CONFIGS));
			algebraicConnectivity = graph.evaluateAlgebraicConnectivity();
			if (algebraicConnectivity >= algebraicConnectivityLowerBound) {
				break;
			}
		}

		panel = new ArenaPanel(postures, epuckRangeCm, portionOfArena);
		return postures;

	}

	public static Posture randomize(Arena portionOfArena) {
		Posture newPosture = new Posture((int) MathUtils.randomize(1, portionOfArena.getWidth(true) - 1),
				(int) MathUtils.randomize(1, portionOfArena.getWidth(true) - 1), 0);
		return newPosture;
	}

	private static boolean isNewPostureInsideSafeZone(int safeDistance, List<Posture> postures, Posture newPosture) {
		return postures.stream().filter(p -> MathUtils.distance(p.getPoint(), newPosture.getPoint()) <= safeDistance)
				.findAny().isPresent();
	}

	public void render() {

		dialog = new JDialog();
		dialog.setPreferredSize(new Dimension(800, 800));
		dialog.setModal(true);
		dialog.pack();

		panel.repaint();

		dialog.add(panel);

	}

	private class ArenaPanel extends JPanel {

		private static final long serialVersionUID = -2590058852300462830L;
		public final BufferedImage image;

		public ArenaPanel(List<Posture> postures, int connectionRange, Arena portionOfArena) {
			image = Posture.drawPosturesOnImage(800, postures, portionOfArena, connectionRange, true);
		}

		@Override
		public void paint(Graphics g) {
			g.drawImage(image, 0, 0, null);
		}

	}

	public void show() {
		render();
		dialog.setVisible(true);

	}

	public static List<File> generateRandomPostures(int qtyOfEpucks, int safeDistance, int numberOfPostures,
			Arena arena, int connectionRange, double algebraicConnectivityLowerBound)
			throws EmptyException, FileNotFoundException, IOException {

		List<File> csvs = new ArrayList<>();

		PostureRandomizer postureRandomizer = new PostureRandomizer(qtyOfEpucks, safeDistance, connectionRange, arena,
				algebraicConnectivityLowerBound);

		for (int i : IntStream.rangeClosed(1, numberOfPostures).toArray()) {
			String filename = String
					.format("range%2d_epsilon%.2f_%3d", connectionRange, algebraicConnectivityLowerBound, i)
					.replace(" ", "0");

			Logger.info(PostureRandomizer.class, "Generating random postures at", filename);

			postureRandomizer.random();
			csvs.add(postureRandomizer.saveToCSV(filename));
			postureRandomizer.saveToPNG(filename);
		}

		return csvs;

	}

	public static boolean isRightlyFormatted(Path csvFilepath) throws IOException {
		List<String> lines = FileUtils.readLines(csvFilepath);
		if (!Pattern.matches(ALGEBRAIC_CONNECTIVITY_REGEX, lines.get(0))) {
			return false;
		}

		Optional<String> findAny = lines.stream().skip(2).filter(l -> !Pattern.matches(Posture.CSV_FORMAT, l))
				.findAny();
		return !findAny.isPresent();
	}

}
