package br.ita.airgroup.israel.epucknetwork.core.algorithm;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.Optional;

import br.ita.airgroup.israel.epucknetwork.core.controller.BluetoothHandler;
import br.ita.airgroup.israel.epucknetwork.core.controller.Topology;
import br.ita.airgroup.israel.epucknetwork.core.imaging.Arena;
import br.ita.airgroup.israel.epucknetwork.helper.BaseSystem;
import br.ita.airgroup.israel.epucknetwork.helper.Posture;
import br.ita.airgroup.israel.epucknetwork.helper.exception.EmptyException;
import br.ita.airgroup.israel.epucknetwork.helper.exception.SettingsException;
import br.ita.airgroup.israel.epucknetwork.helper.file.CSVChooser;
import br.ita.airgroup.israel.epucknetwork.helper.file.Properties;
import br.ita.airgroup.israel.epucknetwork.helper.file.Properties.Key;
import br.ita.airgroup.israel.epucknetwork.helper.message.Messenger;

public class CSVBasedInitialTopologyAlgorithm extends NonAutonomyAlgorithm {

	Optional<Path> csv = Optional.empty();

	public CSVBasedInitialTopologyAlgorithm(Path csvCreatedByPostureRandomizer)
			throws IOException, SettingsException, EmptyException {
		this.csv = Optional.of(csvCreatedByPostureRandomizer);
	}

	public CSVBasedInitialTopologyAlgorithm() {
		this.csv = Properties.getAsPath(Key.CSVBasedInitialTopology, true);
	}

	public Optional<Path> getCsv() {
		return csv;
	}

	public List<Posture> getPostures() {
		return postures;
	}

	public void readInitialTopology(Topology topology) throws IOException, SettingsException, EmptyException {
		setTopology(topology);
		readInitialTopology();
	}

	public void readInitialTopology() throws IOException, SettingsException, EmptyException {

		if (!topology.isPresent()) {
			throw new EmptyException("Topology is absent.");
		}

		if (!csv.isPresent()) {
			askForCSV();
		}

		postures.clear();
		postures.addAll(Topology.readFromCSV(csv.get()));

		if (postures.size() != topology.get().getEpucks().size()) {
			if (wannaChooseAnotherFile()) {
				csv = Optional.empty();
				readInitialTopology();
			} else {
				throw new EmptyException();
			}

		}
	}

	private boolean wannaChooseAnotherFile() {
		return Messenger.answerIfIsTrue(String.format("There are %d postures to %d epucks\nChoose another file?",
				postures.size(), topology.get().getEpucks().size()));
	}

	@Override
	protected void groupOnArena(List<BluetoothHandler> bluetooths, Arena arena) {

		if (postures.isEmpty()) {
			try {
				readInitialTopology();
			} catch (IOException | SettingsException | EmptyException e) {
				e.printStackTrace();
				return;
			}
		}

		moveClosestEpuckToPoint(postures);

	}

	public void askForCSV() throws EmptyException, IOException, SettingsException {
		csv = Optional.of(new CSVChooser("Select your posture randomized CSV file",
				Optional.of(BaseSystem.RANDOM_POSTURES_HOME.toString())).showAndSelectSingle().toPath());
		Properties.add(Key.CSVBasedInitialTopology, csv.get().toString());
	}

	@Override
	public String toString() {
		return csv.toString();
	}

}
