package br.ita.airgroup.israel.epucknetwork.helper.observer;

public interface Observable<T> {
	
	public void notify(T content);

}
