package br.ita.airgroup.israel.epucknetwork.core.comm.port;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Optional;

import org.apache.commons.io.IOUtils;

import br.ita.airgroup.israel.commons.utils.StringUtils;
import br.ita.airgroup.israel.epucknetwork.helper.message.CommandListenable;
import gnu.io.CommPortIdentifier;
import gnu.io.NoSuchPortException;
import gnu.io.PortInUseException;
import gnu.io.RXTXPort;
import gnu.io.SerialPort;
import gnu.io.UnsupportedCommOperationException;

public class RxTxSerialPort implements SerialPortCommunicationally<Integer> {

	private static final int OPENING_TIMEOUT = 500;
	private static final int BUFFER_SIZE = 200;
	String portName;
	RXTXPort commPort;
	boolean commPortOpened = false;
	private boolean listening = false;
	private CommandListenable listener;
	private StringBuffer buffer = new StringBuffer();

	public RxTxSerialPort(String portName) {
		this.portName = portName;
	}

	public CommandListenable getListener() {
		return listener;
	}

	@Override
	public String getPortName() {
		return this.portName.substring(this.portName.lastIndexOf(File.separatorChar) + 1);
	}

	@Override
	public boolean openPort() {

		if (commPortOpened) {
			return true;
		}

		try {
			CommPortIdentifier portIdentifier = CommPortIdentifier.getPortIdentifier(this.portName);
			if (portIdentifier.isCurrentlyOwned()) {
				return false;
			}

			this.commPort = (RXTXPort) portIdentifier.open(RxTxSerialPort.class.getName(), OPENING_TIMEOUT);
			if (!configPort(BAUDRATE, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE,
					Optional.empty())) {
				return false;
			}

			commPortOpened = true;
			return true;
		} catch (PortInUseException | NoSuchPortException e) {
			return false;
		}
	}

	@Override
	public boolean closePort() {

		if (!commPortOpened) {
			return true;
		}

		commPort.close();
		commPortOpened = false;
		return true;
	}

	@Override
	public boolean isOpened() {
		return commPortOpened;
	}

	@Override
	public boolean isListening() {
		return this.listening;
	}

	@Override
	public boolean startListener(CommandListenable listener) {

		this.listener = listener;

		if (isListening()) {
			return true;
		}

		try {
			commPort.addEventListener(e -> getListener().run());
			listening = true;
			return true;
		} catch (Exception e) {
			listening = false;
			return false;
		}

	}

	@Override
	public boolean stopListener() {

		if (!isListening()) {
			return true;
		}

		commPort.removeEventListener();
		listening = false;
		return true;
	}

	@Override
	public void clearBuffer() {
		buffer.setLength(0);
	}

	@Override
	public Optional<String> readString() {
		BufferedInputStream inputStream = null;

		try {
			inputStream = new BufferedInputStream(commPort.getInputStream());

			byte[] buffer = new byte[BUFFER_SIZE];
			inputStream.read(buffer);
			inputStream.close();

			return Optional.ofNullable(StringUtils.toString(buffer));
		} catch (Exception e) {
			e.printStackTrace();
			return Optional.empty();
		}
	}
	@Override
	public Optional<String> readString(int charsCount, int timeoutInMils) {
		InputStream inputStream = null;
		try {

			inputStream = commPort.getInputStream();
			byte[] buffer = new byte[charsCount];
			IOUtils.read(inputStream, buffer);
			return Optional.ofNullable(new String(buffer));
		} catch (Exception e) {
			reconnect();
			return Optional.empty();
		} finally {
			try {
				inputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	}

	@Override
	public Optional<byte[]> readBytes(int bytesCount, int timeoutInMils) {
		Optional<String> read = readString(bytesCount, timeoutInMils);
		if (read.isPresent()) {
			return Optional.of(read.get().getBytes());
		} else {
			return Optional.empty();
		}

	}

	@Override
	public boolean write(byte[] dinamicMessage) {

		Optional<OutputStream> out = Optional.empty();
		try {

			out = Optional.of(commPort.getOutputStream());
			IOUtils.write(dinamicMessage, out.get());

			return true;
		} catch (Exception e) {
			reconnect();
			return false;
		} finally {
			out.ifPresent(o -> {
				try {
					o.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			});

		}
	}

	@Override
	public boolean write(String dinamicMessage) {
		return write(dinamicMessage.getBytes());
	}

	public void reconnect() {
		closePort();
		openPort();
	}

	@Override
	public boolean configPort(int bauds, Integer dataBits, Integer stopBits, Integer parity,
			Optional<List<Integer>> others) {
		try {
			this.commPort.setSerialPortParams(bauds, dataBits, stopBits, parity);
			this.commPort.enableReceiveTimeout(IO_TIMEOUT_IN_MILS);
			commPort.notifyOnDataAvailable(true);
			return true;
		} catch (UnsupportedCommOperationException e) {
			return false;
		}
	}

}
