package br.ita.airgroup.israel.epucknetwork.core.algorithm;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import br.ita.airgroup.israel.epucknetwork.core.controller.BluetoothHandler;
import br.ita.airgroup.israel.epucknetwork.core.controller.Epuck;
import br.ita.airgroup.israel.epucknetwork.core.controller.Topology;
import br.ita.airgroup.israel.epucknetwork.core.imaging.Arena;
import br.ita.airgroup.israel.epucknetwork.helper.Posture;
import br.ita.airgroup.israel.epucknetwork.helper.observer.Observable;
import br.ita.airgroup.israel.epucknetwork.helper.observer.Observer;

public abstract class NonAutonomyAlgorithm extends Algorithm implements Observer<List<Posture>> {

	private static Optional<Observable<List<Posture>>> observable = Optional.empty();

	protected int separationInCm;
	protected boolean waitForEachLine;
	protected int qtyPerLine;
	List<Posture> postures = new ArrayList<>();

	NonAutonomyAlgorithm() {
		super(-1);
	}

	public Algorithm setTopology(Topology topology) {
		super.setTopology(topology);
		this.separationInCm = (int) (4f * Epuck.getBodyRadiusInCm());

		this.qtyPerLine = (int) Math.max(Math.ceil(topology.size() / 3), 1);
		this.waitForEachLine = true;
		return this;
	}

	@Override
	public void start(float... configs) {
		topology.ifPresent(t -> t.getArena().ifPresent(a -> start(t.getBluetooths())));
	}

	@Override
	public void start(List<BluetoothHandler> handlers) {
		running = true;
		
		topology.ifPresent(t -> t.getArena().ifPresent(a -> groupOnArena(handlers, a)));

		observable.ifPresent(o -> o.notify(this.postures));
	}

	@Override
	public void stop() {
		postures.clear();
		observable.ifPresent(o -> o.notify(this.postures));
		running = false;
	}

	protected abstract void groupOnArena(List<BluetoothHandler> bluetooths, Arena arena);

	/**
	 * @return the separationInCm
	 */
	public int getSeparationInCm() {
		return separationInCm;
	}

	/**
	 * @param separationInCm
	 *            the separationInCm to set
	 */
	public void setSeparationInCm(int separationInCm) {
		this.separationInCm = separationInCm;
	}

	/**
	 * @return the qtyPerLine
	 */
	public int getQtyPerLine() {
		return qtyPerLine;
	}

	/**
	 * @param qtyPerLine
	 *            the qtyPerLine to set
	 */
	public void setQtyPerLine(int qtyPerLine) {
		this.qtyPerLine = qtyPerLine;
	}

	/**
	 * @return the waitForEachLine
	 */
	public boolean isWaitForEachLine() {
		return waitForEachLine;
	}

	/**
	 * @param waitForEachLine
	 *            the waitForEachLine to set
	 */
	public void setWaitForEachLine(boolean waitForEachLine) {
		this.waitForEachLine = waitForEachLine;
	}

	protected void moveClosestEpuckToPoint(List<Posture> postures) {
		topology.ifPresent(t -> t.moveClosestEpucksToPostures(postures));
	}

	@Override
	public void observe(Observable<List<Posture>> observable) {
		NonAutonomyAlgorithm.observable = Optional.ofNullable(observable);
	}

	public void resetObservable() {
		NonAutonomyAlgorithm.observable = Optional.empty();
	}

}
