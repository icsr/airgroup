package br.ita.airgroup.israel.epucknetwork;

import br.ita.airgroup.israel.epucknetwork.gui.ApplicationFactory;

public class Main {

	public static void main(String[] args) throws Exception {
		ApplicationFactory.buildSingleton();
	}

}
