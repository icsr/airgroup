package br.ita.airgroup.israel.epucknetwork.helper.file;

import java.io.File;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;

import br.ita.airgroup.israel.commons.utils.FileUtils;
import br.ita.airgroup.israel.epucknetwork.core.controller.BluetoothHandler;
import br.ita.airgroup.israel.epucknetwork.core.controller.Epuck;
import br.ita.airgroup.israel.epucknetwork.core.imaging.Fiducial;
import br.ita.airgroup.israel.epucknetwork.helper.Logger;
import br.ita.airgroup.israel.epucknetwork.helper.exception.EmptyException;
import br.ita.airgroup.israel.epucknetwork.helper.exception.PortAccessException;
import br.ita.airgroup.israel.epucknetwork.helper.exception.SettingsException;
import br.ita.airgroup.israel.epucknetwork.helper.message.Message;

public class ScriptGenerated {

	public static final int LINE_ID_COLUMN = 1;
	public static final int LINE_MAC_COLUMN = 2;
	public static final int LINE_HCI_COLUMN = 3;

	private static final String BT_LIST = System.getProperty("java.io.tmpdir") + File.separatorChar + "btlist";

	public static void populateFiducialsEpucksAndBluetoothMap(Path fiducialFolder,
			Map<Integer, BluetoothHandler> bluetooths, Map<Integer, Epuck> epucks, Map<Integer, Fiducial> fiducials)
			throws SettingsException, EmptyException, PortAccessException {

		List<String> lines = readConnectedEpucksListFileGeneratedByShellScript();

		if (lines.isEmpty()) {
			throw new EmptyException();
		}

		Fiducial.toMap(fiducials, fiducialFolder, lines);

		if (fiducialFolder != null && lines.size() > fiducials.size()) {
			throw new SettingsException(
					String.format("Only %d fiducials were found and there are %d epucks. Quantities should match.",
							fiducials.size(), lines.size()));
		}

		for (String line : lines) {
			String[] connection = line.split(Message.SEPARATOR);

			int id = Integer.parseInt(connection[LINE_ID_COLUMN]);
			String mac = connection[LINE_MAC_COLUMN];
			String bluetoothAdapter = connection[LINE_HCI_COLUMN];

			Epuck epuck = new Epuck(id, mac, bluetoothAdapter);

			epucks.put(id, epuck);
			bluetooths.put(id, new BluetoothHandler(epuck));
		}

	}

	public static void populateBluetoothMap(Map<Integer, BluetoothHandler> bluetooths)
			throws SettingsException, EmptyException, PortAccessException {

		List<String> lines = readConnectedEpucksListFileGeneratedByShellScript();

		if (lines.isEmpty()) {
			throw new EmptyException();
		}

		for (String line : lines) {
			String[] connection = line.split(Message.SEPARATOR);

			int id = Integer.parseInt(connection[LINE_ID_COLUMN]);
			String mac = connection[LINE_MAC_COLUMN];
			String bluetoothAdapter = connection[LINE_HCI_COLUMN];

			bluetooths.put(id, new BluetoothHandler(new Epuck(id, mac, bluetoothAdapter)));
		}

	}

	public static List<String> readConnectedEpucksListFileGeneratedByShellScript() throws SettingsException {
		try {
			return FileUtils.readLines(BT_LIST);
		} catch (Exception e) {
			String message = "No connected epucks found.\n\tRun Topology Script choosing option to CONNECT.";
			Logger.error(ScriptGenerated.class, message);
			throw new SettingsException(message);
		}
	}

	public static Integer getId(String line) {
		return Integer.parseInt(getIdAsString(line));
	}

	public static String getIdAsString(String line) {
		return line.split(Message.SEPARATOR)[1];
	}

}
