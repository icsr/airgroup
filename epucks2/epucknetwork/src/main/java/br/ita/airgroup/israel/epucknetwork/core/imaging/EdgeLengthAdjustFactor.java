package br.ita.airgroup.israel.epucknetwork.core.imaging;

import br.ita.airgroup.israel.commons.collections.LimitedList;
import br.ita.airgroup.israel.commons.utils.MathUtils;
import br.ita.airgroup.israel.epucknetwork.helper.file.Properties;
import br.ita.airgroup.israel.epucknetwork.helper.file.Properties.Key;
import georegression.struct.shapes.Polygon2D_F64;

public class EdgeLengthAdjustFactor {

	private double edgeInPx = 1;
	private double edgeInCm = 1;
	private double factorInCmPerCm = 1;
	private LimitedList<Double> lastAjustFactors = new LimitedList<>(1000);

	public EdgeLengthAdjustFactor(double actualFiducialEdgeInCm) {
		this.edgeInCm = actualFiducialEdgeInCm;
	}
	
	public EdgeLengthAdjustFactor evaluate(Polygon2D_F64 fiducialPolygon) {
		edgeInPx = Math.sqrt(fiducialPolygon.areaSimple());
		factorInCmPerCm = edgeInCm / edgeInPx;
		lastAjustFactors.add(factorInCmPerCm);
		
		return this;
	}

	public double getEdgeInPx() {
		return edgeInPx;
	}

	public double getEdgeInCm() {
		return edgeInCm;
	}

	public double consolidateFactorInCmPerPx() {
		if(lastAjustFactors.isEmpty()){
			return Properties.getAsFloat(Key.AdjustFactorCmPerPx);
		}
					
		return MathUtils.mean(lastAjustFactors);
	}
	
}
