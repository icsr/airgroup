package br.ita.airgroup.israel.epucknetwork.gui.components;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JTextField;

import br.ita.airgroup.israel.commons.messenger.Growl;
import br.ita.airgroup.israel.commons.utils.MathUtils;
import br.ita.airgroup.israel.epucknetwork.helper.file.Properties;
import br.ita.airgroup.israel.epucknetwork.helper.file.Properties.Key;

public class GuiFloat extends JPanel implements Keyable<Float, Float> {
	
	protected JTextField text;
	private Key key;
	private static final long serialVersionUID = -4107071792733901366L;

	public GuiFloat(String label, int columns, Key associatedKey) {
		
		this.key = associatedKey;
		text = new JTextField(columns);
		text.setText(Properties.get(associatedKey));
		this.setBorder(BorderFactory.createTitledBorder(label));
		this.add(text);
		
		text.addKeyListener(new KeyAdapter() {

			@Override
			public void keyTyped(KeyEvent e) {
				
				if(!MathUtils.isNumber(text.getText())){
					text.setText(MathUtils.toNumberAsString(text.getText()));
				}
				
				super.keyTyped(e);
			}
			
		});
		
		whenChange(() -> {});
	}

	@Override
	public Float loadKey() {
		return Properties.getAsFloat(key);
	}

	@Override
	public void saveKey(Float value) {
		Properties.add(key, value);
	}

	@Override
	public Float getValue() {
		return Float.parseFloat(text.getText());
	}

	@Override
	public void setValue(Float value) {
		text.setText(Float.toString(value));
	}

	@Override
	public void whenChange(Runnable listener) {
		this.text.addActionListener(evt -> {
			saveKey(Float.parseFloat(text.getText()));
			listener.run();
			
			Growl.show(String.format("%s saved.", key.name()));
		});
	}

	
	
}
