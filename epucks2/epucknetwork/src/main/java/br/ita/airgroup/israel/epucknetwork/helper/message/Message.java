package br.ita.airgroup.israel.epucknetwork.helper.message;

import java.nio.charset.StandardCharsets;
import java.util.List;

import br.ita.airgroup.israel.commons.utils.StringUtils;

public class Message {

	private static final int ETB = 0x17;

	private static final int ETX = 0x03;

	private static final int STX = 0x02;

	private static final int SYN = 0x16;

	public final static int BT_MSG_LENGTH = 10;

	public byte[] REQUEST_ID = new byte[BT_MSG_LENGTH];
	public byte[] REQUEST_K = new byte[BT_MSG_LENGTH];
	public byte[] SEND_START = new byte[BT_MSG_LENGTH];
	public byte[] SEND_STOP = new byte[BT_MSG_LENGTH];
	public byte[] SEND_PROX = new byte[BT_MSG_LENGTH];
	public byte[] SEND_ROTATE = new byte[BT_MSG_LENGTH];
	public byte[] SEND_NEIGHBORS = new byte[BT_MSG_LENGTH];
	public byte[] REQUEST_MIN_NEIGHBORS = new byte[BT_MSG_LENGTH];
	public byte[] REQUEST_SEGUNDO = new byte[BT_MSG_LENGTH];

	public static final String SEPARATOR = ",";

	public static final String START_MESSAGE = "[";
	public static final String END_MESSAGE = "]";

	public static final String MESSAGE_REGEX = String.format("\\%s.*?\\%s+", START_MESSAGE, END_MESSAGE);

	public Message() {

		REQUEST_ID = mountMessage("000001");
		REQUEST_K = mountMessage("500000");
		REQUEST_MIN_NEIGHBORS = mountMessage("700000");
		REQUEST_SEGUNDO = mountMessage("800000");
		SEND_START = mountMessage("100001");
		SEND_STOP = mountMessage("100002");
		SEND_PROX = mountMessage("300000");
		SEND_ROTATE = mountMessage("400000");
		SEND_NEIGHBORS = mountMessage("600000");
	}

	public static byte[] mountMessage(String msg) {

		byte[] message = msg.getBytes(StandardCharsets.US_ASCII);
		byte[] buffer = new byte[BT_MSG_LENGTH];

		buffer[0] = (byte) 0x16;
		buffer[1] = (byte) 0x02;

		for (int i = 2; i < BT_MSG_LENGTH - 2; i++)
			buffer[i] = message[i - 2];

		buffer[BT_MSG_LENGTH - 2] = (byte) 0x03;
		buffer[BT_MSG_LENGTH - 1] = (byte) 0x17;

		return buffer;
	}

	public static byte[] dinamicMessage(String msg) {

		byte[] message = msg.getBytes();
		byte[] buffer = new byte[msg.length() + 4];

		buffer[0] = (byte) SYN;
		buffer[1] = (byte) STX;

		for (int i = 0; i < msg.length(); i++)
			buffer[i + 2] = message[i];

		buffer[msg.length() + 2] = (byte) ETX;
		buffer[msg.length() + 3] = (byte) ETB;

		return buffer;
	}

	public static List<String> extractMessages(String receivedString) {

		return StringUtils.extract(receivedString, Message.START_MESSAGE.charAt(0), Message.END_MESSAGE.charAt(0));

	}
}
