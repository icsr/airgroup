package br.ita.airgroup.israel.epucknetwork.core.algorithm;

import java.io.File;
import java.util.Optional;

public abstract class TopologyAlgorithm extends AutonomousAlgorithm{
	
	Optional<File> logfile = Optional.empty();
	protected boolean normalizedEachControl, rangeSafety;
	
	public void setNormalizedEachControl(boolean active) {
		this.normalizedEachControl = active;
	}

	public boolean isNormalizedEachControl() {
		return normalizedEachControl;
	}

	TopologyAlgorithm(Integer id) {
		super(id);
	}

	@Override
	public void start(float... configs) {
		topology.ifPresent(t -> t.getEpucks().forEach(e -> e.resetEigens()));
		super.start();
	}
	
}