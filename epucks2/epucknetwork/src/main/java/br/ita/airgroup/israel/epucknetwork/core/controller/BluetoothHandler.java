package br.ita.airgroup.israel.epucknetwork.core.controller;

import java.awt.Point;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.stream.Collectors;

import br.ita.airgroup.israel.commons.collections.LimitedList;
import br.ita.airgroup.israel.commons.utils.StringUtils;
import br.ita.airgroup.israel.epucknetwork.core.algorithm.AutonomousAlgorithm;
import br.ita.airgroup.israel.epucknetwork.core.comm.port.SerialPortCommunicationally;
import br.ita.airgroup.israel.epucknetwork.core.command.EpuckCommand;
import br.ita.airgroup.israel.epucknetwork.core.command.PutSelector;
import br.ita.airgroup.israel.epucknetwork.core.command.Selector;
import br.ita.airgroup.israel.epucknetwork.core.imaging.Arena;
import br.ita.airgroup.israel.epucknetwork.helper.Logger;
import br.ita.airgroup.israel.epucknetwork.helper.Posture;
import br.ita.airgroup.israel.epucknetwork.helper.exception.MaxSendingCommandsAttemptives;
import br.ita.airgroup.israel.epucknetwork.helper.exception.ParametersException;
import br.ita.airgroup.israel.epucknetwork.helper.message.Message;
import br.ita.airgroup.israel.epucknetwork.helper.observer.Observable;
import br.ita.airgroup.israel.epucknetwork.helper.observer.Observer;
import br.ita.airgroup.israel.epucknetwork.matlab.helper.TopologyGain;

/**
 * Handler do Bluetooth
 * 
 * @author israelicsr
 *
 */
public class BluetoothHandler implements Observer<Set<Epuck>>, Comparable<Integer> {

	protected static final int MAX_COMMAND_ATTEMPTS = 5;
	protected static int MAX_READ_BYTE_TIME_IN_MILS = 2000;
	protected SerialPortCommunicationally<?> serialPort;
	byte[] buffer;
	Epuck epuck;
	EpuckCommand epuckCommand;
	static Map<Epuck, Date> activeEpucks = new HashMap<>();
	Timer timer;
	Optional<Observable<Set<Epuck>>> observable = Optional.empty();
	private Optional<AutonomousAlgorithm> algorithm = Optional.empty();
	List<String> commandLines = new LimitedList<>(100);

	public BluetoothHandler(Epuck epuck) {
		this.epuck = epuck;
		serialPort = epuck.getPort();
		this.epuckCommand = new EpuckCommand(this.serialPort, this.epuck);
		this.epuck.addBluetoothHandler(this);

		initActiveEpucksController();
	}

	void updateActiveEpucks() {
		synchronized (activeEpucks) {
			observable.ifPresent(o -> o.notify(activeEpucks.keySet().stream().filter(
					id -> new Date().getTime() - activeEpucks.get(id).getTime() > 2 * MAX_READ_BYTE_TIME_IN_MILS)
					.collect(Collectors.toSet())));
		}
	}

	void initActiveEpucksController() {

		TimerTask activeControllerTask = new TimerTask() {
			@Override
			public void run() {
				updateActiveEpucks();
			}
		};

		timer = new Timer(true);
		timer.scheduleAtFixedRate(activeControllerTask, 2 * MAX_READ_BYTE_TIME_IN_MILS, 2 * MAX_READ_BYTE_TIME_IN_MILS);
	}

	boolean connect() {

		Logger.info(this.getClass(), epuck, "Connecting and starting listener...");

		if (serialPort.isOpened()) {
			disconnect();
		}

		serialPort.openPort();
		serialPort.startListener(() -> receiveString());

		return serialPort.isOpened();
	}

	private boolean processCommandToSend(Selector selector, Integer... params) {
		try {
			return this.epuckCommand.processCommandToSend(selector, params);
		} catch (ParametersException | MaxSendingCommandsAttemptives e) {
			Logger.exception(epuck, e);
			if (selector == Selector.RESET) {
				reconnect();
			}
			return false;
		}
	}

	public boolean processCommandToSend(String command) {
		try {
			return this.epuckCommand.processCommandsToSend(command);
		} catch (ParametersException e) {
			Logger.exception(epuck, command, "has wrong parameters.");
			return false;
		} catch (MaxSendingCommandsAttemptives e) {
			reconnect();
			return false;
		}
	}

	StringBuilder command = new StringBuilder();

	void receiveString() {
		epuckCommand.setReceivingMessage(true);
		Optional<String> receivedString = serialPort.readString();

		receivedString.ifPresent(s -> {
			command.append(s);

			while (command.toString().contains(Message.START_MESSAGE)
					&& command.toString().contains(Message.END_MESSAGE)) {
				processMessage(command);
			}
		});
		epuckCommand.setReceivingMessage(false);
	}

	void processMessage(StringBuilder command) {

		List<String> list = StringUtils.splitAsList(command.toString(), Message.START_MESSAGE, Message.END_MESSAGE);
		for (String m : list) {
			activeEpucks.put(this.getEpuck(), new Date());

			List<String> commands = Message.extractMessages(m);

			for (String c : commands) {
				try {
					epuckCommand.processReceivedCommand(c);
				} catch (ParametersException | MaxSendingCommandsAttemptives e) {
					e.printStackTrace();
				}
			}
			command.replace(command.indexOf(m), command.indexOf(m) + m.length(), "");
		}

	}

	boolean disconnect() {
		Logger.info(this.getClass(), epuck, "Stopping listener and disconnecting...");
		stopAlgorithm();
		serialPort.stopListener();
		serialPort.closePort();
		timer.purge();
		timer.cancel();

		return !serialPort.isOpened();
	}

	boolean reconnect() {
		disconnect();

		try {
			Thread.sleep(MAX_READ_BYTE_TIME_IN_MILS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		return connect();
	}

	public Epuck getEpuck() {
		return epuck;
	}

	public void resetEpuck() {
		this.epuck.reset();
		stopAlgorithm();
		processCommandToSend(Selector.RESET);
	}

	public void lightEpuck(boolean on) {
		processCommandToSend(Selector.LED, on ? 1 : 0);
	}

	public void failEpuck() {
		this.epuck.setOnFail(true);
		processCommandToSend(Selector.FAIL);
	}

	@Override
	public void observe(Observable<Set<Epuck>> observable) {
		this.observable = Optional.of(observable);
	}

	protected void sendPosture() {
		try {
			epuckCommand.sendPosture();
		} catch (ParametersException | MaxSendingCommandsAttemptives e) {
			Logger.exception(epuck, e);
		}
	}

	public void sendMoveCommand(Point pointInCm) {
		sendMoveCommand(pointInCm.x, pointInCm.y);
	}

	public void sendMoveCommand(int xInCm, int yInCm) {

		if (epuck.isOnFail()) {
			Logger.warn(epuck, "is on fail.");
			return;
		}

		processCommandToSend(Selector.MOVE, xInCm, yInCm);
	}

	public void sendAngleCommand(int xInCm, int yInCm) {
		processCommandToSend(Selector.TO_ANGLE,
				(int) Posture.getAngle(xInCm - epuck.getPostureCm().x, yInCm - epuck.getPostureCm().y));
	}

	public void sendArenaBoudary(Arena arena) {
		try {
			epuckCommand.sendArenaBoundary(arena);
		} catch (ParametersException | MaxSendingCommandsAttemptives e) {
			Logger.exception(epuck, e);
		}
	}

	public void setArena(Arena arena) {
		epuck.setArena(arena);
		sendArenaBoudary(arena);
	}

	public void sendReadPosture() {
		processCommandToSend(Selector.READ_CURRENT_POSTURE);
	}

	public boolean isAutoPostureSenderActivated() {
		return epuckCommand.isAutoPostureSenderActivated();
	}

	public void startAlgorithm(AutonomousAlgorithm algorithm) {
		this.algorithm = Optional.of(algorithm);
		processCommandToSend(Selector.TOPOLOGY, algorithm.getId(), 1);
	}

	public void stopAlgorithm() {
		algorithm.ifPresent(a -> {
			processCommandToSend(Selector.TOPOLOGY, a.getId(), 0);
		});
	}

	public void sendTurnToAngle(int angle) {
		processCommandToSend(Selector.TO_ANGLE, angle);
	}

	public void sendRotateAngle(int angle) {
		processCommandToSend(Selector.ROTATE, angle);
	}

	public void sendDislocation(int dislocationInCm) {
		processCommandToSend(Selector.DISTANCE, dislocationInCm);
	}

	public void putCommand(PutSelector putSelector, List<Float> params) {
		try {
			epuckCommand.processCommandToSend(putSelector, params);
		} catch (ParametersException | MaxSendingCommandsAttemptives e) {
			Logger.exception(epuck, e);
		}
	}

	public void sendTopologyGain(TopologyGain gain) {
		try {
			epuckCommand.processCommandsToSend(Selector.GAIN, gain.toArray());
		} catch (ParametersException | MaxSendingCommandsAttemptives e) {
			Logger.exception(epuck, e);
		}
	}

	public void sendPing() {
		try {
			epuckCommand.processCommandsToSend(Selector.PING);
		} catch (ParametersException | MaxSendingCommandsAttemptives e) {
			Logger.exception(epuck, e);
		}
	}

	@Override
	public int hashCode() {
		return epuck.getId();
	}

	@Override
	public boolean equals(Object obj) {
		return BluetoothHandler.class.isInstance(obj)
				&& BluetoothHandler.class.cast(obj).getEpuck().getId() == epuck.getId();
	}

	public void sendGetPosture() {
		try {
			epuckCommand.processCommandToSend(Selector.GET_POSTURE);
		} catch (ParametersException | MaxSendingCommandsAttemptives e) {
			Logger.exception(epuck, e);
		}
	}

	@Override
	public int compareTo(Integer o) {
		return hashCode();
	}

	public void sendNeighbors() {
		try {
			epuckCommand.sendNeighborHood();
		} catch (ParametersException | MaxSendingCommandsAttemptives e) {
			Logger.exception(epuck, e);
		}
	}

	public void setPostureInCm(Point epuckPositionInCm, int angle, double adjustFactorInCmPerPx) {
		epuck.setPostureInCm(epuckPositionInCm, angle, adjustFactorInCmPerPx);

		if (!epuck.isTrackingReached()) {
			sendPosture();
		}
	}

}