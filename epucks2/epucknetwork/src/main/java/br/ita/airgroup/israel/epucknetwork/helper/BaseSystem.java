package br.ita.airgroup.israel.epucknetwork.helper;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import br.ita.airgroup.israel.commons.utils.FileUtils;
import br.ita.airgroup.israel.commons.utils.Font;
import br.ita.airgroup.israel.commons.utils.StringUtils;
import br.ita.airgroup.israel.epucknetwork.core.algorithm.Algorithm;
import br.ita.airgroup.israel.epucknetwork.core.algorithm.CSVBasedInitialTopologyAlgorithm;
import br.ita.airgroup.israel.epucknetwork.core.algorithm.ConnectivityMaintenance;
import br.ita.airgroup.israel.epucknetwork.core.algorithm.ConnectivityMaintenanceAndMatlabControlCombined;
import br.ita.airgroup.israel.epucknetwork.core.algorithm.LinedGroup;
import br.ita.airgroup.israel.epucknetwork.core.algorithm.MatlabControlOnly;
import br.ita.airgroup.israel.epucknetwork.core.algorithm.RandomWaypoints;
import br.ita.airgroup.israel.epucknetwork.core.algorithm.SquareGroup;
import br.ita.airgroup.israel.epucknetwork.helper.exception.EmptyException;
import br.ita.airgroup.israel.epucknetwork.helper.exception.SettingsException;
import br.ita.airgroup.israel.epucknetwork.helper.file.DirectoryChooser;
import br.ita.airgroup.israel.epucknetwork.helper.file.Properties;
import br.ita.airgroup.israel.epucknetwork.helper.file.Properties.Key;
import br.ita.airgroup.israel.epucknetwork.helper.message.Messenger;
import br.ita.airgroup.israel.epucknetwork.helper.security.AvoidUnauthorizedExit;
import br.ita.airgroup.israel.epucknetwork.matlab.MatlabControlFacade;

/**
 * Base System class to handle underlying operating system features as clipboard
 * or execute commands on terminal
 * 
 * @author israelicsr
 *
 */
public class BaseSystem {

	public static final int MAX_EPUCKS = 10;
	public static final String ARCH_32 = "386";
	public static final String ARCH_64 = "64";
	private static final int MAX_PROCESS_WAIT_TIME_IN_SECONDS = 3;
	public static final String JAVA_HOME = System.getProperty("java.home");
	public static final String OS_NAME = System.getProperty("os.name");
	public static final Path javaLib = Paths.get(JAVA_HOME, "lib");
	// TODO system dependency
	public static final Path nativeLib = Paths.get("/usr/lib/jni");
	public static final Path resourcesHome = Paths.get("resources");
	public static final Path imagesHome = Paths.get(resourcesHome.toString(), "images");
	public static final Path rxtxHome = Paths.get(resourcesHome.toString(), "lib", "rxtx");
	public static final Path rxtxJar = Paths.get(rxtxHome.toString(), "RXTXcomm.jar");

	public static final Path squareGridPath = Paths.get(imagesHome.toString(), "Calibration_letter_squaregrid_5x4.png");

	public static final Path APP_HOME = Paths.get(System.getProperty("user.home"), "epuckApp");
	public static Path RECORDING_HOME = Paths.get(APP_HOME.toString(), "recording");
	public static final Path CALIBRATION_HOME = Paths.get(APP_HOME.toString(), "calibration");

	public static final Path CURRENT_DIR = Paths.get(System.getProperty("user.dir"));
	public static final Path CALIBRATION_DATA_DIR = Paths.get(CURRENT_DIR.toString(), "calibration_data");
	public static final Path CALIBRATION_DATA_FILE = Paths.get(CALIBRATION_DATA_DIR.toString(), "intrinsic.yaml");
	public static final Path LOG_HOME = Paths.get(APP_HOME.toString(), "log");
	public static final Path RANDOM_POSTURES_HOME = Paths.get(APP_HOME.toString(), "postures");
	public static final Path RXTX_LOG_FILE = Paths.get(LOG_HOME.toString(), "rxtx.log");
	public static Path POSTURES_DATA_HOME = Paths.get(Properties.get(Key.PosturesHome));
	public static final float threshold = 0.2f;
	public static final float epsilon = 0.2f;
	public static final float range = 20f;

	static {
		plugDefaultAlgoritms();
	}

	private static Timer timer;

	public static enum OS_LIB_EXTENSION {
		LINUX(".so"), WINDOWS(".dll"), UNKNOWN(StringUtils.EMPTY);

		private String extension;

		private OS_LIB_EXTENSION(String extension) {
			this.extension = extension;
		}

		@Override
		public String toString() {
			return extension;
		}

		public static OS_LIB_EXTENSION get() {
			for (OS_LIB_EXTENSION e : OS_LIB_EXTENSION.values()) {
				if (OS_NAME.toLowerCase().contains(e.name().toLowerCase())) {
					return e;
				}
			}

			return UNKNOWN;
		}

	};

	public static void init() {
		initScheduleGC();
		checkGroups();
		checkRxTx(false);
		avoidUnauthorizedExit();
		Font.setBaseSize(Properties.getAsInteger(Key.FontBaseSize));
	}

	private static void plugDefaultAlgoritms() {
		Algorithm.plug(new LinedGroup(), new SquareGroup(), new RandomWaypoints(),
				new CSVBasedInitialTopologyAlgorithm(), new ConnectivityMaintenance(), new MatlabControlOnly(),
				new ConnectivityMaintenanceAndMatlabControlCombined());
	}

	private static void avoidUnauthorizedExit() {
		System.setSecurityManager(new AvoidUnauthorizedExit());
	}

	private static void initScheduleGC() {

		timer = new Timer(true);
		timer.scheduleAtFixedRate(new TimerTask() {

			@Override
			public void run() {
				System.gc();

			}
		}, 10000, 10000);

	}

	private static void checkRxTx(boolean forceLibraryCopy) {

		if (!nativeLib.toFile().exists()) {
			Messenger.info("Run Epuck Config Shell Script.");
			BaseSystem.shutdown();
		}

	}

	private static void checkGroups() {
		try {
			ArrayList<String> commands = new ArrayList<String>();
			commands.addAll(checkNeededGroups());

			warnUserAndSuggestCommands(commands);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		}
	}

	public static String getUsername() {
		return System.getProperty("user.name");
	}

	private static ArrayList<String> checkNeededGroups() throws SettingsException {

		ArrayList<String> commands = new ArrayList<String>();
		if (!isRunningOnLinux()) {
			return commands;
		}

		List<String> allDataGroups = executeCommand("getent group");

		List<String> allGroups = allDataGroups.stream().map(s -> s.split(":")[0]).collect(Collectors.toList());
		List<String> usergroups = Stream.of(executeCommand("groups").get(0).split("\\s")).collect(Collectors.toList());
		List<String> neededGroups = Stream.of("uucp", "dialout", "tty").collect(Collectors.toList());

		if (usergroups.containsAll(neededGroups)) {
			return commands;
		}

		List<String> notPresentGroups = neededGroups.stream().filter(s -> {
			return !allGroups.contains(s);
		}).collect(Collectors.toList());

		commands.addAll(
				notPresentGroups.stream().map(s -> String.format("sudo addgroup %s;", s)).collect(Collectors.toList()));

		commands.addAll(neededGroups.stream().map(p -> String.format("sudo usermod -a -G %s %s;", p, getUsername()))
				.collect(Collectors.toList()));

		return commands;

	}

	public static boolean isRunningOnLinux() {
		return isRunningOn("linux");
	}

	public static boolean isRunningOnWindows() {
		return isRunningOn("windows");
	}

	public static boolean isRunningOn(String os) {
		return System.getProperty("os.name").toLowerCase().contains(os);
	}

	private static void warnUserAndSuggestCommands(List<String> commands) throws SettingsException {

		if (commands.isEmpty()) {
			return;
		}

		Messenger.warn(
				"<html>User has no enough permission. Please, type on terminal,<br/>the commands below and proceed relogin in the aftermath:<br/>&nbsp;</html>",
				commands, Optional.of("Remember to relogin on your operating system!"));

	}

	/**
	 * Copy things to clipboard
	 * 
	 * @param text
	 */
	public static void copyToClipboard(String text) {
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(new StringSelection(text), null);
	}

	/**
	 * Paste {@link String} from clipboard
	 * 
	 * @return content from clipboard or empty string if none content was previously
	 *         copied
	 * @throws HeadlessException
	 * @throws UnsupportedFlavorException
	 * @throws IOException
	 */
	public static String pasteFromClipboard() throws HeadlessException, UnsupportedFlavorException, IOException {
		return Optional.ofNullable((String) Toolkit.getDefaultToolkit().getSystemClipboard().getContents(null)
				.getTransferData(DataFlavor.stringFlavor)).orElse(new String());
	}

	public static List<String> executeCommand(String command) {
		Optional<BufferedReader> bufferedReader = Optional.empty();

		Process answer = null;
		try {
			answer = Runtime.getRuntime().exec(command);
			answer.waitFor(MAX_PROCESS_WAIT_TIME_IN_SECONDS, TimeUnit.SECONDS);
			bufferedReader = Optional.of(new BufferedReader(new InputStreamReader(answer.getInputStream())));

			return bufferedReader.get().lines().collect(Collectors.toList());
		} catch (InterruptedException | IOException e) {
			e.printStackTrace();
			return new ArrayList<String>();
		} finally {
			bufferedReader.ifPresent(b -> {
				try {
					b.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			});
			answer.destroy();
		}
	}

	public static Point centeredPoint(Component component) {
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		return new Point(screenSize.width / 2 - component.getWidth(), screenSize.height / 2 - component.getHeight());
	}

	public static void shutdown() {

		Properties.saveIt();
		Logger.save();

		try {
			MatlabControlFacade.shutdown();
		} catch (Exception e1) {
			e1.printStackTrace();
		}

		for (Thread t : Thread.getAllStackTraces().keySet()) {
			if (t.getState() != Thread.State.TERMINATED) {
				try {
					t.interrupt();
				} catch (Exception e) {
				}
			}
		}
		
		Messenger.shutdown(); 	
		
		System.gc();
		System.exit(0);
	}

	static {
		FileUtils.createFoldersIfNotExists(APP_HOME, RECORDING_HOME, CALIBRATION_HOME, LOG_HOME, RANDOM_POSTURES_HOME,
				POSTURES_DATA_HOME);
	}

	@Override
	protected void finalize() throws Throwable {
		shutdown();
	}

	public static void setRecordingHome(Path recordingFolder) {
		RECORDING_HOME = recordingFolder;

		if (!RECORDING_HOME.toFile().exists()) {
			RECORDING_HOME.toFile().mkdirs();
			Logger.info(BaseSystem.class,
					String.format("Folder %s was successfully created.", RECORDING_HOME.toString()));
		}

	}

	public static void setRecordingHome() {

		try {
			setRecordingHome(new DirectoryChooser("Select Folder to save Recordings.",
					Optional.of(BaseSystem.RECORDING_HOME.toString())).showAndSelectSingle().toPath());
		} catch (EmptyException e) {
			e.printStackTrace();
		}

	}
	
}
