package br.ita.airgroup.israel.epucknetwork.core.imaging;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import boofcv.app.CommandParserFiducialSquare;
import boofcv.app.CreateFiducialSquareImage;
import br.ita.airgroup.israel.commons.utils.FileUtils;
import br.ita.airgroup.israel.epucknetwork.gui.aux.LinkEpuckToFiducial;
import br.ita.airgroup.israel.epucknetwork.helper.Logger;
import br.ita.airgroup.israel.epucknetwork.helper.exception.EmptyException;
import br.ita.airgroup.israel.epucknetwork.helper.exception.SettingsException;
import br.ita.airgroup.israel.epucknetwork.helper.file.ImagesChooser;
import br.ita.airgroup.israel.epucknetwork.helper.file.Properties;
import br.ita.airgroup.israel.epucknetwork.helper.file.Properties.Key;
import br.ita.airgroup.israel.epucknetwork.helper.file.ScriptGenerated;
import br.ita.airgroup.israel.epucknetwork.helper.message.Message;
import br.ita.airgroup.israel.epucknetwork.helper.message.Messenger;

/**
 * Handle fiducial markers by using BoofCV
 * 
 * @author israelicsr
 *
 */
public class Fiducial {

	public static final int SHAPE_EDGES = 4;
	public static final double SHAPE_SIZE_IN_CENTIMETERS = Properties.getAsInteger(Key.FiducialSizeCm);
	
	private File pngFiducial;
	private int id;

	public Fiducial(int id, File pngFiducial) {
		this.id = id;
		this.pngFiducial = pngFiducial;
	}

	public int getId() {
		return this.id;
	}

	public File getFiducialFile() {
		return pngFiducial;
	}

	public static void generateFiducial(float sizeInCentimeters, File outputDir, File originalImage) {

		CommandParserFiducialSquare parser = new CommandParserFiducialSquare("image path");

		String[] args = Stream.of("-OutputFile=" + getPrintableOutputFile(outputDir, originalImage), "-PrintInfo", "-Units=cm",
				String.valueOf(sizeInCentimeters), originalImage.getAbsolutePath()).toArray(s -> new String[s]);
		try {
			parser.execute(args, new CreateFiducialSquareImage());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static String getPrintableOutputFile(File outputDir, File originalImage) {
		String originalFilename = File.separator + Stream.of(originalImage.getAbsolutePath().split(File.separator))
				.collect(Collectors.toCollection(() -> new LinkedList<String>())).getLast();

		String fiducialFilename = (originalFilename.contains(".") ? originalFilename.split("\\.")[0] : originalFilename)
				+ FileUtils.EPS_SUFFIX;
		return outputDir.getAbsolutePath() + File.separator + fiducialFilename;
	}

	/**
	 * @see #generateFiducial(float, File, File)
	 * @param sizeInCentimeters
	 * @return
	 * @throws EmptyException
	 */
	public static Path generatePrintableFiducial(Optional<Float> sizeInCentimeters) throws EmptyException {

		if (!sizeInCentimeters.isPresent()) {
			sizeInCentimeters = Optional.of(Properties.get(Key.FiducialSizeCm)).map(Float::parseFloat);
		}

		List<File> inputs;
		ImagesChooser imagesChooser = new ImagesChooser("Select SOURCE images to generate fiducial markers...",
				Properties.getAsOptional(Key.FiducialFolder), FileUtils.PNG_SUFFIX.substring(1));
		
		inputs = imagesChooser.showAndSelectMultiples();
		File output = inputs.get(0).getParentFile();

		float size = sizeInCentimeters.orElse(7f);
		inputs.stream().forEach(f -> {
			Logger.info(Fiducial.class, "Generating fiducial on", f.getAbsolutePath());
			generateFiducial(size, output, f);
		});

		Messenger.info(String.format("Fiducials created on %s.", output.getAbsolutePath()));

		return output.toPath();

	}

	public static Map<Integer, Fiducial> toMap(Map<Integer, Fiducial> fiducials, Path fiducialFolder,
			List<String> configLinesGeneratedByScript) throws SettingsException, EmptyException {
		return toMap(fiducials, fiducialFolder, configLinesGeneratedByScript, false);
	}

	public static Map<Integer, Fiducial> showEpuckLinkingGUI(Map<Integer, Fiducial> fiducials, Path fiducialFolder)
			throws SettingsException, EmptyException {
		return toMap(fiducials, fiducialFolder, ScriptGenerated.readConnectedEpucksListFileGeneratedByShellScript(),
				true);
	}

	public static Map<Integer, Fiducial> toMap(Map<Integer, Fiducial> fiducials, Path fiducialFolder,
			List<String> configLinesGeneratedByScript, boolean showSelectionGUI)
			throws SettingsException, EmptyException {
		
		if (!fiducialFolder.toFile().exists() || !fiducialFolder.toFile().isDirectory()) {
			throw new SettingsException("Fiducials folder not found!");
		}

		List<File> allFiducialImages = Stream
				.of(fiducialFolder.toFile().listFiles(f -> f.toString().endsWith(FileUtils.PNG_SUFFIX)))
				.sorted((a, b) -> a.hashCode() - a.hashCode()).collect(Collectors.toList());

		if (allFiducialImages.isEmpty()) {
			throw new SettingsException("Expected EPS content file for fiducial.");
		}

		File[] correctFiducialImages = fiducialFolder.toFile().listFiles(f -> Pattern
				.matches(String.format(".*%s[0-9]{4}%s", File.separator, FileUtils.PNG_SUFFIX), f.getPath()));

		if (correctFiducialImages.length >= configLinesGeneratedByScript.size()) {

			Stream.of(correctFiducialImages).forEach(f -> {
				int id = Integer.parseInt(FileUtils.getFilenameWithSuffix(f).split("\\.")[0]);
				fiducials.put(id, new Fiducial(id, f));
			});
		}

		List<Integer> presentEpucks = configLinesGeneratedByScript.stream()
				.map(line -> Integer.parseInt(line.split(Message.SEPARATOR)[ScriptGenerated.LINE_ID_COLUMN]))
				.filter(id -> fiducials.containsKey(id)).collect(Collectors.toList());

		if (showSelectionGUI) {
			return new LinkEpuckToFiducial(configLinesGeneratedByScript, allFiducialImages).getFiducials();
		}
		else if (presentEpucks.size() == configLinesGeneratedByScript.size()) {
			return fiducials;
		} else {
			throw new EmptyException("Epucks not linked to its Fiducials!");
		}

	}

	@Override
	public String toString() {
		return Integer.toString(id);
	}

}
