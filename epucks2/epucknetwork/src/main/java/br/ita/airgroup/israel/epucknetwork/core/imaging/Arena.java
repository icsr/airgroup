package br.ita.airgroup.israel.epucknetwork.core.imaging;

import java.awt.Point;
import java.awt.geom.Area;
import java.awt.geom.Rectangle2D;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import br.ita.airgroup.israel.commons.utils.SwingUtils;
import br.ita.airgroup.israel.epucknetwork.helper.file.Properties;
import br.ita.airgroup.israel.epucknetwork.helper.file.Properties.Key;
import br.ita.airgroup.israel.epucknetwork.helper.tracking.TrackerUtils;

public class Arena {

	private Point diagonalPx;
	private Point originPx;
	private Optional<Double> adjustFactorCmPerPx = Optional.ofNullable(Properties.getAsDouble(Key.AdjustFactorCmPerPx));
	private boolean adjustFactorSet = false;
	private Point diagonalCm;
	private Point originCm;

	Arena(Point originPx, Point diagonalPx) {
		this.originPx = originPx;
		this.diagonalPx = diagonalPx;
	}

	public Arena(int upperLeftCornerXInCm, int upperLeftCornerYInCm, int bottomRightCornerXInCm,
			int bottomRightCornerYInCm) {
		originCm = new Point(upperLeftCornerXInCm, upperLeftCornerYInCm);
		diagonalCm = new Point(bottomRightCornerXInCm, bottomRightCornerYInCm);
		
		adjustPxMeasures();
	}

	public Arena(int upperLeftCornerXInCm, int upperLeftCornerYInCm, int bottomRightCornerXInCm,
			int bottomRightCornerYInCm, double adjustFactorCmPerPx) {
		this(upperLeftCornerXInCm, upperLeftCornerYInCm, bottomRightCornerXInCm, bottomRightCornerYInCm);
		setAjustFactorCmPerPx(adjustFactorCmPerPx);
		adjustPxMeasures();
	}

	private void adjustPxMeasures() {
		originPx = SwingUtils.divide(originCm, adjustFactorCmPerPx.orElse(1d));
		diagonalPx = SwingUtils.divide(diagonalCm, adjustFactorCmPerPx.orElse(1d));
	}

	public void setAjustFactorCmPerPx(double adjustFactor) {
		setAdjustFactorSet(true);
		this.adjustFactorCmPerPx = Optional.of(adjustFactor);
		adjustPxMeasures();
	}

	public double getAdjustFactorCmPerPx() {
		return adjustFactorCmPerPx.orElse(1d);
	}

	public boolean isInsidePx(Point pointInPx) {
		if (pointInPx.equals(originPx)) {
			return false;
		}
		return getRectanglePx().contains(pointInPx);
	}

	public boolean isInsidePx(int x, int y) {
		return getRectanglePx().contains(x, y);
	}

	public boolean isInsideCm(Point relativePointInCm) {
		return isInsideCm(relativePointInCm.x, relativePointInCm.y);
	}

	public boolean isInsideCm(int relativeXInCm, int relativeYInCm) {
		return getRectangleCm().contains(getLeft(true) + relativeXInCm, getTop(true) + relativeYInCm);
	}

	public Point getCenterPx() {
		return new Point((int) getRectanglePx().getCenterX(), (int) getRectanglePx().getCenterY());
	}
	
	public void setOriginCm(Point originCm) {
		this.originCm = originCm;
		adjustPxMeasures();
	}
	
	public void setDiagonalCm(Point diagonalCm) {
		this.diagonalCm = diagonalCm;
		adjustPxMeasures();
	}

	public Point getCenterCm() {
		return TrackerUtils.adjustPoint(getCenterPx(), adjustFactorCmPerPx.orElse(1d));
	}

	public Point getCenterCm(boolean relative) {
		Point center = getCenterCm();

		if (relative) {
			center = TrackerUtils.absolutePositionInCmToRelativePositionInCm(this, center);
		}
		return center;
	}

	public Point getDiagonalPx() {
		return diagonalPx;
	}

	public Point getDiagonal(boolean inCentimeters) {
		return inCentimeters ? this.diagonalCm : this.diagonalPx;
	}

	public Point getOriginPx() {
		return originPx;
	}

	public Point getOrigin(boolean inCentimeters) {
		return inCentimeters? this.originCm : this.originPx;
	}

	public int getLeftPx() {
		return Math.min(originPx.x, diagonalPx.x);
	}

	public int getLeft(boolean inCentimeters) {
		return (int) (getLeftPx() * (inCentimeters ? adjustFactorCmPerPx.orElse(0d) : 1));
	}

	public int getRight(boolean inCentimeters) {
		return (int) (getRightPx() * (inCentimeters ? adjustFactorCmPerPx.orElse(1d) : 1));
	}

	public int getTopPx() {
		return Math.min(diagonalPx.y, originPx.y);
	}

	public int getTop(boolean inCentimeters) {
		return (int) (getTopPx() * (inCentimeters ? adjustFactorCmPerPx.orElse(0d) : 1));
	}

	public int getBottom(boolean inCentimeters) {
		return (int) (getBottomPx() * (inCentimeters ? adjustFactorCmPerPx.orElse(1d) : 1));
	}

	public int getHeight(boolean inCentimeters) {
		return (int) (getHeightPx() * (inCentimeters ? adjustFactorCmPerPx.orElse(1d) : 1));
	}

	public int getWidth(boolean inCentimeters) {
		return (int) (getWidthPx() * (inCentimeters ? adjustFactorCmPerPx.orElse(1d) : 1));
	}

	public int getWidthPx() {
		return Math.abs(diagonalPx.x - originPx.x);
	}

	public int getHeightPx() {
		return Math.abs(diagonalPx.y - originPx.y);
	}

	public int getRightPx() {
		return Math.max(originPx.x, diagonalPx.x);
	}

	public int getBottomPx() {
		return Math.max(originPx.y, diagonalPx.y);
	}

	private Rectangle2D getRectanglePx() {
		return new Rectangle2D.Double(getLeftPx(), getTopPx(), getWidthPx(), getHeightPx());
	}

	private Rectangle2D getRectangleCm() {
		return new Rectangle2D.Double(getLeft(true), getTop(true), getWidth(true), getHeight(true));
	}

	public Area excludeFromTotalArea(Rectangle2D totalArea) {
		Area area = new Area(totalArea);
		area.subtract(new Area(this.getRectanglePx()));
		return area;
	}

	public static Point calculateCenterOf(List<Point> points) {

		Integer x = (int) (points.stream().map(p -> Float.valueOf(p.x)).reduce((x1, x2) -> x1 + x2).get()
				/ points.size());
		Integer y = (int) (points.stream().map(p -> Float.valueOf(p.y)).reduce((x1, x2) -> x1 + x2).get()
				/ points.size());

		return new Point(x, y);
	}

	public List<Point> asListPx() {
		return Arrays.asList(new Point(getLeftPx(), getTopPx()), new Point(getLeftPx() + getWidthPx(), getTopPx()),
				new Point(getLeftPx() + getWidthPx(), getTopPx() + getHeightPx()),
				new Point(getLeftPx(), getTopPx() + getHeightPx()));
	}

	public Point getOffset(boolean inCentimeters) {
		return new Point(getLeft(inCentimeters), getTop(inCentimeters));
	}

	/**
	 * @return the adjustFactorSet
	 */
	public boolean isAdjustFactorSet() {
		return adjustFactorSet;
	}

	/**
	 * @param adjustFactorSet
	 *            the adjustFactorSet to set
	 */
	public void setAdjustFactorSet(boolean adjustFactorSet) {
		this.adjustFactorSet = adjustFactorSet;
	}

	public void saveProperties() {
		Properties.add(Key.ARENA_ORIGIN_X, getOrigin(true).x);
		Properties.add(Key.ARENA_ORIGIN_Y, getOrigin(true).y);
		Properties.add(Key.ARENA_DIAGONAL_X, getDiagonal(true).x);
		Properties.add(Key.ARENA_DIAGONAL_Y, getDiagonal(true).y);
	}

	public void setOriginCm(int x, int y) {
		setOriginCm(new Point(x,y));
	}

	public void setDiagonalCm(int x, int y) {
		setDiagonalCm(new Point(x, y));
	}

}
