package br.ita.airgroup.israel.epucknetwork.helper;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

import com.mathworks.toolbox.javabuilder.MWException;

import br.ita.airgroup.israel.commons.clock.Clock;
import br.ita.airgroup.israel.commons.news.Publisher;
import br.ita.airgroup.israel.commons.news.Subscriber;
import br.ita.airgroup.israel.commons.thread.DaemonThread;
import br.ita.airgroup.israel.commons.utils.FileUtils;
import br.ita.airgroup.israel.epucknetwork.core.controller.Epuck;
import br.ita.airgroup.israel.epucknetwork.core.controller.Topology;
import br.ita.airgroup.israel.epucknetwork.core.imaging.Camera;
import br.ita.airgroup.israel.epucknetwork.helper.exception.SettingsException;
import br.ita.airgroup.israel.epucknetwork.matlab.MatlabLogFacade;

public class ExperimentRecorder implements Subscriber<String> {

	private static final long INTERVAL = 1000;
	private List<Epuck> epucks;
	private boolean running = false;
	private Thread arenaRecordingThread;
	private Topology topology;
	private MatlabLogFacade matlab;
	private Camera camera;
	private LocalDateTime startTime;
	private Experiment experiment;
	private Path basepath;

	public ExperimentRecorder(Experiment experiment, Camera camera) {
		this.epucks = experiment.topology.getEpucks();
		this.topology = experiment.topology;
		try {
			this.matlab = new MatlabLogFacade();
		} catch (MWException e) {
			e.printStackTrace();
		}
		this.camera = camera;
		this.experiment = experiment;
	}

	public void start() throws IOException {

		if (running) {
			return;
		}

		Logger.subscribe(this, Publisher.TYPE.ALL);

		this.basepath = Paths.get(FileUtils.removeSuffix(ExperimentInfo.save(this.experiment).toFile()));

		startTime = Clock.now();

		startArenaRecording();
		topology.setRecordingFolder(this.experiment.getSavingFolder());
		camera.startRecording(this.experiment.getSavingFolder());

	}

	private void startArenaRecording() {
		try {

			running = true;
			this.matlab.setLogFilepath(Paths.get(basepath + FileUtils.MAT_SUFFIX));

			arenaRecordingThread = DaemonThread.buildAndStart("ArenaRecorderThread", () -> {
				while (running) {
					record();
					try {
						Thread.sleep(INTERVAL);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
			stop("exception");
		}
	}

	private synchronized void record() {

		for (Epuck e : epucks) {
			try {
				this.matlab.logControl(e.getId(), Duration.between(startTime, Clock.now()).toMillis(),
						e.getPostureCm().x, e.getPostureCm().y);
			} catch (MWException | SettingsException e1) {
				e1.printStackTrace();
			}
		}
	}

	public void stop(String label) {

		if (!running) {
			return;
		}

		running = false;
		arenaRecordingThread.interrupt();

		camera.stopRecording(label);

		Logger.unsubscribe(this);
		Logger.info(ExperimentRecorder.class, "Experiment Saved on %s.*.", basepath.toString());
	}

	@Override
	public void receiveReport(String newContent) {
		try {
			FileUtils.append(Paths.get(basepath + FileUtils.LOG_SUFFIX), false, newContent);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
