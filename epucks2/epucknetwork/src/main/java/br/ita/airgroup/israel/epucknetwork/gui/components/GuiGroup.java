package br.ita.airgroup.israel.epucknetwork.gui.components;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.LayoutManager;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

public class GuiGroup extends JPanel{

	private static final long serialVersionUID = 3893741868946347073L;

	public GuiGroup(String label, LayoutManager layout) {
		super(layout);
		
		this.setBorder(BorderFactory.createTitledBorder(label));
	}

	public GuiGroup(String label) {
		this(label, new FlowLayout());
	}

	public GuiGroup(String label, int lines, int columns) {
		this(label, new GridLayout(lines, columns));
	}
	
}
