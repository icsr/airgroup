package br.ita.airgroup.israel.epucknetwork.helper.message;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import br.ita.airgroup.israel.commons.coord.Duo;
import br.ita.airgroup.israel.commons.coord.Position;
import br.ita.airgroup.israel.commons.utils.MathUtils;
import br.ita.airgroup.israel.epucknetwork.core.controller.Epuck;
import br.ita.airgroup.israel.epucknetwork.helper.Logger;
import br.ita.airgroup.israel.epucknetwork.helper.file.Properties;
import br.ita.airgroup.israel.epucknetwork.helper.file.Properties.Key;
import br.ita.airgroup.israel.epucknetwork.matlab.helper.UControl;

public class Neighborhood {

	public static final int REFER_TO_NEIGHBORS_POSITION_AND_SECOND_EIGENVALUE = 3;
	public static final int REFER_TO_MATLAB_CONTROL = 2;
	public static final int REFER_TO_CURRENT_EPUCK_POSTURE = 3;
	public static final int REFER_TO_NUMBER_OF_KNOWN_EPUCKS = 1;
	private static final int REFER_TO_OFFSET = REFER_TO_MATLAB_CONTROL + REFER_TO_CURRENT_EPUCK_POSTURE
			+ REFER_TO_NUMBER_OF_KNOWN_EPUCKS;

	private List<Duo<Position, Float>> positionsAndEigens;
	private Optional<Float> matlabControlX = Optional.empty();
	private Optional<Float> matlabControlY = Optional.empty();
	private Epuck epuck;

	public Neighborhood(Epuck epuck) {
		this.epuck = epuck;
		positionsAndEigens = new ArrayList<>();
		populateAllPosturesAndEigensWithThisInstanceAtFirst();
	}

	public Neighborhood(Epuck epuck, float controlX, float controlY) {
		this(epuck);
		addControls(controlX, controlY);
	}

	/**
	 * Generate list of params joined by SEPARATOR to be send to epuck The first
	 * element of this list is the number of epucks in area Followed by: current
	 * epuck posture Then the epuck neighbors positions and eigen values are added.
	 * 
	 * @return Generate list of params joined by SEPARATOR to be send to epuck
	 */
	public List<String> toList() {

		List<String> list = new ArrayList<>();

		list.add(Integer.toString(epuck.getKnownOthersEpucks().size() + 1)); // param[0] at command.c of epuck firmware
		list.add(Integer.toString(epuck.getPostureCm().x)); // param[1] at command.c of epuck firmware
		list.add(Integer.toString(epuck.getPostureCm().y)); // param[2] at command.c of epuck firmware
		list.add(Integer.toString(epuck.getPostureCm().angle)); // param[3] at command.c of epuck firmware

		list.addAll(getOthersPositionsAndEigensAsList());

		list.add(Float
				.toString((float) MathUtils.round(matlabControlX.orElse(0f), Properties.getAsInteger(Key.FloatScale))));
		list.add(Float
				.toString((float) MathUtils.round(matlabControlY.orElse(0f), Properties.getAsInteger(Key.FloatScale))));

		if (list.size() != expectedListSize(epuck.getKnownOthersEpucks().size() + 1)) {
			Logger.warn(epuck, "Neighborhood list don't match size expected by epuck firmware.");
		}

		return list;
	}

	public String toString() {
		return toList().stream().reduce((a, b) -> String.format("%s%s%s", a, Message.SEPARATOR, b)).orElse("");
	}

	public static int expectedListSize(int numberOfKnownEpucks) {
		return REFER_TO_NEIGHBORS_POSITION_AND_SECOND_EIGENVALUE * (numberOfKnownEpucks - 1) + REFER_TO_OFFSET;
	}

	public List<String> getOthersPositionsAndEigensAsList() {

		List<String> list = new ArrayList<>();

		positionsAndEigens.stream().skip(1L).forEach(p -> {
			list.add(Integer.toString((int) p.getLeft().x));
			list.add(Integer.toString((int) p.getLeft().y));
			list.add(Float.toString(p.getRight()));
		});

		return list;
	}

	public List<String> getAllPositionsAsList() {
		return positionsAndEigens.parallelStream().map(pe -> pe.getLeft().toString()).collect(Collectors.toList());
	}

	private void populateAllPosturesAndEigensWithThisInstanceAtFirst() {
		positionsAndEigens.add(
				Duo.of(new Position(epuck.getPostureCm().x, epuck.getPostureCm().y), epuck.getSecondEigenVector()));

		positionsAndEigens.addAll(epuck.getKnownOthersEpucks().parallelStream()
				.map(k -> Duo.of(new Position(k.getPostureCm().x, k.getPostureCm().y), k.getSecondEigenVector()))
				.collect(Collectors.toList()));
	}

	public Neighborhood addControls(float controlX, float controlY) {
		this.matlabControlX = Optional.of((float) MathUtils.round(controlX, Properties.getAsInteger(Key.FloatScale)));
		this.matlabControlY = Optional.of((float) MathUtils.round(controlY, Properties.getAsInteger(Key.FloatScale)));

		return this;
	}

	public List<Position> getAllPositionsWithThisEpuckInstanceAtFirst() {
		return this.positionsAndEigens.stream().map(p -> p.getLeft()).collect(Collectors.toList());
	}

	public Neighborhood addControls(UControl uControl) {
		return addControls((float) uControl.getX(), (float) uControl.getY());
	}

}
