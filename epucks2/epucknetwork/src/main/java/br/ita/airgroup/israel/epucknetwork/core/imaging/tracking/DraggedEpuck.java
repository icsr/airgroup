package br.ita.airgroup.israel.epucknetwork.core.imaging.tracking;

import java.awt.Point;
import java.util.Optional;

import br.ita.airgroup.israel.epucknetwork.core.controller.Epuck;
import br.ita.airgroup.israel.epucknetwork.helper.tracking.TrackerUtils;

public class DraggedEpuck extends Epuck {

	private static final long serialVersionUID = 7002911670187730447L;

	protected boolean multipleSelection;
	protected Optional<Point> targetPointCm = Optional.empty();
	private Optional<Point> targetPointPx = Optional.empty();
	protected boolean dragging;

	private final Epuck linkedEpuck;

	public DraggedEpuck(Epuck epuck) {
		super(epuck);
		
		this.linkedEpuck = epuck;
		this.addBluetoothHandler(epuck.getBluetoothHandler());
	}

	public boolean isMultipleSelection() {
		return multipleSelection;
	}

	public void setMultipleSelection(boolean multipleSelection) {
		this.multipleSelection = multipleSelection;
	}

	public void setTargetPointCm(Optional<Point> targetPointCm) {
		this.targetPointCm = targetPointCm;
	}

	public boolean isDragging() {
		return dragging;
	}

	public void setDragging(boolean dragging) {
		this.dragging = dragging;
	}

	public void setTargetPointPx(Optional<Point> targetPointPx) {
		this.targetPointPx = targetPointPx;

		targetPointPx.ifPresent(tpx -> {
			targetPointCm = Optional.of(TrackerUtils.cameraPositionInPxToAbsolutePositionInCm(arena.get(), tpx, adjustFactorInCmPerPx));
		});
	}

	public Optional<Point> getTargetPoint(boolean inCm) {
		return inCm ? targetPointCm : targetPointPx;
	}
	
	@Override
	public void setSelected(boolean selected) {
		super.setSelected(selected);
		linkedEpuck.setSelected(true);
	}

}
