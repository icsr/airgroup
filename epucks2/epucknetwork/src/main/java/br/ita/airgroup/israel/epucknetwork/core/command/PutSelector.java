package br.ita.airgroup.israel.epucknetwork.core.command;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.jcodec.common.StringUtils;

public enum PutSelector implements Selectable {

	PROXIMITY_THRESHOLD(1, 4, PutSelectorParam.of("Frontal IR", 200f)), COLLISION_DEVIANT(2,
			PutSelectorParam.of("Angle (degrees)", 20f), PutSelectorParam.of("Distance (cm)", 20f)), WAITING_TIMER(3,
					PutSelectorParam.of("Waiting Time (ms)", 200f)), MAX_POSTURE_ERROR(4,
							PutSelectorParam.of("Max Posture Error (cm)", 4f)), MAX_FIXING_POSITION(5,
									PutSelectorParam.of("Max Fixing Position (# of tries)", 4f)), MAX_GETTING_POSITION(
											6,
											PutSelectorParam.of("Max Getting Position (# of tries)",
													3f)), ALGORITHM_CONFIGS(7, 4,
															PutSelectorParam.of("Connectivity Threshold", 0.08f),
															PutSelectorParam.of("Minimum Dislocation", 1f),
															PutSelectorParam.of("Algebraic Connectivity Lower Bound",
																	0.4f),
															PutSelectorParam.of("Maximum Dislocation", 5f)), RANGE(8, 1,
																	PutSelectorParam.of("Range (cm)", 20f));
	private int id;
	private Integer numberOfParams;
	private List<PutSelectorParam> params;

	PutSelector(Integer id, int numberOfParams, PutSelectorParam... param) {

		this.id = id;
		this.numberOfParams = numberOfParams;
		this.params = Arrays.stream(param).collect(Collectors.toList());

	}

	PutSelector(Integer id, PutSelectorParam... param) {
		this(id, param.length, param);
	}

	public int getId() {
		return id;
	}

	@Override
	public boolean isReceivable() {
		return Selector.PUT.isReceivable();
	}

	@Override
	public char getSelectorChar() {
		return Selector.PUT.selector;
	}

	@Override
	public String getDescription() {
		return Selector.PUT.getDescription();
	}

	@Override
	public void setSelected(boolean selected) {
		Selector.PUT.setSelected(selected);
	}

	@Override
	public boolean isSelected() {
		return Selector.PUT.isSelected();
	}

	@Override
	public int getNumberOfParams() {
		return numberOfParams;
	}

	@Override
	public void setNumberOfParams(int numberOfParams) {
		this.numberOfParams = numberOfParams;
	}

	public List<PutSelectorParam> getParams() {
		return params;
	}

	@Override
	public String toString() {
		return StringUtils.capitaliseAllWords(name().replaceAll("_", " ").toLowerCase());
	}

}
