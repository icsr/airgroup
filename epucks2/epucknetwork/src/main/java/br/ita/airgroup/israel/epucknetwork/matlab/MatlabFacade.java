package br.ita.airgroup.israel.epucknetwork.matlab;

import java.util.Optional;

import com.mathworks.toolbox.javabuilder.MWException;

import br.ita.airgroup.israel.epucknetwork.helper.BaseSystem;
import br.ita.airgroup.israel.epucknetwork.helper.message.Messenger;
import performControl.MatlabRuntime;

public abstract class MatlabFacade {
	protected Optional<MatlabRuntime> matlab = Optional.empty();

	public MatlabFacade() throws MWException {
		

		try {
			this.matlab = Optional.of(new MatlabRuntime());
		} catch (Error e) {	
			e.printStackTrace();
			
			boolean mcrinstalled = Messenger.answerIfIsTrue("Have you installed Matlab and Matlab Compile Runtime (MCR) V92?");
			
			if(!mcrinstalled) {
				Messenger.info("Please install Matlab and MCR V92 and set environment property LD_LIBRARY_PATH to MCR/glnxa64 folder!");
				BaseSystem.shutdown();
				System.exit(0);
			}else {
				Messenger.info("Please set environment property LD_LIBRARY_PATH MCR subfolder containing library 'libmwmclmcrrt'");
			}
			
			
		}
		
	}
}
