package br.ita.airgroup.israel.epucknetwork.core.command;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import br.ita.airgroup.israel.epucknetwork.helper.message.SetUtils;

public enum Selector implements Selectable {
	ROTATE('R', "ROTATE", 1, false), MOVE('M', "MOVE TO COORD", 2, true), GET_POSTURE('G', "GET POSTURE", 0,
			true), SET_POSTURE('S', "SET POSTURE", 4, false), PUT('P', "PUT PARAM", -1, true), TOPOLOGY('T',
					"SET TOPOLOGY", 2,
					false), NONE('-', "NONE", -1, true), ANSWER('W', "COMMAND'S ANSWER", -1, true), RESET('0',
							"RESET EPUCK", 0,
							false), FAIL('1', "FAIL", 0, false), LED('2', "LIGHT LED", 1, false), READ_CURRENT_POSTURE(
									'C', "FORCE EPUCK TO GET POSTURE", 0,
									true), DISTANCE('D', "MOVE DISTANCE", 1, false), BOUNDARY('B', "ARENA BOUNDARY", 4,
											true), TO_ANGLE('A', "TURN TO ANGLE", 1, false), SET_NEIGHBORHOOD('N',
													"Set epuck's neighboors", -1, true), SECOND_EIGENVECTOR('V',
															"Receive epuck's second eigenvector", -1,
															true), SECOND_EIGENVALUE('L',
																	"Receive epuck's second eigenvalue", -1,
																	true), GAIN('E', "Topology Gains", 4,
																			false), TRACKING_REACHED('t',
																					"TRACKING REACHED", 0,
																					true), PING('p', "PING", 0,
																							true), SHOW_CLASS_INFOS('-',
																									"Class Informatives",
																									-1,
																									false, false), SHOW_CONTROL_EVALUATION(
																											'-',
																											"Control Evals",
																											-1, false, false);
	char selector;
	private String description;
	private boolean receivable;
	private boolean selected = true;
	private int numberOfParams;
	private boolean command;

	private Selector(char selector, String description, int numberOfParams, boolean receivable, boolean command) {
		this.selector = selector;
		this.description = description;
		this.receivable = receivable;
		this.numberOfParams = numberOfParams;
		this.command = command;
	}
	
	private Selector(char selector, String description, int numberOfParams, boolean receivable) {
		this(selector, description, numberOfParams, receivable, true);
	}

	public static synchronized Selector get(String command) {
		return get(command.charAt(0));
	}

	public static synchronized Selector get(char selector) {
		for (Selector v : Selector.values()) {
			if (v.getSelectorChar() == selector) {
				return v;
			}
		}
		return Selector.NONE;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.ita.airgroup.israel.epucknetwork.core.controller.Selectable#
	 * isReceivable()
	 */
	@Override
	public boolean isReceivable() {
		return receivable;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.ita.airgroup.israel.epucknetwork.core.controller.Selectable#
	 * getSelectorChar()
	 */
	@Override
	public char getSelectorChar() {
		return this.selector;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.ita.airgroup.israel.epucknetwork.core.controller.Selectable#
	 * getDescription()
	 */
	@Override
	public String getDescription() {
		return description;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.ita.airgroup.israel.epucknetwork.core.controller.Selectable#toString()
	 */
	@Override
	public String toString() {
		return Character.toString(selector);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.ita.airgroup.israel.epucknetwork.core.controller.Selectable#
	 * setSelected(boolean)
	 */
	@Override
	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.ita.airgroup.israel.epucknetwork.core.controller.Selectable#isSelected ()
	 */
	@Override
	public boolean isSelected() {
		return selected;
	}

	public static List<Selector> values(boolean selected) {
		return Arrays.stream(Selector.values()).filter(s -> s.isSelected() == selected).collect(Collectors.toList());
	}

	/**
	 * @return the numberOfParams
	 */
	@Override
	public int getNumberOfParams() {
		return numberOfParams;
	}

	@Override
	public void setNumberOfParams(int numberOfParams) {
		this.numberOfParams = numberOfParams;
	}

	public static boolean isSelected(Selectable selector) {
		return SetUtils.belongsTo(selector, Selector.values(true));
	}

	public boolean isCommand() {
		return command;
	}
}