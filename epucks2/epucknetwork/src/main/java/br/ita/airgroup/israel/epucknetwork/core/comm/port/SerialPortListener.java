package br.ita.airgroup.israel.epucknetwork.core.comm.port;

import java.util.TooManyListenersException;

import br.ita.airgroup.israel.epucknetwork.helper.Logger;
import br.ita.airgroup.israel.epucknetwork.helper.message.CommandListenable;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;

public class SerialPortListener implements gnu.io.SerialPortEventListener {

	private CommandListenable listener;

	public SerialPortListener(CommandListenable listener, SerialPort serialPort) throws TooManyListenersException {
		this.listener = listener;
		serialPort.notifyOnBreakInterrupt(true);
		serialPort.notifyOnCarrierDetect(true);
		serialPort.notifyOnCTS(true);
		serialPort.notifyOnDataAvailable(true);
		serialPort.notifyOnDSR(true);
		serialPort.notifyOnFramingError(true);
		serialPort.notifyOnOutputEmpty(true);
		serialPort.notifyOnOverrunError(true);
		serialPort.notifyOnParityError(true);
		serialPort.notifyOnRingIndicator(true);
		
		serialPort.addEventListener(this);
	}

	@Override
	public void serialEvent(SerialPortEvent e) {
		Logger.info(SerialPortListener.class, true, "" + e.getEventType());

		listener.run();
	}

}
