package br.ita.airgroup.israel.epucknetwork.helper.streaming;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.nio.file.Path;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import com.xuggle.mediatool.IMediaWriter;
import com.xuggle.mediatool.ToolFactory;
import com.xuggle.xuggler.ICodec;

import br.ita.airgroup.israel.commons.image.ImageUtils;
import br.ita.airgroup.israel.commons.utils.FileUtils;
import br.ita.airgroup.israel.epucknetwork.helper.Logger;
import br.ita.airgroup.israel.epucknetwork.helper.message.Messenger;

public class VideoConverter {

	private Path outputFile;
	final IMediaWriter writer;
	private long startTime;
	private boolean recording = false;

	public VideoConverter(Path recordingFolder, int width, int height) {
		this(configPreOutputFile(recordingFolder), width, height);
	}

	public VideoConverter(File recordingFile, int width, int height) {
		this.outputFile = recordingFile.toPath();

		writer = ToolFactory.makeWriter(this.outputFile.toString());
		writer.addVideoStream(0, 0, ICodec.ID.CODEC_ID_MPEG4, width, height);
	}

	private static File configPreOutputFile(Path recordingFolder) {
		return FileUtils.getPath(recordingFolder, FileUtils.MP4_SUFFIX).toFile();
	}

	public synchronized void startRecording() {

		if (recording) {
			return;
		}

		Logger.info(VideoConverter.class, "Starting recording...");

		recording = true;
		startTime = System.nanoTime();
	}

	public synchronized void appendImage(BufferedImage image) {

		BufferedImage convertedImage = convertToType(image);

		writer.encodeVideo(0, convertedImage, System.nanoTime() - startTime, TimeUnit.NANOSECONDS);

		convertedImage.flush();
		convertedImage = null;

	}

	private BufferedImage convertToType(BufferedImage image) {
		if (image.getType() == ImageUtils.BUFFERED_IMAGE_TYPE) {
			return image;
		}

		BufferedImage goodTypedImage = new BufferedImage(image.getWidth(), image.getHeight(),
				ImageUtils.BUFFERED_IMAGE_TYPE);
		Graphics g = goodTypedImage.createGraphics();
		g.drawImage(image, 0, 0, null);

		return goodTypedImage;
	}

	public synchronized void stopRecording(String label) {
		if (!recording) {
			return;
		}
		Logger.info(VideoConverter.class, "Stop recording...");

		writer.flush();
		writer.close();
		recording = false;

		File destinationFile = getDestinationFile(label);
		FileUtils.move(outputFile.toFile(), destinationFile, true);

		Logger.info(VideoConverter.class, "Recording saved on", destinationFile.getAbsolutePath());
	}

	public synchronized void stopRecording() {
		Messenger.askSomething("Type a filename for this recording").ifPresent(m -> stopRecording(m));
	}

	public synchronized void stopRecording(Optional<String> label) {
		if (label.isPresent()) {
			stopRecording(label.get());
		} else {
			stopRecording();
		}
	}

	private File getDestinationFile(String label) {
		StringBuilder filename = new StringBuilder(FileUtils.removeSuffix(outputFile.toFile()));

		Optional<String> additionalFilename = FileUtils.toFilename(Optional.of(label));
		additionalFilename.ifPresent(a -> filename.append(" ").append(a));
		filename.append(FileUtils.MP4_SUFFIX);
		File f = new File(filename.toString());
		return f;
	}

	public boolean isRecording() {
		return recording;
	}

}