package br.ita.airgroup.israel.epucknetwork.gui.components;

import javax.swing.JCheckBox;

import br.ita.airgroup.israel.commons.utils.StringUtils;
import br.ita.airgroup.israel.epucknetwork.core.command.Selectable;
import br.ita.airgroup.israel.epucknetwork.core.command.Selector;
import br.ita.airgroup.israel.epucknetwork.helper.file.Properties;

public class SelectableCheckbox extends JCheckBox {

	private static final long serialVersionUID = 1L;
	private Selector selector;

	public SelectableCheckbox(Selector selector) {
		super(StringUtils.capitalize(selector.getDescription().toLowerCase()));
		this.selector = selector;

		setSelected(Properties.getAsBoolean(selector));
		selector.setSelected(isSelected());
	}

	public Selectable getSelector() {
		return selector;
	}
	
	@Override
	protected void fireStateChanged() {
		selector.setSelected(isSelected());
		Properties.add(selector, isSelected());
		super.fireStateChanged();
	}
		
}
