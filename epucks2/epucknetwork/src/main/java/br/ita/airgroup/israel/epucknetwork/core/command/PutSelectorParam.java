package br.ita.airgroup.israel.epucknetwork.core.command;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.tuple.Pair;

public class PutSelectorParam extends Pair<String, Float> {
	
	private static final long serialVersionUID = -7371040620718822257L;
	private String label;
	private Float value;
	
	public PutSelectorParam(String param, Float value) {
		this.label = param;
		this.value = value;
	}

	@Override
	public Float setValue(Float value) {
		return this.value = value;
	}

	public String getLabel() {
		return getLeft();
	}
	
	@Override
	public String getLeft() {
		return this.label;
	}

	@Override
	public Float getValue() {
		return this.getRight();
	}
	
	public static PutSelectorParam of(String param, Float value){
		return new PutSelectorParam(param, value);
	}

	@Override
	public Float getRight() {
		return value;
	}

	public static List<Float> getValues(PutSelector putSelector, List<PutSelectorParam> params) {
		
		while(params.size() < putSelector.getNumberOfParams()){
			params.add(params.get(0));
		}
		
		return params.stream().map(p -> p.getRight()).collect(Collectors.toList());
	}

}
