package br.ita.airgroup.israel.epucknetwork.gui.components;

import java.awt.event.ActionListener;

public interface Keyable<T, V> {

	T loadKey();
	void saveKey(T value);
	V getValue();
	void setValue(V value);
	void whenChange(Runnable listener);
}
