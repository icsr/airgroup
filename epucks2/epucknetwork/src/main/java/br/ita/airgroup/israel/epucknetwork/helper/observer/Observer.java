package br.ita.airgroup.israel.epucknetwork.helper.observer;

public interface Observer<T> {
	public void observe(Observable<T> observable);
}
