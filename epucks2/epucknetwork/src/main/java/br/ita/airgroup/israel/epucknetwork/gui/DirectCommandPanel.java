package br.ita.airgroup.israel.epucknetwork.gui;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;

import br.ita.airgroup.israel.epucknetwork.core.controller.BluetoothHandler;
import br.ita.airgroup.israel.epucknetwork.core.controller.Topology;
import br.ita.airgroup.israel.epucknetwork.core.imaging.tracking.Tracker;
import br.ita.airgroup.israel.epucknetwork.gui.aux.ChooseEpucksToPerformAction.MassiveEpuckAction;
import br.ita.airgroup.israel.epucknetwork.gui.aux.panel.AuxPanel;
import br.ita.airgroup.israel.epucknetwork.gui.components.BluetoothCheckbox;
import br.ita.airgroup.israel.epucknetwork.gui.components.DirectCommandCombobox;
import br.ita.airgroup.israel.epucknetwork.helper.message.Messenger;
import br.ita.airgroup.israel.epucknetwork.helper.observer.Observable;

public class DirectCommandPanel extends AuxPanel implements Observable<Void> {

	private static final long serialVersionUID = 5087318002392656056L;
	private Tracker tracker;
	private List<BluetoothCheckbox> chooseEpuckToCommandCheckBoxes = new ArrayList<>();

	private JMenu chooseEpuckToCommandMenu;
	private DirectCommandCombobox directCommandCombobox;
	private ActionListener actionEvent;

	public DirectCommandPanel() {
		this.chooseEpuckToCommandMenu = new JMenu("Select Epucks");
		this.chooseEpuckToCommandMenu.setEnabled(false);

		this.setLayout(new FlowLayout());

		actionEvent = a -> countEpucksSelectedAndUpdateCommandGui();
	}

	public DirectCommandPanel init() {
		directCommandCombobox = new DirectCommandCombobox(a -> {
			chooseEpuckToCommandCheckBoxes.stream().filter(b -> b.isSelected()).forEach(
					b -> b.getHandler().processCommandToSend(String.class.cast(directCommandCombobox.getItemAt(0))));
		});

		this.setLayout(new FlowLayout());
		setBorder(BorderFactory.createTitledBorder("Direct Commands"));

		JMenuBar chooseEpuckToCommandMenuBar = new JMenuBar();
		chooseEpuckToCommandMenuBar.add(chooseEpuckToCommandMenu);

		add(chooseEpuckToCommandMenuBar);
		add(directCommandCombobox);

		chooseEpuckToCommandMenu.removeAll();
		chooseEpuckToCommandCheckBoxes.clear();
		chooseEpuckToCommandMenu.setEnabled(true);

		JPanel buttons = new JPanel(new GridLayout(0, 1));

		JButton selectAll = new JButton("Select All");
		selectAll.addActionListener(actionEvent);
		markAs(chooseEpuckToCommandMenu, selectAll, true);

		JButton unselectAll = new JButton("Unselect All");
		unselectAll.addActionListener(actionEvent);
		markAs(chooseEpuckToCommandMenu, unselectAll, false);

		buttons.add(selectAll);
		buttons.add(unselectAll);

		chooseEpuckToCommandMenu.add(buttons);
		chooseEpuckToCommandMenu.addSeparator();

		return this;
	}

	public void initWhenTopologyIsSet() {

		JPanel mainPanel = new JPanel(new GridLayout(0, 2));

		for (BluetoothHandler b : topology.getBluetooths()) {
			BluetoothCheckbox checkbox = new BluetoothCheckbox(b);
			checkbox.addActionListener(actionEvent);
			chooseEpuckToCommandCheckBoxes.add(checkbox);

			mainPanel.add(checkbox);
		}

		chooseEpuckToCommandMenu.add(mainPanel);

		countEpucksSelectedAndUpdateCommandGui();

	}

	private void countEpucksSelectedAndUpdateCommandGui() {
		int count = 0;

		for (BluetoothCheckbox c : chooseEpuckToCommandCheckBoxes) {
			if (c.isSelected()) {
				count++;
			}
		}

		chooseEpuckToCommandMenu.setText(String.format("%d %s selected", count, count > 1 ? "epucks" : "epuck"));
	}

	private void markAs(JMenu container, JButton actionButton, boolean selected) {
		actionButton.addActionListener(a -> {
			for (BluetoothCheckbox c : chooseEpuckToCommandCheckBoxes) {
				c.setSelected(selected);
			}
		});
	}

	public void setTracker(Tracker tracker) {
		this.tracker = tracker;
		tracker.observe(this);

		chooseEpuckToCommandCheckBoxes.forEach(c -> {
			c.addChangeListener(l -> countEpucksSelectedAndUpdateCommandGui());
		});

		this.chooseEpuckToCommandMenu.setEnabled(true);
		this.setEnabled(true);
	}

	public Tracker getTracker() {
		return tracker;
	}

	@Override
	public void setTopology(Topology topology) {
		super.setTopology(topology);
		initWhenTopologyIsSet();
	}

	public List<BluetoothCheckbox> getChooseEpuckToCommandCheckBoxes() {
		return chooseEpuckToCommandCheckBoxes;
	}

	public void performTopologyAction(ApplicationGUI applicationGUI, MassiveEpuckAction action, boolean... options) {

		if (options.length == 0 && action != MassiveEpuckAction.Prepare) {
			if (!Messenger.answerIfIsTrue(
					String.format("Do you really want to %s all epucks?", action.name().toLowerCase()))) {
				return;
			}
		}

		if (action == MassiveEpuckAction.Reset) {
			applicationGUI.topology.resetAll();
			return;
		}

		if (action == MassiveEpuckAction.Led) {
			applicationGUI.topology.lightThemAll(options[0]);
			return;
		}

		if (action == MassiveEpuckAction.Prepare) {
			if (Messenger.answerIfIsTrue("Do want to Reset them all?")) {
				applicationGUI.topology.resetAll();
			} else {
				applicationGUI.topology.sendConfigs();
			}
			return;
		}
		
		if (action == MassiveEpuckAction.TurnToZero) {
			applicationGUI.topology.turnToZero();
			return;
		}
		
		if (action == MassiveEpuckAction.TurnTo90) {
			applicationGUI.topology.turnTo90();
			return;
		}
	}

	@Override
	public void notify(Void content) {
		chooseEpuckToCommandCheckBoxes.forEach(c -> c.setSelected(c.getEpuck().isSelected()));
	}
}
