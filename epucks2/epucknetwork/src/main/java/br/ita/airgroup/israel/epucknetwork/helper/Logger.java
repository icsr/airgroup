package br.ita.airgroup.israel.epucknetwork.helper;

import java.io.IOException;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.tuple.Triple;

import br.ita.airgroup.israel.commons.collections.LimitedList;
import br.ita.airgroup.israel.commons.news.Publisher;
import br.ita.airgroup.israel.commons.news.Subscriber;
import br.ita.airgroup.israel.commons.utils.FileUtils;
import br.ita.airgroup.israel.commons.utils.StringUtils;
import br.ita.airgroup.israel.epucknetwork.core.command.Selector;
import br.ita.airgroup.israel.epucknetwork.core.controller.Epuck;

public class Logger {

	private static final LogPublisher publisher = new LogPublisher();
	private static final String SENDING_LABEL_FORMAT = "|@->%s|";
	private static final String RECEIVING_LABEL_FORMAT = "|@<-%s|";
	private static final int MAX_LOG_LINES = 1000;
	private static LimitedList<Triple<LocalDateTime, String, Boolean>> lines = new LimitedList<>(MAX_LOG_LINES,
			() -> append(), true);
	private static StringBuffer logBuffer = new StringBuffer(MAX_LOG_LINES);
	public static DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("HH:mm:ss.SSS");
	private static Path logPath = FileUtils.getPath(FileUtils.getTempDir().toPath(), FileUtils.LOG_SUFFIX);

	private static void append() {
		try {
			FileUtils.append(logPath, false, asString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static String asString(Triple<LocalDateTime, String, Boolean> line) {
		return String.format("%s %s", line.getLeft().format(dateFormat), line.getMiddle());
	}

	public static List<String> asString() {
		return lines.stream().parallel().map(t -> asString(t)).collect(Collectors.toList());
	}

	private static enum Type {
		INFO, ERROR, WARN, EXCEPTION;

		@Override
		public String toString() {
			return name();
		}

	};

	public synchronized static void info(Epuck senderEpuck, boolean visible, String... messages) {
		info(Type.INFO, senderEpuck, visible, messages);
	}

	public synchronized static void info(Epuck senderEpuck, String... messages) {
		info(Type.INFO, senderEpuck, true, messages);
	}

	public synchronized static void warn(Epuck senderEpuck, String... messages) {
		info(Type.WARN, senderEpuck, true, messages);
	}

	public synchronized static void info(Type type, Epuck senderEpuck, boolean visible, String... messages) {
		info(type, String.format(RECEIVING_LABEL_FORMAT, senderEpuck.toString()), visible, messages);
	}

	public synchronized static void info(Class<?> senderClass, Epuck destinationEpuck, String... messages) {
		info(senderClass, destinationEpuck, false, Selector.SHOW_CLASS_INFOS.isSelected(), messages);
	}

	public synchronized static void info(Class<?> senderClass, Epuck destinationEpuck, boolean visible,
			String... messages) {
		info(senderClass, destinationEpuck, false, visible, messages);
	}

	public static void info(Class<?> senderClass, Epuck destinationEpuck, boolean showSenderClassName, boolean visible,
			String... messages) {
		info(Type.INFO, String.format(SENDING_LABEL_FORMAT, destinationEpuck.toString()),
				showSenderClassName ? Optional.of(senderClass) : Optional.empty(), visible, messages);
	}

	private static void info(Type type, String label, Optional<Class<?>> senderClass, boolean visible,
			String[] messages) {

		logBuffer.setLength(0);
		logBuffer.append(label);
		logBuffer.append(StringUtils.SPACE);
		senderClass.ifPresent(s -> {
			logBuffer.append(s.getSimpleName());
			logBuffer.append(StringUtils.SPACE);
		});
		logBuffer.append(StringUtils.join(messages, StringUtils.SPACE));

		String dateTaggedMessage = appendMessage(type, logBuffer.toString(), visible);

		if (visible) {
			System.out.println(dateTaggedMessage);
			report(dateTaggedMessage, Publisher.TYPE.ALL);
			report(dateTaggedMessage, Publisher.TYPE.PARTIAL);
		} else {
			report(dateTaggedMessage, Publisher.TYPE.ALL);
		}

	}

	public synchronized static void info(Class<?> clazz, boolean visible, String... messages) {
		info(Type.INFO, String.format("%s:", clazz.getSimpleName()), visible, messages);
	}

	public synchronized static void error(Class<?> clazz, String... messages) {
		info(Type.ERROR, clazz.getSimpleName(), true, messages);
	}

	public synchronized static void error(Epuck senderEpuck, String... messages) {
		info(Type.ERROR, senderEpuck, true, messages);
	}

	private synchronized static void info(Type type, String label, boolean visible, String... messages) {
		info(type, label, Optional.empty(), visible, messages);
	}

	private static String appendMessage(Type type, String line, boolean visible) {
		lines.addLast(Triple.of(LocalDateTime.now(),
				type == Type.INFO ? line : String.format("(%s) %s", type.name(), line), visible));
		return asString(lines.getLast());
	}

	public static void save(Path selectedDir) {
		append();
		FileUtils.move(logPath.toFile(), selectedDir.toFile(), true);

		Logger.info(Logger.class, true, "Log has been saved into", logPath.toString());
	}

	public static void save() {
		save(BaseSystem.LOG_HOME);
	}

	public static List<String> getLines(boolean reversed, boolean onlyVisible) {
		List<String> copy = new ArrayList<>();

		lines.stream().filter(l -> !onlyVisible || (onlyVisible && l.getRight())).map(l -> l.getMiddle())
				.forEach(l -> copy.add(l));
		if (reversed) {
			Collections.reverse(copy);
		}

		return copy;
	}

	public static void clear() {
		append();
		lines.clear();
	}

	public static void warn(Class<?> clazz, String... messages) {
		info(Type.WARN, clazz.getSimpleName(), true, StringUtils.join(messages, " "));
	}

	public static void info(Class<?> clazz, String... message) {
		info(clazz, true, message);
	}

	public static void exception(Epuck epuck, String... messages) {
		info(Type.EXCEPTION, epuck, false, messages);
	}

	public static void exception(Epuck epuck, Exception e) {
		exception(epuck, e.getMessage(), StringUtils.toString(e));
	}

	public static void warn(Epuck epuck, Exception e) {
		info(Type.WARN, epuck, true, e.getMessage(), StringUtils.toString(e));
	}

	public static void getLines(boolean b, boolean c, LocalDateTime startTime, LocalDateTime now) {
	}

	public static synchronized void subscribe(Subscriber<String> subscriber, Publisher.TYPE type) {
		publisher.subscribe(subscriber, type);
	}

	public static synchronized void unsubscribe(Subscriber<String> subscriber) {
		publisher.unsubscribe(subscriber);
	}

	public static void report(String newContent, Publisher.TYPE type) {
		publisher.report(newContent, type);
	}

}
