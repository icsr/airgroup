package br.ita.airgroup.israel.epucknetwork.core.imaging;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.time.LocalDateTime;
import java.time.ZoneId;

import br.ita.airgroup.israel.commons.image.ImageUtils;
import br.ita.airgroup.israel.commons.utils.DateUtils;
import br.ita.airgroup.israel.commons.utils.Font;
import br.ita.airgroup.israel.epucknetwork.gui.ApplicationGUI;
import br.ita.airgroup.israel.epucknetwork.gui.aux.Exhibitable;
import br.ita.airgroup.israel.epucknetwork.helper.Logger;
import br.ita.airgroup.israel.epucknetwork.helper.tracking.TrackerDrawings;
import br.ita.airgroup.israel.epucknetwork.helper.tracking.TrackerDrawings.Show;
import br.ita.airgroup.israel.epucknetwork.helper.tracking.TrackerUtils;

public class CameraDrawings implements Runnable {

	private Camera camera;
	private LocalDateTime startTime;

	public CameraDrawings(Camera camera) {
		this.camera = camera;
		resetTime();
	}

	public void resetTime() {
		startTime = newTime();
	}

	private LocalDateTime newTime() {
		return LocalDateTime.now(ZoneId.of("GMT"));
	}

	public LocalDateTime getStartTime() {
		return startTime;
	}

	@Override
	public void run() {

		startTime = newTime();
		while (camera.isRunning()) {
			try {
				BufferedImage image = ImageUtils.adjustBrightness(camera.getImage(), camera.brightnessFactor,
						camera.brightnessOffset);

				Graphics2D g = image.createGraphics();
				g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, .4f));

				drawTradeMark(g);
				drawLogoITA(image, g);
				detectAndDrawTrackables(image, g);
				drawMouseLocation(g);
				drawTargetPosturesIfExists(g);

				if (camera.isRecording()) {
					camera.appendVideoFrame(prepareForRecording(image));
					drawRecordingInfos(g);
				}

				camera.setImage(image);

			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}

		Logger.info(Camera.class, "Camera has stopped.");
	}

	private void drawTargetPosturesIfExists(Graphics2D g) {
		if (!camera.hasTargetPostures()) {
			return;
		}

		camera.targetPosturesToDraw.forEach(t -> {
			TrackerDrawings.drawRange(g, t, 10, false, false, Color.LIGHT_GRAY);
		});
	}

	private void drawMouseLocation(Graphics2D g) {
		g.setFont(Font.small(true));
		g.setColor(Color.WHITE);
		g.setComposite(Exhibitable.fullOpaque);

		Point adjustedMouse = adjustMousePosition();
		String mouseLocationFormat = "(%d,%d)";
		String mousePosition = String.format(mouseLocationFormat, adjustedMouse.x, adjustedMouse.y);

		int x, y;
		if (adjustedMouse.x <= camera.getPanel().getWidth()) {
			x = camera.mouse.x + Exhibitable.OFFSET;
		} else {
			x = camera.mouse.x - Exhibitable.OFFSET - g.getFontMetrics().stringWidth(mousePosition);
		}

		if (adjustedMouse.y <= camera.getPanel().getHeight()) {
			y = camera.mouse.y + g.getFontMetrics().getHeight();
		} else {
			y = camera.mouse.y;
		}

		TrackerDrawings.drawString(g, mousePosition, x, y);
		
		mouseLocationFormat = null;
		mousePosition = null;
	}

	public Point adjustMousePosition() {
		Point adjustedMouse;
		if (camera.arena.isPresent()) {
			adjustedMouse = TrackerUtils.cameraPositionInPxToAbsolutePositionInCm(camera.arena.get(), camera.mouse,
					camera.arena.get().getAdjustFactorCmPerPx());
		} else {
			adjustedMouse = camera.mouse;
		}
		return adjustedMouse;
	}

	private void detectAndDrawTrackables(BufferedImage image, Graphics2D g) throws Exception {
		if (camera.isCalibrated()) {
			if (camera.isTracked()) {
				camera.trackable.get().detectAndDrawPatternsAndPolygons(image, g);
			}
		} else {
			g.setFont(Font.big(true));
			g.drawString(Exhibitable.STILL_NOT_CALIBRATED, image.getWidth() / 4, image.getHeight() / 2);
		}
	}

	private void drawRecordingInfos(Graphics2D g) {
		g.setColor(Color.RED);
		g.setFont(Font.big(true));
		g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1f));
		int x = 20;
		int y = 50;
		g.drawString(Exhibitable.REC, x, y);

		// TODO ELIMINATE?
		y += g.getFontMetrics().getHeight();

		g.setColor(Color.WHITE);
		g.setFont(Font.small(true));
		String timeFormat = "Time: %s";
		String timeInfo = String.format(timeFormat, DateUtils.getDurationAsString(startTime, newTime()));
		TrackerDrawings.drawString(g, timeInfo, x, y);
		
		timeFormat = null;
		timeInfo = null;
	}

	private void drawLogoITA(BufferedImage image, Graphics2D g) {
		
		if(!Show.Logo.selected) {
			return;
		}
		
		int offsetX = camera.arena.isPresent() ? camera.arena.get().getDiagonalPx().x - Camera.logoWidth
				: image.getWidth() - Camera.logoWidth;
		int offsetY = camera.arena.isPresent() ? camera.arena.get().getOriginPx().y : 0;
		g.drawImage(Camera.logo, offsetX, offsetY, null);
	}

	private void drawTradeMark(Graphics2D g) {
		int offsetX = camera.getSoutheastCorner().x;
		int offsetY = camera.getSoutheastCorner().y;

		g.setFont(Font.normal(true));

		g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, .8f));
		TrackerDrawings.drawString(g, ApplicationGUI.TITLE, offsetX, offsetY);
	}

	private BufferedImage prepareForRecording(BufferedImage image) {
		if (!camera.arena.isPresent()) {
			return image;
		}

//		try {
//			return ImageUtils.crop(image, camera.arena.get().getOriginPx(), camera.arena.get().getDiagonalPx(),
//					Exhibitable.background.getImage());
//		} catch (Exception e) {
//			e.printStackTrace();
//		}

		return image;
	}

}
