package br.ita.airgroup.israel.epucknetwork.core.algorithm;

import java.util.List;

import br.ita.airgroup.israel.epucknetwork.core.controller.BluetoothHandler;
import br.ita.airgroup.israel.epucknetwork.helper.Logger;

public abstract class AutonomousAlgorithm extends Algorithm {

	AutonomousAlgorithm(Integer id) {
		super(id);
	}

	@Override
	public void start(float... configs) {
		topology.ifPresent(t -> start(t.getBluetooths()));
		Logger.info(this.getClass(), "Algorithm has started...");
	}

	@Override
	public void stop() {
		topology.ifPresent(t -> t.getBluetooths().forEach(b -> b.stopAlgorithm()));

		if (running) {
			Logger.info(this.getClass(), "has stopped.");
		}

		running = false;
	}

	@Override
	public void start(List<BluetoothHandler> handlers) {

		if (!running) {
			Logger.info(this.getClass(), "Has started.");
		}

		running = true;
		handlers.forEach(h -> h.startAlgorithm(this));
		topology.ifPresent(t -> t.getBluetooths().forEach(b -> b.startAlgorithm(this)));
	}
	
}
