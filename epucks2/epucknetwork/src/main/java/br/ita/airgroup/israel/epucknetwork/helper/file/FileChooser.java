package br.ita.airgroup.israel.epucknetwork.helper.file;

import java.io.File;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.swing.JFileChooser;

import br.ita.airgroup.israel.commons.utils.FileUtils;
import br.ita.airgroup.israel.epucknetwork.gui.ApplicationGUI;
import br.ita.airgroup.israel.epucknetwork.helper.exception.EmptyException;

public abstract class FileChooser extends JFileChooser {

	public FileChooser(String title, Optional<String> initFolder) {
		setDialogTitle(title);
		config();
		this.setCurrentDirectory(new File(initFolder.orElse(FileUtils.getTempDir().getAbsolutePath())));
	}

	public FileChooser(String title, Path initFolder) {
		this(title, Optional.of(initFolder.toString()));
	}

	public FileChooser(String title) {
		this(title, Optional.empty());
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 4787570160196465486L;

	protected abstract void config();

	public List<File> showAndSelectMultiples() throws EmptyException {
		int op = showDialog(null, "Select");

		if (op == CANCEL_OPTION) {
			throw new EmptyException();
		}

		File[] files = null;
		if (isMultiSelectionEnabled()) {
			files = Optional.ofNullable(this.getSelectedFiles())
					.orElseThrow(ApplicationGUI.emptySelectionExceptionSupplier);
		} else {
			files = new File[] { Optional.ofNullable(this.getSelectedFile())
					.orElseThrow(ApplicationGUI.emptySelectionExceptionSupplier) };
		}

		return Arrays.asList(files);
	}

	public File showAndSelectSingle() throws EmptyException {
		return showAndSelectMultiples().stream().findFirst()
				.orElseThrow(ApplicationGUI.emptySelectionExceptionSupplier);
	}

}