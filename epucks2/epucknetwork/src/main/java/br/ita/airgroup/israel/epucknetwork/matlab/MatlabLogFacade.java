package br.ita.airgroup.israel.epucknetwork.matlab;

import java.nio.file.Path;

import com.mathworks.toolbox.javabuilder.MWException;

import br.ita.airgroup.israel.epucknetwork.helper.exception.SettingsException;
import br.ita.airgroup.israel.epucknetwork.matlab.helper.TopologyGain;

public class MatlabLogFacade extends MatlabFacade{

	private Path logFilepath;
	
	
	public MatlabLogFacade() throws MWException {
		
	}

	public MatlabLogFacade(Path logfilepath) throws MWException {
		setLogFilepath(logfilepath);
	}

	public void setLogFilepath(Path logfilepath) throws MWException {
		this.logFilepath = logfilepath;
	}

	public void logControl(int robotId, long time, int x, int y, float lambda, float[] dotxy, boolean failComm,
			TopologyGain gains) throws MWException, SettingsException {

		this.matlab.orElseThrow(() -> new SettingsException("logfile path not set")).logControl(logFilepath.toString(), robotId, time, x, y, lambda, dotxy, failComm ? 1 : 0,
				gains.toArray());

	}

	public void logControl(int robotId, long time, int x, int y) throws MWException, SettingsException {
		logControl(robotId, time, x, y, 0f, new float[] { 0f, 0f }, false, new TopologyGain(0f, 0f, 0f, 0f));
	}
}
