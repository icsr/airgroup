package br.ita.airgroup.israel.epucknetwork.core.imaging;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.ds.openimaj.OpenImajDriver;

import boofcv.app.CameraCalibration;
import boofcv.gui.image.ImagePanel;
import boofcv.io.webcamcapture.UtilWebcamCapture;
import br.ita.airgroup.israel.commons.thread.DaemonThread;
import br.ita.airgroup.israel.commons.utils.FileUtils;
import br.ita.airgroup.israel.commons.utils.SwingUtils;
import br.ita.airgroup.israel.epucknetwork.core.imaging.tracking.Trackable;
import br.ita.airgroup.israel.epucknetwork.gui.aux.DialogGUI;
import br.ita.airgroup.israel.epucknetwork.gui.aux.Exhibitable;
import br.ita.airgroup.israel.epucknetwork.helper.BaseSystem;
import br.ita.airgroup.israel.epucknetwork.helper.Logger;
import br.ita.airgroup.israel.epucknetwork.helper.Posture;
import br.ita.airgroup.israel.epucknetwork.helper.exception.SettingsException;
import br.ita.airgroup.israel.epucknetwork.helper.file.Properties;
import br.ita.airgroup.israel.epucknetwork.helper.file.Properties.Key;
import br.ita.airgroup.israel.epucknetwork.helper.message.Messenger;
import br.ita.airgroup.israel.epucknetwork.helper.observer.Observable;
import br.ita.airgroup.israel.epucknetwork.helper.security.AvoidUnauthorizedExit;
import br.ita.airgroup.israel.epucknetwork.helper.streaming.VideoConverter;

/**
 * Camera Handler by using BoofCV
 * 
 * @author israelicsr
 *
 */
public class Camera implements Observable<List<Posture>> {

	protected static Image logo = DialogGUI.logoITA.getImage();
	public static int logoWidth = DialogGUI.logoITA.getIconWidth();
	public static int logoHeight = DialogGUI.logoITA.getIconHeight();
	List<Posture> targetPosturesToDraw;
	private StringBuffer additionalInfo = new StringBuffer();
	float brightnessFactor = 1f + Properties.getAsFloat(Key.Brightness) / 100;
	int brightnessOffset = 15;

	private Webcam camera;

	static {
		Webcam.setDriver(new OpenImajDriver());
	}

	private ImagePanel cameraPanel = new ImagePanel();
	private boolean running = false;
	private List<Webcam> cameras;
	private Path squareGridImage;
	private VideoConverter videoConverter;
	private boolean recording = false;
	private boolean showEnhancedImage = Properties.getAsBoolean(Key.ShowEnhancedImage);

	private enum STATE {
		UNDEFINED, NOT_CALIBRATED, CALIBRATING, CALIBRATED
	};

	private STATE currentState = STATE.UNDEFINED;

	Optional<Trackable> trackable = Optional.empty();

	private List<Observable<?>> callback = new ArrayList<>();
	Optional<Arena> arena = Optional.empty();
	Point mouse;
	private CameraDrawings cameraUpdater;
	double scale;

	public Camera() throws SettingsException {
		this(Optional.empty());
	}

	public Camera(Optional<String> cameraName) throws SettingsException {
		this(cameraName, Optional.empty());
	}

	public Camera(Optional<String> cameraName, Optional<Observable<Void>> callback) throws SettingsException {

		callback.ifPresent(c -> this.callback.add(c));

		cameras = Webcam.getWebcams();

		if (!Optional.ofNullable(cameras).isPresent() || cameras.isEmpty()) {
			throw new SettingsException("No camera available");
		}

		if (cameraName.isPresent()) {
			setCamera(cameraName.get());
		} else {
			setCamera(Webcam.getWebcams().get(0));
		}

		prepareCalibration();
	}

	private void prepareCalibration() {
		squareGridImage = Paths.get(BaseSystem.CALIBRATION_HOME.getParent().toString());
		if (!squareGridImage.toFile().exists()) {
			FileUtils.copy(BaseSystem.squareGridPath.toFile(), squareGridImage, true);
		}

		squareGridImage = Paths.get(squareGridImage.toString(),
				FileUtils.getFilenameWithSuffix(BaseSystem.squareGridPath.toFile()));
	}

	/**
	 * Calibrates camera by using BoofCV
	 * 
	 * @see <a
	 *      href=https://boofcv.org/index.php?title=Tutorial_Camera_Calibration>BoofCV</a>
	 */
	public void calibrate() {

		if (!Messenger.answerIfIsTrue("Have you printed Square Grid 5x4 calibrating image?")) {
			try {
				FileUtils.openOnOperatingSystem(squareGridImage);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		if (!Messenger.answerIfIsTrue("Proceed camera calibration?")) {
			return;
		}

		DaemonThread.buildAndStart("CameraCalibratingThread", () -> {

			Logger.info(Camera.class, "Calibrating...");
			Logger.info(Camera.class, "Restart camera in the aftermath.");
			shutdown();
			AvoidUnauthorizedExit.activate();

			CameraCalibration
					.main(new String[] { FileUtils.getFilenameWithSuffix(getCalibrationParams().toFile()),
							String.format("--Directory=%s", getCalibrationParams().toFile().getParent()),
							String.format("--Camera=%d", findIndex()),
							String.format("--Resolution=%d:%d", (int) camera.getDevice().getResolution().getWidth(),
									(int) camera.getDevice().getResolution().getHeight()),
							"SQUAREGRID", "--Grid=5:4" });

		});

	}

	public List<Webcam> getCameras() {
		return cameras;
	}

	private int findIndex() {

		for (int i = 0; i < this.cameras.size(); i++) {
			if (this.cameras.get(i).getName().equals(this.camera.getName())) {
				return i;
			}
		}

		return 0;
	}

	public Dimension[] getAvailableResolutions() {
		return Stream
				.concat(Arrays.asList(this.camera.getDevice().getResolutions()).stream(), Stream.of(SwingUtils.fullHD))
				.toArray(Dimension[]::new);
	}

	public List<Dimension> getAvailableResolutionsAsList() {
		return Arrays.asList(getAvailableResolutions());
	}

	public Dimension getResolution() {
		return this.camera.getDevice().getResolution();
	}

	public void setResolution(Dimension resolution) {
		shutdown();
		adjustResolution(resolution);
		init();
	}

	private void adjustResolution(Dimension resolution) {
		UtilWebcamCapture.adjustResolution(this.camera, resolution.width, resolution.height);
	}

	public void setCamera(String name) {
		Optional<Webcam> findFirst = this.cameras.stream().filter(c -> c.getName().contains(name)).findFirst();

		setCamera(findFirst.orElse(this.cameras.get(0)));

		Properties.add(Key.CameraName, name);
	}

	private void setCamera(Webcam webcam) {

		if (Optional.ofNullable(camera).isPresent()) {
			shutdown();
			running = false;
		}

		this.camera = webcam;

		Dimension[] availableResolutions = getAvailableResolutions();
		this.camera.setCustomViewSizes(availableResolutions);
		Dimension dimension = SwingUtils.max(availableResolutions);
		this.camera.setViewSize(dimension);
		camera.getDevice().setResolution(dimension);

		this.cameraPanel.setPreferredSize(dimension);
		this.cameraPanel.setSize(dimension);

		init();

	}

	public void init() {

		if (running) {
			shutdown();
		}

		try {
			this.camera.open();
		} catch (Exception e1) {
			e1.printStackTrace();
			BaseSystem.shutdown();
		}

		this.mouse = new Point(0, 0);

		this.cameraPanel.setDoubleBuffered(true);

		this.cameraPanel.addMouseMotionListener(new MouseAdapter() {

			public void mouseMoved(MouseEvent e) {
				mouse.x = e.getX();
				mouse.y = e.getY();
			}

		});

		this.callback.forEach(o -> o.notify(null));
		trackable.ifPresent(Trackable::configureDetectors);

		moveCalibratingParamsIfExists();
		AvoidUnauthorizedExit.deactivate();
		checkIfCalibrated();

		startCameraUpdateThread();
	}

	public void setTracker(Trackable tracker) {
		this.trackable = Optional.of(tracker);
	}

	private void moveCalibratingParamsIfExists() {
		if (BaseSystem.CALIBRATION_DATA_FILE.toFile().exists()) {
			FileUtils.move(BaseSystem.CALIBRATION_DATA_FILE.toFile(), this.getCalibrationParams().toFile(), true);
		}
	}

	private void startCameraUpdateThread() {
		running = true;
		Logger.info(Camera.class, "Camera has initted.");

		cameraUpdater = new CameraDrawings(this);
		DaemonThread.buildAndStart("CameraUpdateThread", cameraUpdater);
	}

	public Point getSoutheastCorner() {
		return new Point(arena.isPresent() ? arena.get().getLeftPx() + Exhibitable.PADDING : Exhibitable.PADDING,
				arena.isPresent() ? arena.get().getDiagonalPx().y - Exhibitable.PADDING
						: cameraPanel.getHeight() - Exhibitable.PADDING);
	}

	public Point getSouthwestCorner() {
		return new Point(arena.isPresent() ? arena.get().getDiagonalPx().x - Exhibitable.PADDING : Exhibitable.PADDING,
				arena.isPresent() ? arena.get().getDiagonalPx().y - Exhibitable.PADDING
						: cameraPanel.getHeight() - Exhibitable.PADDING);
	}

	public ImagePanel getPanel() {
		return this.cameraPanel;
	}

	public void shutdown() {
		if (!running) {
			return;
		}
		running = false;

		if (recording) {
			stopRecording();
		}

		this.camera.close();
	}

	@Override
	protected void finalize() throws Throwable {
		shutdown();
		super.finalize();
	}

	public void startRecording(Path recordingFolder) {

		this.cameraUpdater.resetTime();
		videoConverter = new VideoConverter(recordingFolder, this.cameraPanel.getWidth(), this.cameraPanel.getHeight());

		recording = true;
		videoConverter.startRecording();

	}

	public void stopRecording() {

		if (!recording) {
			return;
		}

		recording = false;
		videoConverter.stopRecording();
	}

	public void stopRecording(String label) {

		if (!recording) {
			return;
		}

		recording = false;
		videoConverter.stopRecording(label);
	}

	public Path getCalibrationParams() {
		return Paths.get(BaseSystem.CALIBRATION_HOME.toString(),
				String.format("%s%s", hashCode(), FileUtils.YAML_SUFFIX));
	}

	public String getName() {
		return this.camera.getName();
	}

	@Override
	public int hashCode() {
		return getName().hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		return this.hashCode() == obj.hashCode();
	}

	public boolean checkIfCalibrated() {
		if (getCalibrationParams().toFile().exists() && getCalibrationParams().toFile().isFile()) {
			currentState = STATE.CALIBRATED;
		} else {
			currentState = STATE.NOT_CALIBRATED;
		}
		return isCalibrated();
	}

	public boolean isCalibrated() {
		return currentState == STATE.CALIBRATED;
	}

	public void setArena(Arena arena) {
		this.arena = Optional.of(arena);
	}

	public boolean isTracked() {
		return trackable.isPresent();
	}

	public boolean isShowEnhancedImage() {
		return showEnhancedImage;
	}

	public void setShowEnhancedImage(boolean ShowEnhancedImage) {
		this.showEnhancedImage = ShowEnhancedImage;
		Properties.add(Key.ShowEnhancedImage, ShowEnhancedImage);
	}

	@Override
	public void notify(List<Posture> targetPosturesToDraw) {
		this.targetPosturesToDraw = targetPosturesToDraw;
	}

	boolean hasTargetPostures() {
		return Optional.ofNullable(targetPosturesToDraw).isPresent() && !targetPosturesToDraw.isEmpty();
	}

	public synchronized void setAdditionalInfo(String info) {
		clearAdditionalInfo();
		this.additionalInfo.append(info);
	}

	public synchronized void clearAdditionalInfo() {
		this.additionalInfo.setLength(0);
	}

	public boolean isRunning() {
		return running;
	}

	public boolean isRecording() {
		return recording;
	}

	public void setBrightness(float factor) {
		Optional.of(factor).filter(f -> f > 0 && f < 2).orElseThrow(() -> new NumberFormatException());
		this.brightnessFactor = factor;
	}

	public BufferedImage getImage() {
		return camera.getImage();
	}

	public void setImage(BufferedImage image) {
		cameraPanel.setImage(image);
		cameraPanel.repaint();
	}

	public void appendVideoFrame(BufferedImage frame) {
		videoConverter.appendImage(frame);
	}

}
