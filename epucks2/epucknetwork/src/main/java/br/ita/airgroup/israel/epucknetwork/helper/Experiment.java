package br.ita.airgroup.israel.epucknetwork.helper;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;

import br.ita.airgroup.israel.commons.clock.Clock;
import br.ita.airgroup.israel.commons.thread.DaemonThread;
import br.ita.airgroup.israel.commons.utils.StringUtils;
import br.ita.airgroup.israel.epucknetwork.core.controller.Epuck;
import br.ita.airgroup.israel.epucknetwork.core.controller.Topology;
import br.ita.airgroup.israel.epucknetwork.core.imaging.Camera;
import br.ita.airgroup.israel.epucknetwork.helper.exception.EmptyException;
import br.ita.airgroup.israel.epucknetwork.helper.exception.SettingsException;
import br.ita.airgroup.israel.epucknetwork.helper.message.Messenger;
import br.ita.airgroup.israel.epucknetwork.helper.observer.Observable;
import br.ita.airgroup.israel.epucknetwork.helper.observer.Observer;
import br.ita.airgroup.israel.epucknetwork.matlab.helper.TopologyGain;

public class Experiment implements Observer<Void> {

	private static final String AUTO_EXPERIMENTER_LABEL = "AUTO";
	private TopologyGain gain;
	private Path savingFolder;
	Topology topology;
	private String groupName;
	private String subgroupName;
	private long timeToRun;
	private ExperimentRecorder experimentRecorder;
	private LocalDateTime startTime;
	private boolean running = false;
	private Path initialPostures;
	private Optional<Observable<Void>> observable = Optional.empty();

	enum STATUS {
		NOT_RUNNED("Still not runned"), OK("ok"), FAIL("fail"), ANALYZE("analyze"), INTERRUPTED(
				"interrupted"), STOPPING("stopping");

		private String label;

		private STATUS(String label) {
			this.label = label;
		}

		@Override
		public String toString() {
			return label;
		}
	};

	STATUS status = STATUS.NOT_RUNNED;
	private int remainingExperiments;

	public Experiment(String groupName, TopologyGain gain, Topology topology, long timeToRunInSecs, Camera camera) {
		this.groupName = groupName;
		this.gain = gain;
		this.topology = topology;
		this.timeToRun = timeToRunInSecs;
		this.experimentRecorder = new ExperimentRecorder(this, camera);

		setSavingFolder();
	}

	public void start(int qtyOfExperiments) {
		Logger.info(Experiment.class, "Has Started...");
		if (running) {
			stopWatch();
		}

		this.remainingExperiments = qtyOfExperiments;
		status = STATUS.OK;

		DaemonThread.buildAndStart("ExperimentStarter", () -> {

			try {

				initialPostures = topology.selectTopologyCSV();

				for (int i = 0; i < qtyOfExperiments && remainingExperiments > 0 && status != STATUS.INTERRUPTED; i++) {

					while (status != STATUS.OK) {
						if (status == STATUS.INTERRUPTED) {
							return;
						}
						Thread.sleep(Topology.ONE_SECOND_IN_MILS);
					}

					Logger.info(Experiment.class,
							String.format("%d out of %d has started...", i + 1, qtyOfExperiments));

					startMoveToInitialPosition();

					Messenger.start();

					startRecordingOnly();

					Thread.sleep(Topology.ONE_SECOND_IN_MILS);

					startTopologyOnly();

					startWatchExperimentExpiration();

					while (running && status != STATUS.INTERRUPTED) {
						Thread.sleep(Topology.TEN_SECONDS_IN_MILS);
					}

				}

				if (status != STATUS.INTERRUPTED) {
					Messenger.beep();
					observable.ifPresent(o -> o.notify(null));
				}

			} catch (EmptyException | InterruptedException e) {
				e.printStackTrace();
				Logger.info(Experiment.class, "has been cancelled.");
				observable.ifPresent(o -> o.notify(null));
			} catch (IOException | SettingsException e) {
				e.printStackTrace();
			}

		});
	}

	private synchronized void startMoveToInitialPosition()
			throws IOException, SettingsException, EmptyException, InterruptedException {

		running = true;
		status = STATUS.NOT_RUNNED;
		startTime = LocalDateTime.now();
		topology.loadTopologyReset();

		Logger.info(Topology.class, "Moving Controller has started...");

		boolean wellPlacedEpucks = true;

		Clock clock = new Clock("Moving Controller Clock");
		clock.start();
		int counter = 0;

		do {
			wellPlacedEpucks = topology.loadTopology(initialPostures);

			if (!wellPlacedEpucks) {
				List<Epuck> notWellPlacedEpucks = topology.getNotWellPlacedEpucks();
				Logger.info(Experiment.class, String.format("Not well placed these %d epucks: %s",
						notWellPlacedEpucks.size(), StringUtils.join(notWellPlacedEpucks)));
			}

			Thread.sleep(Topology.TEN_SECONDS_IN_MILS);

			while (status != STATUS.INTERRUPTED && !hasExpired() && topology.hasAtLeastOneEpuckRecentlyMoving()) {
				System.out.println(String.format("Waiting %s to move, counting #%d",
						StringUtils.join(topology.hasRecentlyMoved()), ++counter));

				if (hasExpired(Topology.FIVE_MINUTES_IN_SEC)) {
					Messenger.alert();
				}

				Thread.sleep(Topology.ONE_SECOND_IN_MILS);

			}

			Clock.tic();
			while (status != STATUS.INTERRUPTED && !hasExpired() && topology.hasUntrackedEpucks() && wellPlacedEpucks) {
				topology.seekTracking(false);
				Thread.sleep(Topology.ONE_SECOND_IN_MILS);
			}
			Logger.info(Topology.class, String.format("Tracking Seeker has runned in %d ms.", Clock.toc()));

		} while (status != STATUS.INTERRUPTED && !hasExpired() && !wellPlacedEpucks);

		Logger.info(Topology.class, String.format("Moving Controller has ended in %d ms.", clock.stop()));

	}

	private synchronized void startWatchExperimentExpiration() {
		
		if(status == STATUS.INTERRUPTED) {
			return;
		}
		
		Logger.info(Experiment.class, "Setting Experiment Expiration Timer...");

		startTime = LocalDateTime.now();
		running = true;

		Runnable controller = () -> {
			while (!hasExpired() && running && status != STATUS.INTERRUPTED) {
				try {
					Thread.sleep(Topology.FIVE_SECONDS_IN_MILS);
					checkMobility();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			stopIt();
		};

		DaemonThread.buildAndStart("SmartRunController", controller);
	}

	private synchronized void checkMobility() {

		topology.wakeSleepyEpucks(true);

		if (topology.hasAllEpucksMoved()) {
			status = STATUS.OK;
		} else {
			status = STATUS.ANALYZE;
		}
	}

	private synchronized void stopWatch() {
		Logger.info(Experiment.class, "Stopping Expiring Timer...");
		running = false;
	}

	public synchronized void startRecordingOnly() {

		if (status == STATUS.INTERRUPTED) {
			return;
		}

		Logger.info(Experiment.class, "Starting Recorder...");
		try {
			experimentRecorder.start();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public synchronized void startTopologyOnly() {

		if (status == STATUS.INTERRUPTED) {
			return;
		}

		Logger.info(Experiment.class, "Starting Topology Controller...");

		topology.start();

	}

	public synchronized void stopRecordingOnly() {
		Logger.info(Experiment.class, "Stopping Recorder...");
		experimentRecorder.stop(AUTO_EXPERIMENTER_LABEL);
	}

	public synchronized void stopTopologyOnly() {

		DaemonThread.buildAndStart("TopologyStopper", () -> {
			topology.stop();

			try {
				Thread.sleep((int) (topology.getEpucks().size() * 1.2 * Topology.ONE_SECOND_IN_MILS));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			try {
				Thread.sleep((int) (topology.getEpucks().size() * 1.2 * Topology.ONE_SECOND_IN_MILS));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		});

	}

	private synchronized void stopIt() {
		Logger.info(Experiment.class, status == STATUS.INTERRUPTED ? "interrupting..." : "stopping...");
		observable.filter(o -> status != STATUS.INTERRUPTED && this.remainingExperiments <= 0)
				.ifPresent(o -> o.notify(null));
		status = STATUS.STOPPING;

		DaemonThread.buildAndStart("ExperimentStopper", () -> {

			stopWatch();
			stopTopologyOnly();
			stopRecordingOnly();

			try {
				Thread.sleep(Topology.FIVE_SECONDS_IN_MILS);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}

			IntStream.range(0, 5).filter(i -> topology.hasAtLeastOneEpuckRecentlyMoving()).forEach(i -> {
				topology.hasRecentlyMoved().forEach(e -> {
					Logger.info(e, String.format("Force algorithm to stop attemptive #%d...", i + 1));
					topology.getBluetooth(e.getId()).stopAlgorithm();
				});
			});

			topology.disconnectAll();

			try {
				Thread.sleep(Topology.FIVE_SECONDS_IN_MILS);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}

			topology.reconnectAll();

			try {
				Thread.sleep(Topology.FIVE_SECONDS_IN_MILS * topology.getEpucks().size());
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}

			Logger.info(Experiment.class, "has stopped.");

			status = STATUS.OK;
		});
	}

	public void stop() {
		status = STATUS.INTERRUPTED;
		remainingExperiments = 0;
		stopIt();
	}

	@Override
	public synchronized void observe(Observable<Void> observable) {
		this.observable = Optional.of(observable);
	}

	public boolean isRunning() {
		return running;
	}

	private void setSavingFolder() {
		subgroupName = String.format("%d%s", topology.getEpucks().size(), gain.getLabel());
		savingFolder = Paths.get(BaseSystem.RECORDING_HOME.toString(), groupName, subgroupName);
	}

	public Path getSavingFolder() {

		if (!savingFolder.toFile().exists()) {
			savingFolder.toFile().mkdirs();
		}

		return savingFolder;
	}

	public String getGroupName() {
		return groupName;
	}

	public String getSubgroupName() {
		return subgroupName;
	}

	public long getTimeToRun() {
		return timeToRun;
	}

	public synchronized boolean hasExpired() {
		return hasExpired(getTimeToRun());
	}

	private synchronized boolean hasExpired(long intervalInSec) {
		return LocalDateTime.now().isAfter(startTime.plusSeconds(intervalInSec));
	}

}
