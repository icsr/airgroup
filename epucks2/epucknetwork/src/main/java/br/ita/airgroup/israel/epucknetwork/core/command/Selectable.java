package br.ita.airgroup.israel.epucknetwork.core.command;

public interface Selectable {
	
	boolean isReceivable();

	char getSelectorChar();

	String getDescription();

	String toString();

	void setSelected(boolean selected);

	boolean isSelected();

	int getNumberOfParams();

	void setNumberOfParams(int numberOfParams);

}
