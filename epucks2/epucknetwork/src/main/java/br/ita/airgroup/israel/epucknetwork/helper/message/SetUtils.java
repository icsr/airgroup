
package br.ita.airgroup.israel.epucknetwork.helper.message;

import java.awt.Point;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections4.SetUtils.SetView;

import br.ita.airgroup.israel.epucknetwork.core.controller.BluetoothHandler;

public class SetUtils extends org.apache.commons.collections.SetUtils {

	public static boolean belongsTo(Object item, Collection<?> collection) {
		return collection.contains(item);
	}

	public static boolean belongsTo(Object item, Object... collection) {
		return Arrays.asList(collection).contains(item);
	}

	public static Set<BluetoothHandler> toSet(List<BluetoothHandler> bluetooths) {
		return bluetooths.stream().collect(Collectors.toSet());
	}

	public static <E> Set<E> difference(Set<E> setA, Set<E> setB) {
		return org.apache.commons.collections4.SetUtils.difference(setA, setB);
	}

	public static <E> Set<E> remove(Set<E> set, E objectToRemove) {
		return set.stream().filter(s -> !s.equals(objectToRemove)).collect(Collectors.toSet());
	}

}
