package br.ita.airgroup.israel.epucknetwork.gui.components;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;

import br.ita.airgroup.israel.commons.utils.Font;
import br.ita.airgroup.israel.epucknetwork.core.command.EpuckCommand;
import br.ita.airgroup.israel.epucknetwork.gui.aux.Exhibitable;
import br.ita.airgroup.israel.epucknetwork.helper.Logger;
import br.ita.airgroup.israel.epucknetwork.helper.exception.NotValidCommandException;
import br.ita.airgroup.israel.epucknetwork.helper.file.Properties;
import br.ita.airgroup.israel.epucknetwork.helper.file.Properties.Key;

public class DirectCommandCombobox extends JComboBox<String> {

	private static final long serialVersionUID = -1000604716018285290L;
	private ActionListener processCommand;

	public DirectCommandCombobox(ActionListener processCommand) {
		super(new DefaultComboBoxModel<>(Properties.getLinesAsArray(Key.LAST_COMMANDS)));

		this.processCommand = processCommand;
		
		setEditable(true);
		setFont(Font.normal(false));

		addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				setBorder(Exhibitable.selectedBorder);
				super.focusGained(e);
			}

			@Override
			public void focusLost(FocusEvent e) {
				setBorder(Exhibitable.unselectedBorder);
				super.focusLost(e);
			}
		});

		grabFocus();
		requestFocusInWindow();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		try {
			saveLastCommands();
			processCommand.actionPerformed(e);
		} catch (NotValidCommandException e1) {
			Logger.info(this.getClass(), e1.getMessage());
		}
	}

	private void saveLastCommands() throws NotValidCommandException {
		String command = String.class.cast(this.getSelectedItem());

		if (!EpuckCommand.isValidCommand(command)) {
			throw new NotValidCommandException("Not a valid Command");
		}

		LinkedList<String> lines = Properties.getLines(Key.LAST_COMMANDS);
		lines.addFirst(command);
		
		List<String> uniqueness = lines.parallelStream().distinct().collect(Collectors.toList());

		removeAllItems();
		uniqueness.forEach(l -> addItem(l));
		
		Properties.addLines(Key.LAST_COMMANDS, uniqueness);
	}
}
