package br.ita.airgroup.israel.epucknetwork.helper.observer;

import java.awt.GridLayout;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.border.Border;

import br.ita.airgroup.israel.epucknetwork.core.controller.Epuck;

public class ObservableLabel extends JMenuBar implements Observable<Set<Epuck>> {

	private static final long serialVersionUID = -4290911212967826365L;
	private JMenu menu;
	private JPanel panel;

	public ObservableLabel() {
	}

	public ObservableLabel(String text) {
		menu = new JMenu(text);
		panel = new JPanel(new GridLayout(0, 2, 5, 5));
		menu.add(panel);
		this.add(menu);
		panel.add(new JLabel("No epucks unactive"));
		panel.setDoubleBuffered(true);
		panel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

	}

	public ObservableLabel(String text, Border border) {
		this(text);
		this.setBorder(border);
	}

	public void setQtyOfEpucks() {
		this.setDoubleBuffered(true);
	}

	@Override
	public void notify(Set<Epuck> unactiveEpucks) {
		menu.setText(unactiveEpucks.isEmpty() ? "Fail: none"
				: String.format("Fail: %d", unactiveEpucks.size()));

		panel.removeAll();

		if (unactiveEpucks.isEmpty()) {
			panel.add(new JLabel("No epucks unactive"));
		} else {
			unactiveEpucks.forEach(e -> panel.add(new ObservableLabel(e.toString(), BorderFactory.createLoweredBevelBorder())));
		}

	}

}
