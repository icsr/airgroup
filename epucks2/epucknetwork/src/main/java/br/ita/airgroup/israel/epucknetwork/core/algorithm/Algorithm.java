package br.ita.airgroup.israel.epucknetwork.core.algorithm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import br.ita.airgroup.israel.epucknetwork.core.controller.BluetoothHandler;
import br.ita.airgroup.israel.epucknetwork.core.controller.Topology;
import br.ita.airgroup.israel.epucknetwork.helper.annotation.AlgorithmId;
import br.ita.airgroup.israel.epucknetwork.helper.annotation.AlgorithmStart;
import br.ita.airgroup.israel.epucknetwork.helper.annotation.AlgorithmStop;
import br.ita.airgroup.israel.epucknetwork.helper.annotation.AlgorithmTopology;

public abstract class Algorithm {

	Optional<Topology> topology = Optional.empty();

	private int id;

	protected boolean running;

	private static List<Algorithm> algorithms = new ArrayList<>();

	public static void plug(Algorithm newAlgorithm) {
		algorithms.add(newAlgorithm);
	}

	public static List<Algorithm> getAlgorithms() {
		return algorithms.stream()
				.sorted((a, b) -> a.getClass().getSimpleName().compareToIgnoreCase(b.getClass().getSimpleName()))
				.collect(Collectors.toList());
	}

	Algorithm(int id) {
		this.id = id;
	}

	@AlgorithmTopology
	public Algorithm setTopology(Topology topology) {
		this.topology = Optional.of(topology);

		return this;
	}

	@AlgorithmStart
	public abstract void start(float... configs);

	@AlgorithmStop
	public abstract void stop();
	
	public abstract void start(List<BluetoothHandler> handlers);

	@AlgorithmId
	public int getId() {
		return id;
	}

	public static void plug(Algorithm... algorithms) {
		Arrays.stream(algorithms).forEach(a -> plug(a));
	}
	
	@Override
	public String toString() {
		return this.getClass().getSimpleName();
	}

	public boolean isRunning() {
		return running;
	}

}
