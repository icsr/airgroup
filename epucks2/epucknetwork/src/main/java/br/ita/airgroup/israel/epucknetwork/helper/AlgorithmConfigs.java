package br.ita.airgroup.israel.epucknetwork.helper;

import br.ita.airgroup.israel.epucknetwork.core.command.PutSelector;
import br.ita.airgroup.israel.epucknetwork.helper.file.Properties;
import br.ita.airgroup.israel.epucknetwork.helper.file.Properties.Key;

public class AlgorithmConfigs {
	
	public static float getThreshold() {
		return Properties.getAsFloat(PutSelector.ALGORITHM_CONFIGS, 0);
	}
	
	public static float getEpsilon() {
		return Properties.getAsFloat(PutSelector.ALGORITHM_CONFIGS, 2);
	}

	public static float getMaxMovingDistance() {
		return Properties.getAsFloat(PutSelector.ALGORITHM_CONFIGS, 3);
	}

	public static float getSafetyRange() {
		return getRange() * (1f - Properties.getAsFloat(Key.RangeSafety) / 100f);
	}

	public static float getRange() {
		return Properties.getFirstAsFloat(PutSelector.RANGE);
	}

	public static boolean isNormalizeEachControl() {
		return Properties.getAsBoolean(Key.NormalizeControl);
	}

	public static boolean isSafeControl() {
		return Properties.getAsBoolean(Key.safeControl);
	}

}
