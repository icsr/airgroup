package br.ita.airgroup.israel.epucknetwork.helper.exception;

public class NotValidCommandException extends Exception {

	public NotValidCommandException(String message) {
		super(message);
	}

	private static final long serialVersionUID = -7825667314705141838L;

}
