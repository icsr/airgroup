package br.ita.airgroup.israel.epucknetwork.helper.exception;

public class SettingsException extends Exception {

	public SettingsException(String message) {
		super(message);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -9095993233526329744L;

}
