package br.ita.airgroup.israel.epucknetwork.matlab.helper;

import br.ita.airgroup.israel.commons.coord.PairOfDoubles;
import br.ita.airgroup.israel.commons.utils.MathUtils;

public class UControl extends PairOfDoubles {

	public UControl(double x, double y) {
		super(x, y);
	}

	protected UControl() {

	}

	public boolean isNull() {
		return MathUtils.equals(x, 0, MAX_DIFFERENCE) && MathUtils.equals(y, 0, MAX_DIFFERENCE);
	}

}
