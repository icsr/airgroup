package br.ita.airgroup.israel.epucknetwork.helper.tracking;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.List;
import java.util.Optional;

import javax.swing.JPanel;

import br.ita.airgroup.israel.epucknetwork.core.controller.Topology;
import br.ita.airgroup.israel.epucknetwork.core.imaging.Arena;
import br.ita.airgroup.israel.epucknetwork.core.imaging.EdgeLengthAdjustFactor;
import br.ita.airgroup.israel.epucknetwork.core.imaging.tracking.DraggedEpuck;
import br.ita.airgroup.israel.epucknetwork.helper.observer.Observable;

public class EpuckDragger {

	private Optional<DraggedEpuck> selectedEpuck = Optional.empty();
	private Topology topology;
	private Arena arena;
	private EdgeLengthAdjustFactor adjustFactor;
	private List<Observable<?>> observables;
	Boolean justAngle = false;
	protected boolean multipleSelection;

	public EpuckDragger(JPanel cameraPanel, Arena arena, Topology topology, EdgeLengthAdjustFactor adjustFactor,
			List<Observable<?>> observables) {

		this.adjustFactor = adjustFactor;
		this.arena = arena;
		this.topology = topology;
		addTrackingSelectionListener(cameraPanel);
		this.observables = observables;

	}

	private void addTrackingSelectionListener(JPanel panel) {

		addMouseButtonListener(panel);

		addKeyListener(panel);

		addMouseMotionListener(panel);

	}

	private void addMouseMotionListener(JPanel panel) {
		panel.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent me) {
				drawDraggingEpuckMoveTargetCoord(me);
			}
		});
	}

	private void addKeyListener(JPanel panel) {
		panel.addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_CONTROL) {
					multipleSelection = true;
				}

				if (e.getKeyCode() == KeyEvent.VK_SHIFT) {
					justAngle = true;
				}

			}

			@Override
			public void keyReleased(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_CONTROL) {
					multipleSelection = false;
				}
				if (e.getKeyCode() == KeyEvent.VK_SHIFT) {
					justAngle = false;
				}
			}
		});

	}

	private void addMouseButtonListener(JPanel panel) {
		panel.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseEntered(MouseEvent e) {
				panel.grabFocus();
				panel.requestFocusInWindow();
				panel.requestFocus();
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				panel.transferFocus();
			}
			

			@Override
			public void mouseClicked(MouseEvent me) {
				selectOrUnselectAClickedEpuck(me, adjustFactor.consolidateFactorInCmPerPx());
			}

			@Override
			public void mousePressed(MouseEvent me) {
				startEpuckDragging(me, adjustFactor.consolidateFactorInCmPerPx());
			}

			@Override
			public void mouseReleased(MouseEvent me) {
				stopEpuckDragging();
			}

		});
	}

	private void selectOrUnselectAClickedEpuck(MouseEvent me, double adjustFactorCmPerPx) {
		selectedEpuck = TrackerUtils.findEpuck(arena, topology, adjustFactorCmPerPx, me);

		selectedEpuck.ifPresent(e -> {
			if (!multipleSelection) {
				unselectAllBut(e);
			}

			e.setSelected(!e.isSelected());
		});

		updateDirectCommandPanel();
	}

	private void startEpuckDragging(MouseEvent me, double adjustFactorCmPerPx) {
		selectedEpuck = TrackerUtils.findEpuck(arena, topology, adjustFactorCmPerPx, me);

		selectedEpuck.ifPresent(e -> {
			e.setDragging(true);
		});
	}

	private void stopEpuckDragging() {
		if (!selectedEpuck.isPresent() || !selectedEpuck.get().isDragging()) {
			return;
		}

		selectedEpuck.get().getTargetPoint(true).ifPresent(d -> {
			if (justAngle) {
				topology.getBluetooth(selectedEpuck.get().getId()).sendAngleCommand(d.x, d.y);
			} else {
				topology.getBluetooth(selectedEpuck.get().getId()).sendMoveCommand(d.x, d.y);
			}
		});

		justAngle = false;
		selectedEpuck.get().setDragging(false);
		selectedEpuck = Optional.empty();
	}

	private void drawDraggingEpuckMoveTargetCoord(MouseEvent me) {
		if (!selectedEpuck.isPresent() || !selectedEpuck.get().isDragging()) {
			return;
		}

		selectedEpuck.ifPresent(s -> {
			s.setTargetPointPx(Optional.of(me.getPoint()));
		});
	}

	private void updateDirectCommandPanel() {
		observables.forEach(o -> o.notify(null));
	}

	private void unselectAllBut(DraggedEpuck e) {
		topology.getEpucks().stream().filter(e2 -> e2.getId() != e.getId()).forEach(e2 -> e2.setSelected(false));
	}

	public Optional<DraggedEpuck> getSelectedEpuck() {
		return selectedEpuck;
	}

}
