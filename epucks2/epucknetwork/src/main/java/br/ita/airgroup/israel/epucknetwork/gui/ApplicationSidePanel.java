package br.ita.airgroup.israel.epucknetwork.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Optional;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import br.ita.airgroup.israel.commons.utils.Font;
import br.ita.airgroup.israel.commons.utils.StringUtils;
import br.ita.airgroup.israel.epucknetwork.gui.aux.ChooseEpucksToPerformAction.MassiveEpuckAction;
import br.ita.airgroup.israel.epucknetwork.gui.components.GuiGroup;
import br.ita.airgroup.israel.epucknetwork.helper.Experiment;
import br.ita.airgroup.israel.epucknetwork.helper.Logger;
import br.ita.airgroup.israel.epucknetwork.helper.file.Properties;
import br.ita.airgroup.israel.epucknetwork.helper.file.Properties.Key;
import br.ita.airgroup.israel.epucknetwork.helper.message.Messenger;
import br.ita.airgroup.israel.epucknetwork.helper.observer.Observable;
import br.ita.airgroup.israel.epucknetwork.helper.observer.ObservableTextArea;
import br.ita.airgroup.israel.epucknetwork.helper.observer.ObservableToggleButton;

public class ApplicationSidePanel extends JPanel implements Observable<Void> {

	private static final long serialVersionUID = -8058458805090514762L;
	private Dimension reference;
	private ApplicationGUI app;

	protected ObservableTextArea logPane;
	protected ObservableToggleButton scrollLogButton;
	protected DirectCommandPanel directCommandPanel = new DirectCommandPanel();
	protected JToggleButton runTopologyButton;
	private Optional<Experiment> experiment = Optional.empty();
	private JToggleButton smartRun;

	protected ApplicationSidePanel(Dimension reference, ApplicationGUI app) {
		this.reference = reference;
		this.app = app;
		init();
	}

	private void init() {

		directCommandPanel.init();

		JPanel commandPanel = initCommandCameraAndTopologyPanel();
		JScrollPane scrollableLogPane = initLogPanel();
		JPanel cameraAndLogPanel = initLogButtonsPanel();

		setLayout(new BorderLayout());
		this.setPreferredSize(new Dimension(400, reference.height));
		this.add(commandPanel, BorderLayout.NORTH);
		this.add(scrollableLogPane, BorderLayout.CENTER);
		this.add(cameraAndLogPanel, BorderLayout.SOUTH);
	}

	private JScrollPane initLogPanel() {
		logPane = new ObservableTextArea();

		logPane.setText(StringUtils.join(Logger.asString(), System.lineSeparator()));
		logPane.setEditable(false);
		logPane.setAutoscrolls(false);
		logPane.setFont(Font.small(false));
		logPane.setBackground(app.getBackground());
		logPane.setAutoscrolls(true);
		logPane.setDoubleBuffered(true);

		JScrollPane scrollableLogPane = new JScrollPane(logPane);
		scrollableLogPane.setBorder(new TitledBorder(new EtchedBorder(), "Log"));
		scrollableLogPane.setDoubleBuffered(true);
		return scrollableLogPane;
	}

	private JPanel initRecordAndTopologyRunningPanel() {
		JPanel mainPanel = new JPanel();
		mainPanel.setBorder(BorderFactory.createTitledBorder("Topology"));

		JToggleButton recordToggleButton = new JToggleButton("Record");
		recordToggleButton.addActionListener(a -> recordCameraToFile(a));
		mainPanel.add(recordToggleButton);

		runTopologyButton = new JToggleButton("Run Topology");
		runTopologyButton.setMnemonic('S');
		runTopologyButton.addActionListener(a -> runTopology(a));
		runTopologyButton.setEnabled(true);

		smartRun = new JToggleButton("Run Experiment");
		smartRun.setMnemonic('R');
		smartRun.addActionListener(a -> {
			runExperiment(Properties.getAsInteger(Key.ExperimentRepetitions));
		});

		mainPanel.add(runTopologyButton);

		mainPanel.add(smartRun);

		return mainPanel;
	}

	void runExperiment(int numberOfRepetitions) {

		if (!smartRun.isSelected()) {
			
			if(!Messenger.answerIfIsTrue("Abort experiment?")) {
				smartRun.setSelected(true);
				return;
			}
			
			app.unlockMenu();
			this.experiment.ifPresent(e -> e.stop());
			return;
		}

		initExperiment(true);

		this.experiment.ifPresent(e -> {
			app.lockMenu();
			e.observe(this);
			e.start(numberOfRepetitions);
		});

	}

	private void initExperiment(boolean newInstance) {
		this.experiment.filter(e -> e.isRunning()).ifPresent(e -> e.stop());

		if (newInstance || !this.experiment.isPresent()) {
			this.experiment = Optional.of(new Experiment(app.experimentGroupName.getValue(), app.topology.getGain(),
					app.topology, (long) (app.experimentTimeToRun.getValue() * 60), app.camera));
		}
	}

	protected JPanel initLogButtonsPanel() {

		JPanel logButtonsPanel = new JPanel(new FlowLayout());
		logButtonsPanel.setBorder(BorderFactory.createTitledBorder("LOG"));

		JButton clearButton = new JButton("Clear log");
		clearButton.setMnemonic('C');
		clearButton.addActionListener(a -> {
			if (Messenger.answerIfIsTrue("Clear all log?")) {
				logPane.clear();
			}
		});

		JButton saveLogButton = new JButton("Save Log");
		saveLogButton.setMnemonic('L');
		saveLogButton.addActionListener(a -> Logger.save());

		scrollLogButton = new ObservableToggleButton("Scroll");
		scrollLogButton.setMnemonic('o');
		scrollLogButton.setSelected(true);

		scrollLogButton.addActionListener(a -> {
			this.logPane.setScroll(scrollLogButton.isSelected());
		});

		JTextField filter = new JTextField(8);
		filter.setBackground(this.getBackground());
		filter.setBorder(BorderFactory.createTitledBorder("Filter"));
		filter.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent e) {
				String text = JTextField.class.cast(e.getSource()).getText();
				if (text.isEmpty()) {
					logPane.filterOff();
				} else {
					logPane.filterBy(text);
				}
			}
		});

		logButtonsPanel.add(clearButton);
		logButtonsPanel.add(saveLogButton);
		logButtonsPanel.add(scrollLogButton);
		logButtonsPanel.add(filter);

		return logButtonsPanel;
	}

	private JPanel initCommandCameraAndTopologyPanel() {
		JPanel mainPanel = new JPanel(new GridLayout(0, 1));

		mainPanel.add(initDiagnosticsPanel());
		mainPanel.add(initRecordAndTopologyRunningPanel());
		mainPanel.add(directCommandPanel);

		app.disableUntilEpucksTracked(mainPanel);

		return mainPanel;
	}

	private JPanel initDiagnosticsPanel() {

		JPanel mainPanel = new JPanel(new BorderLayout());

		JPanel diagnosticsPanel = new JPanel(new FlowLayout());
		diagnosticsPanel.setBorder(BorderFactory.createTitledBorder("Diagnostics"));

		JToggleButton ledButton = new JToggleButton("Light");
		ledButton.setToolTipText("Turn on LEDs to check if epucks are really connected.");
		ledButton.setMnemonic('h');
		ledButton.addActionListener(a -> directCommandPanel.performTopologyAction(app, MassiveEpuckAction.Led,
				JToggleButton.class.cast(a.getSource()).isSelected()));

		JButton prepareButton = new JButton("Prepare");
		prepareButton.setMnemonic('P');
		prepareButton.addActionListener(a -> directCommandPanel.performTopologyAction(app, MassiveEpuckAction.Prepare));

		GuiGroup connections = new GuiGroup("Conections");
		connections.add(ledButton);
		connections.add(prepareButton);

		GuiGroup commands = new GuiGroup("Commands");
		JButton turnToZero = new JButton("0\u00B0");
		turnToZero.setMnemonic('0');
		turnToZero.addActionListener(a -> directCommandPanel.performTopologyAction(app, MassiveEpuckAction.TurnToZero));

		JButton turnTo90 = new JButton("90\u00B0");
		turnTo90.setMnemonic('9');
		turnTo90.addActionListener(a -> directCommandPanel.performTopologyAction(app, MassiveEpuckAction.TurnTo90));

		commands.add(turnToZero);
		commands.add(turnTo90);

		diagnosticsPanel.add(connections);
		diagnosticsPanel.add(commands);

		mainPanel.add(diagnosticsPanel, BorderLayout.CENTER);

		app.disableUntilEpuckTracked(diagnosticsPanel, true);

		return mainPanel;
	}

	private void recordCameraToFile(ActionEvent actionEvent) {
		recordCameraToFile(JToggleButton.class.cast(actionEvent.getSource()).isSelected());
	}

	private void recordCameraToFile(boolean record) {
		initExperiment(false);

		if (record) {
			experiment.ifPresent(e -> e.startRecordingOnly());
		} else {
			experiment.ifPresent(e -> e.stopRecordingOnly());
		}
	}

	private void runTopology(ActionEvent actionEvent) {
		initExperiment(false);

		if (JToggleButton.class.cast(actionEvent.getSource()).isSelected()) {
			experiment.ifPresent(e -> e.startTopologyOnly());
		} else {
			experiment.ifPresent(e -> e.stopTopologyOnly());
		}
	}

	@Override
	public synchronized void notify(Void content) {
		app.unlockMenu();
		this.experiment = Optional.empty();
		smartRun.setSelected(false);
	}

}
