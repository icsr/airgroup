package br.ita.airgroup.israel.epucknetwork.helper;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import br.ita.airgroup.israel.commons.coord.Position;
import br.ita.airgroup.israel.commons.graph.Graph;
import br.ita.airgroup.israel.commons.utils.MathUtils;
import br.ita.airgroup.israel.commons.utils.StringUtils;
import br.ita.airgroup.israel.epucknetwork.core.imaging.Arena;
import br.ita.airgroup.israel.epucknetwork.gui.aux.Exhibitable;
import br.ita.airgroup.israel.epucknetwork.helper.message.Message;
import br.ita.airgroup.israel.epucknetwork.helper.tracking.TrackerUtils;

public class Posture implements Serializable {

	private static final long serialVersionUID = 3524896399043679330L;
	private static final int MAX_DIFFERENCE = 2;
	public static final String CSV_FORMAT = "^([0-9]+,){2}[0-9]+$";
	public int x;
	public int y;
	public int angle;

	public Posture() {

	}

	public Posture(int x, int y, int angle) {
		setPosture(x, y, angle);
	}

	public Posture(Posture posture) {
		setPosture(posture.x, posture.y, posture.angle);
	}

	public List<Integer> asList() {
		return Arrays.asList(x, y, angle);
	}

	public void setPosture(int x, int y, int angle) {
		this.x = x;
		this.y = y;
		this.angle = angle;
	}

	public Point getPoint() {
		return new Point(x, y);
	}

	public static int getAngle(double x, double y) {
		return normalizeAngle((int) (Math.atan2(y, x) * 180 / Math.PI));
	}

	public static double getAngle(Point origin, Point destination) {
		return getAngle(destination.x - origin.x, destination.y - origin.y);
	}

	public static Posture parsePosture(String s) {
		return parsePosture(s, 0);
	}

	public static Posture parsePosture(String otherParamsAndPostureCommaSeparated, int qtyOfTokensToSkip) {

		Integer[] postureInt = Arrays
				.asList(StringUtils.normalize(otherParamsAndPostureCommaSeparated).split(Message.SEPARATOR)).stream()
				.skip(qtyOfTokensToSkip).map(s -> Integer.parseInt(s.trim())).toArray(Integer[]::new);

		return new Posture(postureInt[0], postureInt[1], postureInt[2]);
	}

	private static boolean equals(int dimension1, int dimension2, int maxDifference) {
		return Math.abs(dimension1 - dimension2) <= maxDifference;
	}

	public boolean equals(Posture posture, int maxDifference) {
		return equals(posture.x, x, maxDifference) && equals(posture.y, y, maxDifference) && anglesEquals(posture.angle, angle);
	}

	public static int mirrorAngle(int angle) {
		return normalizeAngle(angle, false);
	}

	public static int normalizeAngle(int angle, boolean alwaysAntiClockwise) {
		while (Math.abs(angle) > 360) {
			if (angle > 0) {
				angle -= 360;
			} else {
				angle += 360;
			}
		}

		if (alwaysAntiClockwise) {
			while (angle < 0) {
				angle += 360;
			}
		} else {
			if (Math.abs(angle) > 180) {
				angle -= 360;
			}
		}

		return angle;
	}

	public static int normalizeAngle(int angle) {
		return normalizeAngle(angle, true);
	}

	public static boolean anglesEquals(int angle1, int angle2) {
		return Math.abs(mirrorAngle(angle1) - mirrorAngle(angle2)) <= MAX_DIFFERENCE;
	}

	@Override
	public boolean equals(Object obj) {

		if (!Posture.class.isInstance(obj)) {
			return false;
		}

		return equals(Posture.class.cast(obj), MAX_DIFFERENCE);
	}

	@Override
	public int hashCode() {
		return String.format("%d%d", x, y).hashCode();
	}

	public void setPosture(Posture posture) {
		setPosture(posture.x, posture.y, posture.angle);
	}

	@Override
	public String toString() {
		return String.format("(%s\u00B0)", asCSV());
	}

	public String asCSV() {
		return String.format("%d,%d,%d", x, y, angle);
	}

	public static Graph<String> toGraph(List<Posture> postures, int connectionRange, float threshold) {
		double[][] adjacency = new double[postures.size()][postures.size()];

		for (int i = 0; i < postures.size(); i++) {
			for (int j = i + 1; j < postures.size(); j++) {
				double distance = MathUtils.distance(postures.get(i).getPoint(), postures.get(j).getPoint());

				if (distance <= 2 * connectionRange) {
					adjacency[i][j] = Math
							.exp(-Math.pow(distance, 2) / 2 / calculateSigmaSquared(threshold, connectionRange));
					adjacency[j][i] = adjacency[i][j];
				}
			}
		}

		return Graph.build(adjacency);
	}

	static double calculateSigmaSquared(float threshold, int range) {
		return -Math.pow(2 * range, 2) / 2 / Math.log(threshold);
	}

	public static BufferedImage drawPosturesOnImage(int imageSize, List<Posture> postures, Arena portionOfArena,
			int range, boolean labelPosturesOnImage) {

		double scaleFactor = imageSize / portionOfArena.getWidth(true);

		BufferedImage image = new BufferedImage(imageSize, imageSize, BufferedImage.TYPE_3BYTE_BGR);

		Graphics2D i2 = Graphics2D.class.cast(image.getGraphics());
		i2.setColor(Color.WHITE);
		i2.fillRect(0, 0, image.getWidth(), image.getHeight());
		i2.setColor(Color.BLACK);
		i2.setStroke(Exhibitable.mediumStroke);
		i2.setComposite(Exhibitable.transparent);

		postures.forEach(p -> {

			Point cr = TrackerUtils.abolutePositionInCmToRelativeCameraPositionInPx(portionOfArena, p.getPoint());
			double crx = cr.x * scaleFactor;
			double cry = cr.y * scaleFactor;
			int offset = (int) (range * scaleFactor);
			int width = 2 * offset;
			i2.fillOval((int) crx - offset, (int) cry - offset, width, width);

			offset = (int) (7 * scaleFactor);
			width = 2 * offset;
			i2.fillOval((int) crx - offset, (int) cry - offset, width, width);

			if (labelPosturesOnImage) {
				i2.setComposite(Exhibitable.halfOpaque);
				String label = p.toString();
				offset = i2.getFontMetrics().stringWidth(label) / 2;
				i2.drawString(label, (int) crx - offset / 2, (int) cry);
				i2.setComposite(Exhibitable.transparent);
			}

		});

		return image;

	}

	public void translate(int deltaX, int deltaY) {
		setPosture(x + deltaX, y + deltaY, angle);
	}

	public void rotate(int deltaAngle) {
		setPosture(x, y, angle + deltaAngle);
	}

	public int[] getPosition() {
		return new int[] { getPoint().x, getPoint().y };
	}
	
	public Position getPos() {
		return new Position(getPoint().x, getPoint().y);
	}

	public static Optional<Posture> findPostureHavingThisPoint(List<Posture> targetPostures, Point targetPoint) {
		return targetPostures.stream().filter(p -> equals(p.x, targetPoint.x, 0) && equals(p.y, targetPoint.y, 0))
				.findFirst();
	}

}
