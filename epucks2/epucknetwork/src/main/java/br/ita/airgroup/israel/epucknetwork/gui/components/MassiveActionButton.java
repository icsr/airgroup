package br.ita.airgroup.israel.epucknetwork.gui.components;

import java.util.List;
import java.util.stream.Collectors;

import javax.swing.JButton;

import br.ita.airgroup.israel.epucknetwork.core.controller.BluetoothHandler;
import br.ita.airgroup.israel.epucknetwork.core.controller.Topology;
import br.ita.airgroup.israel.epucknetwork.gui.aux.ChooseEpucksToPerformAction.MassiveEpuckAction;

public class MassiveActionButton extends JButton {

	public MassiveEpuckAction massiveAction;
	private Topology topology;
	private List<BluetoothCheckbox> options;

	public MassiveActionButton(MassiveEpuckAction massiveAction, Topology topology, List<BluetoothCheckbox> options) {
		super(massiveAction.name());
		this.massiveAction = massiveAction;
		this.topology = topology;
		this.options = options;

		this.addActionListener(a -> processOptions());
	}

	private void processOptions() {

		List<BluetoothHandler> selectedHandlers = options.stream().filter(bc -> bc.isSelected())
				.map(bc -> bc.getHandler()).collect(Collectors.toList());

		switch (massiveAction) {
		case Connect:
			topology.reconnect(selectedHandlers);
			break;
		case Reset:
			topology.resetEpucks(selectedHandlers);
			break;
		case Fail:
			topology.failEpucks(selectedHandlers);
			break;
		case Disconnect:
			topology.disconnect(selectedHandlers);
			break;
		default:
			break;
		}

	}

	private static final long serialVersionUID = -5207905027060125024L;

}
