package br.ita.airgroup.israel.epucknetwork.helper.file;

import java.io.File;
import java.nio.file.Path;
import java.util.Optional;

import javax.swing.filechooser.FileFilter;

import br.ita.airgroup.israel.commons.utils.FileUtils;

public class CSVChooser extends FileChooser {

	public CSVChooser(String title, Optional<String> initFolder) {
		super(title, initFolder);
	}

	public CSVChooser(String title, Path initFolder) {
		super(title, initFolder);
	}

	private static final long serialVersionUID = -2816733052705074569L;

	@Override
	protected void config() {
		
		this.setFileFilter(new FileFilter() {
			
			@Override
			public String getDescription() {
				return null;
			}
			
			@Override
			public boolean accept(File f) {
				return f.isDirectory() || f.getAbsolutePath().endsWith(FileUtils.CSV_SUFFIX);
			}
		});
	}
	
	@Override
	public void approveSelection() {
		
		if(!getSelectedFile().toString().endsWith(FileUtils.CSV_SUFFIX)) {
			return;
		}
		
		super.approveSelection();
	}
}
