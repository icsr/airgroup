package br.ita.airgroup.israel.epucknetwork.core.imaging.tracking;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

public interface Trackable {

	void detectAndDrawPatternsAndPolygons(BufferedImage image, Graphics2D g2) throws Exception;

	void configureDetectors();

}
