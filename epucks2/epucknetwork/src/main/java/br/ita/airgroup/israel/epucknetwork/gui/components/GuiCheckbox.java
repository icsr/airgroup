package br.ita.airgroup.israel.epucknetwork.gui.components;

import java.util.Optional;

import javax.swing.JCheckBox;

import br.ita.airgroup.israel.epucknetwork.helper.file.Properties;
import br.ita.airgroup.israel.epucknetwork.helper.file.Properties.Key;

public class GuiCheckbox<T> extends JCheckBox implements Keyable<Boolean, Optional<T>>{

	private static final long serialVersionUID = -8718921309324529729L;
	private Optional<T> value = Optional.empty();
	private Key key;
	
	public GuiCheckbox(String label, Optional<T> value, Key associatedBooleanKey) {
		super(label);
		this.key = associatedBooleanKey;
		this.setSelected(loadKey());
		setValue(value);
		
		whenChange(() -> {}); 
	}

	public Optional<T> getValue() {
		return value;
	}

	public void setValue(Optional<T> content) {
		this.value = content;
	}

	public void whenChange(Runnable listener) {
		this.addActionListener(evt -> {
			saveKey(this.isSelected());
			listener.run();
		});
	}

	@Override
	public Boolean loadKey() {
		return Properties.getAsBoolean(key);
	}

	@Override
	public void saveKey(Boolean value) {
		Properties.add(key, value);
	}

}
