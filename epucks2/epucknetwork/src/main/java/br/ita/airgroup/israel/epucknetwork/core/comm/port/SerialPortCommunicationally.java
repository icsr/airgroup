package br.ita.airgroup.israel.epucknetwork.core.comm.port;

import java.util.List;
import java.util.Optional;

import br.ita.airgroup.israel.epucknetwork.helper.message.CommandListenable;

public interface SerialPortCommunicationally<T> {
	
	int BAUDRATE = 115200;
	
	int IO_TIMEOUT_IN_MILS = 1000;
	
	String getPortName();

	boolean openPort() ;

	boolean closePort() ;

	boolean isOpened();

	boolean startListener(CommandListenable listener) ;

	boolean stopListener() ;

	Optional<String> readString() ;

	Optional<String> readString(int charsCount, int timeoutInMils) ;

	Optional<byte[]> readBytes(int bytesCount, int timeoutInMils) ;
	
	boolean write(byte[] dinamicMessage) ;
	
	boolean write(String dinamicMessage) ;

	boolean configPort(int bauds, T dataBits, T stopBits, T parity, Optional<List<T>> others);
	
	boolean isListening();

	void clearBuffer();

}
