package br.ita.airgroup.israel.epucknetwork.core.controller;

import java.awt.Point;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import br.ita.airgroup.israel.commons.collections.MapUtils;
import br.ita.airgroup.israel.commons.utils.MathUtils;
import br.ita.airgroup.israel.epucknetwork.helper.message.SetUtils;

public class TopologyLoader {

	Optional<Map<BluetoothHandler, Point>> map = Optional.empty();
	Set<Point> processedPoints = new HashSet<>();
	private Set<BluetoothHandler> pickedEpucks = new HashSet<>();;
	private Set<Point> allPoints;

	public void reset() {
		map = Optional.empty();
		processedPoints.clear();
		pickedEpucks.clear();
	}

	public Map<BluetoothHandler, Point> findBestClosestEpucks(List<BluetoothHandler> bluetooths,
			List<Point> targetPoints) {

		return findBestClosestEpucks(bluetooths.stream().sorted((a, b) -> a.getEpuck().getId() - b.getEpuck().getId())
				.collect(Collectors.toSet()), targetPoints.stream().collect(Collectors.toSet()));

	}

	private Map<BluetoothHandler, Point> findBestClosestEpucks(Set<BluetoothHandler> bluetooths,
			Set<Point> targetPoints) {
		if (map.isPresent()) {
			return map.get();
		}

		processFinding(bluetooths, targetPoints);

		if (pickedEpucks.size() != bluetooths.size()) {
			Set<BluetoothHandler> remaingBluetooths = SetUtils.difference(bluetooths, pickedEpucks);

			Set<Point> pickedPoints = map.get().entrySet().stream().map(e -> e.getValue()).collect(Collectors.toSet());

			Set<Point> remainingPoints = SetUtils.difference(allPoints, pickedPoints);

			map.get().putAll(new TopologyLoader().findBestClosestEpucks(remaingBluetooths, remainingPoints));
		}

		return map.get();
	}

	private void processFinding(Set<BluetoothHandler> bluetooths, Set<Point> targetPoints) {
		map = Optional.of(new HashMap<>());

		processedPoints.clear();
		allPoints = targetPoints.stream().collect(Collectors.toSet());

		for (Point p : targetPoints) {

			if (pickedEpucks.size() == bluetooths.size()) {
				break;
			}

			if (processedPoints.contains(p)) {
				continue;
			}

			Map<BluetoothHandler, Double> distances = evaluateDistancesFromEpucksToPoint(p, bluetooths);

			Optional<BluetoothHandler> findClosestEpuck = findBestClosestEpuck(p, distances);
			processedPoints.add(p);

			findClosestEpuck.ifPresent(e -> {
				pickedEpucks.add(e);
				map.get().put(e, p);
			});

		}

	}

	private Map<BluetoothHandler, Double> evaluateDistancesFromEpucksToPoint(Point currentPoint,
			Set<BluetoothHandler> bluetooths) {
		Map<BluetoothHandler, Double> distances = new HashMap<>();
		bluetooths.parallelStream().forEach(b -> {
			distances.put(b, MathUtils.distance(b.getEpuck().getPostureCm().getPoint(), currentPoint));
		});

		return distances;
	}

	private Optional<BluetoothHandler> findBestClosestEpuck(Point currentPoint,
			Map<BluetoothHandler, Double> distances) {

		Set<Point> remainingPoints = SetUtils.difference(allPoints, processedPoints);

		Map<BluetoothHandler, Double> remainingDistances = distances.entrySet().stream()
				.filter(e -> !pickedEpucks.contains(e.getKey()))
				.collect(Collectors.toMap(Entry::getKey, Entry::getValue));

		List<Entry<BluetoothHandler, Double>> sortedEntries = MapUtils.sortByValue(remainingDistances, true);

		for (Entry<BluetoothHandler, Double> entry : sortedEntries) {
			Point epuckPosition = entry.getKey().getEpuck().getPostureCm().getPoint();

			Point closestPoint = remainingPoints.stream().reduce(
					(p1, p2) -> MathUtils.distance(p1, epuckPosition) < MathUtils.distance(p2, epuckPosition) ? p1 : p2)
					.get();

			if (MathUtils.equals(currentPoint, closestPoint, 0)) {
				return Optional.of(entry.getKey());
			}

		}

		return Optional.empty();
	}

	public static Map<BluetoothHandler, Boolean> areEpucksWellPlaced(List<Point> allPoints,
			List<BluetoothHandler> bluetooths, float maxDifferenceInCm) {

		Map<BluetoothHandler, Point> bestClosestEpucks = new TopologyLoader().findBestClosestEpucks(bluetooths,
				allPoints);
		Map<BluetoothHandler, Boolean> wellPlaced = new HashMap<>();

		bestClosestEpucks.entrySet().forEach(k -> {
			wellPlaced.put(k.getKey(), MathUtils.distance(k.getValue(),
					k.getKey().getEpuck().getPostureCm().getPoint()) <= maxDifferenceInCm);
		});

		return wellPlaced;

	}

}
