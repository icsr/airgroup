package br.ita.airgroup.israel.epucknetwork.core.imaging.tracking;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

import br.ita.airgroup.israel.commons.utils.SwingUtils;
import br.ita.airgroup.israel.epucknetwork.core.controller.Topology;
import br.ita.airgroup.israel.epucknetwork.core.imaging.Arena;
import br.ita.airgroup.israel.epucknetwork.core.imaging.Camera;
import br.ita.airgroup.israel.epucknetwork.core.imaging.EdgeLengthAdjustFactor;

public class ArenaResizer {

	Arena arena;
	Camera camera;

	public enum ArenaBoundaryPhase {
		IDLE, ORIGIN, DIAGONAL
	};

	ArenaBoundaryPhase phase = ArenaBoundaryPhase.IDLE;
	private EdgeLengthAdjustFactor adjustFactor;
	private Topology topology;

	public ArenaResizer(Arena arena, Camera camera, Topology topology, EdgeLengthAdjustFactor adjustFactor) {
		this.arena = arena;
		this.camera = camera;
		this.adjustFactor = adjustFactor;
		this.topology = topology;
	}

	public synchronized void resetArenaBoundary() {
		arena.setOriginCm(0, 0);
		arena.setDiagonalCm((int) (this.camera.getPanel().getWidth() * adjustFactor.consolidateFactorInCmPerPx()),
				(int) (this.camera.getPanel().getHeight() * adjustFactor.consolidateFactorInCmPerPx()));

		saveArenaBoundary();
	}

	public void setArenaBoundary() {

		phase = ArenaBoundaryPhase.ORIGIN;

		MouseMotionAdapter mouseMotionAdapter = new MouseMotionAdapter() {

			@Override
			public void mouseMoved(MouseEvent e) {
				synchronized (arena) {
					if (phase == ArenaBoundaryPhase.ORIGIN) {
						arena.setOriginCm(SwingUtils.multiply(e.getPoint(), adjustFactor.consolidateFactorInCmPerPx()));
					} else if (phase == ArenaBoundaryPhase.DIAGONAL) {
						arena.setDiagonalCm(
								SwingUtils.multiply(e.getPoint(), adjustFactor.consolidateFactorInCmPerPx()));
					}
				}
			}
		};

		this.camera.getPanel().addMouseMotionListener(mouseMotionAdapter);
		this.camera.getPanel().addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {
				synchronized (arena) {
					if (phase == ArenaBoundaryPhase.ORIGIN) {
						phase = ArenaBoundaryPhase.DIAGONAL;
					}

					else if (phase == ArenaBoundaryPhase.DIAGONAL) {
						phase = ArenaBoundaryPhase.IDLE;
						camera.getPanel().removeMouseMotionListener(mouseMotionAdapter);
						camera.getPanel().removeMouseListener(this);
						saveArenaBoundary();
					}
				}
			}

		});
	}

	private void saveArenaBoundary() {
		topology.setArena(arena);
		arena.saveProperties();
	}
}
