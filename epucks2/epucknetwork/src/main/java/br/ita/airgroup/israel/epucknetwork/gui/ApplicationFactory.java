package br.ita.airgroup.israel.epucknetwork.gui;

import java.util.Optional;

import br.ita.airgroup.israel.epucknetwork.helper.BaseSystem;

public class ApplicationFactory {
	
	static Optional<ApplicationGUI> app = Optional.empty();
	
	public static ApplicationGUI buildSingleton() throws Exception{
		if(!app.isPresent()){
			BaseSystem.init();
			app = Optional.of(new ApplicationGUI());
		}
		
		return app.get();
	}

}
