package br.ita.airgroup.israel.epucknetwork.core.comm;

import java.util.Optional;

import br.ita.airgroup.israel.epucknetwork.core.comm.port.RxTxSerialPort;
import br.ita.airgroup.israel.epucknetwork.core.comm.port.SerialPortCommunicationally;
import br.ita.airgroup.israel.epucknetwork.helper.file.Properties;
import br.ita.airgroup.israel.epucknetwork.helper.file.Properties.Key;

public class SerialPortBuilder {

	public enum Type {
		RXTX;

		public static Optional<Type> toType(String type) {
			for (Type v : Type.values()) {
				if (v.name().toLowerCase().equals(type.toLowerCase())) {
					return Optional.of(v);
				}
			}
			return Optional.empty();
		}

		@Override
		public String toString() {
			return this.name();
		}
	};

	private Type type = Type.toType(Properties.get(Key.PortType)).get();
	private String portName;

	public SerialPortBuilder(Type type, String portName) {
		this.type = type;
		this.portName = portName;
	}

	public SerialPortBuilder(String portName) {
		this.portName = portName;
	}

	public SerialPortCommunicationally<?> build() {
		switch (type) {
		default:
		case RXTX:
			return new RxTxSerialPort(portName);
		}
	}

}
