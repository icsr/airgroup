package br.ita.airgroup.israel.epucknetwork.core.controller;

import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.Serializable;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.tuple.Pair;

import com.mathworks.toolbox.javabuilder.MWException;

import br.ita.airgroup.israel.commons.clock.Clock;
import br.ita.airgroup.israel.commons.collections.TimedAndLimitedList;
import br.ita.airgroup.israel.commons.utils.MathUtils;
import br.ita.airgroup.israel.epucknetwork.core.algorithm.Algorithm;
import br.ita.airgroup.israel.epucknetwork.core.algorithm.MatlabAlgorithm;
import br.ita.airgroup.israel.epucknetwork.core.algorithm.TopologyAlgorithm;
import br.ita.airgroup.israel.epucknetwork.core.comm.SerialPortScanner;
import br.ita.airgroup.israel.epucknetwork.core.comm.port.SerialPortCommunicationally;
import br.ita.airgroup.israel.epucknetwork.core.command.PutSelector;
import br.ita.airgroup.israel.epucknetwork.core.imaging.Arena;
import br.ita.airgroup.israel.epucknetwork.helper.Logger;
import br.ita.airgroup.israel.epucknetwork.helper.Posture;
import br.ita.airgroup.israel.epucknetwork.helper.exception.EmptyException;
import br.ita.airgroup.israel.epucknetwork.helper.exception.PortAccessException;
import br.ita.airgroup.israel.epucknetwork.helper.exception.SettingsException;
import br.ita.airgroup.israel.epucknetwork.helper.file.Properties;
import br.ita.airgroup.israel.epucknetwork.helper.file.Properties.Key;
import br.ita.airgroup.israel.epucknetwork.helper.message.Neighborhood;
import br.ita.airgroup.israel.epucknetwork.matlab.MatlabControlFacade;
import br.ita.airgroup.israel.epucknetwork.matlab.helper.NullUControl;
import br.ita.airgroup.israel.epucknetwork.matlab.helper.RandomUControl;
import br.ita.airgroup.israel.epucknetwork.matlab.helper.TopologyGain;
import br.ita.airgroup.israel.epucknetwork.matlab.helper.UControl;

public class Epuck implements Serializable, Comparable<Epuck> {

	private static final int MAX_TIME_TO_RETRACK = 500;

	private static final long serialVersionUID = 7265034476538451265L;

	private transient static List<SerialPortCommunicationally<?>> usedPorts = new ArrayList<>();

	public static Set<Epuck> allEpucks = new HashSet<>();

	public static String ID_REGEX = "^[0-9]{4}$";

	public static List<Integer> FRONTAL_IR_SENSORS = Arrays.asList(0, 1, 6, 7);
	private transient SerialPortCommunicationally<?> port;

	private static int range = Properties.getFirstAsInt(PutSelector.RANGE);
	private String mac;
	private int id;
	private String bluetoothAdapter;
	private Posture posture;
	TimedAndLimitedList<Pair<LocalDateTime, Posture>> previousPosturesOneSecondPerFrame = new TimedAndLimitedList<>(100,
			1);

	private boolean trackedByCamera = false;
	private boolean trackindReached = false;
	private Optional<BluetoothHandler> handler = Optional.empty();

	protected double adjustFactorInCmPerPx = 1;

	protected Optional<Arena> arena = Optional.empty();

	private boolean selected;

	private boolean onFail;

	private LocalDateTime lastTracking = LocalDateTime.now();

	private Timer timer;

	private BufferedImage fiducial;

	private float secondEigenVector;

	private LinkedList<Pair<Long, Float>> secondEigenValues = new LinkedList<>();

	private Optional<Algorithm> algorithm = Optional.empty();

	private Optional<MatlabControlFacade> matlab = Optional.empty();

	private Optional<TopologyGain> topologyGain = Optional.empty();

	private Optional<Pair<LocalDateTime, String>> lastCommand = Optional.empty();

	private Epuck(int id, String bluetoothAdapter) throws EmptyException, SettingsException, PortAccessException {

		this.port = getNextAvailablePort();
		this.id = id;
		this.posture = new Posture();
		this.bluetoothAdapter = bluetoothAdapter;

		this.timer = new Timer();
		timer.scheduleAtFixedRate(new TimerTask() {

			@Override
			public void run() {
				if (LocalDateTime.now().isAfter(lastTracking.plus(MAX_TIME_TO_RETRACK, ChronoUnit.MILLIS))) {
					setTrackedByCamera(false);
				}
			}

		}, 2 * MAX_TIME_TO_RETRACK, MAX_TIME_TO_RETRACK);

		allEpucks.add(this);
	}

	public void setDislocationShouldOversee(int timeInSecs) {
		if (timeInSecs < 1) {
			return;
		}
		previousPosturesOneSecondPerFrame.setMaxSize(timeInSecs);
	}

	protected Epuck(Epuck epuck) {
		this.port = epuck.getPort();
		this.id = epuck.id;
		this.posture = epuck.posture;
		this.bluetoothAdapter = epuck.bluetoothAdapter;
		this.adjustFactorInCmPerPx = epuck.adjustFactorInCmPerPx;
		this.arena = epuck.arena;
		this.trackedByCamera = epuck.trackedByCamera;
	}

	public Epuck(int id, String mac, String bluetoothAdapter)
			throws EmptyException, SettingsException, PortAccessException {
		this(id, mac, bluetoothAdapter, Optional.empty(), Optional.empty());
	}

	public Epuck(int id, String mac, String bluetoothAdapter, Posture initialPostureInCm)
			throws EmptyException, SettingsException, PortAccessException {
		this(id, mac, bluetoothAdapter);
		setPostureInCm(initialPostureInCm);
	}

	public Epuck(int id, String mac, String bluetoothAdapter, Optional<MatlabControlFacade> matlab,
			Optional<TopologyGain> topologyGain) throws EmptyException, SettingsException, PortAccessException {
		this(id, bluetoothAdapter);
		this.mac = mac;

		this.matlab = matlab;

		this.topologyGain = topologyGain;
	}

	public Posture getPostureCm() {
		synchronized (this) {
			return posture;
		}
	}

	void addBluetoothHandler(BluetoothHandler handler) {
		addBluetoothHandler(Optional.of(handler));
	}

	protected void addBluetoothHandler(Optional<BluetoothHandler> handler) {
		this.handler = handler;
	}

	public Optional<BluetoothHandler> getBluetoothHandler() {
		return this.handler;
	}

	public void setPostureInCm(Posture newPosture) {

		trackedByCamera = true;
		lastTracking = LocalDateTime.now();

		previousPosturesOneSecondPerFrame.add(Pair.of(LocalDateTime.now(), newPosture));
		this.posture = newPosture;

	}

	protected void setPostureInCm(Point position, int angle, double adjustFactorInCmPerPx) {
		setAdjustFactorInCmPerPx(adjustFactorInCmPerPx);
		setPostureInCm(position.x, position.y, angle);
	}

	public void setPostureInCm(int x, int y, int angle) {
		setPostureInCm(new Posture(x, y, angle));
	}

	public Optional<Pair<Long, Integer>> lastDislocation() {
		if (previousPosturesOneSecondPerFrame.size() <= 1) {
			return Optional.empty();
		}

		Duration duration = Duration.between(previousPosturesOneSecondPerFrame.getFirst().getLeft(),
				previousPosturesOneSecondPerFrame.getLast().getLeft());
		Double distance = MathUtils.distance(previousPosturesOneSecondPerFrame.getFirst().getRight().getPoint(),
				previousPosturesOneSecondPerFrame.getLast().getRight().getPoint());

		return Optional.of(Pair.of(duration.getSeconds(), distance.intValue()));
	}

	public synchronized boolean hasDislocated() {
		if (previousPosturesOneSecondPerFrame.isEmpty()) {
			return false;
		}

		return getDiffs().filter(d -> d >= 0.8f * getMaxAxialDislocation()).findAny().isPresent();
	}

	private Stream<Double> getDiffs() {
		List<Point> trajectory = previousPosturesOneSecondPerFrame.stream().map(pp -> pp.getRight().getPoint())
				.collect(Collectors.toList());
		return MathUtils.diff(trajectory).stream();
	}

	public synchronized boolean hasRecentlyDislocated() {

		if (previousPosturesOneSecondPerFrame.isEmpty() || !isTrackedByCamera()) {
			return false;
		}

		return getDiffs().skip(allButFiveLastSeconds()).filter(d -> d > getMinAxialDislocation()).findAny().isPresent();
	}

	private int allButFiveLastSeconds() {
		return Math.max(previousPosturesOneSecondPerFrame.size() - 5, 0);
	}

	public static int getMinAxialDislocation() {
		return Properties.getAsInteger(PutSelector.ALGORITHM_CONFIGS, 1);
	}

	public static int getMaxAxialDislocation() {
		return Properties.getAsInteger(PutSelector.ALGORITHM_CONFIGS, 3);
	}

	public void setAdjustFactorInCmPerPx(double adjustFactorInCmPerPx) {
		this.adjustFactorInCmPerPx = adjustFactorInCmPerPx;
	}

	public static List<SerialPortCommunicationally<?>> getUsedPorts() {
		return usedPorts;
	}

	public String getBluetoothAdapter() {
		return this.bluetoothAdapter;
	}

	public SerialPortCommunicationally<?> getPort() {
		return this.port;
	}

	private SerialPortCommunicationally<?> getNextAvailablePort() throws SettingsException, PortAccessException {
		List<SerialPortCommunicationally<?>> scan = new SerialPortScanner(usedPorts.size() + 1).scan();

		SerialPortCommunicationally<?> nextAvailablePort = scan.get(usedPorts.size());
		usedPorts.add(nextAvailablePort);
		return nextAvailablePort;
	}

	public String getMac() {
		return mac;
	}

	public int getId() {
		return id;
	}

	public String fullId() {
		return String.format("@%4s, %-8s, hci%s", this.id, this.port.getPortName(), this.bluetoothAdapter);
	}

	@Override
	public String toString() {
		return String.format("%d", id);
	}

	public static enum EpuckVariable {
		PROXIMITY_VALUE(1), FINAL_IDS_AND_WEIGHTS(2);
		public int index;

		EpuckVariable(int index) {
			this.index = index;
		};

		@Override
		public String toString() {
			return String.valueOf(this.index);
		}
	}

	@Override
	public int hashCode() {
		return getId();
	}

	@Override
	public boolean equals(Object obj) {
		return obj.getClass().isInstance(this) && Epuck.class.cast(obj).hashCode() == this.hashCode();
	}

	public static int getBodyRadiusInCm() {
		return 5;
	}

	public int getRangeInCm() {
		return range;
	}

	public void setRangeInCm(int range) {
		Epuck.range = range;
	}

	public int getBodyRadiusInPx() {
		return (int) (getBodyRadiusInCm() / adjustFactorInCmPerPx);
	}

	public int getRangeInPx() {
		return (int) (getRangeInCm() / adjustFactorInCmPerPx);
	}

	public Posture getPosturePx() {
		return new Posture((int) (posture.x / adjustFactorInCmPerPx), (int) (posture.y / adjustFactorInCmPerPx),
				posture.angle);
	}

	public synchronized void setTrackedByCamera(boolean tracked) {

		if (this.trackedByCamera == tracked) {
			return;
		}

		this.trackedByCamera = tracked;

		if (tracked) {
			sendPosture();
		}
	}

	public void setTrackingReached(boolean reached) {
		this.trackindReached = reached;
	}

	private void sendPosture() {
		algorithm.filter(a -> !TopologyAlgorithm.class.isInstance(a) || !a.isRunning())
				.ifPresent(a -> this.handler.ifPresent(h -> h.sendPosture()));
	}

	public synchronized boolean isTrackingReached() {
		return trackindReached;
	}

	public synchronized boolean isTrackedByCamera() {
		return trackedByCamera;
	}

	public void setArena(Arena arena) {
		this.arena = Optional.of(arena);
	}

	public Optional<Arena> getArena() {
		return this.arena;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public boolean isSelected() {
		return this.selected;
	}

	public int epuckPostureHashcode() {
		return String.format("%d%d%d%d", getId(), getPostureCm().x, getPostureCm().y, getPostureCm().angle).hashCode();
	}

	public int epuckPositionHashcode() {
		return String.format("%d%d%d", getId(), getPostureCm().x, getPostureCm().y).hashCode();
	}

	public boolean isOnFail() {
		return this.onFail;
	}

	public void setOnFail(boolean onFail) {
		this.onFail = onFail;
	}

	public void setFiducial(BufferedImage fiducial) {
		this.fiducial = fiducial;
	}

	public BufferedImage getFiducial() {
		return this.fiducial;
	}

	public double getAdjustFactorInCmPerPx() {
		return adjustFactorInCmPerPx;
	}

	private List<Epuck> getKnownEpucks(boolean onlyNeighbors) {

		List<Epuck> knownEpucks = allEpucks.stream().filter(e -> !e.equals(this))
				.sorted((e1, e2) -> e1.getId() - e2.getId()).collect(Collectors.toList());

		if (!onlyNeighbors) {
			return knownEpucks;
		}

		return filterDirectNeighbors(knownEpucks);
	}

	public List<Epuck> getKnownOthersEpucks() {
		return getKnownEpucks(!areAllEpucksPositionsKnown());
	}

	private List<Epuck> filterDirectNeighbors(List<Epuck> knownEpucks) {
		return knownEpucks.stream().filter(e -> MathUtils.distance(e.getPostureCm().getPoint(),
				this.getPostureCm().getPoint()) <= 2 * getRangeInCm()).collect(Collectors.toList());
	}

	private boolean areAllEpucksPositionsKnown() {
		return Properties.getAsBoolean(Key.allPositionsKnown);
	}

	public List<String> getKnownEpucksPositionsAsString() {
		return initNeighborhood().getAllPositionsAsList();
	}

	public List<String> getNeighborhoodAsList() {
		return initNeighborhood().toList();
	}

	private Neighborhood initNeighborhood() {

		Neighborhood neighborhood = new Neighborhood(this);

		if (!MatlabAlgorithm.class.isInstance(this.algorithm.get())) {
			return neighborhood;
		}

		if (!matlab.isPresent()) {
			Logger.error(this, "Matlab is not set.");
			return neighborhood;
		}

		if (!topologyGain.isPresent()) {
			Logger.error(this, "TopologyGain is not set.");
			return neighborhood;
		}

		return evalMatlabControl(neighborhood, MatlabAlgorithm.class.cast(algorithm.get()));
	}

	private Neighborhood evalMatlabControl(Neighborhood neighborhood, MatlabAlgorithm algorithm) {

		UControl uControl = null;
		try {
			uControl = matlab.get()
					.performControl(neighborhood.getAllPositionsWithThisEpuckInstanceAtFirst(), this.topologyGain.get())
					.get(0);

		} catch (MWException e) {
			if (matlab.get().isConvertNullControlToRandom()) {
				Logger.warn(this, "Null control detected and converted to Random");
				uControl = new RandomUControl();
			} else {
				Logger.warn(this, "Null control detected and ignored.");
				uControl = new NullUControl();
			}

			Logger.exception(this, e);
		}

		return neighborhood.addControls(uControl);
	}

	public void setTopologyGain(TopologyGain topologyGain) {
		this.topologyGain = Optional.of(topologyGain);
	}

	public void setMatlab(Optional<MatlabControlFacade> matlab) {
		this.matlab = matlab;
	}

	public void setSecondEigenValue(float secondEigenValue) {
		getAllSecondEigenValues().addLast(Pair.of(getDiffTime(), secondEigenValue));
	}

	private long getDiffTime() {
		return new Date().getTime()
				- getAllSecondEigenValues().stream().map(p -> p.getKey()).min((a, b) -> Float.compare(a, b)).orElse(0l);
	}

	public Optional<Float> getSecondEigenValue() {
		if (getAllSecondEigenValues().isEmpty()) {
			return Optional.empty();
		}

		return Optional.of(getAllSecondEigenValues().getLast().getValue());
	}

	public void setSecondEigenVector(float secondEigenVector) {
		this.secondEigenVector = secondEigenVector;
	}

	public float getSecondEigenVector() {
		return secondEigenVector;
	}

	public static void clearKnownEpucks() {
		allEpucks.clear();
	}

	public void resetEigens() {
		secondEigenValues.clear();
		secondEigenVector = 0;
	}

	public void reset() {
		setOnFail(false);
		resetEigens();
		resetPreviousPostures();
	}

	private void resetPreviousPostures() {
		this.previousPosturesOneSecondPerFrame.clear();
	}

	public static int getRange() {
		return range;
	}

	public List<Epuck> getDirectNeighbors() {
		return getKnownEpucks(true);
	}

	public List<String> getAllSecondEigenValuesAsCSVList() {
		return getAllSecondEigenValues().stream()
				.map(s -> String.format(Locale.ENGLISH, "%d,%d,%f", getId(), s.getKey(), s.getValue()))
				.collect(Collectors.toList());
	}

	public LinkedList<Pair<Long, Float>> getAllSecondEigenValues() {
		return this.secondEigenValues;
	}

	public void setAlgorithm(Algorithm algorithm) {
		this.algorithm = Optional.of(algorithm);
	}

	public Algorithm getSetAlgorithm() throws EmptyException {
		return algorithm.orElseThrow(() -> new EmptyException());
	}

	@Override
	public int compareTo(Epuck other) {
		return this.hashCode() - other.hashCode();
	}

	public void setLastReceivedCommand(String plainCommand) {
		this.lastCommand = Optional.of(Pair.of(Clock.now(), plainCommand));
	}

	public Optional<Pair<LocalDateTime, String>> getLastCommand() {
		return lastCommand;
	}

}
