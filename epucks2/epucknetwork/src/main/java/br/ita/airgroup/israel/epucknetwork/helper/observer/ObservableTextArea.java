package br.ita.airgroup.israel.epucknetwork.helper.observer;

import java.awt.Point;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.event.CaretEvent;
import javax.swing.plaf.basic.BasicTextUI.BasicCaret;
import javax.swing.text.DefaultCaret;

import br.ita.airgroup.israel.commons.collections.LimitedList;
import br.ita.airgroup.israel.commons.news.Subscriber;
import br.ita.airgroup.israel.commons.utils.StringUtils;

public class ObservableTextArea extends JTextArea implements Subscriber<String> {

	private static final long serialVersionUID = -70669014560484817L;
	private BasicCaret caret;
	private LimitedList<String> lines = new LimitedList<>(1000);
	private Optional<String> filter = Optional.empty();
	private Optional<JScrollPane> parent = Optional.empty();

	public ObservableTextArea() {
		this.setDoubleBuffered(true);
		if (JScrollPane.class.isInstance(this.getParent())) {
			this.getParent().setSize(400, 100);
			parent = Optional.of(JScrollPane.class.cast(this.getParent()));
		}

		caret = (BasicCaret) getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		this.setSize(400, 100);
	}

	@Override
	protected void fireCaretUpdate(CaretEvent e) {
		int old[] = new int[1];
		parent.ifPresent(p -> {
			old[0] = p.getHorizontalScrollBar().getValue();
		});
		super.fireCaretUpdate(e);
		parent.ifPresent(p -> p.getHorizontalScrollBar().setValue(old[0]));
	}

	public void setScroll(boolean scroll) {
		caret.setUpdatePolicy(scroll ? DefaultCaret.ALWAYS_UPDATE : DefaultCaret.NEVER_UPDATE);
	}

	public void filterOff() {
		filter = Optional.empty();
		filter();
	}

	public void filterBy(String text) {
		filter = Optional.of(text);
		filter();
	}

	private void filter() {
		if (filter.filter(s -> s.length() > 0).isPresent()) {
			List<String> filtered = lines.parallelStream()
					.filter(t -> t.toLowerCase().contains(filter.get().toLowerCase())).collect(Collectors.toList());
			setText(StringUtils.join(filtered, StringUtils.NEWLINE));
		} else {
			setText(StringUtils.join(lines, StringUtils.NEWLINE));
		}
	}

	public void clear() {
		lines.clear();
		this.setText("");
	}

	@Override
	public void setText(String t) {
		int beforeX = caret.x;
		super.setText(t);
		repaint();
		caret.setMagicCaretPosition(new Point(beforeX, caret.y));

	}

	@Override
	public void receiveReport(String newContent) {
		lines.add(StringUtils.breakInLines(newContent, 40, true));
		filter();
	}

}
