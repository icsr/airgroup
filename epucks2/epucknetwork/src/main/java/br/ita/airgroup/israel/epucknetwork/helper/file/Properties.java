package br.ita.airgroup.israel.epucknetwork.helper.file;

import java.awt.AlphaComposite;
import java.awt.Composite;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import boofcv.factory.filter.binary.ThresholdType;
import br.ita.airgroup.israel.commons.utils.FileUtils;
import br.ita.airgroup.israel.commons.utils.MathUtils;
import br.ita.airgroup.israel.commons.utils.SortedProperties;
import br.ita.airgroup.israel.commons.utils.StringUtils;
import br.ita.airgroup.israel.epucknetwork.core.algorithm.RandomWaypoints;
import br.ita.airgroup.israel.epucknetwork.core.comm.SerialPortBuilder.Type;
import br.ita.airgroup.israel.epucknetwork.core.command.PutSelector;
import br.ita.airgroup.israel.epucknetwork.core.command.PutSelectorParam;
import br.ita.airgroup.israel.epucknetwork.core.command.Selector;
import br.ita.airgroup.israel.epucknetwork.helper.BaseSystem;
import br.ita.airgroup.israel.epucknetwork.helper.Logger;
import br.ita.airgroup.israel.epucknetwork.helper.tracking.TrackerDrawings.Show;

public class Properties {

	static SortedProperties prop = initProperties();

	public static final String SEPARATOR = ";";

	private static final long MAX_LINES = 10;

	private Properties() {
	}

	private static SortedProperties initProperties() {
		return new SortedProperties(
				Paths.get(BaseSystem.APP_HOME.toString(), String.format("app%s", FileUtils.PROP_SUFFIX)));
	}

	public static enum Key {
		FiducialFolder("No folder still selected"), PortType(Type.RXTX.toString()), FiducialSizeCm("5"),
		CameraName(StringUtils.EMPTY), ARENA_ORIGIN_X("0"), ARENA_ORIGIN_Y("0"), ARENA_DIAGONAL_X("-1"),
		ARENA_DIAGONAL_Y("-1"), TrackerThreshold(ThresholdType.GLOBAL_OTSU.name()),
		LAST_COMMANDS(String.format("R,-180%sD,5", SEPARATOR)), LOCK("false"),
		TopologyAlgorithmName(RandomWaypoints.class.getName()), ShowEnhancedImage("false"), EnhanceImage("false"),
		EnhancerRadius("50"), TrackerThresholdWidth("21"), DetectorThreshold("50"), CSVBasedInitialTopology(""),
		allPositionsKnown("true"), ConnectivityGain("0"), RobustnessGain("100"), CoverageArea("100"), FloatScale("4"),
		PosturesHome(BaseSystem.APP_HOME.toString() + "/postures"), RunningTime("5"),
		ExperimentGroupName("Topology #n"), ExperimentTimeToRunInMinutes("5f"), NormalizeControl("true"),
		RangeSafety("5"), LastCSVFilePath(""), ExperimentRepetitions("1"), FontBaseSize("10"), AdjustFactorCmPerPx("1"),
		Brightness("1"), ConvertNullControlToRandom("true"), CollisionAvoidance("100"), Resolution("true"),
		ArenaOpacity("80"), MAX_POSITIONING_ERROR_IN_CM("4"), safeControl("false");

		private String defaultValue;

		private Key(String defaultValue) {
			this.defaultValue = defaultValue;
		}

		@Override
		public String toString() {
			return this.name();
		}

		public String getDefaultValue() {
			return this.defaultValue;
		}

	};

	static {
		if (!BaseSystem.APP_HOME.toFile().exists()) {
			createFolder(BaseSystem.APP_HOME, true);
		}
		try {
			prop.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void createFolder(Path homeFolder, boolean hideIt) {
		if (System.getProperty("os.name").toLowerCase().contains("win")) {
			try {
				Files.setAttribute(homeFolder, "dos:hidden", Boolean.TRUE, LinkOption.NOFOLLOW_LINKS);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		homeFolder.toFile().mkdirs();
	}

	public static void add(Key key, String value) {
		addIt(key.name(), value);
	}

	private static void addIt(String key, String value) {
		prop.add(key, value);
		saveIt();
	}

	public static void add(Key key, int value) {
		add(key, String.format("%d", value));
	}

	public static String get(Key key) {
		return prop.getProperty(key.toString(), key.getDefaultValue());
	}

	public static Integer getAsInteger(Key key) {
		return (int) Float.parseFloat(get(key));
	}

	public static Optional<String> getAsOptional(Key key) {
		String prop = get(key);
		return prop.length() == 0 ? Optional.empty() : Optional.of(prop);
	}

	public static boolean constains(Key key) {
		return prop.containsKey(key.toString());
	}

	public static Path getAsPath(Key key) {
		return Paths.get(get(key));
	}

	public static boolean get(Show show) {
		if (prop.containsKey(show.name())) {
			return Boolean.parseBoolean(prop.getProperty(show.name()));
		} else {
			return true;
		}
	}

	public static void add(Show showKey, boolean selected) {
		addIt(showKey.name(), Boolean.toString(selected));
	}

	public static void remove(Key key) {
		if (prop.containsKey(key.name())) {
			prop.remove(key.name());
			Logger.info(Properties.class, "Removed key", key.name());
		}
	}

	public static ThresholdType getAsTrackerThreshold(Key trackerthreshold) {

		if (prop.containsKey(trackerthreshold.name())) {
			return Arrays.stream(ThresholdType.values())
					.filter(t -> t.name().equals(prop.getProperty(trackerthreshold.name()))).findFirst()
					.orElse(ThresholdType.GLOBAL_ENTROPY);
		}

		return ThresholdType.GLOBAL_ENTROPY;
	}

	public static String[] getLinesAsArray(Key key) {
		return get(key).split(SEPARATOR);
	}

	public static LinkedList<String> getLines(Key key) {
		return new LinkedList<String>(Arrays.asList(getLinesAsArray(key)));
	}

	private static void addLines(String key, List<String> lines) {
		prop.add(key, StringUtils.join(lines.stream().limit(MAX_LINES).toArray(String[]::new), SEPARATOR));
	}

	public static void addLines(Key key, List<String> lines) {
		addLines(key.name(), lines);
	}

	public static List<Integer> getLinesAsIntegers(Key key) {
		return getLines(key).parallelStream().map(s -> Integer.parseInt(s)).collect(Collectors.toList());
	}

	public static void add(Key key, boolean value) {
		add(key, Boolean.toString(value));
	}

	public static boolean getAsBoolean(Key key) {
		return Boolean.parseBoolean(get(key));
	}

	public static void add(Selector selector, boolean selected) {
		prop.add(selector.name(), Boolean.toString(selected));
	}

	public static boolean getAsBoolean(Selector selector) {
		return Boolean.parseBoolean(prop.getProperty(selector.name(), "true"));
	}

	public static void add(PutSelector putSelector, PutSelectorParam pair) {
		prop.add(getPutSelectorKey(putSelector, pair), Float.toString(pair.getRight()));
	}

	public static void add(ThresholdType configThreshold) {
		add(Key.TrackerThreshold, configThreshold.name());
	}

	public static String get(PutSelector putSelector, PutSelectorParam param) {
		String key = getPutSelectorKey(putSelector, param);
		return prop.getProperty(key, Float.toString(param.getValue()));
	}

	public static String get(PutSelector putSelector, int paramIndex) {
		PutSelectorParam param = putSelector.getParams().get(paramIndex);
		String key = getPutSelectorKey(putSelector, param);
		return prop.getProperty(key, Float.toString(param.getValue()));
	}

	public static String getFirst(PutSelector putSelector) {
		return get(putSelector, 0);
	}

	private static String getPutSelectorKey(PutSelector putSelector, PutSelectorParam param) {
		return String.format("%s-%s", putSelector.name(), param.getLabel()).replaceAll("[\\s=]", "_");
	}

	public static Float getAsFloat(PutSelector putSelector, PutSelectorParam param) {
		return Float.parseFloat(get(putSelector, param));
	}

	public static int getFirstAsInt(PutSelector putSelector) {
		return MathUtils.round(getAsFloat(putSelector, 0));
	}

	public static float getFirstAsFloat(PutSelector putSelector) {
		return getAsFloat(putSelector, 0);
	}

	public static Float getAsFloat(PutSelector putSelector, int paramIndex) {
		return Float.parseFloat(get(putSelector, paramIndex));
	}

	public static Optional<String> get(Key key, boolean rejectDefaultValue) {
		String value = get(key);
		if (!rejectDefaultValue) {
			return Optional.of(value);
		}

		if (value.equals(key.defaultValue)) {
			return Optional.empty();
		}

		return Optional.of(value);
	}

	public static Optional<Path> getAsPath(Key key, boolean rejectDefaultValue) {
		Optional<String> value = get(key, rejectDefaultValue);
		if (!value.isPresent()) {
			return Optional.empty();
		}

		return Optional.of(Paths.get(value.get()));
	}

	public static float getAsFloat(Key key) {
		return Float.parseFloat(get(key));
	}

	public static void add(Key key, float value) {
		add(key, Float.toString(value));
	}

	public static float getAsPercentage(Key key) {
		return getAsFloat(key) / 100;
	}

	public static Composite getAsComposite(Key key) {
		return AlphaComposite.getInstance(AlphaComposite.SRC_OVER, getAsPercentage(key));
	}

	public static Map<String, String> toMap() {
		return prop.keySet().parallelStream().map(o -> String.class.cast(o))
				.collect(Collectors.toMap(s -> s, s -> String.class.cast(prop.get(s))));
	}

	public static void saveIt() {
		try {
			prop.save();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static int getAsInteger(PutSelector putSelector, int paramIndex) {
		return getAsFloat(putSelector, paramIndex).intValue();
	}

	public static Double getAsDouble(Key key) {
		return Double.parseDouble(get(key));
	}

}
