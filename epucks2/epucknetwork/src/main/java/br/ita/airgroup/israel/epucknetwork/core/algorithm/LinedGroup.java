package br.ita.airgroup.israel.epucknetwork.core.algorithm;

import java.util.List;

import br.ita.airgroup.israel.epucknetwork.core.controller.BluetoothHandler;
import br.ita.airgroup.israel.epucknetwork.core.imaging.Arena;
import br.ita.airgroup.israel.epucknetwork.helper.annotation.Algorithm;

@Algorithm
public class LinedGroup extends NonAutonomyAlgorithm {
	
	public LinedGroup() {
		
	}
	
	LinedGroup(int separationInCm, int qtyPerLine, boolean waitForEachLine) {
		this.separationInCm = separationInCm;
		this.qtyPerLine = qtyPerLine;
		this.waitForEachLine = waitForEachLine;
	}

	protected void groupOnArena(List<BluetoothHandler> bluetooths, Arena arena) {
		
		int maxCols = qtyPerLine;
		int maxLines = (int) Math.ceil((bluetooths.size()) / 2);

		int xZero = arena.getCenterCm(true).x - maxCols * separationInCm / 2;
		int yZero = arena.getCenterCm(true).y - maxLines * separationInCm / 2;

		int x = xZero;
		int y = yZero;
		int counter = 0;

		for (BluetoothHandler b : bluetooths) {
			b.sendMoveCommand(x, y);
			if (++counter < qtyPerLine) {
				x += separationInCm;
			} else {				
				x = xZero;
				y += separationInCm;
				counter = 0;
			}
		}
		
		for (BluetoothHandler b : bluetooths) {
			b.sendTurnToAngle(0);
		}

	}

}
