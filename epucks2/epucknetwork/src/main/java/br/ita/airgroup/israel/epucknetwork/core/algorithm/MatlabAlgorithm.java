package br.ita.airgroup.israel.epucknetwork.core.algorithm;

import java.util.Optional;

import com.mathworks.toolbox.javabuilder.MWException;

import br.ita.airgroup.israel.epucknetwork.core.command.PutSelector;
import br.ita.airgroup.israel.epucknetwork.helper.Logger;
import br.ita.airgroup.israel.epucknetwork.helper.file.Properties;
import br.ita.airgroup.israel.epucknetwork.matlab.MatlabControlFacade;

public class MatlabAlgorithm extends TopologyAlgorithm {

	private static Optional<MatlabControlFacade> matlab = Optional.empty();

	protected float threshold, epsilon, range;

	MatlabAlgorithm(Integer id) {
		super(id);
	}

	public void setThreshold(float threshold) {
		this.threshold = threshold;
	}

	public void setEpsilon(float epsilon) {
		this.epsilon = epsilon;
	}

	public float getThreshold() {
		return threshold;
	}

	public float getEpsilon() {
		return epsilon;
	}

	public void setRange(float range) {
		this.range = range;
	}

	public float getRange() {
		return range;
	}

	@Override
	public void start(float... configs) {

		if (configs.length == 0) {
			configs = getDefaultConfigs();
		}

		setThreshold(configs[0]);
		setEpsilon(configs[1]);
		setRange(configs[2]);

		startMatlab();
		
		super.start();
	}

	private float[] getDefaultConfigs() {
		float[] configs;
		configs = new float[] { Properties.getAsFloat(PutSelector.ALGORITHM_CONFIGS, 0),
				Properties.getAsFloat(PutSelector.ALGORITHM_CONFIGS, 2),
				Properties.getFirstAsFloat(PutSelector.RANGE) };
		return configs;
	}

	public void stop(boolean shutdownMatlab) {
		super.stop();
		
		if(shutdownMatlab) {
			stopMatlab();
		}
	}

	public void startMatlab() {
		
		if(matlab.isPresent()) {
			return;
		}
		
		try {
			matlab = Optional.of(new MatlabControlFacade());
			topology.ifPresent(t -> t.getEpucks().forEach(e -> e.setMatlab(matlab)));
		} catch (MWException e) {
			e.printStackTrace();
			Logger.error(this.getClass(), e.getMessage());
		}
	}

	public void stopMatlab() {
		matlab.ifPresent(m -> MatlabControlFacade.shutdown());
		topology.ifPresent(t -> t.getEpucks().forEach(e -> e.setMatlab(Optional.empty())));
		matlab = Optional.empty();
	}

}
