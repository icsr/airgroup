package br.ita.airgroup.israel.epucknetwork.helper.message;

@FunctionalInterface
public interface CommandListenable {

	void run();
	
}
