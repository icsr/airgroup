package br.ita.airgroup.israel.epucknetwork.gui.components;

import java.io.IOException;
import java.util.Optional;

import javax.swing.JRadioButtonMenuItem;

import br.ita.airgroup.israel.epucknetwork.core.algorithm.Algorithm;
import br.ita.airgroup.israel.epucknetwork.core.algorithm.CSVBasedInitialTopologyAlgorithm;
import br.ita.airgroup.israel.epucknetwork.core.algorithm.NonAutonomyAlgorithm;
import br.ita.airgroup.israel.epucknetwork.core.controller.Topology;
import br.ita.airgroup.israel.epucknetwork.core.imaging.Camera;
import br.ita.airgroup.israel.epucknetwork.helper.exception.EmptyException;
import br.ita.airgroup.israel.epucknetwork.helper.exception.SettingsException;
import br.ita.airgroup.israel.epucknetwork.helper.file.Properties;
import br.ita.airgroup.israel.epucknetwork.helper.file.Properties.Key;
import br.ita.airgroup.israel.epucknetwork.helper.observer.Observable;
import br.ita.airgroup.israel.epucknetwork.helper.observer.Observer;

public class AlgorithmRadioButtonMenuItem extends JRadioButtonMenuItem implements Observer<Algorithm> {

	private static final long serialVersionUID = 879023989524876692L;
	private Algorithm algorithm;
	private Optional<Observable<Algorithm>> observable = Optional.empty();
	private Topology topology;
	private Camera camera;

	public AlgorithmRadioButtonMenuItem(Algorithm algorithm, Topology topology, Camera camera) {
		super(algorithm.getClass().getSimpleName());
		this.algorithm = algorithm;
		this.topology = topology;
		this.algorithm.setTopology(topology);
		this.camera = camera;
	}

	public void setAlgorithm(boolean chooseCSV) {
		
		if(!isSelected()){
			return;
		}
		
		if(chooseCSV) {
			chooseCSVIfNecessary();
		}
		
		Properties.add(Key.TopologyAlgorithmName, algorithm.getClass().getName());
		this.topology.setAlgorithm(algorithm);
		observable.ifPresent(o -> o.notify(algorithm));
		
		setCameraWhetherToDrawTargetPostures();
	}

	private void setCameraWhetherToDrawTargetPostures() {
		if (NonAutonomyAlgorithm.class.isInstance(algorithm)) {
			NonAutonomyAlgorithm.class.cast(algorithm).observe(this.camera);
		}else {
			algorithm.stop();
		}
	}
	
	public Algorithm getAlgorithm() {
		return algorithm;
	}

	@Override
	public void observe(Observable<Algorithm> observable) {
		this.observable = Optional.of(observable);
		
		if(Properties.get(Key.TopologyAlgorithmName).equals(algorithm.getClass().getName())){
			this.setSelected(true);
			setAlgorithm(false);
		}
	}
	
	private Algorithm chooseCSVIfNecessary() {
		if(CSVBasedInitialTopologyAlgorithm.class.isInstance(algorithm)) {
			try {
				CSVBasedInitialTopologyAlgorithm.class.cast(algorithm).askForCSV();
			} catch (EmptyException | IOException | SettingsException e) {
				e.printStackTrace();
			}
		}
		
		return algorithm;
	}

}
