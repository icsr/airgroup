package br.ita.airgroup.israel.epucknetwork.helper.security;

import java.security.Permission;

import br.ita.airgroup.israel.epucknetwork.helper.exception.UnauthorizedException;

public class AvoidUnauthorizedExit extends SecurityManager{
	
	private static final CharSequence EXIT_ACTION_NAME = "exit";
	private static boolean activated = false;
	
	public static boolean isActivated() {
		return activated;
	}

	public static void activate() {
		activated = true;
	}
	
	public static void deactivate() {
		activated = false;
	}

	@Override
	public void checkPermission(Permission perm) {
		
		if(isActivated() && perm.getName().contains(EXIT_ACTION_NAME)){
			deactivate();
			throw new UnauthorizedException();
		}
		
	}

}