package br.ita.airgroup.israel.epucknetwork.gui.components;

import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;

import br.ita.airgroup.israel.commons.utils.MathUtils;
import br.ita.airgroup.israel.epucknetwork.helper.file.Properties;
import br.ita.airgroup.israel.epucknetwork.helper.file.Properties.Key;

public class GuiSlider extends JPanel implements Keyable<Integer, Integer> {

	private static final long serialVersionUID = 8746894416846476541L;
	int minValue;
	int actualValue;
	int maxValue;
	JSlider slider;
	private JLabel value;
	private Key key;

	public GuiSlider(String label, int minValue, int maxValue, Key associatedKey) {
		super(new FlowLayout());

		this.key = associatedKey;		
		this.setBorder(BorderFactory.createTitledBorder(label));
		this.minValue = minValue;
		this.actualValue = Properties.getAsInteger(associatedKey);
		this.maxValue = maxValue;
		
		init();
		
		whenChange(() -> {});

	}
	
	private void init() {
		
		if(this.actualValue < minValue || actualValue > maxValue){
			actualValue = (int) MathUtils.median(minValue, maxValue);
		}
		
		slider = new JSlider(minValue, maxValue, loadKey());
		value = new JLabel("99999");
		Dimension size = value.getPreferredSize();
		value.setText(Integer.toString(loadKey()));
		value.setMaximumSize(size);
		value.setMinimumSize(size);
		value.setPreferredSize(size);
		value.setSize(size);
		
		this.add(slider);
		this.add(value);
	}

	@Override
	public void whenChange(Runnable listener) {
		slider.addChangeListener(a -> {
			saveKey(slider.getValue());
			value.setText(Integer.toString(slider.getValue()));
			listener.run();
		});
	}

	public void setLabel(String label) {
		value.setText(label);
	}

	public float getValue(boolean normalized) {
		if(!normalized) {
			return getValue();
		}
			
		return (float) MathUtils.divide(getValue(), getRange());
	}

	public double getRange() {
		return slider.getMaximum() - slider.getMinimum();
	}

	@Override
	public Integer loadKey() {
		return Properties.getAsInteger(key);
	}

	@Override
	public void saveKey(Integer value) {
		Properties.add(key, slider.getValue());
	}

	@Override
	public Integer getValue() {
		return slider.getValue();
	}

	@Override
	public void setValue(Integer value) {
		slider.setValue(value);
	}

}
