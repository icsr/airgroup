package br.ita.airgroup.israel.epucknetwork.matlab.helper;

import br.ita.airgroup.israel.commons.utils.MathUtils;

public class RandomUControl extends UControl{

	public RandomUControl() {
		x = MathUtils.randomize(0, 1);
		y = MathUtils.randomize(0, 1);
		
		x = x / MathUtils.norm(x,y);
		y = y / MathUtils.norm(x,y);
	}

}
