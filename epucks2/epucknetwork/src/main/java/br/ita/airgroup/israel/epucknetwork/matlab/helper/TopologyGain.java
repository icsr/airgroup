package br.ita.airgroup.israel.epucknetwork.matlab.helper;

import java.util.Arrays;
import java.util.List;

public class TopologyGain {

	private float connectivity;
	private float robustness;
	private float coverageArea;
	private float collisionAvoidance;

	public static TopologyGain newInstance(float connectivity, float robustness, float coverageArea,
			float collisionAvoidance) {
		return new TopologyGain(connectivity, robustness, coverageArea, collisionAvoidance);
	}

	public TopologyGain(float connectivity, float robustness, float coverageArea, float collisionAvoidance) {
		setConnectivity(connectivity);
		setRobustness(robustness);
		setCoverageArea(coverageArea);
		setCollisionAvoidance(collisionAvoidance);
	}

	public TopologyGain(float[] gains) {
		this(gains[0], gains[1], gains[2], gains[3]);
	}

	public float getConnectivity() {
		return connectivity;
	}

	public void setConnectivity(float connectivity) {

		if (connectivity < 0) {
			throw new IllegalArgumentException();
		}

		this.connectivity = connectivity;
	}

	public float getRobustness() {
		return robustness;
	}

	public void setRobustness(float robustness) {
		if (robustness < 0) {
			throw new IllegalArgumentException();
		}
		this.robustness = robustness;
	}

	public float getCoverageArea() {
		return coverageArea;
	}

	public void setCoverageArea(float coverageArea) {
		if (coverageArea < 0) {
			throw new IllegalArgumentException();
		}
		this.coverageArea = coverageArea;
	}

	public float getCollisionAvoidance() {
		return collisionAvoidance;
	}

	public void setCollisionAvoidance(float collisionAvoidance) {
		if (collisionAvoidance < 0) {
			return;
		}
		this.collisionAvoidance = collisionAvoidance;
	}

	public Float[] toArray() {
		return new Float[] { connectivity, robustness, coverageArea };
	}

	public String getLabel() {

		StringBuilder str = new StringBuilder();

		str.append(getLabel(connectivity, "C"));
		str.append(getLabel(robustness, "R"));
		str.append(getLabel(coverageArea, "A"));

		return str.toString();
	}

	public List<Float> asList() {
		return Arrays.asList(connectivity, robustness, coverageArea, collisionAvoidance);
	}

	private String getLabel(float aspect, String label) {
		if (aspect == 0) {
			return "";
		} else if (aspect != 1) {
			return label.toLowerCase();
		} else {
			return label;
		}
	}

}
