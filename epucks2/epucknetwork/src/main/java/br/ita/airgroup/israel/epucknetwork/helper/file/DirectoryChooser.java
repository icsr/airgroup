package br.ita.airgroup.israel.epucknetwork.helper.file;

import java.util.Optional;

import javax.swing.JFileChooser;

public class DirectoryChooser extends FileChooser {

	public DirectoryChooser(String title, Optional<String> initFolder) {
		super(title, initFolder);
		config();
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 6618455681145473464L;

	@Override
	public void config() {
		setMultiSelectionEnabled(false);
		setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
	}

	@Override
	public void approveSelection() {
		if (this.getSelectedFile().isFile() || !this.getSelectedFile().isDirectory()) {
			this.setSelectedFile(this.getSelectedFile().getParentFile());
		} else {
			super.approveSelection();
		}
	}

}
