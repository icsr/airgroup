package br.ita.airgroup.israel.epucknetwork.helper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import br.ita.airgroup.israel.commons.utils.FileUtils;
import br.ita.airgroup.israel.epucknetwork.core.imaging.Arena;

public class PostureTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	private Posture posture;
	private int angle;
	private int y;
	private int x;

	@Before
	public void setUp() throws Exception {
		x = 10;
		y = 10;
		angle = 1;
		this.posture = new Posture(x, y, angle);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testAsList() {
		List<Integer> asList = posture.asList();

		assertEquals(3, asList.size());

		assertEquals((long) x, (long) asList.get(0));
		assertEquals((long) y, (long) asList.get(1));
		assertEquals((long) angle, (long) asList.get(2));

	}

	@Test
	public void testGetPoint() {
		assertEquals(new Point(x, y), posture.getPoint());
	}

	@Test
	public void testGetAngle() {
		assertEquals(45, Posture.getAngle(10, 10), 0);
		assertEquals(180 - 45, Posture.getAngle(-10, 10), 0);
		assertEquals(180 + 45, Posture.getAngle(-10, -10), 0);
		assertEquals(360 - 45, Posture.getAngle(10, -10), 0);

		assertEquals(0, Posture.getAngle(10, 0), 0);
		assertEquals(180, Posture.getAngle(-10, 0), 0);
		assertEquals(90, Posture.getAngle(0, 10), 0);
		assertEquals(270, Posture.getAngle(0, -10), 0);

		assertEquals(360 + 180 / Math.PI * Math.atan2(-5, 10), Posture.getAngle(10, -5), 1);
	}

	@Test
	public void testParsePosture() {

		int px = 10;
		int py = 5;
		int pangle = 50;
		Posture parsePosture = Posture.parsePosture(String.format("jfkjkf,jkjfdkfj,%s,%s,%s", px, py, pangle), 2);

		assertEquals(px, parsePosture.x);
		assertEquals(py, parsePosture.y);
		assertEquals(pangle, parsePosture.angle);

		parsePosture = Posture.parsePosture(String.format("jfkjkf,jkjfdkfj, jlkjlkj, %s,%s,%s", px, py, pangle), 3);

		assertEquals(px, parsePosture.x);
		assertEquals(py, parsePosture.y);
		assertEquals(pangle, parsePosture.angle);

	}

	@Test
	public void testmirrorAngle() {

		assertEquals(-1, Posture.mirrorAngle(-1));
		assertEquals(1, Posture.mirrorAngle(1));
		assertEquals(-1, Posture.mirrorAngle(-361));
		assertEquals(1, Posture.mirrorAngle(361));

	}

	@Test
	public void testAnglesEquals() {
		assertTrue(Posture.anglesEquals(-1, 1));
		assertTrue(Posture.anglesEquals(0, 0));
		assertTrue(Posture.anglesEquals(0, 1));
		assertTrue(Posture.anglesEquals(0, -1));
		assertTrue(Posture.anglesEquals(0, 2));
		assertTrue(Posture.anglesEquals(0, -2));
		assertTrue(Posture.anglesEquals(1, 2));
		assertTrue(Posture.anglesEquals(-1, -2));
		assertTrue(Posture.anglesEquals(-2, -1));
		assertTrue(Posture.anglesEquals(-2, -2));
		assertFalse(Posture.anglesEquals(-2, 2));
		assertFalse(Posture.anglesEquals(-1, 2));
	}

	@Test
	public void testEqualsObject() {
		Posture p = new Posture(posture);
		assertEquals(p, posture);
	}

	@Test
	public void testSavePostures() {
		List<Posture> postures = new ArrayList<>();
		postures.add(new Posture(20, 50, 0));
		postures.add(new Posture(55, 50, 0));

		BufferedImage image = Posture.drawPosturesOnImage(800, postures, new Arena(0, 0, 100, 100, 1f), 20, true);
		try {
			FileUtils.saveImage(Paths.get("/tmp/", "teste.png"), image);
		} catch (IOException e) {
			fail(e.getMessage());
		}
		
	}

}
