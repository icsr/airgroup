package br.ita.airgroup.israel.epucknetwork.core.algorithm;

import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import br.ita.airgroup.israel.epucknetwork.core.imaging.TestUtils;

public class SquareGroupTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

	}

	private SquareGroup squareGroup;

	@Before
	public void setUp() throws Exception {

		squareGroup = new SquareGroup();
		squareGroup.setTopology(TestUtils.mockTopologyForAlgorithmTest());
		squareGroup.waitForEachLine = false;
	}

	@After
	public void tearDown() throws Exception {
	}

	@Ignore
	@Test
	public void testSquaredGroup() {
		squareGroup.start();
		try {
			TestUtils.drawAndCheckPoints(SquareGroup.class.getSimpleName());
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

}
