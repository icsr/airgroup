package br.ita.airgroup.israel.epucknetwork.core.algorithm;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import br.ita.airgroup.israel.epucknetwork.core.controller.Topology;
import br.ita.airgroup.israel.epucknetwork.core.imaging.Arena;
import br.ita.airgroup.israel.epucknetwork.core.imaging.TestUtils;
import br.ita.airgroup.israel.epucknetwork.helper.Posture;
import br.ita.airgroup.israel.epucknetwork.helper.PostureRandomizer;

public class CSVBasedInitialTopologyAlgorithmTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	private CSVBasedInitialTopologyAlgorithm algorithm;
	private int qtyOfEpucks;
	private Arena arena;
	private PostureRandomizer postureRandomizer;
	private Path csv;

	@Before
	public void setUp() throws Exception {
		
		Topology mockedTopology = TestUtils.mockTopologyForAlgorithmTest();
		
		qtyOfEpucks = mockedTopology.getEpucks().size();
		arena = new Arena(20, 20, 120, 120, 1f / 5f);
		postureRandomizer = new PostureRandomizer(qtyOfEpucks, 15, 20, arena, 0.2);
		postureRandomizer.random();

		csv = Paths.get("/tmp/", "teste.csv");
		postureRandomizer.saveToCSV(csv);

		algorithm = new CSVBasedInitialTopologyAlgorithm(csv);
		algorithm.setTopology(mockedTopology);
		
		algorithm.readInitialTopology();

	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCSVReading() {

		List<Posture> postures = algorithm.getPostures();
		assertEquals(qtyOfEpucks, postures.size());

		postures.forEach(p -> {
			assertTrue(arena.isInsideCm(p.getPoint()));
		});

	}

	@Ignore
	@Test
	public void testGrouping() {
		algorithm.start();

		try {
			TestUtils.drawAndCheck(CSVBasedInitialTopologyAlgorithm.class.getSimpleName(), postureRandomizer.toPanel());
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
	
}
