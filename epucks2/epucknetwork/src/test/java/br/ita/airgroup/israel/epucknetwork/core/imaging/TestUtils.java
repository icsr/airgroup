package br.ita.airgroup.israel.epucknetwork.core.imaging;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import br.ita.airgroup.israel.commons.utils.MathUtils;
import br.ita.airgroup.israel.epucknetwork.core.controller.BluetoothHandler;
import br.ita.airgroup.israel.epucknetwork.core.controller.Epuck;
import br.ita.airgroup.israel.epucknetwork.core.controller.Topology;
import br.ita.airgroup.israel.epucknetwork.core.imaging.Arena;
import br.ita.airgroup.israel.epucknetwork.gui.components.PointsCanvas;
import br.ita.airgroup.israel.epucknetwork.helper.Posture;

public class TestUtils {

	public static final int GROUP_MEMBER_PER_LINE = 4;
	public static final int GROUP_MEMBER_SEPARATION = 40;
	public static final int ARENA_SIZE = 800;
	public static final int GROUP_SIZE = 10;
	public static List<Point> points = new ArrayList<>();
	public static Arena arena;

	public static Topology mockTopologyForAlgorithmTest() {
		points.clear();
		Topology mockedTopology = mock(Topology.class);
		arena = new Arena(new Point(0, 0), new Point(ARENA_SIZE, ARENA_SIZE));

		Epuck.clearKnownEpucks();

		doReturn(Optional.of(arena)).when(mockedTopology).getArena();
		doReturn(Integer.valueOf(GROUP_SIZE)).when(mockedTopology).size();

		doAnswer(a -> {

			List<BluetoothHandler> list = new ArrayList<>();

			IntStream.range(0, GROUP_SIZE).forEach(i -> {
				BluetoothHandler mockedHandler = mock(BluetoothHandler.class);

				Epuck mockedEpuck = mock(Epuck.class);
				doReturn(new Posture((int) MathUtils.randomize(10, 100), (int) MathUtils.randomize(10, 100),
						(int) MathUtils.randomize(10, 180))).when(mockedEpuck).getPostureCm();

				doReturn(mockedEpuck).when(mockedHandler).getEpuck();

				doAnswer(m -> {

					doReturn(new Posture((Integer) m.getArguments()[0], (Integer) m.getArguments()[1], 0))
							.when(mockedEpuck).getPostureCm();

					points.add(new Point((Integer) m.getArguments()[0], (Integer) m.getArguments()[1]));

					return true;

				}).when(mockedHandler).sendMoveCommand(anyInt(), anyInt());

				list.add(mockedHandler);
			});

			return list;

		}).when(mockedTopology).getBluetooths();

		ArrayList<Epuck> mockedEpuckList = new ArrayList<>();
		IntStream.range(0, GROUP_SIZE).forEach(i -> {
			mockedEpuckList.add(mock(Epuck.class));
		});

		doReturn(mockedEpuckList).when(mockedTopology).getEpucks();

		return mockedTopology;
	}

	public static void drawAndCheckPoints(String className) throws Exception {

		JPanel panel = new JPanel(new BorderLayout());

		panel.add(new PointsCanvas(points, new Dimension(arena.getWidth(true), arena.getHeight(true))),
				BorderLayout.CENTER);
		drawAndCheck(className, panel);

	}

	public static void drawAndCheck(String className, JPanel panel) throws Exception {
		panel.add(new JLabel("Is this lining up correct?"), BorderLayout.SOUTH);

		panel.setPreferredSize(new Dimension(arena.getWidth(true), arena.getHeight(true)));

		int op = JOptionPane.showConfirmDialog(null, panel, String.format("%s %d members in %dx%d", className,
				GROUP_SIZE, (GROUP_SIZE / GROUP_MEMBER_PER_LINE), GROUP_MEMBER_PER_LINE), JOptionPane.YES_NO_OPTION);
		if (JOptionPane.YES_OPTION != op) {
			throw new Exception("Fail!");
		}
	}

}
