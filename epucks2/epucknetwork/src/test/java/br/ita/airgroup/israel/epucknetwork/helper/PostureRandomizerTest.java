package br.ita.airgroup.israel.epucknetwork.helper;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import br.ita.airgroup.israel.commons.utils.FileUtils;
import br.ita.airgroup.israel.epucknetwork.core.imaging.Arena;

public class PostureRandomizerTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testRandom() throws Exception {

		Arena arena = new Arena(20, 20, 140, 140, 1f / 5f);
		int qtyOfEpucks = 5;
		int numberOfPostures = qtyOfEpucks;
		int safeDistance = 18;

		int connectionRange = 20;
		double algebraicConnectivityLowerBound = 0.05;

		File savingFolder = FileUtils.getTempDir();
		PostureRandomizer.savingFolder = savingFolder.toPath();

		List<File> generatedRandomPostures = PostureRandomizer.generateRandomPostures(qtyOfEpucks, safeDistance, numberOfPostures, arena,
				connectionRange, algebraicConnectivityLowerBound);

		generatedRandomPostures.forEach(f -> {
			try {
				assertTrue(f.exists() && f.isFile());
				assertTrue(PostureRandomizer.isRightlyFormatted(f.toPath()));
			} catch (IOException e) {
				e.printStackTrace();
				fail("File not readable.");
			}
		});

	}

}
