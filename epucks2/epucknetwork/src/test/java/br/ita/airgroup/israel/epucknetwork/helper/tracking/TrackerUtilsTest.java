package br.ita.airgroup.israel.epucknetwork.helper.tracking;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

import java.awt.Point;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import br.ita.airgroup.israel.epucknetwork.core.imaging.Arena;
import georegression.struct.point.Point2D_F64;

public class TrackerUtilsTest {

	private static final int OFFSET_X = 100;
	private static final int OFFSET_Y = 150;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	private Arena mockedArena;

	@Before
	public void setUp() throws Exception {
		mockedArena = mock(Arena.class);
		doReturn(new Point(OFFSET_X, OFFSET_Y)).when(mockedArena).getOffset(false);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCameraPositionInPxToRelativePositionInCmViceVersa() {
		checkCameraPositionInPxToRelativePositionInCmViceVersa(500, 400, 5d/25d);
		checkCameraPositionInPxToRelativePositionInCmViceVersa(200, 100, 5d/27d);
		checkCameraPositionInPxToRelativePositionInCmViceVersa(100, 100, 5d/30d);
		checkCameraPositionInPxToRelativePositionInCmViceVersa(0, 0, 5d/30d);
		checkCameraPositionInPxToRelativePositionInCmViceVersa(100, 100, 1);
		checkCameraPositionInPxToRelativePositionInCmViceVersa(159, 120, 0.9);
	}

	private void checkCameraPositionInPxToRelativePositionInCmViceVersa(int x, int y, double adjustFactorInCmPerPx) {
		Point pointInPx = new Point(500, 400);
		Point relativePositionInCm = TrackerUtils.cameraPositionInPxToAbsolutePositionInCm(mockedArena, pointInPx,
				adjustFactorInCmPerPx);

		assertEquals((pointInPx.x - mockedArena.getOffset(false).x) * adjustFactorInCmPerPx, relativePositionInCm.x, 0.5);
		assertEquals((pointInPx.y - mockedArena.getOffset(false).y) * adjustFactorInCmPerPx, relativePositionInCm.y, 0.5);
		
		Point2D_F64 cameraPositionInPx = TrackerUtils.absolutePositionInCmTocameraCoordinateInPx(mockedArena,
				relativePositionInCm, adjustFactorInCmPerPx);
		assertEquals(pointInPx.x, cameraPositionInPx.x, 5);
		assertEquals(pointInPx.y, cameraPositionInPx.y, 5);
	}

}
