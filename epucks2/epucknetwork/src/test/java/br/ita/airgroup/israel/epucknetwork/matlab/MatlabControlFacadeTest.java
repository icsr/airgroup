// Inserir nos argumentos da VM: -Djava.library.path="/opt/matlab/bin/glnxa64/"
// e nas variáveis de ambiente: LD_LIBRARY_PATH=/opt/mcr/v92/runtime/glnxa64/

package br.ita.airgroup.israel.epucknetwork.matlab;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.mathworks.toolbox.javabuilder.MWException;

import br.ita.airgroup.israel.commons.clock.Clock;
import br.ita.airgroup.israel.commons.coord.PairOfDoubles;
import br.ita.airgroup.israel.commons.coord.Position;
import br.ita.airgroup.israel.epucknetwork.core.controller.Epuck;
import br.ita.airgroup.israel.epucknetwork.helper.AlgorithmConfigs;
import br.ita.airgroup.israel.epucknetwork.helper.BaseSystem;
import br.ita.airgroup.israel.epucknetwork.helper.file.Properties;
import br.ita.airgroup.israel.epucknetwork.helper.file.Properties.Key;
import br.ita.airgroup.israel.epucknetwork.matlab.helper.TopologyGain;
import br.ita.airgroup.israel.epucknetwork.matlab.helper.UControl;

public class MatlabControlFacadeTest {

	private static final float MAX_ERROR = (float) Math.pow(10, -4);
	private static MatlabControlFacade matlab;
	private static List<Epuck> epucks;
	private static float previousRangeSafety;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		BaseSystem.init();
		
		previousRangeSafety = Properties.getAsFloat(Key.RangeSafety);
		
		Properties.add(Key.RangeSafety, 0f);
		Properties.saveIt();
		
		assertEquals(AlgorithmConfigs.getRange(), AlgorithmConfigs.getSafetyRange(), 0);

		matlab = new MatlabControlFacade();

		epucks = IntStream.rangeClosed(1, 5).mapToObj(i -> {
			try {
				Epuck e = new Epuck(i, Integer.toString(i), Integer.toString(i));
				e.setPostureInCm(5 * i, 5 * i, 0);
				return e;
			} catch (Exception e) {
				return null;
			}
		}).collect(Collectors.toList());
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		Properties.add(Key.RangeSafety, previousRangeSafety);
		Properties.saveIt();
//		MatlabControlFacade.shutdown();
		
	}

	private List<Position> positions1;
	private List<Position> positions2;
	private List<Position> positions3;
	private List<Position> positions4;

	@Before
	public void setUp() throws Exception {

		float[] X = MatlabControlFacade.toArray(-30, 30, -20, 20, -30, 30);
		float[] Y = MatlabControlFacade.toArray(30, 30, 0, 0, -30, -30);
		positions1 = MatlabControlFacade.toList(X, Y);

		X = MatlabControlFacade.toArray(-5, 5);
		Y = MatlabControlFacade.toArray(0, 0);
		positions2 = MatlabControlFacade.toList(X, Y);

		X = MatlabControlFacade.toArray(-5, 5, 0, 0);
		Y = MatlabControlFacade.toArray(0, 0, 5, -5);
		positions3 = MatlabControlFacade.toList(X, Y);

		X = MatlabControlFacade.toArray(-5, 5, 0, 0);
		Y = MatlabControlFacade.toArray(0, 0, 6, -3);
		positions4 = MatlabControlFacade.toList(X, Y);

	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testPerformControlListOfEpuck() {

		try {
			matlab.performControl(epucks.stream().map(e -> e.getPostureCm().getPos()).collect(Collectors.toList()),
					TopologyGain.newInstance(1, 0, 0, 0));
		} catch (MWException e) {
			e.printStackTrace();
			fail("Was Matlab Runtime Control correctly configured?");
		}

	}

	@Test
	public void testPosition1WithGains111() {
		
		assertEquals(0.2f, AlgorithmConfigs.getThreshold(), 0);
		assertEquals(20, AlgorithmConfigs.getRange(), 0);
		assertEquals(AlgorithmConfigs.getEpsilon(), AlgorithmConfigs.getThreshold(), 0);
		
		float[] X = MatlabControlFacade.toArray(0.40266326, -0.40266326, 1, 0, 0.4026600, -0.40266326);
		float[] Y = MatlabControlFacade.toArray(-0.91534823, -0.91534823, 0, 1, 0.9153500, 0.91534823);
		proceedTest(X, Y, positions1, TopologyGain.newInstance(1f, 1f, 1f, 1f));
	}


	@Test
	public void testPosition4WithGains001() {

		
		float[] expectedX = MatlabControlFacade.toArray(-0.8547800, 0.831229, 0.101020, 0.293799);
		float[] exprectedY = MatlabControlFacade.toArray(0.5189899, 0.555920, 0.994880, -0.9558699);
		proceedTest(expectedX, exprectedY, positions4, TopologyGain.newInstance(0f, 0f, 1f, 0f));
	}

	@Test
	public void testPosition1WithGains100() {

		float[] X = MatlabControlFacade.toArray(0.316227766016838, -0.316227766016838, 1.000000000000000,
				-1.000000000000000, 0.316227766016838, -0.316227766016838);
		float[] Y = MatlabControlFacade.toArray(-0.948683298050514, -0.948683298050514, -0.000000000000000,
				0.000000000000000, 0.948683298050514, 0.948683298050514);
		proceedTest(X, Y, positions1, TopologyGain.newInstance(1f, 0f, 0f, 0f));
	}

	private void proceedTest(float[] expectedX, float[] expectedY, List<Position> pos, TopologyGain gain) {
		try {
			List<PairOfDoubles> expectedDotxy1 = toList(expectedX, expectedY);

			Clock.tic();
			List<UControl> evaluatedDotxy1 = matlab.performControl(pos, gain);
			long elapsedTime = Clock.toc();
			System.out.println(String.format("Topology Control has performed in %d ms", elapsedTime));

			checkEquals(expectedDotxy1, evaluatedDotxy1);
		} catch (MWException e) {
			e.printStackTrace();
			fail("Matlab error! Check stack trace.");
		}
	}

	private void checkEquals(List<PairOfDoubles> expectedDotxy1, List<UControl> evaluatedDotxy1) {

		assertEquals(expectedDotxy1.size(), evaluatedDotxy1.size());

		for (int i = 0; i < expectedDotxy1.size(); i++) {
			assertEquals(expectedDotxy1.get(i).x, evaluatedDotxy1.get(i).x, MAX_ERROR);
			assertEquals(expectedDotxy1.get(i).y, evaluatedDotxy1.get(i).y, MAX_ERROR);
		}

	}

	public static List<PairOfDoubles> toList(float[] X, float[] Y) {

		List<PairOfDoubles> list = new ArrayList<>();
		IntStream.range(0, X.length).forEach(i -> list.add(new PairOfDoubles(X[i], Y[i])));

		return list;

	}

}
