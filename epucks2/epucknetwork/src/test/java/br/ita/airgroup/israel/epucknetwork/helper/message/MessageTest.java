package br.ita.airgroup.israel.epucknetwork.helper.message;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import br.ita.airgroup.israel.commons.utils.StringUtils;

public class MessageTest {
	
	private static final int ETB = 0x17;

	private static final int ETX = 0x03;

	private static final int STX = 0x02;

	private static final int SYN = 0x16;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testMessageFormat() {
		
		String content = "content";
		
		String header = new String(new byte[] {(byte)SYN, (byte)STX});
		String footer = new String(new byte[] {(byte)ETX, (byte)ETB});
		
		assertEquals(StringUtils.join(header, content, footer), new String(Message.dinamicMessage(content)));
	}
	
	@Test
	public void testExtractMessages() {
		
		String c1 = "M,1,2,3,4";
		String c2 = "G";
		String c3 = "0";
		
		StringBuilder str = new StringBuilder();
		append(c1, str);
		append(c2, str);
		append("[[[" + c3 + "]]]", str);
		append("][", str);
		append("]fdfdfdfdfxxx    x[", str);
		append("]fdfdfdfdfxxx    x[" + c3 + "]", str);
		
		List<String> extractMessages = Message.extractMessages(str.toString());
		
		assertEquals(8, extractMessages.size());
		
		assertEquals(c1, extractMessages.get(0));
		assertEquals(c2, extractMessages.get(1));
		assertEquals(c3, extractMessages.get(2));
		assertEquals(c3, extractMessages.get(3));
		
	}

	private StringBuilder append(String c1, StringBuilder str) {
		return str.append(String.format("%s%s%s", Message.START_MESSAGE, c1, Message.END_MESSAGE));
	}

}
