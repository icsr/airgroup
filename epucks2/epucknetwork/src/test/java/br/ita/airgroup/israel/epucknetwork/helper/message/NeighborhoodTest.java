package br.ita.airgroup.israel.epucknetwork.helper.message;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import br.ita.airgroup.israel.epucknetwork.core.controller.Epuck;
import br.ita.airgroup.israel.epucknetwork.helper.Posture;

public class NeighborhoodTest {

	private List<Epuck> epucks = new ArrayList<>();
	private Neighborhood neighborhood;

	@Before
	public void setUp() throws Exception {
		Epuck.clearKnownEpucks();

		epucks.add(new Epuck(1, "0", "0", new Posture( 1,2,15)));
		epucks.add(new Epuck(2, "1", "0", new Posture(10,5,90)));
		epucks.add(new Epuck(3, "2", "0", new Posture(5,10,180)));
		epucks.add(new Epuck(4, "4", "0", new Posture(5,-5,-2)));
		
		neighborhood = new Neighborhood(epucks.get(0));
	}

	@Test
	public void testToString() {
		assertEquals("4,1,2,15,10,5,0.0,5,10,0.0,5,-5,0.0,0.0,0.0", neighborhood.toString());
		assertEquals("4,10,5,90,1,2,0.0,5,10,0.0,5,-5,0.0,0.0,0.0", new Neighborhood(epucks.get(1)).toString());		
	}

	@Test
	public void testExpectedListSize() {
		assertEquals(3 * (epucks.size() - 1) + 6, Neighborhood.expectedListSize(epucks.size()));
	}

}
