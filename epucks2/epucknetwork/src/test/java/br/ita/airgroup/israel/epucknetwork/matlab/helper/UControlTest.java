package br.ita.airgroup.israel.epucknetwork.matlab.helper;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class UControlTest {

	private UControl ucontrol;

	@Before
	public void setUp() throws Exception {
		this.ucontrol = new UControl();
	}

	@Test
	public void testIsNull() {
		assertTrue(this.ucontrol.isNull());
	}

}
