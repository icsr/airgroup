package br.ita.airgroup.israel.epucknetwork.core.command;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyFloat;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import br.ita.airgroup.israel.epucknetwork.core.controller.Epuck;
import br.ita.airgroup.israel.epucknetwork.helper.Posture;
import br.ita.airgroup.israel.epucknetwork.helper.message.Message;
import br.ita.airgroup.israel.epucknetwork.helper.message.Messenger;

@RunWith(MockitoJUnitRunner.class)
public class EpuckCommandTest {

	private Epuck mockedEpuck1;
	private List<Epuck> allEpucks = new ArrayList<>();
	private EpuckCommand epuckCommand;
	private Float answer;

	@Before
	public void setUp() throws Exception {
		
		Messenger.setActive(false);

		mockedEpuck1 = mock(Epuck.class);
		doAnswer(new Answer<Void>() {

			@Override
			public Void answer(InvocationOnMock invocation) throws Throwable {

				answer = (Float) invocation.getArguments()[0];

				return null;
			}
		}).when(mockedEpuck1).setSecondEigenVector(anyFloat());

		doCallRealMethod().when(mockedEpuck1).getKnownEpucksPositionsAsString();
		doCallRealMethod().when(mockedEpuck1).getNeighborhoodAsList();

		doReturn(new Posture(10, 20, 0)).when(mockedEpuck1).getPostureCm();
		doReturn(0).when(mockedEpuck1).getId();

		doAnswer(new Answer<List<Epuck>>() {

			@Override
			public List<Epuck> answer(InvocationOnMock invocation) throws Throwable {

				return allEpucks;
			}
		}).when(mockedEpuck1).getKnownOthersEpucks();

		doReturn(0.12f).when(mockedEpuck1).getSecondEigenVector();

		Epuck mockedEpuck2 = mock(Epuck.class);
		doReturn(new Posture(10, 20, 0)).when(mockedEpuck2).getPostureCm();
		doReturn(1).when(mockedEpuck2).getId();

		allEpucks.add(mockedEpuck1);
		allEpucks.add(mockedEpuck2);

		Epuck.allEpucks.addAll(allEpucks);

		epuckCommand = new EpuckCommand(null, mockedEpuck1);

	}

	@Test
	public void testSaveSecondEigenVector() {

		Float[] expectedAnswer = new Float[] { 5f, 0.87f, -0.23f, 0.45f, 0.02f, 0.0f };

		int index = 4;
		
		String setEigenVectorPlainCommand = "V," + expectedAnswer[index];
		assertTrue(Pattern.matches(EpuckCommand.SECOND_EIGENVECTOR_SET_COMMAND_REGEX, setEigenVectorPlainCommand));

		epuckCommand.saveSecondEigenVector(setEigenVectorPlainCommand);

		assertEquals(expectedAnswer[index], answer);

	}

	@Test
	public void testIsReceivableCommand() {
		assertTrue(epuckCommand.isReceivableCommand(
				String.format("%c%s%s", Selector.NONE.selector, Message.SEPARATOR, "Olá enfermeira!")));
		
		assertTrue(epuckCommand.isReceivableCommand(
				String.format("%c%s%s", Selector.NONE.selector, Message.SEPARATOR, "Olá enfermeira!,123")));
		
		assertFalse(epuckCommand.isReceivableCommand(
				String.format("%c%s%s", Selector.ANSWER.selector, Message.SEPARATOR, "Olá enfermeira!,123")));
		
		assertTrue(epuckCommand.isReceivableCommand(
				String.format("%c%s%d", Selector.ANSWER.selector, Message.SEPARATOR, 1)));
		
		
	}

}
