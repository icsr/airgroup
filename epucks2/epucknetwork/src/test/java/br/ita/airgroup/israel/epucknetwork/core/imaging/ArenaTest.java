package br.ita.airgroup.israel.epucknetwork.core.imaging;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class ArenaTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	private Arena arena;
	private Point origin;
	private Point diagonal;

	@Before
	public void setUp() throws Exception {
		origin = new Point(0,0);
		diagonal = new Point(10,20);
		this.arena = new Arena(origin, diagonal);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testIsInside() {

		assertFalse(arena.isInsidePx(new Point(1,21)));
		assertFalse(arena.isInsidePx(new Point(9,21)));
		assertTrue(arena.isInsidePx(new Point(9,0)));
		assertFalse(arena.isInsidePx(new Point(0,20)));
		assertTrue(arena.isInsidePx(new Point(1,19)));
		assertFalse(arena.isInsidePx(diagonal));
		assertFalse(arena.isInsidePx(origin));
		assertTrue(arena.isInsidePx(new Point(1,9)));
		assertTrue(arena.isInsidePx(new Point(9,1)));
		assertTrue(arena.isInsidePx(new Point(5,5)));
		
		assertEquals(new Point(5,10), Arena.calculateCenterOf(arena.asListPx()));
		
		
		List<Point> points = new ArrayList<>();
		points.add(new Point(3,3));
		points.add(new Point(5,5));
		points.add(new Point(3,5));
		points.add(new Point(5,3));
		
		assertTrue(arena.isInsidePx(Arena.calculateCenterOf(points)));
	}
	
}
