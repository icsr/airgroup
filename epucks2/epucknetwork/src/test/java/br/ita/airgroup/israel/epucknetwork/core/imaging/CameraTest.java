package br.ita.airgroup.israel.epucknetwork.core.imaging;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import br.ita.airgroup.israel.epucknetwork.helper.BaseSystem;
import br.ita.airgroup.israel.epucknetwork.helper.file.Properties;
import br.ita.airgroup.israel.epucknetwork.helper.file.Properties.Key;

public class CameraTest {

	private Camera camera;
	
	@BeforeClass
	public static void tearUp() {
		BaseSystem.init();
	}
	
	@Before
	public void setUp() throws Exception {
		this.camera = new Camera(Properties.getAsOptional(Key.CameraName));
	}
	
	@After
	public void setDown() {
		this.camera.shutdown();
	}

	@Test
	public void test() {
	}

}
