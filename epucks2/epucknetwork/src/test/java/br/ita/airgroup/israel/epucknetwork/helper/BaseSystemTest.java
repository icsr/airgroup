package br.ita.airgroup.israel.epucknetwork.helper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public class BaseSystemTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testInit(){
		try {
			BaseSystem.init();
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	

	@Test
	public void testClipboard() throws Exception {
		String texto = "Texto de Exemplo bláblablá";
		BaseSystem.copyToClipboard(texto);
		
		assertEquals(texto, BaseSystem.pasteFromClipboard());
	}

	@Test
	public void testExecuteCommand() throws Exception {
		
		String message = "blablabla";
		
		List<String> answer = BaseSystem.executeCommand(String.format("echo %s", message));
		assertEquals(1, answer.size());
		assertEquals(answer.get(0), message);
	}
	
}
