package br.ita.airgroup.israel.epucknetwork.helper;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import br.ita.airgroup.israel.epucknetwork.core.command.PutSelector;

public class ExperimentInfoTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testGetAsKeyPutSelectorPutSelectorParam() {
		assertEquals("AlgorithmConfigs->ConnectivityThreshold", ExperimentInfo.getAsKey(PutSelector.ALGORITHM_CONFIGS,
				PutSelector.ALGORITHM_CONFIGS.getParams().get(0)));
	}

	@Test
	public void testGetAsKeyString() {
		assertEquals("OláTudoBem?____?", ExperimentInfo.getAsKey("olá tudo bem?====?"));
	}

}