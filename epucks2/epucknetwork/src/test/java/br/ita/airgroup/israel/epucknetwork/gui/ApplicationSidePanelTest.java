package br.ita.airgroup.israel.epucknetwork.gui;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Optional;

import javax.swing.JToggleButton;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import br.ita.airgroup.israel.epucknetwork.core.controller.Topology;
import br.ita.airgroup.israel.epucknetwork.core.imaging.Camera;
import br.ita.airgroup.israel.epucknetwork.gui.components.GuiFloat;
import br.ita.airgroup.israel.epucknetwork.gui.components.GuiText;

public class ApplicationSidePanelTest {

	private ApplicationSidePanel applicationSidePanel;
	private JToggleButton button;

	@Before
	public void setUp() throws Exception {
		
		Topology topology = Mockito.mock(Topology.class);
		Mockito.doNothing().when(topology).stop();
		
		Mockito.doReturn(anyObject()).when(topology).getGain();
		
		ApplicationGUI app = Mockito.mock(ApplicationGUI.class);
		app.topology = topology;
		
		GuiText guitext = Mockito.mock(GuiText.class);
		Mockito.doReturn("teste").when(guitext).getValue();
		
		GuiFloat guifloat = Mockito.mock(GuiFloat.class);
		Mockito.doReturn(0.1f).when(guifloat).getValue();
		
		app.experimentGroupName = guitext;
		app.experimentTimeToRun = guifloat;
				
		Camera camera = Mockito.mock(Camera.class);
		Mockito.doNothing().when(camera).stopRecording();
		app.camera = camera;
		
		app.tracker = Optional.empty();
		
		applicationSidePanel = new ApplicationSidePanel(new Dimension(400,400), app);
		button = new JToggleButton("test");
		
		
	}

	@Test
	public void testRunController() throws FileNotFoundException, IOException, InterruptedException {

		button.setSelected(true);
		applicationSidePanel.runExperiment(1);
		
		button.setSelected(false);
		applicationSidePanel.runExperiment(1);
		
		System.out.println("fim");
		
	}

}
