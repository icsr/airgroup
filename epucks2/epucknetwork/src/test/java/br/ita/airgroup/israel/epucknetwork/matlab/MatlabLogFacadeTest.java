package br.ita.airgroup.israel.epucknetwork.matlab;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import com.mathworks.toolbox.javabuilder.MWException;

import br.ita.airgroup.israel.commons.clock.Clock;
import br.ita.airgroup.israel.commons.utils.FileUtils;
import br.ita.airgroup.israel.epucknetwork.helper.exception.SettingsException;
import br.ita.airgroup.israel.epucknetwork.matlab.helper.TopologyGain;

public class MatlabLogFacadeTest {
	
	private static final int MAX_ELAPSED_TIME = 600;
	private MatlabLogFacade matlab;
	private Path logPath;

	@Before
	public void setUp() throws Exception {
		logPath = Paths.get(FileUtils.getTempDir().getAbsolutePath(), "logtest.mat");
		this.matlab = new MatlabLogFacade(logPath);
	}

	@Test
	public void logTest() throws SettingsException  {
		try {
			
			int robotId = 1234;
			long time = new Date().getTime();
			int x = 10;
			int y = 100;
			float lambda = 0.4f;
			boolean failComm = false;
			
			
			Clock.tic();
			matlab.logControl(robotId, time, x, y, lambda, MatlabControlFacade.toArray(1, -1), failComm,
					TopologyGain.newInstance(1f, 0f, 0f, 0f));
			
			matlab.logControl(9999, 1, 10, -1);
			assertTrue(Clock.toc() <= MAX_ELAPSED_TIME / 10);
			
			assertTrue(logPath.toFile().exists());
		} catch (MWException e) {
			e.printStackTrace();
			fail("Matlab error! Check stack trace.");
		}
	}

}
