package br.ita.airgroup.israel.epucknetwork.core.algorithm;

import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import br.ita.airgroup.israel.epucknetwork.core.imaging.TestUtils;

public class LinedGroupTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

	}

	private LinedGroup linedGroup;

	@Before
	public void setUp() throws Exception {
		
		linedGroup = new LinedGroup();
		linedGroup.setTopology(TestUtils.mockTopologyForAlgorithmTest());
		linedGroup.waitForEachLine = false;
	}

	@After
	public void tearDown() throws Exception {
	}

	@Ignore
	@Test
	public void testLinedGroup() {
		linedGroup.start();
		try {
			TestUtils.drawAndCheckPoints(LinedGroup.class.getSimpleName());
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

}
