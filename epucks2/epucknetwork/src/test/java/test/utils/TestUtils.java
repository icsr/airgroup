package test.utils;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.mockito.Mockito;

import br.ita.airgroup.israel.commons.utils.FileUtils;
import br.ita.airgroup.israel.commons.utils.SortedProperties;
import br.ita.airgroup.israel.epucknetwork.core.controller.BluetoothHandler;
import br.ita.airgroup.israel.epucknetwork.core.controller.Epuck;
import br.ita.airgroup.israel.epucknetwork.helper.BaseSystem;
import br.ita.airgroup.israel.epucknetwork.helper.Posture;
import br.ita.airgroup.israel.epucknetwork.helper.file.Properties;
import br.ita.airgroup.israel.epucknetwork.helper.file.Properties.Key;

public class TestUtils {

	public static LinkedList<Epuck> epucks = new LinkedList<Epuck>();
	public static List<BluetoothHandler> bluetooths = new ArrayList<BluetoothHandler>();

	public static void clear() {
		epucks.clear();
		bluetooths.clear();
	}

	public static void mockBluetooths() {
		epucks.forEach(e -> {
			BluetoothHandler b = Mockito.mock(BluetoothHandler.class);
			Mockito.doReturn(e).when(b).getEpuck();
			bluetooths.add(b);
		});
	}

	public static void mockEpuck(int id, int x, int y) {

		// Properties prop = Mockito.mock(Properties.class);
		// Mockito.doNothing().when(prop).getFirstAsInt(Mockito.anyObject());
		// Mockito.doNothing().when(prop).getAsInteger(Mockito.anyObject(),
		// Mockito.anyInt());
		// Mockito.doNothing().when(prop).getAsBoolean(Mockito.any(Key.class));

		Mockito.doReturn(Paths.get(FileUtils.getTempDir().toString(), "mocked"))
				.when(new SortedProperties(Mockito.any(Path.class)));

		Posture posture = new Posture(x, y, 0);
		epucks.add(Mockito.mock(Epuck.class));
		Mockito.doReturn(id).when(epucks.getLast()).getId();
		Mockito.doReturn(posture).when(epucks.getLast()).getPostureCm();
		Mockito.doReturn("" + id).when(epucks.getLast()).toString();
	}

}
